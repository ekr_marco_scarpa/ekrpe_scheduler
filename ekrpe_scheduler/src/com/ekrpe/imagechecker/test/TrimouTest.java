package com.ekrpe.imagechecker.test;

import org.trimou.Mustache;
import org.trimou.engine.MustacheEngine;
import org.trimou.engine.MustacheEngineBuilder;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class TrimouTest {

 public static void main(String[] args) {
	 try{
		  // TODO Auto-generated method stub
		  MustacheEngine engine = MustacheEngineBuilder.newBuilder().build();

		  String template = "{{#ns1_contenuto}}<div>last-name: {{last-name}}</div>{{/ns1_contenuto}}";
		   Mustache mustache = engine.compileMustache("sapOdataUrlTemplate", template);
		   String data="{\"ns1_contenuto\":[{\"first-name\":\"Aenean\",\"last-name\":\"Nullam\",\"phone\":\"(965)420-5608\"},{\"first-name\":\"Aenean\",\"last-name\":\"Erat\",\"phone\":\"(699)917-1169\"},{\"first-name\":\"Aenean\",\"last-name\":\"Volutpat\",\"phone\":\"(443)531-9176\"}]}";
		   data= data.replaceAll("ns1.", "ns1_");
		   JsonElement jsonElement = new JsonParser().parse(data);
		      String ret = mustache.render(jsonElement);
		      System.out.println("ret: " + ret);
	 }catch (Exception e) {
		 System.out.println("Exception" + e.getMessage());
	 }
 }

}
