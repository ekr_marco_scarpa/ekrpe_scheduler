package com.ekrpe.imagechecker.test;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FilenameUtils;
import org.trimou.Mustache;
import org.trimou.engine.MustacheEngineBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ekrpe.scheduler.util.Constants;
import com.ekrpe.scheduler.util.WebService;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ImageProcessor {
	public static SpreadsheetService service =new SpreadsheetService("MySpreadsheetIntegration-v1");
	
	
	public static void main(String[] args) throws IOException, ServiceException, SAXException, ParserConfigurationException, URISyntaxException, TransformerException{
		ImageProcessor imageProcessor= new ImageProcessor();
		imageProcessor.processImages("1qy6euHao9tBH8WL3RVFCtcCuHt1GsQhjamL1Ux3wZ7o", "1ZwJ5IKYAT8z0pGTqu6halJtZNFasKKH2_GrUwpTscQ4");
	}
	public void processImages(String settingsGdocId, String productGdocId) throws IOException, ServiceException, SAXException, ParserConfigurationException, URISyntaxException, TransformerException{
		
		Map<String, Object> worksheetsValues = getSettingTabDetails(settingsGdocId);
		List<String> productIDList = getProductIDList(productGdocId);
		Map<String, String> imageList = getImageListFromAllPath((Map<String, Object>) worksheetsValues.get(Constants.IMAGE_FOLDER_TAB));
		Map<String, Object> xmlList = createProductImageXML(productIDList, imageList, (Map<String, Object>) worksheetsValues.get(Constants.IMAGE_NAME_RULES_TAB));
		updateDB(xmlList, (Map<String, Object>) worksheetsValues.get(Constants.GENERAL_SETTINGS_TAB));

	}
	
	public Map<String, Object> getSettingTabDetails(String settingsGdocId) throws IOException, ServiceException, URISyntaxException{
		URL url = FeedURLFactory.getDefault().getWorksheetFeedUrl(settingsGdocId, "public", "basic");
		System.out.println("GDOC_IMAGE_ID Url: "+url);
		
		WorksheetFeed feed = service.getFeed(url, WorksheetFeed.class);
		List<WorksheetEntry> worksheetList = feed.getEntries();
		System.out.println("Number of Worksheets: "+worksheetList.size());
		Map<String, Object> worksheetsValues = new HashMap<String, Object>();
		
		for(int i=0;i<worksheetList.size();i++){
			
			WorksheetEntry worksheetEntry = worksheetList.get(i);
			System.out.println("WorkSheet Tab Name: "+worksheetEntry.getTitle().getPlainText());
			
			ListQuery listQuery = new ListQuery(worksheetEntry.getListFeedUrl());
		    ListFeed listFeed = service.query( listQuery, ListFeed.class );
		    Map<String, Object> rowValues = new HashMap<String, Object>();
		    
			if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.GENERAL_SETTINGS_TAB)){
				
			    for (ListEntry row : listFeed.getEntries()) {
			    	System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("settingvalue: ", ""));
			    	rowValues.put(row.getTitle().getPlainText(), row.getPlainTextContent().replace("settingvalue: ", ""));
			     }
			}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_FOLDER_TAB)){
				
			    for (ListEntry row : listFeed.getEntries()) {
			    	System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("settingvalue: ", ""));
			    	rowValues.put(row.getTitle().getPlainText(), row.getPlainTextContent().replace("settingvalue: ", ""));
			     }
			}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_NAME_RULES_TAB)){
				
			    for (ListEntry row : listFeed.getEntries()) {
			    	//System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("mustachepattern: ", ""));
			    	rowValues.put(row.getTitle().getPlainText(), row.getPlainTextContent().replace("mustachepattern: ", ""));
			     }
			}
			
			worksheetsValues.put(worksheetEntry.getTitle().getPlainText(), rowValues);
		}
		return worksheetsValues;
	}
	
	
	public List<String> getProductIDList(String productGdocId) throws IOException, ServiceException{

		List<String> productIDList = new ArrayList<String>();
		
		URL url = FeedURLFactory.getDefault().getWorksheetFeedUrl(productGdocId, "public", "basic");
		System.out.println("GDOC_PRODUCT_ID Url: "+url);
		
	    WorksheetFeed feed = service.getFeed(url, WorksheetFeed.class);
	    List<WorksheetEntry> worksheetList = feed.getEntries();
		System.out.println("Number of Worksheets: "+worksheetList.size());

			
		for(int i=0;i<worksheetList.size();i++){

			WorksheetEntry worksheetEntry = worksheetList.get(i);
			System.out.println("WorkSheet Tab Name: "+worksheetEntry.getTitle().getPlainText());
			if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.PRODUCT_ID_TAB)){
				ListQuery listQuery = new ListQuery(worksheetEntry.getListFeedUrl());
			    //listQuery.setSpreadsheetQuery(query);
			    ListFeed listFeed = service.query( listQuery, ListFeed.class );
			    List<ListEntry> list = listFeed.getEntries();
			    for (ListEntry row : list) {
			    	if(!productIDList.contains(row.getTitle().getPlainText())){
			    		productIDList.add(row.getTitle().getPlainText());
			    	}
			    }
			    System.out.println("productIDList Count : "+productIDList.size());
			}
		}
		return productIDList;

	}
	
	
	public Map<String, String> getImageListFromAllPath(Map<String, Object> pathList) throws IOException{
		Map<String, String> imageList = new HashMap<String, String>();
		 
		for(String path : pathList.keySet()){
			imageList.putAll(getImageListFromPath(path, imageList));
		}
		
		return imageList;
		
		
	}
	
	private Map<String, String> getImageListFromPath(String path, Map<String, String> imageList) throws IOException{
		
		File root = new File(path);
	    File[] list = root.listFiles();
	    
	    if (list != null){
	    	for ( File f : list ) {
	    		 if ( f.isFile()) {
	    			 imageList.put(FilenameUtils.getBaseName(f.getName()),f.getAbsolutePath());
	    		 }else{
	    			 imageList.putAll(getImageListFromPath(f.getAbsolutePath(), imageList));
	    		 }
	    	}
	    	
	    }
		return imageList;
	}
	
	public Map<String, Object> createProductImageXML(List<String> productIDList, Map<String, String> imageList, Map<String, Object> imageRules) throws SAXException, IOException, ParserConfigurationException{
		Map<String, Object> xmlList = new HashMap<String, Object>();
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		for(String productID : productIDList){
			List<Node> contenutoXML = new ArrayList<Node>();
			for(String imageFileName : imageList.keySet()){
				if(imageFileName.contains(productID)){
					for (Map.Entry<String, Object> rule : imageRules.entrySet()){
						if(imageFileName.endsWith(rule.getKey())){
							Node currentNode = docBuilder.parse( new InputSource(new StringReader(rule.getValue().toString()))).getDocumentElement();
							contenutoXML.add(currentNode);
						} 
					}
				}
			}
			if(contenutoXML.size() > 0){
				xmlList.put(productID,contenutoXML);
			}
		}

		System.out.println("xmlList: "+xmlList.size());
		return xmlList;
	 }

	private void updateDB(Map<String, Object> xmlList, Map<String, Object> settingsMap) throws ParserConfigurationException, SAXException, IOException, TransformerException{

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		InputSource inStream = new InputSource();
		final String END_POINT_URL = settingsMap.get("ekb_service_endpoint").toString().trim();
		final String EKR_AVAILABLE_IMAGES = settingsMap.get("pallet_images_node_name").toString().trim();
		final String MANDANTE = settingsMap.get("mandante").toString().trim();
		System.out.println("END_POINT_URL: "+END_POINT_URL);
		
		Predicate<Map.Entry<String, Object>> placeHolderFilter = new Predicate<Map.Entry<String, Object>>() {
			@Override
			public boolean apply(Entry<String, Object> val) {
				return (val.getValue().toString().trim().startsWith("{{") && val.getValue().toString().trim().endsWith("}}"));
			}
		};

		Map<String, Object> placeHolderMap = Maps.filterEntries(settingsMap,placeHolderFilter);
		for (Map.Entry<String, Object> xmlNodeList : xmlList.entrySet()){
			Map<String, String> placeHolderNewMap = new HashMap<String, String>();
			for (Map.Entry<String, Object> placeHolder : placeHolderMap.entrySet()){
				placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), xmlNodeList.getKey().toString().trim());
			}
			Gson gson = new Gson(); 
			String placeHolderJson = gson.toJson(placeHolderNewMap); 
			System.out.println("placeHolder Json: "+placeHolderJson);
			
			String xmlData = "<Envelope xmlns='http://schemas.xmlsoap.org/soap/envelope/'>"
				+"<Body>"
				+"<dettaglioFormEKBSIN xmlns='http://new.webservice.namespace'>"
				+"<mandante>"+MANDANTE+"</mandante>"
				+"<codice>"+xmlNodeList.getKey()+"</codice>"
				+"</dettaglioFormEKBSIN>"
				+"</Body>"
				+"</Envelope>";

				System.out.println("xmlData: "+ xmlData);
			
				String dataFromDB = WebService.invoke(END_POINT_URL, xmlData,null,null,null,null);
		    	System.out.println("ProductID: "+ xmlNodeList.getKey()+", dataFromDB: "+dataFromDB);

				inStream.setCharacterStream(new StringReader(dataFromDB));
		    	Document doc = docBuilder.parse(inStream);
		    	doc.normalizeDocument();
		    	NodeList nodeList = doc.getElementsByTagName("*");
		    	String parsedDoc = convertXMLToString(doc);
				for (int s = 0; s < nodeList.getLength(); s++) {
					Element e = (Element)nodeList.item(s);
					String tipo = e.getAttribute("tipo");
					if(tipo.equalsIgnoreCase(EKR_AVAILABLE_IMAGES)){
						Node versioneNode = e.getElementsByTagName("versione").item(0);
						NodeList childNodeList = versioneNode.getChildNodes();

						for(Node childNode = versioneNode.getFirstChild(); childNode!=null;){
						    Node nextChild = childNode.getNextSibling();
						    versioneNode.removeChild(childNode);
						    childNode = nextChild;
						}

						for (Node contenutoXML : (List<Node>)xmlNodeList.getValue()) {
							Node firstDocImportedNode = doc.importNode(contenutoXML, true);
							versioneNode.appendChild(firstDocImportedNode);
						}

						JsonElement placeHolderJsonElement = new JsonParser().parse(placeHolderJson);
						Mustache docMustache = MustacheEngineBuilder.newBuilder().build().compileMustache(xmlNodeList.getKey(), convertXMLToString(doc));
		    		    parsedDoc = docMustache.render(placeHolderJsonElement);

						break;
					}
				}

				parsedDoc = parsedDoc.replace("dettaglioFormEKBSOUT", "aggiornaFormEKBSIN");
				
				System.out.println("After Appending Doc: "+parsedDoc);
				
				String updateToDB = WebService.invoke(END_POINT_URL, parsedDoc,null,null,null,null);
		    	System.out.println("ProductID: "+ xmlNodeList.getKey()+", updateToDB: "+updateToDB);
				
		}

	}
	
	private String convertXMLToString(Object xml) throws TransformerException{
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource((Node) xml), new StreamResult(writer));
		String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		return output;
		
	}
	
}
