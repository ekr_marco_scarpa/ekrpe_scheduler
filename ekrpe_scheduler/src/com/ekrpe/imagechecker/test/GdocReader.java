package com.ekrpe.imagechecker.test;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.ekrpe.scheduler.util.Constants;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;

public class GdocReader {
	public static String key = "";//Constants.GDOC_PRODUCT_ID;
	public static SpreadsheetService service =new SpreadsheetService("MySpreadsheetIntegration-v1");
	public static ArrayList<String> productList = new ArrayList<String>();
	public static void main(String[] args) throws IOException, ServiceException{

		 //======Start Getting Row Data of all the tabs=====================//
		URL url = FeedURLFactory.getDefault().getWorksheetFeedUrl(key, "public", "basic");
		System.out.println("url: "+url);
		
	    WorksheetFeed feed = service.getFeed(url, WorksheetFeed.class);
		System.out.println("Number of Worksheets: "+feed.getEntries().size());
		for(int i=0;i<feed.getEntries().size();i++){
			List<WorksheetEntry> worksheetList = feed.getEntries();
			System.out.println("worksheetList: "+worksheetList.get(i));
			WorksheetEntry worksheetEntry = worksheetList.get(i);
			System.out.println("worksheetEntry: "+worksheetEntry.getColCount());
			
			ListQuery listQuery = new ListQuery(worksheetEntry.getListFeedUrl());
		    //listQuery.setSpreadsheetQuery(query);
		    ListFeed listFeed = service.query( listQuery, ListFeed.class );
		    List<ListEntry> list = listFeed.getEntries();
		    for (ListEntry row : list) {
		    	productList.add(row.getPlainTextContent());
		    }
		    
		    System.out.println("productList.size() : "+productList.size());
		    for(int j=0;j<productList.size();j++){
		    	System.out.println("productList: "+productList.get(j));
		    }
		   
		}
		
		
		//======Start Getting Row Data of all the tabs=====================//
		
	}
}

