package com.ekrpe.imagechecker.test;
import java.io.File;
import java.util.ArrayList;

import com.ekrpe.scheduler.util.Utilities;

public class Filewalker {
	private static final String folderPath ="D:\\Projects_Details\\EKR\\2015_na";
	public static ArrayList<String> imagesList = new ArrayList<String>();
	
	public static void main(String[] args) {
		Filewalker fw = new Filewalker();
        fw.walk(folderPath);
	}
	
    public void walk( String path ) {
    	
        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() );
                System.out.println( "Dir: " + f.getAbsoluteFile() );
                //imagesList.add(f.getAbsoluteFile().toString());
            }
            else {
                System.out.println( "File: " + f.getAbsoluteFile() );
                //imagesList.add(f.getAbsoluteFile().toString());
            }
        }
    }
}
