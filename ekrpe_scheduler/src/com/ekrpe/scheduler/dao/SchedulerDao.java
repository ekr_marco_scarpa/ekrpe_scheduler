package com.ekrpe.scheduler.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.ekrpe.scheduler.criteria.ScheduledTaskVOCriteria;
import com.ekrpe.scheduler.model.ImageTaskVO;
import com.ekrpe.scheduler.model.ScheduledTaskVO;
import com.ekrpe.scheduler.runnable.TaskBase;
import com.ekrpe.scheduler.util.Constants;
import com.ekrpe.scheduler.util.Utilities;

public class SchedulerDao extends DaoBase{
	private static final Logger logger = Logger.getLogger(SchedulerDao.class);
	
	public List<ScheduledTaskVO> getPendingJobs() {
		ScheduledTaskVOCriteria pendingCriteria= new ScheduledTaskVOCriteria();
		pendingCriteria.setJobStatus(TaskBase.STATUS_PENDING);
		return searchScheduleTaskVO(pendingCriteria);
		
	}
	
	
public List<ScheduledTaskVO> searchScheduleTaskVO(ScheduledTaskVOCriteria scheduledTaskVOCriteria) {
	Session session = getSession();
	Criteria criteria = session.createCriteria(ScheduledTaskVO.class);
	if(scheduledTaskVOCriteria.getJobId() != null){
		criteria.add(Restrictions.eq("jobId", scheduledTaskVOCriteria.getJobId()));
	}
	if(scheduledTaskVOCriteria.getJobStatus()!=null){
		criteria.add(Restrictions.eq("jobStatus", scheduledTaskVOCriteria.getJobStatus()));
	}
	
	List<ScheduledTaskVO> list = criteria.list();
	return list;
}
	public void  enqueueJobs() {
		String inboxPath=Constants.FILE_PROCESSING_DIR_INBOX;
		logger.info("enqueueJobs Inbox Path: " + inboxPath);
		File inbox =new File(inboxPath);
		if (inbox.exists()){
			File[] listOfFiles = inbox.listFiles();
			logger.info("enqueueJobs Inbox Path found file count : " + listOfFiles.length);
		    for (int i = 0; i < listOfFiles.length; i++) {
		    	if (listOfFiles[i].isFile()) {
		    		String fileAbsolutePath=listOfFiles[i].getAbsolutePath();
		    		logger.info("processing file : "+fileAbsolutePath);
		    		String fileName=listOfFiles[i].getName();
		    		try{
		    			//va usato l'encoding utf-8			    			
		    			FileInputStream inputStream = new FileInputStream(fileAbsolutePath);
		    			InputStreamReader reader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
			    		BufferedReader input = new BufferedReader(reader);
			    	    String last=null;
			    	    String line=null;
	
			    	    int linesCounter = 0;
			    	    while ((line = input.readLine()) != null) {
			    	        last = line;
			    	        linesCounter ++;
			    	    }
			    	    logger.info("lines count : "+linesCounter);
			    	    input.close();
			    	    logger.info("line ending : "+ last.substring(last.length()-"</source>".length(), last.length()));
			    	    if (last!=null && last.endsWith("</source>")){
			    	    
				            logger.info("Inbox File " + fileAbsolutePath);
				            
				            String timeStamp = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
				            
				            fileAbsolutePath= Constants.FILE_PROCESSING_DIR_QUEUE + "/" + 
				            FilenameUtils.getBaseName(fileName) + "_" +
				            timeStamp + "." +		
				            FilenameUtils.getExtension(fileName);
				            
				            
				            if (!new File(fileAbsolutePath).exists()){	
					            listOfFiles[i].renameTo(new File(fileAbsolutePath));
					            ScheduledTaskVO scheduledTaskVO= new ScheduledTaskVO();
					            scheduledTaskVO.setJobInputFile(fileAbsolutePath);
					            scheduledTaskVO.setJobStatus(TaskBase.STATUS_PENDING);
					            scheduledTaskVO.setJobType(ScheduledTaskVO.JOB_TYPE_PDF_PROCESSING);
					            getHibernateTemplate().saveOrUpdate(scheduledTaskVO);
				            }
				            else{
				            	logger.error(fileAbsolutePath + " already exists!");
				            }
			    	    }
		    		}
		    		catch (Exception e){
		    			logger.error(e.getMessage(), e);
		    		}
		            
		          }
		    }
		}
		else{
			logger.error("Folder does not exist: " + inboxPath);
		}
		
	}
	
	public ScheduledTaskVO saveScheduledTaskVO(ScheduledTaskVO scheduledTaskVO){
		 getHibernateTemplate().saveOrUpdate(scheduledTaskVO);
		 return scheduledTaskVO;
	}

	public String getImageCollections(String productID) {
		String filesFound = null;
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try
		{

				Session session = getSession();
				con=session.connection();
				String sqlStatement="SELECT FILES_FOUND FROM \"IMAGE_PROCESSOR_LOG\" WHERE productid='" + productID.trim()+"'";
				logger.info("IMAGE_PROCESSOR_LOG: "+sqlStatement);
				preparedStatement=con.prepareStatement(sqlStatement);
				resultSet=preparedStatement.executeQuery();
				while(resultSet.next())
				{
					filesFound = resultSet.getString(1);
				}

				System.out.println("filesFound : "+filesFound);

		}
		catch (Exception e) {
			logger.info("ERROR in fetching findProductFiles. Message: "+e.getMessage());
			System.out.println("ERROR in fetching findProductFiles. Message: "+e.getMessage());
		}
		finally{
			try{
				if(Utilities.isStringPopulated(resultSet))
				{
					resultSet.close();
					resultSet=null;
				}
				if(Utilities.isStringPopulated(preparedStatement))
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if(Utilities.isStringPopulated(con))
				{
					con.close();
					con=null;
				}
			}
			catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		
		/*if(scheduledTaskVOCriteria.getJobId() != null){
			criteria.add(Restrictions.eq("jobId", scheduledTaskVOCriteria.getJobId()));
		}*/
		
		return filesFound;
	}


	public String updateImageProcessorLog(String newJson, String productID) {

		String filesFound = null;
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement1 = null;
		ResultSet resultSet1 = null;
		PreparedStatement preparedStatement2 = null;
		ResultSet resultSet2 = null;
		
		try
		{
				Session session = getSession();
				con=session.connection();
				String sqlStatement="SELECT Count(1) FROM \"IMAGE_PROCESSOR_LOG\" WHERE productid='" + productID.trim()+"'";
				logger.info("AddUpdatenewJson: "+sqlStatement);
				preparedStatement=con.prepareStatement(sqlStatement);
				resultSet=preparedStatement.executeQuery();
				while(resultSet.next())
				{
					filesFound = resultSet.getString(1);
				}

				System.out.println("filesFound : "+filesFound);
				
				if(filesFound.equalsIgnoreCase("1")){
					try
					{
					String updatesqlStatement="UPDATE \"IMAGE_PROCESSOR_LOG\" SET files_found='"+newJson+"' WHERE productid='" + productID.trim()+"'";
					logger.info("updatesqlStatement: "+updatesqlStatement);
					preparedStatement1=con.prepareStatement(updatesqlStatement);
					preparedStatement1.executeUpdate();
					System.out.println("UpdateDB : Completed");
					}catch (Exception e) {
						logger.info("ERROR in updating log table Message: "+e.getMessage());
						System.out.println("ERROR in updating log table Message: "+e.getMessage());
					}
				}else{
					try
					{
					String insertsqlStatement="INSERT INTO \"IMAGE_PROCESSOR_LOG\"(files_found,productid) VALUES('"+newJson+"','"+productID.trim()+"')";
					logger.info("insertsqlStatement: "+insertsqlStatement);
					preparedStatement2=con.prepareStatement(insertsqlStatement);
					preparedStatement2.executeUpdate();
					System.out.println("InsertDB : Completed");
					}catch (Exception e) {
						logger.info("ERROR in InsertDB log table Message: "+e.getMessage());
						System.out.println("ERROR in InsertDB log table Message: "+e.getMessage());
					}
				}

		}
		catch (Exception e) {
			logger.info("ERROR in AddUpdatenewJson. Message: "+e.getMessage());
			System.out.println("ERROR in AddUpdatenewJson. Message: "+e.getMessage());
		}
		finally{
			try{
				if(Utilities.isStringPopulated(resultSet))
				{
					resultSet.close();
					resultSet=null;
					resultSet1.close();
					resultSet1=null;
					resultSet2.close();
					resultSet2=null;
				}
				if(Utilities.isStringPopulated(preparedStatement))
				{
					preparedStatement.close();
					preparedStatement=null;
					preparedStatement1.close();
					preparedStatement1=null;
					preparedStatement2.close();
					preparedStatement2=null;
				}
				if(Utilities.isStringPopulated(con))
				{
					con.close();
					con=null;
				}
			}
			catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		
		return filesFound;
	}
	
	
}
