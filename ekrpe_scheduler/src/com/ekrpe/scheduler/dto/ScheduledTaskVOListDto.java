package com.ekrpe.scheduler.dto;

import java.util.ArrayList;
import java.util.List;

import com.ekrpe.scheduler.model.ScheduledTaskVO;

public class ScheduledTaskVOListDto {
	List<ScheduledTaskVO> scheduledTaskVOList= new ArrayList<ScheduledTaskVO>();

	public List<ScheduledTaskVO> getScheduledTaskVOList() {
		return scheduledTaskVOList;
	}

	public void setScheduledTaskVOList(List<ScheduledTaskVO> scheduledTaskVOList) {
		this.scheduledTaskVOList = scheduledTaskVOList;
	}
}
