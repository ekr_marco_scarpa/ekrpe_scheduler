package com.ekrpe.scheduler.listener;
import it.ekr.pdffromselector.parsingengine.ParsingSapXml;

import javax.servlet.ServletContextEvent;  
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.ekrpe.scheduler.dao.SchedulerDao;
import com.ekrpe.scheduler.model.ScheduledTaskVO;
import com.ekrpe.scheduler.runnable.ImageProcessor;
import com.ekrpe.scheduler.runnable.PdfTask;
import com.ekrpe.scheduler.service.SchedulerManager;
import com.ekrpe.scheduler.util.ServiceRegistry;
import com.ekrpe.scheduler.util.Utilities;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SchedulerContextListener implements ServletContextListener{
	private static final Logger logger = Logger.getLogger(SchedulerContextListener.class);
	public static Integer threadCounter = 0;
	public static BlockingQueue<Runnable> blockingQueue = new LinkedBlockingQueue<Runnable>(50);
	public static CustomThreadPoolExecutor executor = new CustomThreadPoolExecutor(15,
            20, 5000, TimeUnit.MILLISECONDS, blockingQueue);
	
	 @Override
	    public void contextInitialized(ServletContextEvent servletContextEvent) {
		 	logger.info("SchedulerContextListener Starting up!");
	        executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {
	            @Override
	            public void rejectedExecution(Runnable r,
	                    ThreadPoolExecutor executor) {
	            	logger.warn("DemoTask Rejected : "+ ((PdfTask) r).getJobId());
	            	logger.warn("Waiting for a second !!");
	                try {
	                    Thread.sleep(1000);
	                } catch (InterruptedException e) {
	                    e.printStackTrace();
	                }
	                logger.warn("Lets add another time : "+ ((PdfTask) r).getJobId());
	                executor.execute(r);
	            }
	        });
	        // Let start all core threads initially
	        executor.prestartAllCoreThreads();
	        
	        
	        
	        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
	        ScheduledFuture scheduledProducer =
	    		    scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
	    		        public void run() {	    
	    		        	
	    		        	try{
	    		        	logger.info("Executing Producer!");
	    		        	SchedulerManager schedulerManager =(SchedulerManager)ServiceRegistry.getBean("schedulerManager");
	    		        	schedulerManager.enqueueJobs();	    		        		    		        	
	    		        	List<ScheduledTaskVO> list = schedulerManager.getPendingJobs();	    		        	
	    		        	for (ScheduledTaskVO scheduledTaskVO: list){
	    		        		logger.info("Adding task: " + scheduledTaskVO.getJobId());
	    		        		if (ScheduledTaskVO.JOB_TYPE_PDF_PROCESSING.equals(scheduledTaskVO.getJobType())){	    		        				    		        		
	    		        			ParsingSapXml pdfTask = new ParsingSapXml();			    		        	
			    		        	pdfTask.setJobId(scheduledTaskVO.getJobId());
			    		        	pdfTask.setDataSourceFilePath(scheduledTaskVO.getJobInputFile());
			    		        	executor.execute(pdfTask);
	    		        		}
	    		        		else if (ScheduledTaskVO.JOB_TYPE_IMAGE_CHECKER.equals(scheduledTaskVO.getJobType()) || ScheduledTaskVO.JOB_TYPE_IMAGE_CHECKER_ODATAV.equals(scheduledTaskVO.getJobType())){
	    		        			
	    		        			ImageProcessor imageProcessor= new ImageProcessor();
	    		        			imageProcessor.setJobId(scheduledTaskVO.getJobId());
	    		        			if(Utilities.isStringPopulated(scheduledTaskVO.getJobInputFile())){

		    		        			String params[]=scheduledTaskVO.getJobInputFile().split(";");
		    		        			imageProcessor.setSettingsGdocId(params[0]);
		    		        			logger.info("imageProcessor.setSettingsGdocId: " + params[0]);
		    		        			System.out.println("imageProcessor.setSettingsGdocId: " + params[0]);
		    		        			
		    		        			imageProcessor.setProductGdocId(params[1]);
		    		        			logger.info("imageProcessor.setProductGdocId: " + params[1]);
		    		        			System.out.println("imageProcessor.setProductGdocId: " + params[1]);
		    		        			
		    		        			imageProcessor.setJobType(scheduledTaskVO.getJobType());
		    		        			logger.info("imageProcessor.setJobType: " + imageProcessor.getJobType());
		    		        			System.out.println("imageProcessor.setJobType: " + imageProcessor.getJobType());
		    		        			
		    		        			if(ScheduledTaskVO.JOB_TYPE_IMAGE_CHECKER_ODATAV.equals(scheduledTaskVO.getJobType())){
			    		        			imageProcessor.setSkuPackCode(params[2]);
			    		        			logger.info("imageProcessor.setSkuPackCode: " + params[2]);
			    		        			System.out.println("imageProcessor.setSkuPackCode: " + params[2]);
		    		        			}
		    		        			
		    		        			executor.execute(imageProcessor);
	    		        			}
	    		        			
	    		        		}
	    		        	}
	    		        	logger.info("Executed Producer!");
	    		        	}
	    		        	catch (Exception e){
	    		        		logger.error(e.getMessage(), e);
	    		        	}
	    		        	
	    		       
	    		        }
	    		    },
	    		    0,
	    		    5,
	    		    TimeUnit.SECONDS);
	    }

	    @Override
	    public void contextDestroyed(ServletContextEvent servletContextEvent) {
	    	logger.info("SchedulerContextListener Shutting down!");
	    }
}
