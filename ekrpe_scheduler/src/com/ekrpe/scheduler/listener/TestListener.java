package com.ekrpe.scheduler.listener;
import it.ekr.pdffromselector.parsingengine.ParsingSapXml;




import javax.servlet.ServletContextEvent;  
import javax.servlet.ServletContextListener;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.ekrpe.scheduler.dao.SchedulerDao;
import com.ekrpe.scheduler.model.ScheduledTaskVO;
import com.ekrpe.scheduler.runnable.ImageProcessor;
import com.ekrpe.scheduler.runnable.PdfTask;
import com.ekrpe.scheduler.service.SchedulerManager;
import com.ekrpe.scheduler.util.Constants;
import com.ekrpe.scheduler.util.ServiceRegistry;
import com.ekrpe.scheduler.util.Utilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
//_____________________IMPORT MM____________________
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.gdata.client.GoogleService;
import com.google.gdata.client.spreadsheet.CellQuery;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

//________________________________



import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.http.mapper.Mapper;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.trimou.Mustache;
import org.trimou.engine.MustacheEngineBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ekrpe.scheduler.listener.TestListener;
import com.ekrpe.scheduler.model.ImageTaskVO;
import com.ekrpe.scheduler.model.LoggerVO;
import com.ekrpe.scheduler.model.ScheduledTaskVO;
import com.ekrpe.scheduler.service.SchedulerManager;
import com.ekrpe.scheduler.util.Constants;
import com.ekrpe.scheduler.util.ServiceRegistry;
import com.ekrpe.scheduler.util.Utilities;
import com.ekrpe.scheduler.util.WebService;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.google.gdata.client.spreadsheet.CellQuery;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.DateTime;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class TestListener implements ServletContextListener{
	private static final Logger logger = Logger.getLogger(TestListener.class);
	public static Integer threadCounter = 0;
	public static BlockingQueue<Runnable> blockingQueue = new LinkedBlockingQueue<Runnable>(50);
	public static CustomThreadPoolExecutor executor = new CustomThreadPoolExecutor(5,
            10, 5000, TimeUnit.MILLISECONDS, blockingQueue);
	
	 @Override
	    public void contextInitialized(ServletContextEvent servletContextEvent) {
		 	logger.info("SchedulerContextListener Starting up!");
	        executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {
	            @Override
	            public void rejectedExecution(Runnable r,
	                    ThreadPoolExecutor executor) {
	            	logger.warn("DemoTask Rejected : "+ ((PdfTask) r).getJobId());
	            	logger.warn("Waiting for a second !!");
	                try {
	                    Thread.sleep(1000);
	                } catch (InterruptedException e) {
	                    e.printStackTrace();
	                }
	                logger.warn("Lets add another time : "+ ((PdfTask) r).getJobId());
	                executor.execute(r);
	            }
	        });
	        // Let start all core threads initially
	        executor.prestartAllCoreThreads();
	        
	        
	        
	        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
	        ScheduledFuture scheduledProducer =
	    		    scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
	    		        public void run() {	    
	    		        	
	    		        	try{
	    		        	//modifica MM per test
	    		        	//_______________________________________
	    		        	System.out.println("inizio");

	    		        	//String settingsGdocId="15B5Iux0oi1rvG1Zy63NFaRFjjoOG5tUjvc72jZMteNk";//pubblicato
							String settingsGdocId="1upwCcQpYQL-OtZslk68nYId93X5DxqVU3Kk74mWVv6k";//non pubblicato
							//String productGdocId="1XUax6dwE2jhJlrzv6BsNpvzkcBz6NWsBoAI0-77K1w8";//pubblicato
							String productGdocId="1I-Hl_oFF79U_K6ODNCe8FlAopzDEgFDnIx39RmW3afc";//non pubblicato
							System.out.println("prima di getSettingTabDetails");
							Map<String, Object> worksheetsValues = getSettingTabDetails(settingsGdocId);
							System.out.println("dopo getSettingTabDetails!");
							List<String> productIDList = getProductIDList(productGdocId, (Map<String, Object>) worksheetsValues.get(Constants.GENERAL_SETTINGS_TAB));
							System.out.println("dopo getProductIDList");
							Map<String, Object> imageList = getImageListFromAllPath((Map<String, Object>) worksheetsValues.get(Constants.IMAGE_FOLDER_TAB));
							System.out.println("dopo getImageListFromAllPath");

	    		        	}catch(Exception e){
	    		    		System.out.println("errore ? "+e.getMessage());
	    		    	}
	    		    	    try {
	    		    	    	System.out.println("sto aspettando");
	    		        		Thread.sleep(600000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	    		       
	    		        }
	    		    },
	    		    0,
	    		    5,
	    		    TimeUnit.SECONDS);
	    }

	    @Override
	    public void contextDestroyed(ServletContextEvent servletContextEvent) {
	    	logger.info("SchedulerContextListener Shutting down!");
	    }
	    
	    
	    //_____________________________________ da inserire in imageProcessor___________________________________
	    //funzioni nuove con auth
	    public Map<String, Object> getSettingTabDetails(String settingsGdocId) throws IOException, ServiceException, URISyntaxException{
			//no auth
	    	//URL url = FeedURLFactory.getDefault().getWorksheetFeedUrl(settingsGdocId, "public", "basic");
	    	//con auth
	    	URL url = FeedURLFactory.getDefault().getWorksheetFeedUrl(settingsGdocId, "private", "full");
			System.out.println("GDOC_IMAGE_ID Url: "+url);
			
			//no auth
			//SpreadsheetService service =new SpreadsheetService("MySpreadsheetIntegration-v1");
			//con auth 
			SpreadsheetService service =getauthService();
			WorksheetFeed feed = service.getFeed(url, WorksheetFeed.class);
			List<WorksheetEntry> worksheetList = feed.getEntries();
			System.out.println("Number of Worksheets: "+worksheetList.size());
			Map<String, Object> worksheetsValues = new HashMap<String, Object>();

			for(int i=0;i<worksheetList.size();i++){

				WorksheetEntry worksheetEntry = worksheetList.get(i);
				System.out.println("WorkSheet Tab Name: "+worksheetEntry.getTitle().getPlainText());

				ListQuery listQuery = new ListQuery(worksheetEntry.getListFeedUrl());
				ListFeed listFeed = service.query( listQuery, ListFeed.class );
				Map<String, Object> rowValues = new LinkedHashMap<String, Object>();

				if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.GENERAL_SETTINGS_TAB)){

					for (ListEntry row : listFeed.getEntries()) {
						System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("settingvalue: ", ""));
						rowValues.put(row.getTitle().getPlainText(), row.getPlainTextContent().replace("settingvalue: ", ""));
					}
				}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_FOLDER_TAB)){

					/*for (ListEntry row : listFeed.getEntries()) {
				    	System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("settingvalue: ", ""));
				    	rowValues.put(row.getTitle().getPlainText(), row.getPlainTextContent().replace("settingvalue: ", ""));
				     }*/



					System.out.println("===========START IMAGE_FOLDER_TAB=============");
					int colNumber=1;

					CellQuery cellQuery = new CellQuery(worksheetEntry.getCellFeedUrl());
					CellFeed cellFeed = service.query(cellQuery, CellFeed.class);
					List<CellEntry> cell = cellFeed.getEntries();
					//Map<String, Object> imgFolderMap = new HashMap<String, Object>();
					int totalColumn = 4;
					// Iterate through each cell, printing its value.
					for (int j = 0; j < cell.size(); j++){
						if(totalColumn < j){
							break;
						}
						CellEntry row = cell.get(j);
						System.out.println("row.getPlainTextContent() : "+row.getPlainTextContent());
						if(row.getPlainTextContent().equalsIgnoreCase("folder path")){
							colNumber = j+1;
							URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
							CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
							rowValues.put("folder_path", cellFeedFinal);
						}else  if(row.getPlainTextContent().equalsIgnoreCase("start date")){
							colNumber = j+1;
							URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
							CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
							rowValues.put("start_date", cellFeedFinal);
						}else  if(row.getPlainTextContent().equalsIgnoreCase("end date")){
							colNumber = j+1;
							URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
							CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
							rowValues.put("end_date", cellFeedFinal);
						}else  if(row.getPlainTextContent().equalsIgnoreCase("destination_path")){
							colNumber = j+1;
							URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
							CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
							rowValues.put("destination_path", cellFeedFinal);
						}
					}
					System.out.println("===========END IMAGE_FOLDER_TAB=============");
					//URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
					//CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);

					// Iterate through each cell, printing its value.
					/*for (CellEntry cellFinal : cellFeedFinal.getEntries()) {
				      if(!productIDList.contains(cellFinal.getPlainTextContent())){  
		    			  System.out.println("row.getTitle().getPlainText() : "+cellFinal.getTitle().getPlainText());
				          System.out.println("row.getPlainTextContent() : "+cellFinal.getPlainTextContent());
				          productIDList.add(cellFinal.getPlainTextContent());
		    		  }
				    }*/

					//System.out.println("productIDList Count : "+productIDList.size());



				}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_NAME_RULES_TAB)){

					for (ListEntry row : listFeed.getEntries()) {
						//System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("mustachepattern: ", ""));
						String key = row.getTitle().getPlainText();
						if(row.getTitle().getPlainText().startsWith("Row:")){
							key = "EMPTY";
						}
						rowValues.put(key, row.getPlainTextContent().replace("mustachepattern: ", ""));
					}
				}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_NAME_RULES_ODATAV_TAB)){
					for (ListEntry row : listFeed.getEntries()) {
						//System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("mustachepatternodatav: ", ""));
						String key = row.getTitle().getPlainText();
						if(row.getTitle().getPlainText().startsWith("Row:")){
							key = "EMPTY";
						}

						rowValues.put(key, row.getPlainTextContent().replace("mustachepatternodatav: ", ""));
					}
				}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_RESAMPLING_TAB)){

					System.out.println("===========START IMAGE_RESAMPLING_TAB=============");
					int colNumber=1;

					CellQuery cellQuery = new CellQuery(worksheetEntry.getCellFeedUrl());
					CellFeed cellFeed = service.query(cellQuery, CellFeed.class);
					List<CellEntry> cell = cellFeed.getEntries();
					//Map<String, Object> imgFolderMap = new HashMap<String, Object>();
					int totalColumn = 3;
					// Iterate through each cell, printing its value.
					for (int j = 0; j < cell.size(); j++){
						if(totalColumn < j){
							break;
						}
						CellEntry row = cell.get(j);
						System.out.println("row.getPlainTextContent() : "+row.getPlainTextContent());
						if(row.getPlainTextContent().equalsIgnoreCase("resized_subfolder")){
							colNumber = j+1;
							URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
							CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
							rowValues.put("resized_subfolder", cellFeedFinal);
						}else  if(row.getPlainTextContent().equalsIgnoreCase("imagemagick_options")){
							colNumber = j+1;
							URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
							CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
							rowValues.put("imagemagick_options", cellFeedFinal);
						}else  if(row.getPlainTextContent().equalsIgnoreCase("format")){
							colNumber = j+1;
							URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
							CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
							rowValues.put("format", cellFeedFinal);
						}
					}
					System.out.println("===========END IMAGE_RESAMPLING_TAB=============");

				}

				worksheetsValues.put(worksheetEntry.getTitle().getPlainText(), rowValues);
			}
			return worksheetsValues;
		}
	    
	    //per la parte di service auth
	    public SpreadsheetService getauthService(){
	    	SpreadsheetService service = null;
	    	try{
        		HttpTransport httpTransport = new NetHttpTransport();
        		JsonFactory jsonFactory = new JacksonFactory();
        		String pathp12="C:\\gdrivekey\\key.p12";
        		String [] SCOPESArray= {
    	    		"https://spreadsheets.google.com/feeds", 
    	    		"https://spreadsheets.google.com/feeds/spreadsheets/private/full", 
    	    		"https://docs.google.com/feeds",
    	    		"https://www.googleapis.com/auth/drive.file",
    	    	    "https://www.googleapis.com/auth/userinfo.email",
    	    	    "https://www.googleapis.com/auth/userinfo.profile",
    	    		DriveScopes.DRIVE
    	    		};
    	    
        		final List SCOPES = Arrays.asList(SCOPESArray);
        		GoogleCredential credential = new GoogleCredential.Builder()
				.setTransport(httpTransport)
				.setJsonFactory(jsonFactory)
				.setServiceAccountId("188998181103-toh61h9nv02s84cl3i695f5o9ob8sg18@developer.gserviceaccount.com")
				.setServiceAccountScopes(SCOPES)
				.setServiceAccountPrivateKeyFromP12File(
				                   new java.io.File(pathp12))
				                   .build();
        		service = new SpreadsheetService("MySpreadsheetIntegration-v1");
        		credential.refreshToken();
        		String accessToken=credential.getAccessToken();
        		System.out.println("accessToken "+accessToken);
        		service.setHeader("Authorization", "Bearer " + accessToken);
        		System.out.println("son connessa!!!");
        		
	    }catch(Exception e){
	    	System.out.println("errore in getauthService "+e.getMessage());
	    }
        return service;
	   }
	   
	    //con auth
	    public List<String> getProductIDList(String productGdocId, Map<String, Object> settingsMap) throws IOException, ServiceException, URISyntaxException{
			final String PRODUCTID_COLUMN_NAME = Utilities.isStringPopulated(settingsMap.get("productid_column_name"))?settingsMap.get("productid_column_name").toString().trim():"";
			System.out.println("PRODUCTID_COLUMN_NAME: "+PRODUCTID_COLUMN_NAME);
			List<String> productIDList = new ArrayList<String>();

			//no auth
			//URL url = FeedURLFactory.getDefault().getWorksheetFeedUrl(productGdocId, "public", "basic");
			//si auth
			URL url=FeedURLFactory.getDefault().getWorksheetFeedUrl(productGdocId, "private", "full");
			System.out.println("GDOC_PRODUCT_ID Url: "+url);
			//no auth
			//SpreadsheetService service =new SpreadsheetService("MySpreadsheetIntegration-v1");
			//con auth
			SpreadsheetService service =getauthService();
			WorksheetFeed feed = service.getFeed(url, WorksheetFeed.class);
			List<WorksheetEntry> worksheetList = feed.getEntries();
			System.out.println("Number of Worksheets: "+worksheetList.size());

			for(int i=0;i<worksheetList.size();i++){

				WorksheetEntry worksheetEntry = worksheetList.get(i);
				System.out.println("WorkSheet Tab Name: "+worksheetEntry.getTitle().getPlainText());

				if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.PRODUCT_ID_TAB)){

					int colNumber=1;

					CellQuery cellQuery = new CellQuery(worksheetEntry.getCellFeedUrl());
					CellFeed cellFeed = service.query(cellQuery, CellFeed.class);
					List<CellEntry> cell = cellFeed.getEntries();

					// Iterate through each cell, printing its value.
					for (int j = 0; j < cell.size(); j++){
						CellEntry row = cell.get(j);
						if(row.getPlainTextContent().equalsIgnoreCase(PRODUCTID_COLUMN_NAME)){
							colNumber = j+1;
							break;
						}
					}

					URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
					CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);

					// Iterate through each cell, printing its value.
					for (CellEntry cellFinal : cellFeedFinal.getEntries()) {
						if(!productIDList.contains(cellFinal.getPlainTextContent())){  
							System.out.println("row.getTitle().getPlainText() : "+cellFinal.getTitle().getPlainText());
							System.out.println("row.getPlainTextContent() : "+cellFinal.getPlainTextContent());
							productIDList.add(cellFinal.getPlainTextContent());
						}
					}

					System.out.println("productIDList Count : "+productIDList.size());
				}
			}

			return productIDList;

		}
	    
	    
//______________________________________________________
	   
	   //NON MODIFICATE
	    public Map<String, Object> getImageListFromAllPath(Map<String, Object> pathList) throws IOException{
			Map<String, Object> imageList = new HashMap<String, Object>();
			int rowNum = 0;
			CellFeed folderPathList = (CellFeed) pathList.get("folder_path");
			for (CellEntry cellFinal : folderPathList.getEntries()) {
				CellFeed startDateList = (CellFeed) pathList.get("start_date");
				String startDate = startDateList.getEntries().get(rowNum).getPlainTextContent();

				CellFeed endDateList = (CellFeed) pathList.get("end_date");
				String endDate = endDateList.getEntries().get(rowNum).getPlainTextContent();

				CellFeed destinationPathList = (CellFeed) pathList.get("destination_path");
				String destinationPath = destinationPathList.getEntries().get(rowNum).getPlainTextContent().replace("\\", "/");	
				String mainPath = cellFinal.getPlainTextContent();
				try{
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					long formatterStartDate = formatter.parse(startDate).getTime();
					long formatterEndDate = formatter.parse(endDate).getTime();
					long currentDate = formatter.parse(dateToStringFormat(new Date(),"MM/dd/yyyy")).getTime();

					if ( (currentDate >= formatterStartDate) && (currentDate <= formatterEndDate) ){

						imageList.putAll(getImageListFromPath(cellFinal.getPlainTextContent(), startDate,endDate,destinationPath,imageList, mainPath));

					}

				}catch (Exception e) {
					System.out.println("Error : "+e.getMessage());
				}
				rowNum++;
			}

			System.out.println("imageList : "+imageList.size());
			return imageList;


		}
	    
	    public static String dateToStringFormat(Date date,String format) {
			String dateFormat = null;
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
				dateFormat = simpleDateFormat.format(date);
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
				return format;
			}
			return dateFormat;
		}

	    private Map<String, Object> getImageListFromPath(String path, String startDate, String endDate, String destinationPath, Map<String, Object> imageList, String mainPath) throws IOException{

			try{
				java.io.File root = new java.io.File(path);
				java.io.File[] list = root.listFiles();

				if (list != null){
					for ( java.io.File f : list ){
						System.out.println("File Path : "+f.getAbsolutePath()+" ,FileName : "+f.getName()+" , lastModified : "+f.lastModified());
						//if ( f.isFile() && ( (f.lastModified() >= formatterStartDate) && (f.lastModified() <= formatterEndDate) ) ) {
						if ( f.isFile()) {

							Map<String, String> fileObject = new HashMap<String, String>();
							String absPath = FilenameUtils.getFullPathNoEndSeparator(f.getAbsolutePath()).replace("\\", "/");
							String relPath = absPath.replaceAll(mainPath.replace("\\", "/"), "");

							fileObject.put("file_name", FilenameUtils.getBaseName(f.getName()));
							fileObject.put("abs_path", f.getAbsolutePath());
							fileObject.put("rel_path", relPath );
							fileObject.put("destination_path",destinationPath);
							imageList.put(FilenameUtils.getBaseName(f.getName()),fileObject);
							System.out.println("File Path1 : "+f.getAbsolutePath()+" ,FileName1 : "+f.getName()+" , lastModified1 : "+f.lastModified());
						}else{
							imageList.putAll(getImageListFromPath(f.getAbsolutePath(), startDate,endDate,destinationPath,imageList, mainPath));
						}
					}

				}
			}catch (Exception e) {
				System.out.println("Error : "+e.getMessage());
			}
			return imageList;
		}

}
