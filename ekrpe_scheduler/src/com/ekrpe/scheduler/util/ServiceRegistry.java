package com.ekrpe.scheduler.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class ServiceRegistry {

	private static ApplicationContext context = null;

	static{		
        String[] file = {Utilities.getSetting("SPRING_CONFIG")};
		initialize(file);
	}
	
	public static Object getBean(final String key) {	
		return get(key);
		
	}
	private static Object get(final String key) {
		try{
			return context.getBean(key);
		}catch(Exception e){			
			return null;
		}
	}
	private static void initialize(final String[] file) {		
		if (context == null) {				
			context = new FileSystemXmlApplicationContext(file);				
		}	
		System.out.print("done");
	}
}
