package com.ekrpe.scheduler.util;

public class Constants {

	public static final String FILE_PROCESSING_DIR_INBOX=Utilities.getSetting("FILE_PROCESSING_DIR_INBOX");
	public static final String FILE_PROCESSING_DIR_QUEUE=Utilities.getSetting("FILE_PROCESSING_DIR_QUEUE");
	
	//Start Sample Copy///
	//public static final String GDOC_PRODUCT_ID="1ZwJ5IKYAT8z0pGTqu6halJtZNFasKKH2_GrUwpTscQ4"; //https://docs.google.com/spreadsheets/d/1ZwJ5IKYAT8z0pGTqu6halJtZNFasKKH2_GrUwpTscQ4/edit#gid=1372571499
	//public static final String GDOC_IMAGE_ID="1qy6euHao9tBH8WL3RVFCtcCuHt1GsQhjamL1Ux3wZ7o"; //https://docs.google.com/spreadsheets/d/1qy6euHao9tBH8WL3RVFCtcCuHt1GsQhjamL1Ux3wZ7o/edit#gid=1236401645
	//End Sample Copy///
	
	//public static final String GDOC_PRODUCT_ID="1KYNGAx_zAjQZlxloTb-twZ63EcoZyN4wmQ9q4QB50to"; //https://docs.google.com/spreadsheets/d/1KYNGAx_zAjQZlxloTb-twZ63EcoZyN4wmQ9q4QB50to/edit#gid=0
	//public static final String GDOC_IMAGE_ID="19YGaQ4PjI6fc3R5Ll-lJN4ZVYiEUBREaWNTRhiQFtSU"; //https://docs.google.com/spreadsheets/d/19YGaQ4PjI6fc3R5Ll-lJN4ZVYiEUBREaWNTRhiQFtSU/edit#gid=1815583129
	public static final String GENERAL_SETTINGS_TAB = "general_settings";
	public static final String IMAGE_FOLDER_TAB = "image_folders";
	public static final String IMAGE_NAME_RULES_TAB = "image_name_rules";
	public static final String IMAGE_NAME_RULES_ODATAV_TAB = "image_name_rules_odatav";
	public static final String PRODUCT_ID_TAB = "products";
	public static final String IMAGE_RESAMPLING_TAB = "image_resampling";
	public static final String IMAGEMAGICK_EXE_PATH = Utilities.getSetting("IMAGEMAGICK_EXE_PATH");
	public static final String GOOGLE_USER = Utilities.getSetting("GOOGLE_USER");
	//public static final String SAP_USERNAME = "SYSTEM";
	//public static final String SAP_PASSWORD = "3KR005y573m";
	//public static final String END_POINT_URL = "http://54.93.218.6:8000/p1941448227trial/hanaxs/fashioncoe/services/skuclassificationproperties_in_progress.xsjs";
	//public static final String END_POINT_URL = "http://54.93.218.6:8000/p1941448227trial/hanaxs/fashioncoe/app/mk-launchpad/#/launchpad";
	
}
