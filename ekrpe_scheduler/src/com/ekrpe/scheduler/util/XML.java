package com.ekrpe.scheduler.util;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;




public class XML 
{
	private static final Logger logger=Logger.getLogger(XML.class);
  public XML()
  {
  }
  
  /**
   * Esegue il pasing di una stringa contenente un documento XML utilizzndo il SAXParse
   * @return Document 
   * @param xmlData string XML
   */
  public static Document parse(String xmlData){
  
  Document document=null;
  try {
          DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
          DocumentBuilder builder = dbf.newDocumentBuilder();
          StringReader r = new StringReader(xmlData); 
          org.xml.sax.InputSource is = new org.xml.sax.InputSource(r); 
          document= builder.parse(is);
  } 
  catch (ParserConfigurationException pce) {
	  
	  
  }  
  catch (SAXException sxe) {
	  
	   
  }
  catch (IOException ioe) {
	  
             
  }
  
  return document;
  }
  
  /**
   * 
   * @return 
   * @param tagName
   * @param document
   */
  public static NodeList getElementsByTagName(Document document, String tagName){
   return document.getElementsByTagName(tagName);
  }

  /** 
   * Ritorna il valore contenuto in un nodo
   * @return 
   * @param node
   */
  public static String getNodeValue(Node node){
   return node.getChildNodes().item(0).getNodeValue();
  }
  
  /**
   * 
   * @return 
   * @param tagName
   * @param document
   */
  public static String getTagValue(Document document, String tagName){
   NodeList nodeList = getElementsByTagName(document, tagName);
   String value="";
   try{
   if (nodeList.getLength()>0 ){
    Node node = nodeList.item(0).getChildNodes().item(0) ; 

    value=node.getNodeValue();
   } 
   }catch(Exception ex){}
   return value;
  }
  
  /**
   * Usare 1 per il primo elemento
   * @return 
   * @param index
   * @param tagName
   * @param document
   */
  public static String getTagValue(Document document, String tagName, int index){
   NodeList nodeList = getElementsByTagName(document, tagName);
   String value="";
   
   if (nodeList.getLength()>0 ){
    Node node = nodeList.item(index - 1).getChildNodes().item(0) ; 
    value=node.getNodeValue();
   } 
   return value;
  }
  /**
   * Dato un nodo, ritorna un nodo figlio riconoscendolo dal nome.
   * Se nel nodo padre, sono presenti pi� nodi figli con lo stesso nome,
   * ritorna l'ultimo nodo figlio trovato!
   * @return Node nodo figlio con nome = name
   * @param name nome nodo figlio
   * @param nodoParent riferimento a nodo padre
   */
  public static Node getNodeByName(Node nodoParent, String name) 
  {
    NodeList nodeLst = nodoParent.getChildNodes();
    Node retNode = null;
    for(int x=0; x< nodeLst.getLength(); x++) {
        Node nodeSon = nodeLst.item(x);
        short sNodeSonType = nodeSon.getNodeType();
        if(sNodeSonType== Node.ELEMENT_NODE) 
        {
          String nameId = nodeSon.getNodeName();
          if(nameId.equals(name)) {
            retNode= nodeSon;
          }
        }
    
   }
       return retNode;
  }
  
  
    /**
   * Dato il riferimento di un nodo, ritorna la HashMap di tutti i sottonodi(key = nome nodo; value = valore nodo).
   * Se un sottonodo � ripetuto pi� volte,viene inserito il valore dell'ultimo sottonodo duplicato.
   * @return 
   * @param map
   * @param currentNode
   */
    public static HashMap printNodeMap(Node currentNode, HashMap map) {
            HashMap mapbuff =map;
            short sNodeType = currentNode.getNodeType();
            //Se � di tipo Element ricavo le informazioni e le stampo
            if (sNodeType == Node.ELEMENT_NODE) {
                String sNodeName = currentNode.getNodeName();
                String sNodeValue = searchTextInElement(currentNode);
                NamedNodeMap nnmAttributes = currentNode.getAttributes();
                //strbuff.append(" Attributi: " + printAttributes(nnmAttributes));
                if (sNodeValue!=null && !sNodeValue.trim().equalsIgnoreCase("")) {
                    mapbuff.put(sNodeName, sNodeValue);
                }
                else {
                  mapbuff.put(sNodeName, "");
                }
            }
            int iChildNumber = currentNode.getChildNodes().getLength();
          //Se non si tratta di una foglia continua l'esplorazione ricorsivamente
              if (currentNode.hasChildNodes()) {
                NodeList nlChilds = currentNode.getChildNodes();
                for (int iChild = 0; iChild < iChildNumber; iChild++) {
                  HashMap mapb  = new HashMap();
                     mapbuff.putAll(printNodeMap(nlChilds.item(iChild), mapb));
              }
            }
            return mapbuff;
    }
    
    
  /**
   * 
   * @return 
   * @param elementNode
   */
    public static String searchTextInElement(Node elementNode) {
        String sText = "";
        if (elementNode.hasChildNodes()) {
                //Il child node di tipo testo � il primo (e unico nel nostro caso)
            Node nTextChild = elementNode.getChildNodes().item(0);
                sText = nTextChild.getNodeValue();
            }
            return sText;
    }
    
    
    
    public static Document parse(File file){
  
  Document document=null;
  try {
          DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
          DocumentBuilder builder = dbf.newDocumentBuilder();
          FileReader r = new FileReader(file); 
          org.xml.sax.InputSource is = new InputSource(r); 
          document= builder.parse(is);
  } 
  catch (Exception pce) {
	  
  }  
  
  
  return document;
  }

}