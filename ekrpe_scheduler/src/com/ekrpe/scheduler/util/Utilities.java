package com.ekrpe.scheduler.util;

import it.ekr.pdffromselector.parsingengine.ParsingSapXml;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

public class Utilities {

	private static final Logger logger = Logger.getLogger(Utilities.class);
	
	public static String getSetting(String key){

	    String value="";
	    try{
	    	// modified by Marco Scarpa marco.scarpa@ekr.it
			// first I search data from properties file in ROOT_PE
			String env = System.getenv(ParsingSapXml.ROOT_PE_ENV_VAR);// need to
																		// check
																		// environment
																		// variable
																		// to
																		// look
																		// for
																		// the
																		// property
																		// value
			String rootPe = ParsingSapXml.DEFAULT_ROOT_PE;
			if (env != null) {
				rootPe = env;
			}
			File rootPeFile = new File(rootPe);
			Path rootPePath = rootPeFile.toPath();
			Path settingsTeDir = rootPePath
					.resolve(ParsingSapXml.TE_SETTINGS_SUBDIR);
			Path teConfigPath = settingsTeDir
					.resolve(ParsingSapXml.TE_CONFIG_FILE);
			File teConfigFile = teConfigPath.toFile();
			//loading properties file
			Properties configProperties = new Properties();
			if (teConfigFile.exists()) {
				FileInputStream teConfigStream = new FileInputStream(
						teConfigFile);
				configProperties.load(teConfigStream);
			}
			value = configProperties.getProperty(key);
			//property not found in external file, looking for it in embedded resource bundle
			if (value == null) {
				ResourceBundle res = ResourceBundle
						.getBundle("com.ekrpe.scheduler.application");
				value = res.getString(key);
			}
	    }
	    catch(Exception e)
	    {
	    	logger.error(e.getMessage(), e);
	    }
	    return value;
	  }
	
	 public static boolean isValidCollection(Collection param) {
			if (param != null && param.size() > 0) {
				return true;
			}
			return false;
		 }
			
		public static boolean isStringPopulated(Object data) {
			if (data != null && !data.toString().trim().equalsIgnoreCase(""))
				return true;
			else
				return false;
		}

}
