package com.ekrpe.scheduler.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ekrpe.scheduler.model.ScheduledTaskVO;

public class WebService {

	private static final Logger logger = Logger.getLogger(WebService.class);
	
	public WebService() {
	}

	/**
	 * Invio un documento XML ad un'altro server http per la richiesta di un
	 * servizio(ad esempio il Tracking UPS)
	 * 
	 * @return
	 * @param xmlData
	 * @param serviceUrl
	 */
	public static String invoke(String serviceUrl, String xmlData,String jobType, String username, String password, HttpClient httpclient) {

		StringBuffer xmlString = new StringBuffer();
		PostMethod post = null;
		//HttpPost post = null;
		BufferedReader rd = null;
		String line = "";
		String charset = "json";
		
		try {
			
				post = new UTF8PostMethod(serviceUrl);
				StringRequestEntity sre =null;
				if(ScheduledTaskVO.JOB_TYPE_IMAGE_CHECKER_ODATAV.equals(jobType)){
					//============SOAP JSON CALLS============//
					Base64 b = new Base64();
		            String encoding = b.encodeAsString(new String(username+":"+password).getBytes());
		            post.addRequestHeader("Authorization", "Basic " + encoding);
		            post.addRequestHeader("Accept-Charset", charset);
					sre = new StringRequestEntity(xmlData,"application/json","UTF-8");
				}else{
					//============EKR XML SOAP CALLS============//
					post.setRequestHeader("ContentType", "text/xml;charset=UTF-8");
	
					if (xmlData.contains("aggiornaFormEKBSIN")){
						post.addRequestHeader("SOAPAction","aggiornaFormEKBSIN");
					}
					else if (xmlData.contains("dettaglioFormEKBSIN")){
						post.addRequestHeader("SOAPAction","dettaglioFormEKBSIN");
					}
		            
					sre = new StringRequestEntity(xmlData,"text/xml", "UTF-8");
				}
				post.setRequestEntity(sre);
				post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,new DefaultHttpMethodRetryHandler(5, false));

				int result = httpclient.executeMethod(post);
				
				rd = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));

				for (; (line = rd.readLine()) != null;) {
					xmlString.append(line);
					//System.out.println("xmlString : "+new String(xmlString));
				}
				
		} catch (Exception e) {
			logger.error(e.getMessage(),e );
		}

		finally {
			if (post != null) {
				try {
					//post.releaseConnection();
					post.abort();
					
				} catch (Exception ex) {
				}
			}
			if (rd != null) {
				try {
					rd.close();
				} catch (Exception ex) {
				}
			}
		}
		return xmlString.toString();
	}

	public static class UTF8PostMethod extends PostMethod {
		public UTF8PostMethod(String url) {
			super(url);
		}
	}

	public static List<String> parseXmlString(String xmlString,
			List<String> nodes) {
		List<String> xmlNodeList = new ArrayList<String>();
		try {
			if (nodes != null && nodes.size() > 0) {
				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder db = factory.newDocumentBuilder();
				InputSource inStream = new InputSource();
				inStream.setCharacterStream(new StringReader(xmlString));
				Document doc = db.parse(inStream);
				doc.getDocumentElement().normalize();
				NodeList nodeLst = doc.getElementsByTagName(nodes.get(0));
				for (int s = 0; s < nodeLst.getLength(); s++) {
					Node fstNode = nodeLst.item(s);
					if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
						Element fstElmnt = (Element) fstNode;
						for (int i = 1; i < nodes.size(); i++) {
							NodeList nodeList = fstElmnt
									.getElementsByTagName(nodes.get(i));
							Element elmnt = (Element) nodeList.item(0);
							NodeList childNodes = elmnt.getChildNodes();
							String value = ((Node) childNodes.item(0))
									.getNodeValue();
							xmlNodeList.add(value.toUpperCase());
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return xmlNodeList;

	}
	
}

