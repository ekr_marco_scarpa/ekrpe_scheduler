package com.ekrpe.scheduler.model;

import java.io.Serializable;
import java.util.Date;


public class LoggerVO implements Serializable{

	public LoggerVO(String code, String description){
		logCode= code;
		logDescription=description;
	}
	private Date logDate= new Date();
	private String logCode="";
	private String logDescription="";
	public Date getLogDate() {
		return logDate;
	}
	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
	public String getLogCode() {
		return logCode;
	}
	public void setLogCode(String logCode) {
		this.logCode = logCode;
	}
	public String getLogDescription() {
		return logDescription;
	}
	public void setLogDescription(String logDescription) {
		this.logDescription = logDescription;
	}
	
}
