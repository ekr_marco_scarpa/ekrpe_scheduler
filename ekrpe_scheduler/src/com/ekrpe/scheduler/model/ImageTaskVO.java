package com.ekrpe.scheduler.model;

import java.io.Serializable;
import java.util.Date;


public class ImageTaskVO implements Serializable{

	private String productId;
	private String files_found;
	private boolean findExistingImages=false;

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductId() {
		return productId;
	}

	public void setFiles_found(String files_found) {
		this.files_found = files_found;
	}

	public String getFiles_found() {
		return files_found;
	}

	public void setFindExistingImages(boolean findExistingImages) {
		this.findExistingImages = findExistingImages;
	}

	public boolean isFindExistingImages() {
		return findExistingImages;
	}

}
