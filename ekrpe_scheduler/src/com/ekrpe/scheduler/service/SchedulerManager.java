package com.ekrpe.scheduler.service;

import java.util.List;

import com.ekrpe.scheduler.criteria.ScheduledTaskVOCriteria;
import com.ekrpe.scheduler.dto.ScheduledTaskVOListDto;
import com.ekrpe.scheduler.model.ImageTaskVO;
import com.ekrpe.scheduler.model.ScheduledTaskVO;


public interface SchedulerManager {
	public void enqueueJobs();
	public List<ScheduledTaskVO> getPendingJobs();
	public ScheduledTaskVOListDto searchScheduleTaskVO(ScheduledTaskVOCriteria scheduledTaskVOCriteria);
	public ScheduledTaskVO saveScheduledTaskVO(ScheduledTaskVO scheduledTaskVO);
	public String getImageCollections(String productID);
	public String updateImageProcessorLog(String newJson, String key);
}
