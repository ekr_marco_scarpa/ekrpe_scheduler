package com.ekrpe.scheduler.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.ekrpe.scheduler.criteria.ScheduledTaskVOCriteria;
import com.ekrpe.scheduler.dao.SchedulerDao;
import com.ekrpe.scheduler.dto.ScheduledTaskVOListDto;
import com.ekrpe.scheduler.model.ImageTaskVO;
import com.ekrpe.scheduler.model.ScheduledTaskVO;


public class SchedulerManagerImpl implements SchedulerManager{
	private SchedulerDao schedulerDao;
	private static final Logger logger = Logger.getLogger(SchedulerManagerImpl.class);
	@Override
	public void enqueueJobs() {
		schedulerDao.enqueueJobs();
	}
	public SchedulerDao getSchedulerDao() {
		return schedulerDao;
	}
	public void setSchedulerDao(SchedulerDao schedulerDao) {
		this.schedulerDao = schedulerDao;
	}
	

	public List<ScheduledTaskVO> getPendingJobs(){
		return schedulerDao.getPendingJobs();
	}
	
	public ScheduledTaskVOListDto searchScheduleTaskVO(ScheduledTaskVOCriteria scheduledTaskVOCriteria){
		ScheduledTaskVOListDto scheduledTaskVOListDto= new ScheduledTaskVOListDto();
		scheduledTaskVOListDto.setScheduledTaskVOList(schedulerDao.searchScheduleTaskVO(scheduledTaskVOCriteria));
		return scheduledTaskVOListDto;
	}
	public ScheduledTaskVO saveScheduledTaskVO(ScheduledTaskVO scheduledTaskVO){
		return schedulerDao.saveScheduledTaskVO(scheduledTaskVO);
	}
	
	@Override
	public String getImageCollections(String productID) {
		
		return schedulerDao.getImageCollections(productID);
	}
	@Override
	public String updateImageProcessorLog(String newJson, String productID) {
		return schedulerDao.updateImageProcessorLog(newJson,productID);
		
	}
	
}
