package com.ekrpe.scheduler.ws;


import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jws.WebService;

import com.ekrpe.scheduler.criteria.ScheduledTaskVOCriteria;
import com.ekrpe.scheduler.dto.AddJobRequestDto;
import com.ekrpe.scheduler.dto.AddJobResponseDto;
import com.ekrpe.scheduler.dto.ScheduledTaskVOListDto;
import com.ekrpe.scheduler.model.ScheduledTaskVO;
import com.ekrpe.scheduler.runnable.ImageProcessor;
import com.ekrpe.scheduler.runnable.TaskBase;
import com.ekrpe.scheduler.service.SchedulerManager;
import com.ekrpe.scheduler.util.Constants;
import com.ekrpe.scheduler.util.ServiceRegistry;
import com.sun.xml.wss.impl.misc.Base64;

@WebService(endpointInterface = "com.ekrpe.scheduler.ws.SchedulerService")
public class SchedulerServiceImpl implements SchedulerService{
	@Override		
	public ScheduledTaskVOListDto searchScheduleTaskVO(ScheduledTaskVOCriteria scheduledTaskVOCriteria){
		SchedulerManager schedulerManager =(SchedulerManager)ServiceRegistry.getBean("schedulerManager");		
			return schedulerManager.searchScheduleTaskVO(scheduledTaskVOCriteria);
	}
	
	
	public AddJobResponseDto addJob(AddJobRequestDto addJobRequestDto){
		
		AddJobResponseDto addJobResponseDto= new AddJobResponseDto();
		try{
			SchedulerManager schedulerManager =(SchedulerManager)ServiceRegistry.getBean("schedulerManager");
			
			ScheduledTaskVO scheduledTaskVO= new ScheduledTaskVO();
			scheduledTaskVO.setJobType(addJobRequestDto.getJobType());
			if (ScheduledTaskVO.JOB_TYPE_PDF_PROCESSING.equals(scheduledTaskVO.getJobType())){
				String timeStamp = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());		
				String filePath=Constants.FILE_PROCESSING_DIR_QUEUE + "/" + timeStamp;			
				byte[] btDataFile = Base64.decode(addJobRequestDto.getParameters());  		
				FileOutputStream osf = new FileOutputStream(filePath); 
				osf.write(btDataFile);  
				osf.flush(); 
				osf.close();			
		        scheduledTaskVO.setJobInputFile(filePath);
			}
			else if (ScheduledTaskVO.JOB_TYPE_IMAGE_CHECKER.equals(scheduledTaskVO.getJobType()) || ScheduledTaskVO.JOB_TYPE_IMAGE_CHECKER_ODATAV.equals(scheduledTaskVO.getJobType())){
				scheduledTaskVO.setJobInputFile(addJobRequestDto.getParameters());
			}
	        
	        scheduledTaskVO.setJobStatus(TaskBase.STATUS_PENDING);
	        scheduledTaskVO= schedulerManager.saveScheduledTaskVO(scheduledTaskVO);	
	        addJobResponseDto.setJobId(scheduledTaskVO.getJobId());	        
		}
		catch (Exception e){			
			e.printStackTrace();
		}
		
		return addJobResponseDto;
	}
}
