package com.ekrpe.scheduler.ws;

import javax.jws.WebService;

import com.ekrpe.scheduler.criteria.ScheduledTaskVOCriteria;
import com.ekrpe.scheduler.dto.AddJobRequestDto;
import com.ekrpe.scheduler.dto.AddJobResponseDto;
import com.ekrpe.scheduler.dto.ScheduledTaskVOListDto;

@WebService
public interface SchedulerService {
	public ScheduledTaskVOListDto searchScheduleTaskVO(ScheduledTaskVOCriteria scheduledTaskVOCriteria);	
	public AddJobResponseDto addJob(AddJobRequestDto addJobRequestDto);
	
	
}
