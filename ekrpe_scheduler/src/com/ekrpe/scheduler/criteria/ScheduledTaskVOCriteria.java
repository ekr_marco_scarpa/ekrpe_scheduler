package com.ekrpe.scheduler.criteria;

public class ScheduledTaskVOCriteria {

	
	private String jobStatus;
	private Long jobId;
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public Long getJobId() {
		return jobId;
	}
	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}
}
