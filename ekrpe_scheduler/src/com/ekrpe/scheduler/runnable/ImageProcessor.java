package com.ekrpe.scheduler.runnable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.trimou.Mustache;
import org.trimou.engine.MustacheEngineBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sun.net.www.protocol.http.HttpURLConnection;

import com.ekrpe.scheduler.model.LoggerVO;
import com.ekrpe.scheduler.model.ScheduledTaskVO;
import com.ekrpe.scheduler.service.SchedulerManager;
import com.ekrpe.scheduler.util.Constants;
import com.ekrpe.scheduler.util.ServiceRegistry;
import com.ekrpe.scheduler.util.Utilities;
import com.ekrpe.scheduler.util.WebService;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.drive.DriveScopes;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.google.gdata.client.spreadsheet.CellQuery;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ImageProcessor extends TaskBase{

	private String settingsGdocId;
	private String productGdocId;
	private String jobType;
	private String skuPackCode;
	private static final Logger logger = Logger.getLogger(ImageProcessor.class);
	public void doRun() {
		setStatus(STATUS_RUNNING);
		addLog(new LoggerVO("1", "Starting processing of file " + getDataSourceFilePath()));
		try{
			processImages();
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			setStatus(STATUS_ERROR);
		}

		setStatus(STATUS_COMPLETED);
		addLog(new LoggerVO("2", "Finished"));


	}

	public static void main(String[] args) throws IOException, ServiceException, SAXException, ParserConfigurationException, URISyntaxException, TransformerException, InterruptedException{
		
		ImageProcessor imageProcessor= new ImageProcessor();
		//imageProcessor.setSettingsGdocId("19YGaQ4PjI6fc3R5Ll-lJN4ZVYiEUBREaWNTRhiQFtSU");
		//imageProcessor.setProductGdocId("1KYNGAx_zAjQZlxloTb-twZ63EcoZyN4wmQ9q4QB50to");
		imageProcessor.setSettingsGdocId("1qy6euHao9tBH8WL3RVFCtcCuHt1GsQhjamL1Ux3wZ7o");
		imageProcessor.setProductGdocId("1ZwJ5IKYAT8z0pGTqu6halJtZNFasKKH2_GrUwpTscQ4");
		//imageProcessor.setSettingsGdocId("1uD0JqkVlbsHUkx8vUzGWalm1JUoPyih0puTjAuEkakU");
		//imageProcessor.setProductGdocId("1K-a6W6W74rjsAhzkBSf0N2qKCl0MjxjE4wQQ_pKTMEc");
		imageProcessor.setJobType("image_check_odatav");
		//imageProcessor.setJobType("image_check");
		imageProcessor.setSkuPackCode("7010F15");
		imageProcessor.processImages(); 

	}

	public void processImages() throws IOException, ServiceException, SAXException, ParserConfigurationException, URISyntaxException, TransformerException{

		System.out.println("===========Start imageProcessor============");
		Map<String, Object> worksheetsValues = getSettingTabDetails(settingsGdocId);
		List<String> productIDList = getProductIDList(productGdocId, (Map<String, Object>) worksheetsValues.get(Constants.GENERAL_SETTINGS_TAB));
		Map<String, Object> imageList = getImageListFromAllPath((Map<String, Object>) worksheetsValues.get(Constants.IMAGE_FOLDER_TAB));

		if(ScheduledTaskVO.JOB_TYPE_IMAGE_CHECKER.equals(jobType)){
			Map<String, Object> xmlList = createProductImageXML(productIDList, imageList, (Map<String, Object>) worksheetsValues.get(Constants.IMAGE_NAME_RULES_TAB), (Map<String, Object>) worksheetsValues.get(Constants.GENERAL_SETTINGS_TAB),(Map<String, Object>) worksheetsValues.get(Constants.IMAGE_RESAMPLING_TAB));
			updateDB(xmlList,(Map<String, Object>) worksheetsValues.get(Constants.GENERAL_SETTINGS_TAB), (Map<String, Object>) worksheetsValues.get(Constants.IMAGE_NAME_RULES_TAB), imageList,(Map<String, Object>) worksheetsValues.get(Constants.IMAGE_RESAMPLING_TAB));
		}else if(ScheduledTaskVO.JOB_TYPE_IMAGE_CHECKER_ODATAV.equals(jobType)){
			Map<String, Object> jsonList = createProductImageJSON(productIDList, imageList, (Map<String, Object>) worksheetsValues.get(Constants.IMAGE_NAME_RULES_ODATAV_TAB),(Map<String, Object>) worksheetsValues.get(Constants.GENERAL_SETTINGS_TAB),(Map<String, Object>) worksheetsValues.get(Constants.IMAGE_RESAMPLING_TAB));
			updateSAP(jsonList,(Map<String, Object>) worksheetsValues.get(Constants.GENERAL_SETTINGS_TAB), (Map<String, Object>) worksheetsValues.get(Constants.IMAGE_NAME_RULES_ODATAV_TAB), imageList,(Map<String, Object>) worksheetsValues.get(Constants.IMAGE_RESAMPLING_TAB));
		}
		System.out.println("===========End imageProcessor============");
	}

	/**
	 * builds the right Spreadsheet service if the GOOGLE_USER const is configured or left empty (default in application.properties)
	 * @return the adeguate SpreadsheetService if authentication is set or not
	 */
	public SpreadsheetService getAdeguateService()
	{
		if (Constants.GOOGLE_USER.trim().length()>0)//Constants.GOOGLE_USER is never null by construction
		{
			return getauthService();
		}
		SpreadsheetService notAuthService =new SpreadsheetService("MySpreadsheetIntegration-v1");
		return notAuthService;
	}
	
	/**
	 * calculates the url for the sheet feed evaluating if the authentication is set or not
	 * @param sheetId the sheet id which url is needed
	 * @return the url for the given sheet id evaluated if the authentication is set or not
	 * @throws MalformedURLException
	 */
	public URL getAdeguateSpreadsheetUrl(String sheetId) throws MalformedURLException
	{
		if (Constants.GOOGLE_USER.trim().length()>0)//Constants.GOOGLE_USER is never null by construction
		{
			return FeedURLFactory.getDefault().getWorksheetFeedUrl(sheetId, "private", "full");
		}
		return FeedURLFactory.getDefault().getWorksheetFeedUrl(sheetId, "public", "basic");
	}
	
	//per la parte di service auth
	public SpreadsheetService getauthService(){
    	SpreadsheetService service = null;
    	try{
    		HttpTransport httpTransport = new NetHttpTransport();
    		JsonFactory jsonFactory = new JacksonFactory();
    		String pathp12="C:\\gdrivekey\\key.p12";
    		String [] SCOPESArray= {
	    		"https://spreadsheets.google.com/feeds", 
	    		"https://spreadsheets.google.com/feeds/spreadsheets/private/full", 
	    		"https://docs.google.com/feeds",
	    		"https://www.googleapis.com/auth/drive.file",
	    	    "https://www.googleapis.com/auth/userinfo.email",
	    	    "https://www.googleapis.com/auth/userinfo.profile",
	    		DriveScopes.DRIVE
	    		};
	    
  
    		final List SCOPES = Arrays.asList(SCOPESArray);
    		GoogleCredential credential = new GoogleCredential.Builder()
			.setTransport(httpTransport)
			.setJsonFactory(jsonFactory)
			.setServiceAccountId(Constants.GOOGLE_USER)
			.setServiceAccountScopes(SCOPES)
			.setServiceAccountPrivateKeyFromP12File(
			                   new java.io.File(pathp12))
			                   .build();
    		service = new SpreadsheetService("MySpreadsheetIntegration-v1");
    		credential.refreshToken();
    		String accessToken=credential.getAccessToken();
    		System.out.println("accessToken "+accessToken);
    		service.setHeader("Authorization", "Bearer " + accessToken);
    		System.out.println("son connessa!!!");
    		
    }catch(Exception e){
    	System.out.println("errore in getauthService "+e.getMessage());
    }
    return service;
   }
	private Map<String, Object> createProductImageJSON(List<String> productIDList, Map<String, Object> imageList, Map<String, Object> imageRules, Map<String, Object> settingsMap,Map<String, Object> imageMagickProcess) throws ParserConfigurationException, SAXException, IOException {
		Map<String, Object> jsonList = new HashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		for(String productID : productIDList){
			List<JsonNode> odatavJSON = new ArrayList<JsonNode>();
			List<JsonNode> odatavAllJSON = new ArrayList<JsonNode>();
			Map<String, String> imagePaths = new HashMap<String, String>();
			//Map<String, String> checkResampling = new HashMap<String, String>();

			Predicate<Map.Entry<String, Object>> placeHolderFilter = new Predicate<Map.Entry<String, Object>>() {
				@Override
				public boolean apply(Entry<String, Object> val) {
					return (val.getValue().toString().trim().startsWith("{{") && val.getValue().toString().trim().endsWith("}}"));
				}
			};

			Map<String, Object> placeHolderMap = Maps.filterEntries(settingsMap,placeHolderFilter);
			Map<String, String> placeHolderNewMap = new HashMap<String, String>();

			for(String imageFileName : imageList.keySet()){
				if(imageFileName.contains(productID)){
					for (Map.Entry<String, Object> rule : imageRules.entrySet()){
						HashMap<String, Object> imageFileObject = (HashMap<String, Object>) imageList.get(imageFileName);
						String imageFilePath = (String) imageFileObject.get("abs_path");
						if(rule.getKey().equalsIgnoreCase("EMPTY") && imageFileName.endsWith(productID)){
							JsonNode currentNode =  mapper.readTree(rule.getValue().toString());

							imagePaths.put(imageFilePath, getFileCheckSum(imageFilePath));

							for (Map.Entry<String, Object> placeHolder : placeHolderMap.entrySet()){
								if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_name")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), FilenameUtils.getName(imageFilePath));
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),FilenameUtils.getFullPathNoEndSeparator(imageFilePath).replace("\\", "/") );
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("product_code")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),productID );
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("destination_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), (String) imageFileObject.get("destination_path"));
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("relative_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), (String) imageFileObject.get("rel_path"));
								}
							}

							Gson gson = new Gson(); 
							String placeHolderJson = gson.toJson(placeHolderNewMap); 
							//System.out.println("placeHolder Json: "+placeHolderJson);

							JsonElement placeHolderJsonElement = new JsonParser().parse(placeHolderJson);
							Mustache docMustache = MustacheEngineBuilder.newBuilder().build().compileMustache(rule.getKey(), currentNode.toString());
							String parsedDoc = docMustache.render(placeHolderJsonElement);
							//System.out.println("parsedDoc Json: "+parsedDoc);
							currentNode =  mapper.readTree(parsedDoc);
							odatavJSON.add(currentNode);

						}else if(imageFileName.endsWith(rule.getKey())){
							/*HashMap<String, Object> imageFileObject = (HashMap<String, Object>) imageList.get(imageFileName);
							String imageFilePath = (String) imageFileObject.get("abs_path");*/
							JsonNode currentNode =  mapper.readTree(rule.getValue().toString());

							imagePaths.put(imageFilePath, getFileCheckSum(imageFilePath));

							for (Map.Entry<String, Object> placeHolder : placeHolderMap.entrySet()){
								if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_name")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), FilenameUtils.getName(imageFilePath));
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),FilenameUtils.getFullPathNoEndSeparator(imageFilePath).replace("\\", "/") );
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("product_code")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),productID );
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("destination_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), (String) imageFileObject.get("destination_path"));
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("relative_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), (String) imageFileObject.get("rel_path"));
								}
							}
							Gson gson = new Gson(); 
							String placeHolderJson = gson.toJson(placeHolderNewMap); 
							//System.out.println("placeHolder Json: "+placeHolderJson);

							JsonElement placeHolderJsonElement = new JsonParser().parse(placeHolderJson);
							Mustache docMustache = MustacheEngineBuilder.newBuilder().build().compileMustache(rule.getKey(), currentNode.toString());
							String parsedDoc = docMustache.render(placeHolderJsonElement);
							//System.out.println("parsedDoc Json: "+parsedDoc);
							currentNode =  mapper.readTree(parsedDoc);
							odatavJSON.add(currentNode);

						}

					}
				}
			}

			placeHolderNewMap = new HashMap<String, String>();
			for (Map.Entry<String, Object> rule : imageRules.entrySet()){
				JsonNode currentNode =  mapper.readTree(rule.getValue().toString());

				for (Map.Entry<String, Object> placeHolder : placeHolderMap.entrySet()){
					if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_name")){
						placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), "y");
					}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_path")){
						placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),"x");
					}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("product_code")){
						placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),productID );
					}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("destination_path")){
						placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), "a");
					}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("relative_path")){
						placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), "b");
					}	
				}
				Gson gson = new Gson(); 
				String placeHolderJson = gson.toJson(placeHolderNewMap); 
				//System.out.println("placeHolder Json: "+placeHolderJson);

				JsonElement placeHolderJsonElement = new JsonParser().parse(placeHolderJson);
				Mustache docMustache = MustacheEngineBuilder.newBuilder().build().compileMustache(rule.getKey(), currentNode.toString());
				String parsedDoc = docMustache.render(placeHolderJsonElement);
				//System.out.println("parsedDoc Json: "+parsedDoc);
				currentNode =  mapper.readTree(parsedDoc);

				odatavAllJSON.add(currentNode); 
			}


			if(odatavJSON.size() > 0 || odatavAllJSON.size() > 0){
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("json", odatavJSON);
				map.put("paths", imagePaths);
				map.put("alljson", odatavAllJSON);
				jsonList.put(productID,map);
			}
			//System.out.println("odatavJSON.size(): "+odatavJSON.size());
		}

		System.out.println("odatav jsonList: "+jsonList.size());
		return jsonList;
	}

	public Map<String, Object> getSettingTabDetails(String settingsGdocId) throws IOException, ServiceException, URISyntaxException{
		//no auth
    	//URL url = FeedURLFactory.getDefault().getWorksheetFeedUrl(settingsGdocId, "public", "basic");
    	//con auth
    	URL url = getAdeguateSpreadsheetUrl(settingsGdocId);//FeedURLFactory.getDefault().getWorksheetFeedUrl(settingsGdocId, "private", "full");
		System.out.println("GDOC_IMAGE_ID Url: "+url);
		
		//no auth
		//SpreadsheetService service =new SpreadsheetService("MySpreadsheetIntegration-v1");
		//con auth 
		SpreadsheetService service =getAdeguateService();
		WorksheetFeed feed = service.getFeed(url, WorksheetFeed.class);
		List<WorksheetEntry> worksheetList = feed.getEntries();
		System.out.println("Number of Worksheets: "+worksheetList.size());
		Map<String, Object> worksheetsValues = new HashMap<String, Object>();

		for(int i=0;i<worksheetList.size();i++){

			WorksheetEntry worksheetEntry = worksheetList.get(i);
			//System.out.println("WorkSheet Tab Name: "+worksheetEntry.getTitle().getPlainText());

			ListQuery listQuery = new ListQuery(worksheetEntry.getListFeedUrl());
			ListFeed listFeed = service.query( listQuery, ListFeed.class );
			Map<String, Object> rowValues = new LinkedHashMap<String, Object>();

			if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.GENERAL_SETTINGS_TAB)){

				for (ListEntry row : listFeed.getEntries()) {
					//System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("settingvalue: ", ""));
					rowValues.put(row.getTitle().getPlainText(), row.getPlainTextContent().replace("settingvalue: ", ""));
				}
			}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_FOLDER_TAB)){

				/*for (ListEntry row : listFeed.getEntries()) {
			    	System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("settingvalue: ", ""));
			    	rowValues.put(row.getTitle().getPlainText(), row.getPlainTextContent().replace("settingvalue: ", ""));
			     }*/



				//System.out.println("===========START IMAGE_FOLDER_TAB=============");
				int colNumber=1;

				CellQuery cellQuery = new CellQuery(worksheetEntry.getCellFeedUrl());
				CellFeed cellFeed = service.query(cellQuery, CellFeed.class);
				List<CellEntry> cell = cellFeed.getEntries();
				//Map<String, Object> imgFolderMap = new HashMap<String, Object>();
				int totalColumn = 4;
				// Iterate through each cell, printing its value.
				for (int j = 0; j < cell.size(); j++){
					if(totalColumn < j){
						break;
					}
					CellEntry row = cell.get(j);
					//System.out.println("row.getPlainTextContent() : "+row.getPlainTextContent());
					if(row.getPlainTextContent().equalsIgnoreCase("folder path")){
						colNumber = j+1;
						URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
						CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
						rowValues.put("folder_path", cellFeedFinal);
					}else  if(row.getPlainTextContent().equalsIgnoreCase("start date")){
						colNumber = j+1;
						URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
						CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
						rowValues.put("start_date", cellFeedFinal);
					}else  if(row.getPlainTextContent().equalsIgnoreCase("end date")){
						colNumber = j+1;
						URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
						CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
						rowValues.put("end_date", cellFeedFinal);
					}else  if(row.getPlainTextContent().equalsIgnoreCase("destination_path")){
						colNumber = j+1;
						URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
						CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
						rowValues.put("destination_path", cellFeedFinal);
					}
				}
				//System.out.println("===========END IMAGE_FOLDER_TAB=============");
				//URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
				//CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);

				// Iterate through each cell, printing its value.
				/*for (CellEntry cellFinal : cellFeedFinal.getEntries()) {
			      if(!productIDList.contains(cellFinal.getPlainTextContent())){  
	    			  System.out.println("row.getTitle().getPlainText() : "+cellFinal.getTitle().getPlainText());
			          System.out.println("row.getPlainTextContent() : "+cellFinal.getPlainTextContent());
			          productIDList.add(cellFinal.getPlainTextContent());
	    		  }
			    }*/

				//System.out.println("productIDList Count : "+productIDList.size());



			}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_NAME_RULES_TAB)){

				for (ListEntry row : listFeed.getEntries()) {
					//System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("mustachepattern: ", ""));
					String key = row.getTitle().getPlainText();
					if(row.getTitle().getPlainText().startsWith("Row:")){
						key = "EMPTY";
					}
					rowValues.put(key, row.getPlainTextContent().replace("mustachepattern: ", ""));
				}
			}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_NAME_RULES_ODATAV_TAB)){
				for (ListEntry row : listFeed.getEntries()) {
					//System.out.println("Key: "+row.getTitle().getPlainText() + ", Value: " + row.getPlainTextContent().replace("mustachepatternodatav: ", ""));
					String key = row.getTitle().getPlainText();
					if(row.getTitle().getPlainText().startsWith("Row:")){
						key = "EMPTY";
					}

					rowValues.put(key, row.getPlainTextContent().replace("mustachepatternodatav: ", ""));
				}
			}else if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.IMAGE_RESAMPLING_TAB)){

				//System.out.println("===========START IMAGE_RESAMPLING_TAB=============");
				int colNumber=1;

				CellQuery cellQuery = new CellQuery(worksheetEntry.getCellFeedUrl());
				CellFeed cellFeed = service.query(cellQuery, CellFeed.class);
				List<CellEntry> cell = cellFeed.getEntries();
				//Map<String, Object> imgFolderMap = new HashMap<String, Object>();
				int totalColumn = 3;
				// Iterate through each cell, printing its value.
				for (int j = 0; j < cell.size(); j++){
					if(totalColumn < j){
						break;
					}
					CellEntry row = cell.get(j);
					//System.out.println("row.getPlainTextContent() : "+row.getPlainTextContent());
					if(row.getPlainTextContent().equalsIgnoreCase("resized_subfolder")){
						colNumber = j+1;
						URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
						CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
						rowValues.put("resized_subfolder", cellFeedFinal);
					}else  if(row.getPlainTextContent().equalsIgnoreCase("imagemagick_options")){
						colNumber = j+1;
						URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
						CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
						rowValues.put("imagemagick_options", cellFeedFinal);
					}else  if(row.getPlainTextContent().equalsIgnoreCase("format")){
						colNumber = j+1;
						URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
						CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);
						rowValues.put("format", cellFeedFinal);
					}
				}
				//System.out.println("===========END IMAGE_RESAMPLING_TAB=============");

			}

			worksheetsValues.put(worksheetEntry.getTitle().getPlainText(), rowValues);
		}
		return worksheetsValues;
	}

	public List<String> getProductIDList(String productGdocId, Map<String, Object> settingsMap) throws IOException, ServiceException, URISyntaxException{
		final String PRODUCTID_COLUMN_NAME = Utilities.isStringPopulated(settingsMap.get("productid_column_name"))?settingsMap.get("productid_column_name").toString().trim():"";
		//System.out.println("PRODUCTID_COLUMN_NAME: "+PRODUCTID_COLUMN_NAME);
		List<String> productIDList = new ArrayList<String>();

		//no auth
				//URL url = FeedURLFactory.getDefault().getWorksheetFeedUrl(productGdocId, "public", "basic");
				//si auth
				URL url=getAdeguateSpreadsheetUrl(productGdocId);//FeedURLFactory.getDefault().getWorksheetFeedUrl(productGdocId, "private", "full");
				//System.out.println("GDOC_PRODUCT_ID Url: "+url);
				//no auth
				//SpreadsheetService service =new SpreadsheetService("MySpreadsheetIntegration-v1");
				//con auth
				SpreadsheetService service =getAdeguateService();
		WorksheetFeed feed = service.getFeed(url, WorksheetFeed.class);
		List<WorksheetEntry> worksheetList = feed.getEntries();
		//System.out.println("Number of Worksheets: "+worksheetList.size());

		for(int i=0;i<worksheetList.size();i++){

			WorksheetEntry worksheetEntry = worksheetList.get(i);
			//System.out.println("WorkSheet Tab Name: "+worksheetEntry.getTitle().getPlainText());

			if(worksheetEntry.getTitle().getPlainText().equalsIgnoreCase(Constants.PRODUCT_ID_TAB)){

				int colNumber=1;

				CellQuery cellQuery = new CellQuery(worksheetEntry.getCellFeedUrl());
				CellFeed cellFeed = service.query(cellQuery, CellFeed.class);
				List<CellEntry> cell = cellFeed.getEntries();

				// Iterate through each cell, printing its value.
				for (int j = 0; j < cell.size(); j++){
					CellEntry row = cell.get(j);
					if(row.getPlainTextContent().equalsIgnoreCase(PRODUCTID_COLUMN_NAME)){
						colNumber = j+1;
						break;
					}
				}

				URL cellFeedUrl = new URI(worksheetEntry.getCellFeedUrl().toString()+ "?min-row=2&min-col="+colNumber+"&max-col="+colNumber).toURL();
				CellFeed cellFeedFinal = service.getFeed(cellFeedUrl, CellFeed.class);

				// Iterate through each cell, printing its value.
				for (CellEntry cellFinal : cellFeedFinal.getEntries()) {
					if(!productIDList.contains(cellFinal.getPlainTextContent())){  
						//System.out.println("row.getTitle().getPlainText() : "+cellFinal.getTitle().getPlainText());
						//System.out.println("row.getPlainTextContent() : "+cellFinal.getPlainTextContent());
						productIDList.add(cellFinal.getPlainTextContent());
					}
				}

				System.out.println("productIDList Count : "+productIDList.size());
			}
		}

		return productIDList;

	}

	public Map<String, Object> getImageListFromAllPath(Map<String, Object> pathList) throws IOException{
		Map<String, Object> imageList = new HashMap<String, Object>();
		int rowNum = 0;
		CellFeed folderPathList = (CellFeed) pathList.get("folder_path");
		for (CellEntry cellFinal : folderPathList.getEntries()) {
			CellFeed startDateList = (CellFeed) pathList.get("start_date");
			String startDate = startDateList.getEntries().get(rowNum).getPlainTextContent();

			CellFeed endDateList = (CellFeed) pathList.get("end_date");
			String endDate = endDateList.getEntries().get(rowNum).getPlainTextContent();

			CellFeed destinationPathList = (CellFeed) pathList.get("destination_path");
			String destinationPath = destinationPathList.getEntries().get(rowNum).getPlainTextContent().replace("\\", "/");	
			String mainPath = cellFinal.getPlainTextContent();
			try{
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				long formatterStartDate = formatter.parse(startDate).getTime();
				long formatterEndDate = formatter.parse(endDate).getTime();
				long currentDate = formatter.parse(dateToStringFormat(new Date(),"MM/dd/yyyy")).getTime();

				if ( (currentDate >= formatterStartDate) && (currentDate <= formatterEndDate) ){

					imageList.putAll(getImageListFromPath(cellFinal.getPlainTextContent(), startDate,endDate,destinationPath,imageList, mainPath));

				}

			}catch (Exception e) {
				System.out.println("Error : "+e.getMessage());
			}
			rowNum++;
		}

		System.out.println("imageList : "+imageList.size());
		return imageList;


	}

	public void createImageMagickProcess(String sourceFilePath,Map<String, Object> imageMagickProcess,String destinationPath,String relPath) throws IOException{

		int rowNum = 0;
		CellFeed resizedSubfolderList = (CellFeed) imageMagickProcess.get("resized_subfolder");
		for (CellEntry cellFinal : resizedSubfolderList.getEntries()) {

			String resizedSubfolder = cellFinal.getPlainTextContent().replace("\\", "/");
			CellFeed imageMagickOptionsList = (CellFeed) imageMagickProcess.get("imagemagick_options");
			String imageMagickOptions = imageMagickOptionsList.getEntries().get(rowNum).getPlainTextContent();
			CellFeed formatList = (CellFeed) imageMagickProcess.get("format");
			String format = formatList.getEntries().get(rowNum).getPlainTextContent();

			try{
				String destinationFolderPath = destinationPath+resizedSubfolder+relPath;
				String destinationFilePath = destinationFolderPath+"/"+FilenameUtils.getBaseName(sourceFilePath)+"."+format;
				//System.out.println("destinationFilePath: " + destinationFilePath);

				File destinationPathDir = new File(destinationFolderPath);
				if(!destinationPathDir.exists()){
					destinationPathDir.mkdirs();
				}

				String exePath =Constants.IMAGEMAGICK_EXE_PATH+" "+imageMagickOptions+" "+sourceFilePath+" "+destinationFilePath;
				//System.out.println("exePath: " + exePath);
				Process pv = Runtime.getRuntime().exec(exePath);

				int exitCode = pv.waitFor();
				//System.out.println("Exit Code: " + exitCode);
			}catch(Exception e){
				logger.error("Error: " + e.getMessage());
				//System.out.println("Error: " + e.getMessage());
			}



			rowNum++;
		}
		//System.out.println("createImageMagickProcess rowNum : "+rowNum);


	}

	private Map<String, Object> getImageListFromPath(String path, String startDate, String endDate, String destinationPath, Map<String, Object> imageList, String mainPath) throws IOException{

		try{
			File root = new File(path);
			File[] list = root.listFiles();

			if (list != null){
				for ( File f : list ){
					//System.out.println("File Path : "+f.getAbsolutePath()+" ,FileName : "+f.getName()+" , lastModified : "+f.lastModified());
					//if ( f.isFile() && ( (f.lastModified() >= formatterStartDate) && (f.lastModified() <= formatterEndDate) ) ) {
					if ( f.isFile()) {

						Map<String, String> fileObject = new HashMap<String, String>();
						String absPath = FilenameUtils.getFullPathNoEndSeparator(f.getAbsolutePath()).replace("\\", "/");
						String relPath = absPath.replaceAll(mainPath.replace("\\", "/"), "");

						fileObject.put("file_name", FilenameUtils.getBaseName(f.getName()));
						fileObject.put("abs_path", f.getAbsolutePath());
						fileObject.put("rel_path", relPath );
						fileObject.put("destination_path",destinationPath);
						imageList.put(FilenameUtils.getBaseName(f.getName()),fileObject);
						//System.out.println("File Path1 : "+f.getAbsolutePath()+" ,FileName1 : "+f.getName()+" , lastModified1 : "+f.lastModified());
					}else{
						imageList.putAll(getImageListFromPath(f.getAbsolutePath(), startDate,endDate,destinationPath,imageList, mainPath));
					}
				}

			}
		}catch (Exception e) {
			System.out.println("Error : "+e.getMessage());
		}
		return imageList;
	}

	public Map<String, Object> createProductImageXML(List<String> productIDList, Map<String, Object> imageList, Map<String, Object> imageRules, Map<String, Object> settingsMap,Map<String, Object> imageMagickProcess) throws SAXException, IOException, ParserConfigurationException, TransformerException{
		Map<String, Object> xmlList = new HashMap<String, Object>();
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		for(String productID : productIDList){
			List<Node> contenutoXML = new ArrayList<Node>();
			Map<String, String> imagePaths = new HashMap<String, String>();
			//Map<String, String> imagePaths = new HashMap<String, String>();

			Predicate<Map.Entry<String, Object>> placeHolderFilter = new Predicate<Map.Entry<String, Object>>() {
				@Override
				public boolean apply(Entry<String, Object> val) {
					return (val.getValue().toString().trim().startsWith("{{") && val.getValue().toString().trim().endsWith("}}"));
				}
			};

			Map<String, Object> placeHolderMap = Maps.filterEntries(settingsMap,placeHolderFilter);
			Map<String, String> placeHolderNewMap = new HashMap<String, String>();


			for(String imageFileName : imageList.keySet()){

				if(imageFileName.contains(productID)){
					for (Map.Entry<String, Object> rule : imageRules.entrySet()){
						HashMap<String, Object> imageFileObject = (HashMap<String, Object>) imageList.get(imageFileName);
						String imageFilePath = (String) imageFileObject.get("abs_path");
						if(rule.getKey().equalsIgnoreCase("EMPTY") && imageFileName.endsWith(productID)){
							Node currentNode = docBuilder.parse( new InputSource(new StringReader(rule.getValue().toString()))).getDocumentElement();

							imagePaths.put(imageFilePath, getFileCheckSum(imageFilePath));

							for (Map.Entry<String, Object> placeHolder : placeHolderMap.entrySet()){
								if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_name")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), FilenameUtils.getName(imageFilePath));
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),FilenameUtils.getFullPathNoEndSeparator(imageFilePath).replace("\\", "/") );
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("product_code")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),productID );
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("destination_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), (String) imageFileObject.get("destination_path"));
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("relative_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), (String) imageFileObject.get("rel_path"));
								}
							}
							Gson gson = new Gson(); 
							String placeHolderJson = gson.toJson(placeHolderNewMap); 
							//System.out.println("placeHolder Json: "+placeHolderJson);
							JsonElement placeHolderJsonElement = new JsonParser().parse(placeHolderJson);
							Mustache docMustache = MustacheEngineBuilder.newBuilder().build().compileMustache(rule.getKey(), convertXMLToString(currentNode));
							String parsedDoc = docMustache.render(placeHolderJsonElement);
							currentNode = docBuilder.parse( new InputSource(new StringReader(parsedDoc))).getDocumentElement();
							contenutoXML.add(currentNode);
							//System.out.println("currentNode: "+convertXMLToString(currentNode));

						}else if(imageFileName.endsWith(rule.getKey())){
							Node currentNode = docBuilder.parse( new InputSource(new StringReader(rule.getValue().toString()))).getDocumentElement();

							imagePaths.put(imageFilePath, getFileCheckSum(imageFilePath));

							for (Map.Entry<String, Object> placeHolder : placeHolderMap.entrySet()){
								if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_name")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), FilenameUtils.getName(imageFilePath));
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("file_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),FilenameUtils.getFullPathNoEndSeparator(imageFilePath).replace("\\", "/") );
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("product_code")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),productID );
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("destination_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), (String) imageFileObject.get("destination_path"));
								}else if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("relative_path")){
									placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2), (String) imageFileObject.get("rel_path"));
								}	
							}
							Gson gson = new Gson(); 
							String placeHolderJson = gson.toJson(placeHolderNewMap); 
							//System.out.println("placeHolder Json: "+placeHolderJson);

							JsonElement placeHolderJsonElement = new JsonParser().parse(placeHolderJson);
							Mustache docMustache = MustacheEngineBuilder.newBuilder().build().compileMustache(rule.getKey(), convertXMLToString(currentNode));
							String parsedDoc = docMustache.render(placeHolderJsonElement);
							currentNode = docBuilder.parse( new InputSource(new StringReader(parsedDoc))).getDocumentElement();
							contenutoXML.add(currentNode);
							//System.out.println("currentNode: "+convertXMLToString(currentNode));

						}
					}
				}
			}


			if(contenutoXML.size() > 0){
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("xml", contenutoXML);
				map.put("paths", imagePaths);
				xmlList.put(productID,map);
			}else{
				String PALLET_IMAGES_NODE_NO_IMAGES_XML = settingsMap.get("pallet_images_node_no_images_xml").toString().trim();
				Node currentNode = docBuilder.parse( new InputSource(new StringReader(PALLET_IMAGES_NODE_NO_IMAGES_XML))).getDocumentElement();
				for (Map.Entry<String, Object> placeHolder : placeHolderMap.entrySet()){
					if(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2).equalsIgnoreCase("product_code")){
						placeHolderNewMap.put(placeHolder.getValue().toString().trim().substring(2, placeHolder.getValue().toString().trim().length()-2),productID );
					}	
				}
				Gson gson = new Gson(); 
				String placeHolderJson = gson.toJson(placeHolderNewMap); 
				//System.out.println("placeHolder Json: "+placeHolderJson);
				JsonElement placeHolderJsonElement = new JsonParser().parse(placeHolderJson);
				Mustache docMustache = MustacheEngineBuilder.newBuilder().build().compileMustache("noImage", convertXMLToString(currentNode));
				String parsedDoc = docMustache.render(placeHolderJsonElement);
				currentNode = docBuilder.parse( new InputSource(new StringReader(parsedDoc))).getDocumentElement();
				contenutoXML.add(currentNode);

				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("xml", contenutoXML);
				map.put("paths", imagePaths);
				xmlList.put(productID,map);
			}
			System.out.println("contenutoXML: "+contenutoXML.size());
		}
		System.out.println("xmlList: "+xmlList.size());

		return xmlList;
	}

	private void updateDB(Map<String, Object> xmlList, Map<String, Object> settingsMap, Map<String, Object> imageRules, Map<String, Object> imageList,Map<String, Object> imageMagickProcess) throws ParserConfigurationException, SAXException, IOException, TransformerException{

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setNamespaceAware(true); 
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		InputSource inStream = new InputSource();
		String END_POINT_URL = settingsMap.get("ekb_service_endpoint").toString().trim();
		System.out.println("END_POINT_URL: "+END_POINT_URL);

		String EKR_AVAILABLE_IMAGES = settingsMap.get("pallet_images_node_name").toString().trim();
		String MANDANTE = settingsMap.get("mandante").toString().trim();
		String PALLET_NAME_PREFIX = settingsMap.get("pallet_name_prefix").toString().trim();
		String PALLET_NAME_SUFFIX = settingsMap.get("pallet_name_suffix").toString().trim();

		try{
			HttpClient httpclient = new HttpClient();
			
			for (Map.Entry<String, Object> xmlNodeList : xmlList.entrySet()){
				System.out.println("productId (jobId): "+xmlNodeList.getKey() +" for jobId: "+ getJobId());
				int isImageChange  = checkImageChange(xmlNodeList,imageList,imageMagickProcess);
				System.out.println("isImageFileExists: "+isImageChange+" ("+ xmlNodeList.getKey()+")");
	
				//If isImageChange=false means new image files are uploaded to DB
				if(isImageChange > 0)
				{
					if(isImageChange == 1){
						String xmlData = "<Envelope xmlns='http://schemas.xmlsoap.org/soap/envelope/'>"
							+"<Body>"
							+"<dettaglioFormEKBSIN xmlns='http://new.webservice.namespace'>"
							+"<mandante>"+MANDANTE+"</mandante>"
							+"<codice>"+PALLET_NAME_PREFIX+xmlNodeList.getKey()+PALLET_NAME_SUFFIX+"</codice>"
							+"</dettaglioFormEKBSIN>"
							+"</Body>"
							+"</Envelope>";
	
						//System.out.println("xmlData: "+ xmlData);
	
						String dataFromDB = WebService.invoke(END_POINT_URL, xmlData,jobType, null, null,httpclient);
						//System.out.println("ProductID: "+ xmlNodeList.getKey()+", dataFromDB: "+dataFromDB);
	
						inStream.setCharacterStream(new StringReader(dataFromDB));
						Document doc = docBuilder.parse(inStream);
						doc.normalizeDocument();
						NodeList nodeList = doc.getElementsByTagName("*");
						//System.out.println("nodeList.getLength(): "+nodeList.getLength());
						String parsedDoc = convertXMLToString(doc);
						for (int s = 0; s < nodeList.getLength(); s++) {
							Element e = (Element)nodeList.item(s);
							String tipo = e.getAttribute("tipo");
							if(tipo.equalsIgnoreCase(EKR_AVAILABLE_IMAGES)){
								Node versioneNode = e.getElementsByTagNameNS("*","versione").item(0);
	
								for(Node childNode = versioneNode.getFirstChild(); childNode!=null;){
									Node nextChild = childNode.getNextSibling();
									versioneNode.removeChild(childNode);
									childNode = nextChild;
								}
	
								HashMap<String, Object> map = (HashMap<String, Object>)xmlNodeList.getValue();
								for (Node contenutoXML : (List<Node>)map.get("xml")) {
									Node firstDocImportedNode = doc.importNode(contenutoXML, true);
									versioneNode.appendChild(firstDocImportedNode);
								}
	
								parsedDoc = convertXMLToString(doc);
								break;
							}
						}
	
						//System.out.println("Before isImageChange parsedDoc: "+parsedDoc);
	
						parsedDoc = parsedDoc.replace("dettaglioFormEKBSOUT", "aggiornaFormEKBSIN");
	
						//System.out.println("After Appending Doc: "+parsedDoc);
	
						String updateToDB = WebService.invoke(END_POINT_URL, parsedDoc,jobType, null, null,httpclient);
						//System.out.println("ProductID: "+ xmlNodeList.getKey()+", updateToDB: "+updateToDB);
					}
	
					HashMap<String, Object> map = (HashMap<String, Object>)xmlNodeList.getValue();
					HashMap<String, String> filePaths = (HashMap<String, String>) map.get("paths");
					String newJson = "";
	
					ObjectMapper mapper = new ObjectMapper();
					ObjectNode objNode = mapper.createObjectNode();
					if(filePaths.size()>0){
						ObjectNode resultNode = mapper.createObjectNode();
						ArrayNode resultArrNode = mapper.createArrayNode();
						objNode.put("data", resultArrNode);
						
						for (Map.Entry<String, Object> rule : imageRules.entrySet()){
							for (Map.Entry<String, String> filePathEntry : filePaths.entrySet()){
								String path = filePathEntry.getKey();
								String md5 = filePathEntry.getValue();
								if(rule.getKey().equalsIgnoreCase("EMPTY")){
									if(FilenameUtils.getBaseName(path).equalsIgnoreCase(xmlNodeList.getKey())){
										resultNode.put("file", path);
										resultNode.put("md5", md5);
	
										resultArrNode.add(resultNode);
										resultNode = mapper.createObjectNode();		
										break;
									}
								}else if(FilenameUtils.getBaseName(path).endsWith(rule.getKey())){
									resultNode.put("file", path);
									resultNode.put("md5", md5);
	
									resultArrNode.add(resultNode);
									resultNode = mapper.createObjectNode();	
									break;
								}
							}
						}
						newJson = objNode.toString().replace("{}", "");
					}
	
					System.out.println("newJson for XML: "+newJson);
	
					SchedulerManager schedulerManager =(SchedulerManager)ServiceRegistry.getBean("schedulerManager");
					String updateImageProcessorLog = schedulerManager.updateImageProcessorLog(newJson,xmlNodeList.getKey());
	
	
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e );
		}

	}
	
	private void updateSAP(Map<String, Object> jsonList, Map<String, Object> settingsMap, Map<String, Object> imageRules, Map<String, Object> imageList,Map<String, Object> imageMagickProcess) throws ParserConfigurationException, SAXException, IOException, TransformerException{
		ObjectMapper mapper = new ObjectMapper();
		String END_POINT_URL = settingsMap.get("sap_service_endpoint").toString().trim();
		String SAP_USERNAME = settingsMap.get("sap_username").toString().trim();
		String SAP_PASSWORD = settingsMap.get("sap_password").toString().trim();
		String MANDANTE = settingsMap.get("mandante").toString().trim();
		String SAP_GET_TOKEN_ENDPOINT = settingsMap.get("sap_get_token_endpoint").toString().trim();
		String SAP_POST_LOGOUT_ENDPOINT = settingsMap.get("sap_post_logout_endpoint").toString().trim();
		System.out.println("END_POINT_URL: "+END_POINT_URL);
		//System.out.println("SAP_GET_TOKEN_ENDPOINT: "+SAP_GET_TOKEN_ENDPOINT);
		//System.out.println("SAP_POST_LOGOUT_ENDPOINT: "+SAP_POST_LOGOUT_ENDPOINT);
		
		try {
		
			HttpClient httpclient = new HttpClient();

			for (Map.Entry<String, Object> jsonNodeList : jsonList.entrySet()){
				System.out.println("productId (jobId): "+jsonNodeList.getKey() +" for jobId: "+ getJobId());
				int isImageChange  = checkImageChange(jsonNodeList,imageList,imageMagickProcess);
				System.out.println("isImageFileExists: "+isImageChange+" ("+ jsonNodeList.getKey()+")");
	
				//If isImageChange=false means new image files are uploaded to DB
				if(isImageChange > 0){
	
					if(isImageChange == 1){
						ObjectNode objNode = mapper.createObjectNode();
						objNode.put("operation", "W");
						objNode.put("mandt", MANDANTE);
						objNode.put("skuPackCode", skuPackCode);
						objNode.put("sku", jsonNodeList.getKey());
						objNode.put("classificationPropertyID", "xyz");
						ObjectNode resultNode = mapper.createObjectNode();
						ArrayNode resultArrNode = mapper.createArrayNode();
						objNode.put("list", resultNode);
						resultNode.put("results", resultArrNode);
	
						HashMap<String, Object> map = (HashMap<String, Object>)jsonNodeList.getValue();
						//System.out.println("jsonNodeList.getValue(): "+jsonNodeList.getValue());
						for (JsonNode odatavJson : (List<JsonNode>)map.get("json")) {
							resultArrNode.add(odatavJson);	
						}
	
						//System.out.println("objNode: "+objNode.toString());
	
						String parsedDoc = objNode.toString();
						//System.out.println("parsedDoc: "+parsedDoc.toString());
	
						//Deleting are all previous records from the SAP DB using the "IMAGE_PROCESSOR_LOG" where the previous records info are stored
						// sapDeletePreviousDbrecords(jsonNodeList.getKey(),settingsMap,imageRules);
	
						ObjectNode delObjNode = mapper.createObjectNode();
						delObjNode.put("operation", "D");
						delObjNode.put("mandt", MANDANTE);
						delObjNode.put("skuPackCode", skuPackCode);
						delObjNode.put("sku", jsonNodeList.getKey());
						delObjNode.put("classificationPropertyID", "xyz");
						ObjectNode delResultNode = mapper.createObjectNode();
						ArrayNode delResultArrNode = mapper.createArrayNode();
						delObjNode.put("list", delResultNode);
						delResultNode.put("results", delResultArrNode);
	
						for (JsonNode odatavAllJson : (List<JsonNode>)map.get("alljson")) {
							delResultArrNode.add(odatavAllJson);	
						}

						String delParsedDoc = delObjNode.toString();
						//System.out.println("delParsedDoc: "+delParsedDoc.toString());

						String delFromSAP = WebService.invoke(END_POINT_URL, delParsedDoc.toString(), jobType, SAP_USERNAME, SAP_PASSWORD,httpclient);
						//System.out.println("delFromSAP Delete: "+delFromSAP);
						
						String updateToSAP = WebService.invoke(END_POINT_URL, parsedDoc.toString(), jobType, SAP_USERNAME, SAP_PASSWORD,httpclient);
						//System.out.println("updateToSAP Write: "+updateToSAP);
						
					}
	
					HashMap<String, Object> mapJson = (HashMap<String, Object>)jsonNodeList.getValue();
					HashMap<String, String> filePaths = (HashMap<String, String>) mapJson.get("paths");
					String newJson = "";
	
					ObjectMapper jsonMapper = new ObjectMapper();
					ObjectNode objJsonNode = jsonMapper.createObjectNode();
	
					if(filePaths.size()>0){
						ObjectNode resultJsonNode = jsonMapper.createObjectNode();
						ArrayNode resultJsonArrNode = jsonMapper.createArrayNode();
						objJsonNode.put("data", resultJsonArrNode);
						for (Map.Entry<String, Object> rule : imageRules.entrySet()){
							for (Map.Entry<String, String> filePathEntry : filePaths.entrySet()){
								String path = filePathEntry.getKey();
								String md5 = filePathEntry.getValue();
	
								if(rule.getKey().equalsIgnoreCase("EMPTY")){
									if(FilenameUtils.getBaseName(path).equalsIgnoreCase(jsonNodeList.getKey())){
										resultJsonNode.put("file", path);
										resultJsonNode.put("md5", md5);
	
										resultJsonArrNode.add(resultJsonNode);
										resultJsonNode = jsonMapper.createObjectNode();
										break;
									}
								}else if(FilenameUtils.getBaseName(path).endsWith(rule.getKey())){
	
									resultJsonNode.put("file", path);
									resultJsonNode.put("md5", md5);
	
									resultJsonArrNode.add(resultJsonNode);
									resultJsonNode = jsonMapper.createObjectNode();
									break;
								}
							}
						}
						newJson = objJsonNode.toString().replace("{}", "");
					}
	
					//System.out.println("newJson for SAP: "+newJson);
	
					SchedulerManager schedulerManager =(SchedulerManager)ServiceRegistry.getBean("schedulerManager");
					String updateImageProcessorLog = schedulerManager.updateImageProcessorLog(newJson,jsonNodeList.getKey());
	
				}
	
			}
			
			//Using cookies get Sessionid info
			org.apache.commons.httpclient.Cookie[] cookies = httpclient.getState().getCookies();
			String cookieSessions="";
			
			for (Cookie cookie : cookies) {
				if(cookie.getName().startsWith("xsId") || cookie.getName().startsWith("sapxslb")){
					cookieSessions+=cookie.getName()+"="+cookie.getValue()+";";
				}
			}
			cookieSessions=cookieSessions.replaceAll(";$", "");
			//System.out.println("Request Token (GET) cookieSessions name: "+ cookieSessions);
			
			//GetResponse Headers - Request Token (GET) xCsrfToken
			String xCsrfToken = requestToken(SAP_GET_TOKEN_ENDPOINT,SAP_USERNAME,SAP_PASSWORD,cookieSessions,httpclient);
			//System.out.println("requestToken() (GET) xCsrfToken: "+ xCsrfToken);
			
			//logout request (POST) - If the value of the xCsrfToken is NOT unsafe then we need to execute the below postLogout method
			if(!xCsrfToken.equalsIgnoreCase("unsafe")){
				postLogout(SAP_POST_LOGOUT_ENDPOINT,SAP_USERNAME,SAP_PASSWORD,xCsrfToken,cookieSessions,httpclient);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(),e );
			System.out.println("ERROR : in getting requestToken() (GET) xCsrfToken: "+ e.getMessage());
		}
		
	}

	private String requestToken(String SAP_GET_TOKEN_ENDPOINT,String SAP_USERNAME,String SAP_PASSWORD,String cookieSessions,HttpClient httpclient){
		String xCsrfToken = "";
		BufferedReader rd = null;
		GetMethod getToken = null;
		
		try{
			getToken = new GetMethod(SAP_GET_TOKEN_ENDPOINT);
			Base64 b = new Base64();
		    String encoding = b.encodeAsString(new String(SAP_USERNAME+":"+SAP_PASSWORD).getBytes());
		    getToken.addRequestHeader("Authorization", "Basic " + encoding);
		    getToken.addRequestHeader("x-csrf-token", "Fetch");
		    getToken.addRequestHeader("Cookies", cookieSessions);
			
			int getResult = httpclient.executeMethod(getToken);
			rd = new BufferedReader(new InputStreamReader(getToken.getResponseBodyAsStream()));
			
			//Get getResponseHeaders
			Header[] getHeaders = getToken.getResponseHeaders();
			for (Header header : getHeaders){
				if(header.getName().equalsIgnoreCase("x-csrf-token")){
					xCsrfToken = header.getValue();
					//System.out.println("Request Token (GetMethod) xCsrfToken: "+ xCsrfToken);					
				}
			}
			
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			System.out.println("ERROR : in getting Request Token (GetMethod) xCsrfToken: "+ ex.getMessage());
		}finally {
			if (getToken != null) {
				try {
					getToken.releaseConnection();
					getToken.abort();
					
				} catch (Exception ex) {
				}
			}
			if (rd != null) {
				try {
					rd.close();
				} catch (Exception ex) {
				}
			}
		}
		
		return xCsrfToken;
	}

	private void postLogout(String SAP_POST_LOGOUT_ENDPOINT,String SAP_USERNAME,String SAP_PASSWORD,String xCsrfToken,String cookieSessions,HttpClient httpclient){
		PostMethod postLogout = null;
		BufferedReader rd = null;
		
		try{
			postLogout = new PostMethod(SAP_POST_LOGOUT_ENDPOINT);
			Base64 b = new Base64();
	        String encoding = b.encodeAsString(new String(SAP_USERNAME+":"+SAP_PASSWORD).getBytes());
	        postLogout.addRequestHeader("Authorization", "Basic " + encoding);
	        postLogout.addRequestHeader("x-csrf-token", xCsrfToken);
	        postLogout.addRequestHeader("Cookies", cookieSessions);
	        
			int postLogoutResult = httpclient.executeMethod(postLogout);
			rd = new BufferedReader(new InputStreamReader(postLogout.getResponseBodyAsStream()));
			
			//Get PostResponseHeaders
			Header[] getHeaders = postLogout.getResponseHeaders();
			for (Header header : getHeaders){
				if(header.getName().equalsIgnoreCase("set-cookie")){
					//System.out.println("Request Logout (POST) GetHeader name: "+ header.getName()+", value: "+header.getValue());					
				}
			}
			
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			System.out.println("ERROR : in postLogout Request Token (POST): "+ ex.getMessage());
		}finally {
			if (postLogout != null) {
				try {
					postLogout.releaseConnection();
					postLogout.abort();
					
				} catch (Exception ex) {
				}
			}
			if (rd != null) {
				try {
					rd.close();
				} catch (Exception ex) {
				}
			}
		}	
		
	}
	
	private int checkImageChange(Map.Entry<String, Object> xmlNodeList, Map<String, Object> imageList,Map<String, Object> imageMagickProcess) throws JsonProcessingException, IOException{

		SchedulerManager schedulerManager =(SchedulerManager)ServiceRegistry.getBean("schedulerManager");
		//Get all images from the folder

		String productID = xmlNodeList.getKey();
		String getImageCollections = schedulerManager.getImageCollections(productID);

		HashMap<String, Object> map = (HashMap<String, Object>)xmlNodeList.getValue();
		HashMap<String, String> filePaths = (HashMap<String, String>) map.get("paths");
		//System.out.println("filePaths: "+ filePaths);

		//System.out.println("filePaths.size(): "+ filePaths.size());

		//Calling db when the images are removed or added newly
		if(filePaths.size()>0){
			boolean found = true;
			boolean isMD5Changed = false;
			//Calling db only when the images are added newly
			for (Map.Entry<String, String> filePathEntry : filePaths.entrySet()){
				if(Utilities.isStringPopulated(getImageCollections)){
					ObjectMapper mapper = new ObjectMapper();
					JsonNode filesFoundJsonNode =  mapper.readTree(getImageCollections);
					ArrayNode filesFoundNode = (ArrayNode) filesFoundJsonNode.get("data");
					//System.out.println("filesFoundNode.size(): "+ filesFoundNode.size());
					Iterator<JsonNode> filesFoundNodeIterator = filesFoundNode.getElements();
					while (filesFoundNodeIterator.hasNext()) {
						JsonNode filesNode = filesFoundNodeIterator.next();
						if(filesNode.get("file").asText().equalsIgnoreCase(filePathEntry.getKey().toString())){
							found = (found && true);
							//System.out.println(filesNode.get("file"));
							//System.out.println(filesNode.get("md5"));
							//System.out.println(filesNode.get("file").asText());

							if(!filesNode.get("md5").asText().equalsIgnoreCase(filePathEntry.getValue().toString())){
								isMD5Changed = true;
								String imageFilePath = filePathEntry.getKey().toString();
								String imageFileName = FilenameUtils.getName(imageFilePath);
								HashMap<String, Object> imageFileObject = (HashMap<String, Object>) imageList.get(FilenameUtils.getBaseName(imageFileName));
								createImageMagickProcess(imageFilePath,imageMagickProcess,(String) imageFileObject.get("destination_path"),(String) imageFileObject.get("rel_path"));

							}

							System.out.println("found :" + found);
						}else{
							found = (found && false);
						}
					}

				}else{
					found = false;
				}
				if(!found){
					String imageFilePath = filePathEntry.getKey().toString();
					String imageFileName = FilenameUtils.getName(imageFilePath);
					HashMap<String, Object> imageFileObject = (HashMap<String, Object>) imageList.get(FilenameUtils.getBaseName(imageFileName));
					createImageMagickProcess(imageFilePath,imageMagickProcess,(String) imageFileObject.get("destination_path"),(String) imageFileObject.get("rel_path"));
					//  return 1;
				}
			}

			if(!found){
				return 1;
			}else if(isMD5Changed){
				return 2;
			}

		}else{
			return 1;
		}

		return 0;
	}

	private String convertXMLToString(Object xml) throws TransformerException{

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource((Node) xml), new StreamResult(writer));
		String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		return output;

	}

	public String getSettingsGdocId() {
		return settingsGdocId;
	}

	public void setSettingsGdocId(String settingsGdocId) {
		this.settingsGdocId = settingsGdocId;
	}

	public String getProductGdocId() {
		return productGdocId;
	}

	public void setProductGdocId(String productGdocId) {
		this.productGdocId = productGdocId;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public void setSkuPackCode(String skuPackCode) {
		this.skuPackCode = skuPackCode;
	}

	public String getSkuPackCode() {
		return skuPackCode;
	}

	private String getFileCheckSum(String filePath){
		String checksum = "";
		try {
			FileInputStream fis = new FileInputStream(new File(filePath));
			checksum = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return checksum;
	}

	public static String dateToStringFormat(Date date,String format) {
		String dateFormat = null;
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
			dateFormat = simpleDateFormat.format(date);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			return format;
		}
		return dateFormat;
	}
}
