/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.itextpdf.text.*;
import de.fhcon.idmllib.api.*;
import de.fhcon.idmllib.api.datatypes.enums.*;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.*;
import it.ekr.pdffromselector.parsingengine.selectorobjects.*;
import java.io.*;
import java.net.*;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorLayoutCellAndTrialsWrapper
{
    private Integer trialLimit = null;
    private Integer currentTrial = null;
    private Double contentHeight = null;
    private Double contentWidth = null;
    private Double filledPart = null;
    private SelectorLayoutCellAndRectangleWrapper cellAndRect;
    
    public void initializeFromReachedCellAndRectangle(SelectorLayoutCellAndRectangleWrapper cellAndRect)
    {
        this.setCellAndRect(cellAndRect);
    }
    
    public SelectorLayoutCell getLayoutCell()
    {
        SelectorLayoutCell layoutCell = getCellAndRect().getLayoutCell();
        return layoutCell;
    }
    
    public boolean reachedTrialLimit()
    {
        if (getTrialLimit() == null||getCurrentTrial()==null)
        {
            return false;
        }else
        {
            return getCurrentTrial() >= getTrialLimit();
        }
        
    }
    
    public boolean fitsCell() throws URISyntaxException, IOException, IdmlLibException, SelectorDocumentException
    {
        if (getContentHeight()== null || getContentWidth() == null)
        {
            //non avbendo le dimensioni, ci sta dentro
            return true;
        }
        if (getContentWidth()>getLayoutCell().getAvailableWidth())
        {
            return false;
        }
        double layoutCellAvailableHeight = getLayoutCell().getAvailableHeight();
        if (getContentHeight()>layoutCellAvailableHeight)
        {
            return false;
        }
        //se la cella è già parzialmente riempita, devo verificare che lo spazio rimanente basti
        if(getFilledPart() != null)
        {
            double remainingSpace = layoutCellAvailableHeight-getFilledPart();
            if (getContentHeight()>remainingSpace)
            {
                return false;
            }
        }
        
        //se sono arrivato qui vuol dire che le dimensioni vanno bene
        return true;
    }
    public void incTrial()
    {
        if (getCurrentTrial() == null)
        {
            setCurrentTrial(new Integer(0));
        }
        setCurrentTrial((Integer) (getCurrentTrial() + 1));
    }
    
    public Rectangle calculateCenteredRectangle(VerticalJustification verticalJustification)
    {
        if (verticalJustification == null)
        {
            //non essendo impostata assumo che sia in alto
            //non dovrebbe servire calcolare il rettangolo interno, in teoria l'inserimento dovrebbe considerare i bordi
            return getCellAndRect().getRectangle();
            
        }
        if (verticalJustification.equals(VerticalJustification.TopAlign))
        {
            //non dovrebbe servire calcolare il rettangolo interno, in teoria l'inserimento dovrebbe considerare i bordi
            return getCellAndRect().getRectangle();
        }else
        {
            //calcolo i rettangoli interni perchè questi non hanno bordi
            Rectangle originalRect = getCellAndRect().getRectangle();
            //devo calcolare quanto manca
            float height = originalRect.getHeight();
            double remainingPart = 0;
            if (getFilledPart() != null)
            {
                remainingPart = height-getFilledPart();
            }
            float llx =
                    originalRect.getBottom(originalRect.getBorderWidthBottom());
            float lly = originalRect.getLeft(originalRect.getBorderWidthLeft());
            float ury =
                    originalRect.getRight(originalRect.getBorderWidthRight());
            if (verticalJustification.equals(VerticalJustification.BottomAlign))
            {
                double gap = remainingPart;
                float urx =
                        originalRect.getTop(originalRect.getBorderWidthTop())-(float)gap;
                //devo impostare un rettangolo che inizi in x con la stessa coordinata di quello attuale, eliminando lo spazio rimanente (in pdf 0,0 parte dal basso)
                //tolgo i bordi già da qui perchè vi devo inserire i contenuti direttamente
                Rectangle bottomRect = new Rectangle(llx, lly, urx, ury);//new Rectangle(llx, lly, urx, ury);
                return bottomRect;
            }else if (verticalJustification.equals(verticalJustification.CenterAlign))
            {
                double gap = remainingPart/2.0;
                float urx =
                        originalRect.getTop(originalRect.getBorderWidthTop())-(float)gap;
                Rectangle middleRect = new Rectangle(llx, lly, urx, ury);//new Rectangle(llx, lly, urx, ury);
                return middleRect;
            }else
            {
                throw new IllegalArgumentException("non consentito allineamento diverso da \"In alto\",\"In basso\" e \"Al centro\"");
            }
            
        }
    }

    /**
     * @return the trialLimit
     */
    public Integer getTrialLimit()
    {
        return trialLimit;
    }

    /**
     * @param trialLimit the trialLimit to set
     */
    public void setTrialLimit(Integer trialLimit)
    {
        this.trialLimit = trialLimit;
    }

    /**
     * @return the currentTrial
     */
    public Integer getCurrentTrial()
    {
        return currentTrial;
    }

    /**
     * @param currentTrial the currentTrial to set
     */
    public void setCurrentTrial(Integer currentTrial)
    {
        this.currentTrial = currentTrial;
    }

    /**
     * @return the contentHeight
     */
    public Double getContentHeight()
    {
        return contentHeight;
    }

    /**
     * @param contentHeight the contentHeight to set
     */
    public void setContentHeight(Double contentHeight)
    {
        this.contentHeight = contentHeight;
    }

    /**
     * @return the contentWidth
     */
    public Double getContentWidth()
    {
        return contentWidth;
    }

    /**
     * @param contentWidth the contentWidth to set
     */
    public void setContentWidth(Double contentWidth)
    {
        this.contentWidth = contentWidth;
    }

    /**
     * @return the filledPart
     */
    public Double getFilledPart()
    {
        return filledPart;
    }

    /**
     * @param filledPart the filledPart to set
     */
    public void setFilledPart(Double filledPart)
    {
        this.filledPart = filledPart;
    }

    /**
     * @return the cellAndRect
     */
    public SelectorLayoutCellAndRectangleWrapper getCellAndRect()
    {
        return cellAndRect;
    }

    /**
     * @param cellAndRect the cellAndRect to set
     */
    public void setCellAndRect(
            SelectorLayoutCellAndRectangleWrapper cellAndRect)
    {
        this.cellAndRect = cellAndRect;
    }
}
