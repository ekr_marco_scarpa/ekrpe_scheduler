/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.logging.*;

/**
 *
 * @author Marco Scarpa
 */
public class PageLayoutCellsMaker extends PdfPageEventHelper
{

    private List<SelectorLayoutCellAndRectangleWrapper> layoutCellsAndrectangles =
            null;//la ricerca ricorsiva fa si che si trovi prima la cella sullo sfondo e poi le foglie
    private File file = null;

    @Override
    public void onStartPage(PdfWriter writer, Document document)
    {
        int pageNumber = document.getPageNumber();
       
        PdfContentByte directContent = writer.getDirectContent();
        for (SelectorLayoutCellAndRectangleWrapper cellAndRect
                : getLayoutCellsAndrectangles())
        {
            Rectangle rect = cellAndRect.getRectangle();

            if (rect != null)
            {
                BaseColor backgroundColor = rect.getBackgroundColor();
                BaseColor borderColor = rect.getBorderColor();
                if ((backgroundColor != null) && (borderColor != null))
                {
                    directContent.saveState();
                    PdfGState gState = new PdfGState();
                    if (backgroundColor != null)
                    {
                        gState.setFillOpacity(backgroundColor.getAlpha() / 255f);
                    }
                    if (borderColor != null)
                    {
                        gState.setStrokeOpacity(borderColor.getAlpha() / 255f);
                    }

                    directContent.setGState(gState);//setGstate va messo prima di set rectangle; se no non prende le trasparenze
                    directContent.rectangle(rect);
                    
                    if (backgroundColor != null)
                    {
                        directContent.setColorFill(backgroundColor);
                        directContent.fill();
                    }
                    if (borderColor != null)
                    {
                        directContent.setColorStroke(borderColor);
                        directContent.fillStroke();
                    }

                    directContent.restoreState();
                }
                cellAndRect.setAlreadyInsertedRectangle(true);
            }

        }
    }

    /**
     * @return the layoutCellsAndrectangles
     */
    public List<SelectorLayoutCellAndRectangleWrapper> getLayoutCellsAndrectangles()
    {
        return layoutCellsAndrectangles;
    }

    /**
     * @param layoutCellsAndrectangles the layoutCellsAndrectangles to set
     */
    public void setLayoutCellsAndrectangles(
            List<SelectorLayoutCellAndRectangleWrapper> layoutCellsAndrectangles)
    {
        this.layoutCellsAndrectangles = layoutCellsAndrectangles;
    }

    /**
     * @return the file
     */
    public File getFile()
    {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(File file)
    {
        this.file = file;
    }

}
