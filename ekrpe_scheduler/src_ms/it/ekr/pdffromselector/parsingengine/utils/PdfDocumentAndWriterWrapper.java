/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

/**
 *
 * @author Marco Scarpa
 */
public class PdfDocumentAndWriterWrapper
{

    public PdfDocumentAndWriterWrapper(Document pdf, PdfWriter writer, it.ekr.pdffromselector.parsingengine.utils.HeaderFooterMaker headerFooterMaker, ImageBackgrounder imageBackgrounder,PageLayoutCellsMaker pageLayoutCellsMaker,PageNumberCreator pageNumberCreator)
    {
        this.pdf = pdf;
        this.writer = writer;
        this.headerFooterMaker = headerFooterMaker;
        this.imageBackgrounder = imageBackgrounder;
        this.pageLayoutCellsMaker = pageLayoutCellsMaker;
        this.pageNumberCreator = pageNumberCreator;
    }
    
    private Document pdf;
    private PdfWriter writer;
    private HeaderFooterMaker headerFooterMaker;
    private ImageBackgrounder imageBackgrounder;
    private PageLayoutCellsMaker pageLayoutCellsMaker;
    private PageNumberCreator pageNumberCreator;

    /**
     * @return the pdf
     */
    public Document getPdf()
    {
        return pdf;
    }

    /**
     * @param pdf the pdf to set
     */
    public void setPdf(Document pdf)
    {
        this.pdf = pdf;
    }

    /**
     * @return the writer
     */
    public PdfWriter getWriter()
    {
        return writer;
    }

    /**
     * @param writer the writer to set
     */
    public void setWriter(PdfWriter writer)
    {
        this.writer = writer;
    }

    /**
     * @return the headerFooterMaker
     */
    public HeaderFooterMaker getHeaderFooterMaker()
    {
        return headerFooterMaker;
    }

    /**
     * @param headerFooterMaker the headerFooterMaker to set
     */
    public void setHeaderFooterMaker(
            HeaderFooterMaker headerFooterMaker)
    {
        this.headerFooterMaker = headerFooterMaker;
    }

    /**
     * @return the imageBackgrounder
     */
    public ImageBackgrounder getImageBackgrounder()
    {
        return imageBackgrounder;
    }

    /**
     * @param imageBackgrounder the imageBackgrounder to set
     */
    public void setImageBackgrounder(
            ImageBackgrounder imageBackgrounder)
    {
        this.imageBackgrounder = imageBackgrounder;
    }

    /**
     * @return the pageLayoutCellsMaker
     */
    public PageLayoutCellsMaker getPageLayoutCellsMaker()
    {
        return pageLayoutCellsMaker;
    }

    /**
     * @param pageLayoutCellsMaker the pageLayoutCellsMaker to set
     */
    public void setPageLayoutCellsMaker(
            PageLayoutCellsMaker pageLayoutCellsMaker)
    {
        this.pageLayoutCellsMaker = pageLayoutCellsMaker;
    }

    /**
     * @return the pageNumberCreator
     */
    public PageNumberCreator getPageNumberCreator()
    {
        return pageNumberCreator;
    }

    /**
     * @param pageNumberCreator the pageNumberCreator to set
     */
    public void setPageNumberCreator(PageNumberCreator pageNumberCreator)
    {
        this.pageNumberCreator = pageNumberCreator;
    }
}
