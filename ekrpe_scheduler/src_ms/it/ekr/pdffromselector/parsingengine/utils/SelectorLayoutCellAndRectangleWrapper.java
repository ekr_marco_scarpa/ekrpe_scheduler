/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;


import com.itextpdf.text.*;
import it.ekr.pdffromselector.parsingengine.selectorobjects.*;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorLayoutCellAndRectangleWrapper
{
    private SelectorLayoutCell layoutCell;
    private Rectangle rectangle;
    private boolean alreadyInsertedRectangle;

    /**
     * @return the layoutCell
     */
    public SelectorLayoutCell getLayoutCell()
    {
        return layoutCell;
    }

    /**
     * @param layoutCell the layoutCell to set
     */
    public void setLayoutCell(
            SelectorLayoutCell layoutCell)
    {
        this.layoutCell = layoutCell;
    }

    /**
     * @return the rectangle
     */
    public Rectangle getRectangle()
    {
        return rectangle;
    }

    /**
     * @param rectangle the rectangle to set
     */
    public void setRectangle(Rectangle rectangle)
    {
        this.rectangle = rectangle;
    }

    /**
     * @return the alreadyInsertedRectangle
     */
    public boolean isAlreadyInsertedRectangle()
    {
        return alreadyInsertedRectangle;
    }

    /**
     * @param alreadyInsertedRectangle the alreadyInsertedRectangle to set
     */
    public void setAlreadyInsertedRectangle(boolean alreadyInsertedRectangle)
    {
        this.alreadyInsertedRectangle = alreadyInsertedRectangle;
    }
}
