/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;

/**
 * classe creata per impostare il colore di sfondo delle celle di tabella, per supportare il canale alpha bisogna ricorrere ad eventi
 * @author Marco Scarpa
 */
public class CellBackgroundColoring implements PdfPCellEvent
{

    private TintAndColorWrapper color = null;

    /**
     * il colore applicato alla cella
     *
     * @return il colore applicato alla cella
     */
    public TintAndColorWrapper getColor()
    {
        return color;
    }

    /**
     * imposta il colore applicato alla cella
     *
     * @param color
     */
    public void setColor(TintAndColorWrapper color)
    {
        this.color = color;
    }

    /**
     * calcola il colore da applicare alla cella, fornendo un BaseColor per PDF
     * @return il BaseColor rappresentante il colore della cella
     */
    protected BaseColor calculateCellColor()
    {
        BaseColor toReturn = PdfSupportUtils.
                calculateBaseColorFromTintAndColorWrapper(color);
        return toReturn;
    }

    /**
     * imposta il colore di sfondo della cella
     * @param cell
     * @param position
     * @param canvases 
     */
    @Override
    public void cellLayout(PdfPCell cell, Rectangle position,
                           PdfContentByte[] canvases)
    {
        BaseColor calculateCellColor = calculateCellColor();
        if (calculateCellColor != null)
        {
            PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
            cb.saveState();
            PdfGState gState = new PdfGState();
            gState.setFillOpacity(calculateCellColor.getAlpha() / 255f);//il colore restituisce in base 255 mentre al fill opacity serve in base 1
            cb.rectangle(position.getLeft(), position.getBottom(), position.
                    getWidth(), position.getHeight());//imposto il rettangolo in cui opero
            cb.setGState(gState);
            cb.setColorFill(calculateCellColor);
            cb.fill();
            cb.restoreState();
        }

    }
}
