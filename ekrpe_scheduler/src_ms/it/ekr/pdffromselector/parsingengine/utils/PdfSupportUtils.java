package it.ekr.pdffromselector.parsingengine.utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.*;

import de.fhcon.idmllib.api.datatypes.enums.ColorSpace;
import de.fhcon.idmllib.api.datatypes.enums.Justification;
import de.fhcon.idmllib.api.datatypes.enums.VerticalJustification;
import de.fhcon.idmllib.api.elements.graphic.Color;
import it.ekr.pdffromselector.parsingengine.selectorobjects.*;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import sun.awt.Win32FontManager;
import sun.font.Font2D;
import sun.font.FontManager;
import sun.font.PhysicalFont;
import sun.font.SunFontManager;

/**
 *
 * @author Marco Scarpa
 */
public class PdfSupportUtils
{

    protected static Dictionary<String, Dictionary<String, BaseFont>> baseFontsCache =
            null;

    public static Font searchFont(String fontName, String fontStyle)
    {
        String toSearch = fontName;
        if (fontStyle != null)
        {
            toSearch = fontName + " " + fontStyle;
        }
        //System.out.println("cercato "+toSearch+" ----------------------------------------------------------");
        GraphicsEnvironment localGraphicsEnvironment =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font[] allFonts = localGraphicsEnvironment.getAllFonts();
        for (Font f : allFonts)
        {
            String name = f.getName();
            //System.out.println(name);
            if (name.equals(toSearch))
            {
                return f;
            }
        }
        String toSearchPS = fontName.replaceAll(" ", "");
        if (fontStyle != null)
        {
            toSearchPS = toSearchPS + "-" + fontStyle;
        }
        //System.out.println("cercato ps "+toSearchPS+" ----------------------------------------------------------");
        for (Font f : allFonts)
        {
            String psName = f.getPSName();
            //System.out.println(psName);
            if (psName.equals(toSearchPS))
            {
                //System.out.println("trovato ps " +f.getName());
                //System.out.println("trovato ps fontname " +f.getFontName());
                return f;
            }
        }
        //se non riesco a trovare con il font style, provo a cercare senza il font style (verificato nel times new roman che il regular da problemi)
        //System.out.println("cercato solo nome "+fontName+" ----------------------------------------------------------");
        for (Font f : allFonts)
        {
            String name = f.getName();
            //System.out.println(name);
            if (name.equals(fontName))
            {
                return f;
            }
        }

        return Font.decode(toSearchPS);

    }

    public static String findFontPath(String fontFamilyString, String fontStyle)
            throws
            SecurityException, IllegalAccessException,
            IllegalArgumentException, NoSuchFieldException
    {
        Font foundFont = searchFont(fontFamilyString, fontStyle);
        SunFontManager instance = Win32FontManager.getInstance();
        //System.out.println("cercato in find font path "+foundFont.getFontName());
        //System.out.println("cercato in find font path style "+foundFont.getStyle());
        Font2D findFont2D =
                instance.findFont2D(foundFont.getFontName(),
                        foundFont.getStyle(), FontManager.LOGICAL_FALLBACK);
        findFont2D = findFont2D.handle.font2D;
        Field platName = PhysicalFont.class.getDeclaredField("platName");
        platName.setAccessible(true);
        String fontPath = (String) platName.get(findFont2D);
        platName.setAccessible(false);
        return fontPath;
    }

    public static BaseFont getBaseFont(String fontFamilyString, String fontStyle)
            throws
            SecurityException, IllegalAccessException,
            IllegalArgumentException, NoSuchFieldException,
            DocumentException, IOException
    {
        if (baseFontsCache == null)
        {
            baseFontsCache = new Hashtable<>();
        }
        Dictionary<String, BaseFont> storedFontFamily = baseFontsCache.get(fontFamilyString);
        if (storedFontFamily == null)
        {
            storedFontFamily = new Hashtable<>();
            baseFontsCache.put(fontFamilyString, storedFontFamily);
        }
        BaseFont stored = (BaseFont) storedFontFamily.get(fontStyle);
        if (stored != null)
        {
            return stored;
        } else
        {
            String fontPath = findFontPath(fontFamilyString, fontStyle);
            BaseFont createFont =
                    BaseFont.createFont(fontPath,
                            BaseFont.IDENTITY_H, true);
            storedFontFamily.put(fontStyle, createFont);
            return createFont;
        }
    }

    public static BaseColor calculateBaseColorFromTintAndColorWrapper(
            TintAndColorWrapper color)
    {
        BaseColor toReturn = null;
        if (color != null)
        {
            Color baseColor = color.getBaseColor();
            Double tintValue = color.getTintValue();
            if ((tintValue != null) && (tintValue < 0))
            {
                tintValue = null;//se è negativo assumo che non voglia dare il valore della tint
            }
            ColorSpace colorSpace = baseColor.getSpace();
            if (colorSpace.equals(ColorSpace.CMYK))
            {
                List<Double> colorValue = baseColor.getColorValue();
                Double cyan = colorValue.get(0);
                Double magenta = colorValue.get(1);
                Double yellow = colorValue.get(2);
                Double key = colorValue.get(3);
                CMYKColor cmykColor =
                        new CMYKColor(cyan.floatValue() / 100f, magenta.
                                floatValue() / 100f,
                                yellow.floatValue() / 100f, key.floatValue() /
                                100f);
                if (tintValue != null && tintValue != 100)//se è al 100% salto la conversione, in modo da mantenere cmyk per migliore resa cromatica
                {
                    //visto che solo con il base color posso impostare l'alpha, riconverto il colore in rgb
                    int red = cmykColor.getRed();
                    int green = cmykColor.getGreen();
                    int blue = cmykColor.getBlue();
                    int alpha = IdmlUtils.convertFromBase100ToBase255(tintValue);
                    toReturn = new BaseColor(red, green, blue, alpha);
                } else
                {
                    toReturn = cmykColor;
                }
            } else
            {
                List<Double> colorValue = baseColor.getColorValue();
                Double red = colorValue.get(0);
                Double green = colorValue.get(1);
                Double blue = colorValue.get(2);
                toReturn = new BaseColor(red.floatValue() / 255f,
                        green.floatValue() / 255f, blue.floatValue() / 255f);//sembra che per rgb usi base 255
                if (tintValue != null && tintValue != 100)//se è al 100% salto la conversione, in modo da mantenere cmyk per migliore resa cromatica
                {
                    toReturn = new BaseColor(red.floatValue() / 255f,
                            green.floatValue() / 255f, blue.floatValue() / 255f,
                            tintValue.floatValue() / 100f);
                }
            }
        }
        return toReturn;
    }

    public static int convertParagraphJustification(
            Justification idmlJustification)
    {
        if (idmlJustification != null)
        {
            if (idmlJustification.equals(Justification.CenterAlign))
            {
                return Paragraph.ALIGN_CENTER;
            } else if (idmlJustification.equals(Justification.RightAlign))
            {
                return Paragraph.ALIGN_RIGHT;
            } else if (idmlJustification.equals(Justification.LeftAlign))
            {
                return Paragraph.ALIGN_LEFT;
            } else if (idmlJustification.equals(Justification.LeftJustified))
            {
                return Paragraph.ALIGN_JUSTIFIED;
            } else if (idmlJustification.equals(Justification.FullyJustified))
            {
                return Paragraph.ALIGN_JUSTIFIED_ALL;
            }
        }

        //assumo che di default vada allineato a sinistra
        return Paragraph.ALIGN_LEFT;
    }

    public static int convertTableCellVerticalJustification(
            VerticalJustification justification)
    {
        if (justification != null)
        {
            if (justification.equals(VerticalJustification.TopAlign))
            {
                return PdfPCell.ALIGN_TOP;
            } else if (justification.equals(VerticalJustification.BottomAlign))
            {
                return PdfPCell.ALIGN_BOTTOM;
            } else if (justification.equals(VerticalJustification.CenterAlign))
            {
                return PdfPCell.ALIGN_MIDDLE;
            } else if (justification.equals(VerticalJustification.JustifyAlign))
            {
                return PdfPCell.ALIGN_JUSTIFIED;
            }
        }
        return PdfPCell.ALIGN_TOP;
    }
    
    public static int convertFloatingBoxAlignToTableAlign(String floatingBoxAlign)
    {
        if (floatingBoxAlign.equals(SelectorFloatingBox.RIGHT_ALIGNMENT))
        {
            return PdfPCell.ALIGN_RIGHT;
        }else if (floatingBoxAlign.equals(SelectorFloatingBox.LEFT_ALIGNMENT))
        {
            return PdfPCell.ALIGN_LEFT;
        }else if (floatingBoxAlign.equals(SelectorFloatingBox.CENTER_ALIGNMENT))
        {
            return PdfPTable.ALIGN_CENTER;
        }
        return PdfPTable.ALIGN_CENTER;
    }
}
