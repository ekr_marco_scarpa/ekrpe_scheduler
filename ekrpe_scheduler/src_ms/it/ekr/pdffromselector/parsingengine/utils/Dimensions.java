/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

/**
 * classe atta a contenere le dimensioni di oggetti
 * @author Marco Scarpa
 */
public class Dimensions
{

    private Double height;
    private Double width;

    /**
     * costruttore per creare un oggetto
     * @param height l'altezza dell'oggetto o null se non ha altezza
     * @param width la larghezza dell'oggetto o null se l'oggetto non ha larghezza
     */
    public Dimensions(Double height, Double width)
    {
        this.height = height;
        this.width = width;
    }

    /**
     * costruttore di default che crea un oggetto lergo 0 e alto 0
     */
    public Dimensions()
    {
        this(0d, 0d);
    }

    /**
     * fornisce l'altezza dell'oggetto
     * @return un Double contenente l'altezza dell'oggetto, null per dire che l'altezza non è valida 
     */
    public Double getHeight()
    {
        return height;
    }

    /**
     * imposta l'altezza dell'oggetto
     * @param height un Double contenente l'altezza dell'oggetto, null per dire che l'altezza non è valida 
     */
    public void setHeight(Double height)
    {
        this.height = height;
    }

    /**
     * fornisce la larghezza dell'oggetto
     * @return un Double contenente la larghezza dell'oggetto, null per dire che la larghezza non è valida 
     */
    public Double getWidth()
    {
        return width;
    }

    /**
     * imposta la larghezza dell'oggetto
     * @param width un Double contenente la larghezza dell'oggetto, null per dire che la larghezza non è valida 
     */
    public void setWidth(Double width)
    {
        this.width = width;
    }

    /**
     * fornisce il valore dell'altezza, 0 se non è valida
     * @return il valore dell'altezza o 0 se l'altezza non è valida
     */
    public double getHeightValue()
    {
        if (isHeightValid())
        {
            return height;
        }
        return 0;
    }

    /**
     * imposta il valore di altezza che per costruzione è valido
     * @param height l'altezza valida che si sta impostando
     */
    public void setHeightValue(double height)
    {
        this.height = height;
    }

    /**
     * fornisce il valore della larghezza, 0 se non è valida
     * @return il valore della larghezza o 0 se la larghezza non è valida
     */
    public double getWidthValue()
    {
        if (getWidth() != null)
        {
            return width;
        }
        return 0;
    }

    /**
     * imposta il valore di larghezza che per costruzione è valido
     * @param width la larghezza valida che si sta impostando
     */
    public void setWidthValue(double width)
    {
        this.width = width;
    }

    /**
     * dice se l'altezza è valida
     * @return true se l'altezza è valida
     */
    public boolean isHeightValid()
    {
        return getHeight() != null;
    }

    /**
     * dice se la larghezza è valida
     * @return true se la larghezza è valida
     */
    public boolean isWidthValid()
    {
        return getWidth() != null;
    }
}
