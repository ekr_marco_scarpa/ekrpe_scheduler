package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.elements.Idml;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.XmlManagement;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorDocument extends AbstractSelectorObject implements
        ISelectorObject
{

    private File documentoXmlFile = null;
    private File documentFolder = null;

    private Idml supertemplate = null;

    private List<SelectorFascicle> fascicles = null;

    /**
     *
     * @param documentFolder
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws SelectorFileNotExistentException
     */
    public SelectorDocument(File documentFolder) throws
            ParserConfigurationException, SAXException, IOException,
            SelectorFileNotExistentException
    {
        super(null, null);//il documento non ha parent ed xml description per ora non è inizializzata
        this.documentFolder = documentFolder;
        Path selectorDocumentFolderPath = documentFolder.toPath();
        Path documentoXmlPath = selectorDocumentFolderPath.resolve(
                "documento.xml");
        documentoXmlFile = documentoXmlPath.toFile();
        if (documentoXmlFile.exists())
        {
            DocumentBuilderFactory newInstance =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder newDocumentBuilder =
                    newInstance.newDocumentBuilder();
            xmlDescription = newDocumentBuilder.parse(documentoXmlFile);

        } else
        {
            throw new SelectorFileNotExistentException("Nella cartella " +
                    documentFolder.getAbsolutePath() +
                    " non è presente il file documento.xml ");
        }
    }

    /**
     * Get the value of supertemplate
     *
     * @return the value of supertemplate
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     */
    public Idml getSupertemplate() throws
            URISyntaxException, IOException, IdmlLibException,
            SelectorFileNotExistentException, SelectorDocumentException
    {
        if (supertemplate == null)
        {
            //se non ho ancora caricato il supertemplate, lo creo
            supertemplate = createSupertemplateField();
        }

        return supertemplate;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    public de.fhcon.idmllib.api.elements.Document getSupertemplateDocument()
            throws URISyntaxException, IOException, IdmlLibException,
            SelectorDocumentException
    {
        Idml currentSupertemplate = getSupertemplate();
        return currentSupertemplate.getAbstractDocument().getAsDocument();
    }

    private Idml createSupertemplateField() throws
            SelectorFileNotExistentException, SelectorDocumentException,
            IOException, IdmlLibException, URISyntaxException
    {
        Idml creatingSupertemplateField = null;
        List<Node> idmlTagsInDocumentoXml =
                XmlManagement.retrieveNodesRecByName(xmlDescription, "idml");
        if (!idmlTagsInDocumentoXml.isEmpty())
        {
            Node idmlTag = idmlTagsInDocumentoXml.get(0);
            NamedNodeMap attributes = idmlTag.getAttributes();
            Node idmlFileAttribute = attributes.getNamedItem("file");
            String idmlFileUriString = idmlFileAttribute.getTextContent();
            URI idmlFileUri = new URI(idmlFileUriString);
            File idmlFileFile = new File(idmlFileUri);
            if (idmlFileFile.exists())
            {

                creatingSupertemplateField = new Idml(idmlFileFile.getPath());

            } else
            {
                throw new SelectorFileNotExistentException("Il percorso " +
                        idmlFileUriString + " indicato in " + documentoXmlFile.
                        getAbsolutePath() + " non è stato trovato");
            }
        } else
        {
            throw new SelectorDocumentException("manca il tag idml nel file " +
                    documentoXmlFile.getAbsolutePath());
        }
        return creatingSupertemplateField;
    }

    /**
     * Get the value of fascicles
     *
     * @return the value of fascicles
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public List<SelectorFascicle> getFascicles() throws
            SelectorDocumentException
    {
        if (fascicles == null)
        {
            fascicles = createFasciclesField();
        }
        if (fascicles == null)
        {
            //se anche dopo il calcolo resta null
            return null;
        }
        return Collections.unmodifiableList(fascicles);
    }

    private ListIterator<SelectorFascicle> fasciclesSearchIterator = null;

    /**
     *
     * @param fascicleName
     * @return
     * @throws SelectorDocumentException
     */
    public SelectorFascicle searchFascicleByName(String fascicleName) throws
            SelectorDocumentException
    {
        //di norma la ricerca per nome va per fascicoli successivi, pertanto uso iterator per rendere più rapida questa fattispecie
        if (fasciclesSearchIterator == null)
        {
            List<SelectorFascicle> currentFascicles = getFascicles();
            if (currentFascicles != null)
            {
                fasciclesSearchIterator = currentFascicles.listIterator();

            }
        }
        if (fasciclesSearchIterator != null)
        {
            if (fasciclesSearchIterator.hasNext())//vedo se il fascicolo cercato corrisponde a quello successivo a quello appena elaborato
            {
                SelectorFascicle next = fasciclesSearchIterator.next();
                if (next.getName().equals(fascicleName))
                {
                    return next;
                }
            }
            //se sono qui vuol dire che il successivo non va bene, resetto l'iterator
            List<SelectorFascicle> currentFascicles = getFascicles();
            fasciclesSearchIterator = currentFascicles.listIterator();
            while (fasciclesSearchIterator.hasNext())
            {
                SelectorFascicle next = fasciclesSearchIterator.next();
                if (next.getName().equals(fascicleName))
                {
                    return next;
                }

            }
        }

        return null;

    }

    private List<SelectorFascicle> createFasciclesField() throws
            SelectorDocumentException
    {
        List<SelectorFascicle> creatingFascicles = null;
        List<Node> documentoTagList =
                XmlManagement.retrieveNodesRecByName(xmlDescription,
                        "documento");
        if (!documentoTagList.isEmpty())
        {
            Node documentoTag = documentoTagList.get(0);
            NamedNodeMap documentoTagAttributes =
                    documentoTag.getAttributes();
            Node riferimentoMastroAttribute =
                    documentoTagAttributes.getNamedItem("riferimento_mastro");
            String riferimentoMastroForDocument = riferimentoMastroAttribute.
                    getTextContent();
            List<Node> fasciclesXmlList =
                    XmlManagement.retrieveNodesRecByName(documentoTag,
                            "fascicolo"); //visto che fa ricerca ricorsiva, salta il contenitore "fascicoli"
            creatingFascicles = new ArrayList<>();
            Path selectorDocumentFolderPath = documentFolder.toPath();
            Path pagesFolderPath;
            pagesFolderPath = selectorDocumentFolderPath.resolve("pages");
            for (Node fascicleXml : fasciclesXmlList)
            {
                SelectorFascicle creatingFascicle = new SelectorFascicle(
                        fascicleXml, riferimentoMastroForDocument,
                        pagesFolderPath, this);
                creatingFascicles.add(creatingFascicle);
            }

        } else
        {
            throw new SelectorDocumentException(
                    "manca il tag documento nel file " + documentoXmlFile.
                    getAbsolutePath());
        }

        return creatingFascicles;
    }

    @Override
    public SelectorDocument getParentSelectorDocument()
    {
        return this;
    }

    @Override
    public List<ISelectorObject> getContainedObjects() throws
            SelectorDocumentException
    {
        List<ISelectorObject> toReturn;
        List<SelectorFascicle> currentFascicles = getFascicles();
        toReturn = new ArrayList<>();
        for (SelectorFascicle fascicle:currentFascicles)
        {
            toReturn.add(fascicle);
        }
        return toReturn;
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorFascicle> currentFascicles = getFascicles();
        List<ISelectorObject> toReturn = new ArrayList<>();
        for(SelectorFascicle fascicle:currentFascicles)
        {
            toReturn.add(fascicle);
        }
        return toReturn;
    }

    /**
     *
     * @return
     */
    @Override
    public SelectorPage getParentPage()
    {
        return null;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        if (supertemplate != null)
        {
            supertemplate.close();
        }
        supertemplate = null;
        fasciclesSearchIterator = null;
        fascicles = null;
    }

    

}
