/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.elements.Document;
import de.fhcon.idmllib.api.elements.styles.ObjectStyle;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.utils.IdmlUtils;
import it.ekr.pdffromselector.parsingengine.utils.TintAndColorWrapper;
import it.ekr.pdffromselector.parsingengine.utils.XmlManagement;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorSpaceImage extends AbstractSelectorImage
{

    public static final String WIDTH_ATTRIBUTE_LABEL = "larghezza";
    public static final String HEIGHT_ATTRIBUTE_LABEL = "altezza";
    public static final String PERCENT_HEIGHT_FLAG_ATTRIBUTE_LABEL =
            "altezza_percentuale";
    public static final String PERCENT_WIDTH_FLAG_ATTRIBUTE_LABEL =
            "larghezza_percentuale";
    private Boolean enableStroke;
    private Double height = null;

    private Double width = null;

    private boolean percentHeight = false;

    private boolean percentWidth = false;

    protected List<ObjectStyle> objectStylesChain = null;

    private Double strokeWeight = null;

    private String strokeColor = null;

    private Double strokeTint = null;

    private TintAndColorWrapper strokeTintAndColor =
            null;

    private ObjectStyle objectStyle;

    /**
     *
     * @param parent
     * @param xmlDescription
     * @param precedingTypes
     */
    public SelectorSpaceImage(ISelectorObject parent, Node xmlDescription,
                              List<String> precedingTypes)
    {
        super(parent, xmlDescription, precedingTypes);
    }

    @Override
    public URI getHref() throws URISyntaxException
    {

        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node namedItem = attributes.getNamedItem("href");
        String textContent = namedItem.getTextContent();
        return new URI(textContent);
    }//*/

    /**
     * mi da il valore effettivo dell'altezza dell'immagine
     *
     * @return
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    @Override
    public double getAvailableHeight() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        //temporaneo, deve calcolare ancora iul riempimento proporzionale
        double candidateAvailableHeight = calculateAvailableHeightCandidate();
        return candidateAvailableHeight;
    }

    /**
     *
     * mi da il valore effettivo della larghezza dell'immagine
     *
     * @return
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    @Override
    public double getAvailableWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        //temporaneo, deve calcolare ancora iul riempimento proporzionale
        double candidateAvailableWidth = calculateAvailableWidthCandidate();
        return candidateAvailableWidth;
    }

    private double calculateAvailableWidthCandidate() throws
            URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        double candidateSize = getWidth();
        return calculateCandidateSize(candidateSize);
    }

    protected double calculateCandidateSize(double candidateSize) throws
            IdmlLibException, IOException, URISyntaxException,
            SelectorDocumentException
    {
        Double currentStrokeWeight = getStrokeWeight();
        if (currentStrokeWeight != null)
        {
            candidateSize -= 2 * currentStrokeWeight;
        }
        return candidateSize;
    }

    private double calculateAvailableHeightCandidate() throws
            IdmlLibException, URISyntaxException, IOException,
            SelectorDocumentException
    {
        double candidateSize = getHeight();
        return calculateCandidateSize(candidateSize);
    }

    /**
     * mi da l'altezza completa del box contenitore
     *
     * @return
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        if (height == null)
        {
            height = calculateHeight();
        }
        if (height != null)
        {
            return height;
        } else
        {
            return 0;
        }
    }

    private Double calculateHeight() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        boolean calculatingHeight = true;
        return calculateSizeValue(calculatingHeight);
    }

    private Double calculateWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        boolean calculatingHeight = false;
        return calculateSizeValue(calculatingHeight);
    }

    protected Double calculateSizeValue(boolean calculatingHeight) throws
            URISyntaxException, IdmlLibException, IOException,
            NumberFormatException,
            SelectorDocumentException, DOMException
    {
        Double sizeValue = null;
        boolean percentSize = isPercentHeight();
        String sizeAttributeLabel = HEIGHT_ATTRIBUTE_LABEL;
        if (!calculatingHeight)
        {
            percentSize = isPercentWidth();
            sizeAttributeLabel = WIDTH_ATTRIBUTE_LABEL;
        }
        double availableParentSize = 0;
        availableParentSize =
                calculateAvailabeParentSize(calculatingHeight);
        sizeValue =
                calculateCandidateSizeValue(sizeAttributeLabel);
        if (percentSize)
        {
            double factor = sizeValue / 100.0;
            sizeValue = availableParentSize * factor;
        }
        return sizeValue;
    }

    protected double calculateAvailabeParentSize(boolean calculatingHeight)
            throws
            SelectorDocumentException, IdmlLibException,
            URISyntaxException, IOException
    {
        double availableParentSize = 0;
        ISelectorContainer ancestorContainer =
                getAncestorContainer();
        if (ancestorContainer != null)
        {
            if (calculatingHeight)
            {
                availableParentSize = ancestorContainer.getAvailableHeight();
            } else
            {
                availableParentSize = ancestorContainer.getAvailableWidth();
            }

        }
        return availableParentSize;
    }

    public Double calculateCandidateSizeValue(
            String sizeAttributeLabel) throws
            NumberFormatException, DOMException
    {
        Double sizeValue = null;
        NamedNodeMap attributes = xmlDescription.getAttributes();
        if (attributes != null)
        {
            Node namedItem = attributes.getNamedItem(sizeAttributeLabel);
            if (namedItem != null)
            {
                String textContent = namedItem.getTextContent();
                sizeValue = new Double(textContent);
            }
        } else
        {
            sizeValue = null;
        }
        return sizeValue;
    }

    /**
     * mi da la larghezza completa del box contenitore
     *
     * @return
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        if (width == null)
        {
            width = calculateWidth();
        }
        if (width != null)
        {
            return width;
        } else
        {
            return 0;
        }
    }

    /**
     * Get the value of percentHeight
     *
     * @return the value of percentHeight
     */
    public boolean isPercentHeight()
    {
        String percentHeightAttributeLabel = PERCENT_HEIGHT_FLAG_ATTRIBUTE_LABEL;
        percentHeight = calculatePercentFlag(percentHeightAttributeLabel);

        return percentHeight;
    }

    public boolean calculatePercentFlag(
            String percentSizeAttributeLabel) throws
            DOMException
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        if (attributes != null)
        {

            Node flagAttribute = attributes.getNamedItem(
                    percentSizeAttributeLabel);
            if (flagAttribute != null)
            {
                String textContent = flagAttribute.getTextContent();
                boolean percentFlagValue = XmlManagement.convertStringToBoolean(
                        textContent);
                return percentFlagValue;
            }
        }
        return false;
    }

    /**
     * Get the value of percentWidth
     *
     * @return the value of percentWidth
     */
    public boolean isPercentWidth()
    {
        String percentWidthAttributeLabel = PERCENT_WIDTH_FLAG_ATTRIBUTE_LABEL;
        percentWidth = calculatePercentFlag(percentWidthAttributeLabel);
        return percentWidth;
    }

    /**
     * trova lo stile di oggetto di questo oggetto e quelli in cui si basa, in
     * modo da poter cercare le proprietà grafiche
     *
     * @return the value of paragraphStylesChain
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    protected List<ObjectStyle> getObjectStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException, URISyntaxException
    {
        if (objectStylesChain == null)
        {
            objectStylesChain = calculateObjectStylesChain();
        }
        return Collections.unmodifiableList(objectStylesChain);
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    protected List<ObjectStyle> calculateObjectStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        return IdmlUtils.retrieveObjectStylesCurrentAndBasedOnChain(
                getParentSelectorDocument().getSupertemplateDocument(),
                getObjectStyle());
    }

    /**
     * Get the value of strokeWeight
     *
     * @return the value of strokeWeight
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public Double getStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (strokeWeight == null)
        {
            strokeWeight = calculateStrokeWeight();
        }
        return strokeWeight;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    private Double calculateStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (isEnableStroke() == null || !isEnableStroke())
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Double candidateStrokeWeight = style.getStrokeWeight();
            if (candidateStrokeWeight != null)
            {
                return candidateStrokeWeight;
            }
        }
        return null;
    }

    public Boolean isEnableStroke() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (enableStroke == null)
        {
            enableStroke = calculateEnableStroke();
        }
        return enableStroke;
    }

    private Boolean calculateEnableStroke() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Boolean candidateEnable = style.isEnableStroke();
            if (candidateEnable != null)
            {
                return candidateEnable;
            }
        }
        return null;
    }

    public String getStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (strokeColor == null)
        {
            strokeColor = calculateStrokeColor();
        }
        return strokeColor;
    }

    private String calculateStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (!isEnableStroke())
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            String candidateColor = style.getStrokeColor();
            if (candidateColor != null)
            {
                return candidateColor;
            }
        }
        return null;
    }

    public Double getStrokeTint() throws
            SelectorDocumentException, URISyntaxException, IOException,
            IdmlLibException
    {
        if (strokeTint == null)
        {
            strokeTint = calculateStrokeTint();
        }
        return strokeTint;
    }

    private Double calculateStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (!isEnableStroke())
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Double candidateTint = style.getStrokeTint();
            if (candidateTint != null)
            {
                return candidateTint;
            }
        }
        return null;
    }

    /**
     * Get the value of strokeTintAndColor
     *
     * @return the value of strokeTintAndColor
     * @throws java.io.IOException
     * @throws java.net.URISyntaxException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     */
    public TintAndColorWrapper getStrokeTintAndColor() throws
            IOException, URISyntaxException,
            SelectorDocumentException, IdmlLibException
    {
        if (strokeTintAndColor == null)
        {
            strokeTintAndColor = calculateStrokeTintAndColor();
        }
        return strokeTintAndColor;
    }

    public TintAndColorWrapper calculateStrokeTintAndColor() throws
            URISyntaxException,
            SelectorDocumentException, IdmlLibException, IOException
    {
        if (!isEnableStroke())
        {
            return null;
        }
        String fillColorOrTintSelf = getStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        fillColorOrTintSelf);
        Double fillTintValue = getStrokeTint();
        if (fillTintValue != null)
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(fillTintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;
    }

    /**
     * Get the value of objectStyle
     *
     * @return the value of objectStyle
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public ObjectStyle getObjectStyle() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        if (objectStyle == null)
        {
            objectStyle = calculateObjectStyle();
        }
        return objectStyle;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    protected ObjectStyle calculateObjectStyle() throws
            URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        SelectorDocument currentParentSelectorDocument =
                this.getParentSelectorDocument();
        Document supertemplateDocument =
                currentParentSelectorDocument.getSupertemplateDocument();

        String objectStyleName =
                IdmlUtils.convertTypePathToIdmlName(getTypePath(null));
        ObjectStyle found = IdmlUtils.retrieveObjectStyleByName(
                supertemplateDocument, objectStyleName);
        return found;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        enableStroke = null;
        height = null;
        objectStyle = null;
        objectStylesChain = null;
        percentHeight = false;
        percentWidth = false;
        strokeColor = null;
        strokeTint = null;
        strokeTintAndColor = null;
        strokeWeight = null;
        width = null;
    }
    
    
}
