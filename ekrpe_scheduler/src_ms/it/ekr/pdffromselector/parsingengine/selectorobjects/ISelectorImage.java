/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author Marco Scarpa
 */
public interface ISelectorImage extends ISelectorContainer
{

    /**
     *
     * @return @throws URISyntaxException
     */
    URI getHref() throws URISyntaxException;
}
