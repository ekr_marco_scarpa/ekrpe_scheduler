/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import org.w3c.dom.Node;

/**
 *
 * @author Marco Scarpa
 */
public abstract class AbstractSelectorContainer extends AbstractSelectorObject
        implements ISelectorContainer
{

    public AbstractSelectorContainer(ISelectorObject parent, Node xmlDescription)
    {
        super(parent, xmlDescription);
    }

    @Override
    public ISelectorContainer getAncestorContainer()
    {
        ISelectorObject currentAncestor = getParent();
        while (currentAncestor != null)
        {
            if (currentAncestor instanceof ISelectorContainer)
            {
                return (ISelectorContainer) currentAncestor;
            }
            currentAncestor = currentAncestor.getParent();
        }

        return null;
    }

}
