package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.elements.styles.*;
import it.ekr.measureconverter.*;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.XmlManagement;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorFloatingBox extends AbstractSelectorContainer implements
        ISelectorObject, ISelectorContainer
{

    public static final String FLOATING_BOX_TAG = "box_in_flusso";

    public static final String PERCENT_WIDTH = "larghezza_percentuale";
    public static final String WIDTH = "larghezza";
    public static final String PERCENT_HEIGHT = "altezza_percentuale";
    public static final String HEIGHT = "altezza";

    public static final String SPACE_BEFORE_ATTRIBUTE = "spazio_prima";
    public static final String SPACE_AFTER_ATTRIBUTE = "spazio_dopo";
    private SelectorLayoutCell mainCellFloatingBox = null;
    private Double spaceBefore = null;

    private Double spaceAfter = null;
    private String horizontalAlign = null;

    /**
     *
     * @param parent
     * @param xmlDescription
     */
    public SelectorFloatingBox(ISelectorObject parent, Node xmlDescription)
    {
        super(parent, xmlDescription);
    }

    /**
     * Get the value of mainCellFloatingBox
     *
     * @return the value of mainCellFloatingBox
     */
    public SelectorLayoutCell getMainCellFloatingBox()
    {
        if (mainCellFloatingBox == null)
        {
            mainCellFloatingBox = calculateMainCellFloatingBox();
        }
        return mainCellFloatingBox;
    }

    private SelectorLayoutCell calculateMainCellFloatingBox()
    {
        List<Node> mainCellTagList =
                XmlManagement.retrieveNodesRecByNameStoppingAtFirstLevelFound(
                        xmlDescription, SelectorLayoutCell.LAYOUT_CELL_TAG);
        Node mainCellTag = mainCellTagList.get(0);
        SelectorLayoutCell creatingCell = new SelectorLayoutCell(mainCellTag,
                this);
        return creatingCell;
    }

    @Override
    public double getAvailableHeight() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        return getHeight();
    }

    @Override
    public double getAvailableWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        return getWidth();//sarà la main cell a dare lo spazio disponibile alle celle figlie
    }

    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        return calculateHeightOrWidth(true);
    }

    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        return calculateHeightOrWidth(false);
    }

    protected double calculateHeightOrWidth(boolean calculateHeight) throws
            URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        String sizeAttributeTag = HEIGHT;
        String percentSizeFlagTag = PERCENT_HEIGHT;
        if (!calculateHeight)
        {
            sizeAttributeTag = WIDTH;
            percentSizeFlagTag = PERCENT_WIDTH;
        }
        Node sizeAttribute = attributes.getNamedItem(sizeAttributeTag);
        Node percentSizeFlag = attributes.getNamedItem(percentSizeFlagTag);
        String sizeAttributeText = sizeAttribute.getTextContent();
        Double sizeValue = new Double(sizeAttributeText);
        if (percentSizeFlag != null)
        {
            String percentSizeFlagText = percentSizeFlag.getTextContent();
            boolean percentSizeFlagValue =
                    XmlManagement.convertStringToBoolean(percentSizeFlagText);
            if (!percentSizeFlagValue)
            {
                return MeasureConverter.mmToPixel(sizeValue);
            } else
            {
                ISelectorContainer ancestorContainer = getAncestorContainer();
                double availableSize = ancestorContainer.getAvailableHeight();
                if (!calculateHeight)
                {
                    availableSize = ancestorContainer.getAvailableWidth();
                }
                double factor = sizeValue / 100.0;
                double calculatedSize = availableSize * factor;
                return calculatedSize;
            }
        }
        return sizeValue;
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> wrapper = new ArrayList<>();
        wrapper.add(getMainCellFloatingBox());
        return wrapper;
    }

    @Override
    public String getType()
    {
        return getMainCellFloatingBox().getType();
    }

    @Override
    public List<SelectorFloatingBox> getContainedSelectorFloatingBoxes() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorFloatingBox> wrapperAndDescendants = new ArrayList<>();
        wrapperAndDescendants.add(this);
        List<SelectorFloatingBox> descendantsFloatingBoxes =
                super.getContainedSelectorFloatingBoxes();
        wrapperAndDescendants.addAll(descendantsFloatingBoxes);
        return wrapperAndDescendants;
    }

    /**
     * Get the value of spaceBefore
     *
     * @return the value of spaceBefore
     */
    public Double getSpaceBefore()
    {
        if (spaceBefore == null)
        {
            spaceBefore = calculateSpaceBefore();
        }
        return spaceBefore;
    }

    public Double calculateSpaceBefore()
    {
        return calculateSpaceAfterOrBefore(SPACE_BEFORE_ATTRIBUTE);
    }

    /**
     * Get the value of spaceAfter
     *
     * @return the value of spaceAfter
     */
    public Double getSpaceAfter()
    {
        if (spaceAfter == null)
        {
            spaceAfter = calculateSpaceAfter();
        }
        return spaceAfter;
    }

    public Double calculateSpaceAfter()
    {
        return calculateSpaceAfterOrBefore(SPACE_AFTER_ATTRIBUTE);
    }

    public Double calculateSpaceAfterOrBefore(
            String spacaAttributeName) throws
            DOMException, NumberFormatException
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node spaceTag = attributes.getNamedItem(spacaAttributeName);
        if (spaceTag != null)
        {
            String textContent = spaceTag.getTextContent();
            Double spaceTagValue = new Double(textContent);
            if (spaceTagValue != null)
            {
                spaceTagValue = MeasureConverter.mmToPixel(spaceTagValue);
            }
            return spaceTagValue;
        }
        return 0.0;//di default non ho spazio prima o dopo
    }

   public String getHorizontalAlign()
   {
       if (horizontalAlign == null)
       {
            horizontalAlign = calculateHorizontalAlign();
       }
      
       return horizontalAlign;
   }

    private String calculateHorizontalAlign()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node spaceTag = attributes.getNamedItem(HORIZONTAL_ALIGNMENT_ATTRIBUTE);
        if (spaceTag != null)
        {
            String textContent = spaceTag.getTextContent();
            
            return textContent;
        }
        return null;
    }
    public static final String HORIZONTAL_ALIGNMENT_ATTRIBUTE =
            "allineamento_orizzontale";
    public static String RIGHT_ALIGNMENT = "rightAlignment";
    public static String LEFT_ALIGNMENT = "leftAlignment";
    public static String CENTER_ALIGNMENT = "centerAlignment";

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        horizontalAlign = null;
        mainCellFloatingBox = null;
        spaceAfter = null;
        spaceBefore = null;
    }
    
    

}
