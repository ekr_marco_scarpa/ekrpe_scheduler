package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.elements.Document;
import de.fhcon.idmllib.api.elements.styles.TableStyle;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorTable extends AbstractSelectorContainer implements
        ISelectorObject, ISelectorContainer
{

    private TableStyle tableStyle = null;

    protected double leftBorderStrokeWeight = 0;

    protected double rightBorderStrokeWeight = 0;

    protected double topBorderStrokeWeight = 0;

    protected double bottomBorderStrokeWeight = 0;

    protected double height = 0;

    protected double width = 0;

    protected List<TableStyle> tableStylesChain = null;

    private List<SelectorTableRow> headerRows = null;

    private List<SelectorTableRow> bodyRows = null;

    private List<SelectorTableRow> tableRows = null;

    private int maxColumns = 0;
    private String leftBorderStrokeColor = null;
    private String rightBorderStrokeColor = null;
    private String topBorderStrokeColor = null;
    private String bottomBorderStrokeColor = null;
    private Double bottomBorderStrokeTint = null;
    private Double rightBorderStrokeTint = null;
    private Double leftBorderStrokeTint = null;
    private Double topBorderStrokeTint = null;
    private TintAndColorWrapper rightBorderStrokeTintAndColorWrapper = null;
    private TintAndColorWrapper leftBorderStrokeTintAndColorWrapper = null;
    private TintAndColorWrapper topBorderStrokeTintAndColorWrapper = null;
    private TintAndColorWrapper bottomBorderStrokeTintAndColorWrapper = null;
    private Double spaceBefore = null;
    private Double spaceAfter = null;
    private List<Double> columnWidths = null;

    public SelectorTable(ISelectorObject parent, Node xmlDescription)
    {
        super(parent, xmlDescription);
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<Node> rigaTags = XmlManagement.retrieveElementChildren(xmlDescription);
        for (int i = 0; i < rigaTags.size(); i++)
        {
            Node rigaTag = rigaTags.get(i);
            SelectorTableRow creatingRow = new SelectorTableRow(this,
                    rigaTag);
            toReturn.add(creatingRow);
        }
        return toReturn;
    }

    @Override
    public double getAvailableHeight() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        return getHeight() - getTopBorderStrokeWeight() -
                getBottomBorderStrokeWeight();
    }

    @Override
    public double getAvailableWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        return getWidth() - getLeftBorderStrokeWeight() -
                getRightBorderStrokeWeight();
    }

    public double getLeftBorderStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (leftBorderStrokeWeight == 0)
        {
            leftBorderStrokeWeight = calculateLeftBorderStrokeWeight();
        }
        return leftBorderStrokeWeight;
    }

    protected double calculateLeftBorderStrokeWeight() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateLeftBorderStrokeWeight = style.
                    getLeftBorderStrokeWeight();
            if (candidateLeftBorderStrokeWeight != null)
            {
                return candidateLeftBorderStrokeWeight;
            }
        }
        return 0;
    }

    public double getRightBorderStrokeWeight() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (rightBorderStrokeWeight == 0)
        {
            rightBorderStrokeWeight = calculateRightBorderStrokeWeight();
        }
        return rightBorderStrokeWeight;
    }

    protected double calculateRightBorderStrokeWeight() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateRightBorderStrokeWeight = style.
                    getRightBorderStrokeWeight();
            if (candidateRightBorderStrokeWeight != null)
            {
                return candidateRightBorderStrokeWeight;
            }
        }
        return 0;
    }

    public double getTopBorderStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (topBorderStrokeWeight == 0)
        {
            topBorderStrokeWeight = calculateTopBorderStrokeWeight();
        }
        return topBorderStrokeWeight;
    }

    protected double calculateTopBorderStrokeWeight() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateTopBorderStrokeWeight = style.
                    getTopBorderStrokeWeight();
            if (candidateTopBorderStrokeWeight != null)
            {
                return candidateTopBorderStrokeWeight;
            }
        }
        return 0;
    }

    public double getBottomBorderStrokeWeight() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (bottomBorderStrokeWeight == 0)
        {
            bottomBorderStrokeWeight = calculateBottomBorderStrokeWeight();
        }
        return bottomBorderStrokeWeight;
    }

    protected double calculateBottomBorderStrokeWeight() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateBottomBorderStrokeWeight = style.
                    getBottomBorderStrokeWeight();
            if (candidateBottomBorderStrokeWeight != null)
            {
                return candidateBottomBorderStrokeWeight;
            }
        }
        return 0;
    }

    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        //l'altezza non è importante, non verrà usata
        if (height == 0)
        {
            height = calculateHeight();
        }
        return height;
    }

    protected double calculateHeight()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        if (attributes != null)
        {
            Node altezzaRenderizzataAttribute = attributes.getNamedItem(
                    "altezza_renderizzata");
            if (altezzaRenderizzataAttribute != null)
            {
                String altezzaRenderizzataStringValue =
                        altezzaRenderizzataAttribute.getTextContent();

                if ((altezzaRenderizzataStringValue != null) &&
                        (!altezzaRenderizzataStringValue.isEmpty()))
                {
                    try
                    {
                        Double altezzaRenderizzataValue = new Double(
                                altezzaRenderizzataStringValue);
                        return altezzaRenderizzataValue;
                    } catch (NumberFormatException nfex)
                    {
                        //se non riesco a convertire, restituisco 0
                        return 0;
                    }

                }

            }

        }
        return 0;
        //altezza_renderizzata="13.758"
    }

    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        if (width == 0)
        {
            width = calculateWidth();
        }
        return width;
    }

    protected double calculateWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        ISelectorContainer ancestorContainer = getAncestorContainer();
        double availableWidthOfAncestor = ancestorContainer.getAvailableWidth();
        return availableWidthOfAncestor;
    }

    @Override
    protected List<SelectorTable> calculateContainedSelectorTables() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorTable> wrapperAndDesc = new ArrayList<>();
        wrapperAndDesc.add(this);
        List<ISelectorObject> currentContainedObjects = getContainedObjects();
        for (ISelectorObject contained : currentContainedObjects)
        {
            List<SelectorTable> innerContainedTables = contained.
                    getContainedSelectorTables();
            wrapperAndDesc.addAll(innerContainedTables);
        }
        return wrapperAndDesc;
    }

    /**
     * Get the value of tableStyle
     *
     * @return the value of tableStyle
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public TableStyle getTableStyle() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        if (tableStyle == null)
        {
            tableStyle = calculateAppliedTableStyle();
        }
        return tableStyle;
    }

    private TableStyle calculateAppliedTableStyle() throws
            URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        SelectorDocument currentParentSelectorDocument =
                this.getParentSelectorDocument();
        Document supertemplateDocument =
                currentParentSelectorDocument.getSupertemplateDocument();
        List<String> currentTypePath = getTypePath(null);
        String tableStyleName =
                IdmlUtils.convertTypePathToIdmlName(currentTypePath);
        TableStyle found = IdmlUtils.retrieveTableStyleByName(
                supertemplateDocument, tableStyleName);
        return found;
    }

    /**
     * trova lo stile di tabella di questo oggetto e quelli in cui si basa, in
     * modo da poter cercare le proprietà grafiche
     *
     * @return the value of paragraphStylesChain
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    protected List<TableStyle> getTableStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException, URISyntaxException
    {
        if (tableStylesChain == null)
        {
            tableStylesChain = calculateTableStylesChain();
        }
        return Collections.unmodifiableList(tableStylesChain);
    }

    protected List<TableStyle> calculateTableStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        return IdmlUtils.retrieveTableStylesCurrentAndBasedOnChain(
                getParentSelectorDocument().getSupertemplateDocument(),
                getTableStyle());
    }

    /**
     * Get the value of headerRows
     *
     * @return the value of headerRows
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public List<SelectorTableRow> getHeaderRows() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (headerRows == null)
        {
            headerRows = calculateHeaderRows();
        }
        return Collections.unmodifiableList(headerRows);
    }

    protected List<SelectorTableRow> calculateHeaderRows() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorTableRow> toReturn = new ArrayList<>();
        List<SelectorTableRow> examiningRows = getTableRows();
        for (SelectorTableRow row : examiningRows)
        {
            if (row.isHeader())
            {
                toReturn.add(row);
            }
        }
        return toReturn;
    }

    /**
     * Get the value of bodyRows
     *
     * @return the value of bodyRows
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public List<SelectorTableRow> getBodyRows() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (bodyRows == null)
        {
            bodyRows = calculateBodyRows();
        }
        return Collections.unmodifiableList(bodyRows);
    }

    protected List<SelectorTableRow> calculateBodyRows() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorTableRow> toReturn = new ArrayList<>();
        List<SelectorTableRow> examiningRows = getTableRows();
        for (SelectorTableRow row : examiningRows)
        {
            if (!row.isHeader())
            {
                toReturn.add(row);
            }
        }
        return toReturn;
    }

    /**
     * Get the value of tableRows
     *
     * @return the value of tableRows
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public List<SelectorTableRow> getTableRows() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (tableRows == null)
        {
            tableRows = calculateTableRows();
        }
        return Collections.unmodifiableList(tableRows);
    }

    private List<SelectorTableRow> calculateTableRows() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorTableRow> toReturn = new ArrayList<>();
        List<ISelectorObject> children = getContainedObjects();
       
        /*children.stream().
                map((child) ->
                        (SelectorTableRow) child).
                forEach((row) ->
                        {
                            toReturn.add(row);
                });*/
            //kors non supporta java 8, va rifatto in java 7
          for(ISelectorObject child:children)
          {
              SelectorTableRow row = (SelectorTableRow) child;
              toReturn.add(row);
          }
        return toReturn;
    }

    /**
     * Get the value of maxColumns
     *
     * @return the value of maxColumns
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public int getMaxColumns() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (maxColumns == 0)
        {
            maxColumns = calculateMaxColumns();
        }
        return maxColumns;
    }

    protected int calculateMaxColumns() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        int acc = 0;
        List<SelectorTableRow> containedRows = getTableRows();
        for (SelectorTableRow row : containedRows)
        {
            List<ISelectorObject> rowCells = row.getContainedObjects();
            int rowCellsCount = rowCells.size();
            if (rowCellsCount > acc)
            {
                acc = rowCellsCount;
            }
        }
        return acc;
    }

    public String getLeftBorderStrokeColor() throws IdmlLibException,
            URISyntaxException, IOException, SelectorDocumentException
    {
        if (leftBorderStrokeColor == null)
        {
            leftBorderStrokeColor = calculateLeftBorderStrokeColor();
        }
        return leftBorderStrokeColor;
    }

    private String calculateLeftBorderStrokeColor() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            String candidateStrokeColor = style.getLeftBorderStrokeColor();
            if (candidateStrokeColor != null)
            {
                return candidateStrokeColor;
            }
        }
        return null;
    }

    private String calculateBottomBorderStrokeColor() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            String candidateStrokeColor = style.getBottomBorderStrokeColor();
            if (candidateStrokeColor != null)
            {
                return candidateStrokeColor;
            }
        }
        return null;
    }

    private String calculateRightBorderStrokeColor() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            String candidateStrokeColor = style.getRightBorderStrokeColor();
            if (candidateStrokeColor != null)
            {
                return candidateStrokeColor;
            }
        }
        return null;
    }

    private String calculateTopBorderStrokeColor() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            String candidateStrokeColor = style.getTopBorderStrokeColor();
            if (candidateStrokeColor != null)
            {
                return candidateStrokeColor;
            }
        }
        return null;
    }

    public String getBottomBorderStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (bottomBorderStrokeColor == null)
        {
            bottomBorderStrokeColor = calculateBottomBorderStrokeColor();
        }
        return bottomBorderStrokeColor;
    }

    private Double calculateBottomBorderStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateStrokeTint = style.getBottomBorderStrokeTint();
            if (candidateStrokeTint != null && candidateStrokeTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
            {
                return candidateStrokeTint;
            }
        }
        return null;

    }

    private Double calculateTopBorderStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateStrokeTint = style.getTopBorderStrokeTint();
            if (candidateStrokeTint != null && candidateStrokeTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
            {
                return candidateStrokeTint;
            }
        }
        return null;

    }

    private Double calculateLeftBorderStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateStrokeTint = style.getLeftBorderStrokeTint();
            if (candidateStrokeTint != null && candidateStrokeTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
            {
                return candidateStrokeTint;
            }
        }
        return null;

    }

    private Double calculateRightBorderStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateStrokeTint = style.getRightBorderStrokeTint();
            if (candidateStrokeTint != null && candidateStrokeTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
            {
                return candidateStrokeTint;
            }
        }
        return null;

    }

    public String getRightBorderStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (rightBorderStrokeColor == null)
        {
            rightBorderStrokeColor = calculateRightBorderStrokeColor();
        }
        return rightBorderStrokeColor;

    }

    public String getTopBorderStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (topBorderStrokeColor == null)
        {
            topBorderStrokeColor = calculateTopBorderStrokeColor();
        }
        return topBorderStrokeColor;
    }

    public Double getRightBorderStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (rightBorderStrokeTint == null)
        {
            rightBorderStrokeTint = calculateRightBorderStrokeTint();
        }
        return rightBorderStrokeTint;
    }

    public Double getLeftBorderStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (leftBorderStrokeTint == null)
        {
            leftBorderStrokeTint = calculateLeftBorderStrokeTint();
        }
        return leftBorderStrokeTint;

    }

    public Double getTopBorderStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (topBorderStrokeTint == null)
        {
            topBorderStrokeTint = calculateTopBorderStrokeTint();
        }
        return topBorderStrokeTint;

    }

    public Double getBottomBorderStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (bottomBorderStrokeTint == null)
        {
            bottomBorderStrokeTint = calculateBottomBorderStrokeTint();
        }
        return bottomBorderStrokeTint;
    }

    public TintAndColorWrapper getRightBorderStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (rightBorderStrokeTintAndColorWrapper == null)
        {
            rightBorderStrokeTintAndColorWrapper =
                    calculateRightBorderStrokeTintAndColorWrapper();
        }
        return rightBorderStrokeTintAndColorWrapper;
    }

    private TintAndColorWrapper calculateRightBorderStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String colorOrTintSelf = getRightBorderStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        colorOrTintSelf);
        Double tintValue = getRightBorderStrokeTint();
        if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(tintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;

    }

    private TintAndColorWrapper calculateLeftBorderStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String colorOrTintSelf = getLeftBorderStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        colorOrTintSelf);
        Double tintValue = getLeftBorderStrokeTint();
        if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(tintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;

    }

    private TintAndColorWrapper calculateTopBorderStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String colorOrTintSelf = getTopBorderStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        colorOrTintSelf);
        Double tintValue = getTopBorderStrokeTint();
        if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(tintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;

    }

    private TintAndColorWrapper calculateBottomBorderStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String colorOrTintSelf = getBottomBorderStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        colorOrTintSelf);
        Double tintValue = getBottomBorderStrokeTint();
        if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(tintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;

    }

    public TintAndColorWrapper getLeftBorderStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (leftBorderStrokeTintAndColorWrapper == null)
        {
            leftBorderStrokeTintAndColorWrapper =
                    calculateLeftBorderStrokeTintAndColorWrapper();
        }
        return leftBorderStrokeTintAndColorWrapper;

    }

    public TintAndColorWrapper getTopBorderStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (topBorderStrokeTintAndColorWrapper == null)
        {
            topBorderStrokeTintAndColorWrapper =
                    calculateTopBorderStrokeTintAndColorWrapper();
        }
        return topBorderStrokeTintAndColorWrapper;

    }

    public TintAndColorWrapper getBottomBorderStrokeTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (bottomBorderStrokeTintAndColorWrapper == null)
        {
            bottomBorderStrokeTintAndColorWrapper =
                    calculateBottomBorderStrokeTintAndColorWrapper();
        }
        return bottomBorderStrokeTintAndColorWrapper;
    }

    public Double getSpaceBefore() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (spaceBefore == null)
        {
            spaceBefore = calculateSpaceBefore();
        }
        return spaceBefore;
    }

    private Double calculateSpaceBefore() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateSpace = style.getSpaceBefore();
            if (candidateSpace != null)
            {
                return candidateSpace;
            }
        }
        return null;
    }

    public Double getSpaceAfter() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (spaceAfter == null)
        {
            spaceAfter = calculateSpaceAfter();
        }
        return spaceAfter;
    }

    private Double calculateSpaceAfter() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<TableStyle> stylesChain = getTableStylesChain();
        for (TableStyle style : stylesChain)
        {
            Double candidateSpace = style.getSpaceAfter();
            if (candidateSpace != null)
            {
                return candidateSpace;
            }
        }
        return null;
    }
    
    public List<Double> getColumnWidths() throws SelectorDocumentException, SelectorFileNotExistentException, ParserConfigurationException, SAXException, IOException, URISyntaxException, IdmlLibException
    {
        if (columnWidths == null)
        {
            columnWidths = calculateColumnWidths();
        }
        return columnWidths;
    }

    protected List<Double> calculateColumnWidths() throws SAXException,
            IOException, IdmlLibException, URISyntaxException,
            ParserConfigurationException, SelectorDocumentException
    {
        List<SelectorTableRow> currentTableRows = getTableRows();
        List<Double> toReturn = null;
        int rowMaxColumns = 0;
        for (SelectorTableRow row:currentTableRows)
        {
            List<Double> cellsWidths = row.getCellsWidths();
            if (cellsWidths.size()>rowMaxColumns)
            {
                toReturn = cellsWidths;
            }
        }
        return toReturn;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        bodyRows = null;
        bottomBorderStrokeColor = null;
        bottomBorderStrokeTint = null;
        bottomBorderStrokeTintAndColorWrapper = null;
        bottomBorderStrokeWeight = 0;
        columnWidths = null;
        headerRows = null;
        height = 0;
        leftBorderStrokeColor = null;
        leftBorderStrokeTint = null;
        leftBorderStrokeTintAndColorWrapper = null;
        leftBorderStrokeWeight = 0;
        maxColumns = 0;
        rightBorderStrokeColor = null;
        rightBorderStrokeTint = null;
        rightBorderStrokeTintAndColorWrapper = null;
        rightBorderStrokeWeight = 0;
        spaceAfter = null;
        spaceBefore = null;
        tableRows = null;
        tableStyle = null;
        tableStylesChain = null;
        topBorderStrokeColor = null;
        topBorderStrokeTint = null;
        topBorderStrokeTintAndColorWrapper = null;
        topBorderStrokeWeight = 0;
        width = 0;
    }

    @Override
    public SelectorTable getTableAncestor() throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException
    {
        return this;//ritorno me stessa come tabella antenata
    }

    @Override
    public List<SelectorParagraph> getContainedSelectorParagraphsExcludingTableOnes()
            throws SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        return null;//visto che sono una tabella, non ho paragrafi che non siano contenuti dentro una tabella
    }

    
}
