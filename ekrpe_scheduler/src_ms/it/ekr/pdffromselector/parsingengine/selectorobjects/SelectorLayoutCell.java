package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.datatypes.enums.*;
import de.fhcon.idmllib.api.elements.Document;
import de.fhcon.idmllib.api.elements.shared.TextFramePreference;
import de.fhcon.idmllib.api.elements.styles.ObjectStyle;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorLayoutCell extends AbstractSelectorContainer implements
        ISelectorObject, ISelectorContainer
{

    public static final String LAYOUT_CELL_TAG = "cella_layout";
    public static final String PERCENT_WIDTH_LABEL = "percentuale_larghezza";
    public static final String PERCENT_HEIGHT_LABEL = "percentuale_altezza";
    protected static final String HORIZONTAL_DIRECTION = "orizzontale";
    protected static final String VERTICAL_DIRECTION = "verticale";
    protected static final String DIRECTION_ATTRIBUTE_LABEL = "direzione";

    private double height;

    private double width;
    private TintAndColorWrapper fillTintAndColorWrapper;

    private Double percentWidth = null;

    private Double percentHeight = null;

    private Double strokeWeight = null;

    private ObjectStyle objectStyle;

    /**
     *
     */
    protected List<ObjectStyle> objectStylesChain = null;

    private String fillColor = null;

    private Boolean enableFill = null;

    private Double fillTint = null;
    private Boolean enableStrokeAndCornerOptions = null;

    private Boolean enableStroke = null;

    private String strokeColor = null;

    private Double strokeTint = null;

    private TintAndColorWrapper strokeTintAndColor =
            null;

    private String direction = null;
    private List<SelectorLayoutCell> containedLayoutCells = null;
    private Boolean enableTextFrameGeneralOptions = null;
    private TextFramePreference textFramePreference = null;
    private VerticalJustification verticalJustification = null;
    private List<SelectorLayoutCell> containedLeafLayoutCells;

    private String flowLabel = null;
    private Integer flowIndex = null;
    private List<SelectorLayoutCell> currentAndContainedLayoutCellsRec;

    /**
     *
     * @param layoutCellXml
     * @param parent
     */
    public SelectorLayoutCell(Node layoutCellXml, ISelectorObject parent)
    {
        super(parent, layoutCellXml);
    }

    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (height == 0)
        {
            height = calculateHeight();
        }
        return height;
    }

    protected Double absoluteX = null;

    protected Double absoluteY = null;

    public Double getAbsoluteX() throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IdmlLibException, IOException, URISyntaxException
    {
        if (absoluteX == null)
        {
            absoluteX = calculateAbsoluteX();
        }
        return absoluteX;
    }

    public Double getAbsoluteY() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException
    {
        if (absoluteY == null)
        {
            absoluteY = calculateAbsoluteY();
        }
        return absoluteY;
    }

    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (width == 0)
        {
            width = calculateWidth();
        }
        return width;
    }

    private double calculateSize(boolean calculateHeight) throws
            URISyntaxException, IOException, IdmlLibException,
            SelectorDocumentException
    {
        if (getParent() instanceof SelectorPage)
        {
            SelectorPage parentPage = (SelectorPage) getParent();
            if (calculateHeight)
            {
                return parentPage.getAvailableHeight();
                //return parentPage.getHeight();
            } else
            {
                return parentPage.getAvailableWidth();
                //return parentPage.getWidth();
            }
        } else
        {
            ISelectorContainer ancestorContainer = getAncestorContainer();
            NamedNodeMap attributes = xmlDescription.getAttributes();
            if (ancestorContainer != null)
            {
                if (calculateHeight)
                {
                    double parentAvailableHeight = ancestorContainer.
                            getAvailableHeight();
                    if (attributes != null)
                    {
                        String percentHeightAttributeLabel =
                                PERCENT_HEIGHT_LABEL;
                        double calculatedHeight =
                                calculateSizeUsingPercentAttribute(attributes,
                                        percentHeightAttributeLabel,
                                        parentAvailableHeight);
                        return calculatedHeight;
                    }

                    return parentAvailableHeight;

                } else
                {
                    double parentAvailableWidth = ancestorContainer.
                            getAvailableWidth();
                    if (attributes != null)
                    {
                        String percentWidthAttributeLabel =
                                PERCENT_WIDTH_LABEL;
                        double calculatedWidth =
                                calculateSizeUsingPercentAttribute(attributes,
                                        percentWidthAttributeLabel,
                                        parentAvailableWidth);
                        return calculatedWidth;
                    }
                    return parentAvailableWidth;
                }
            }

        }
        return 0;
    }

    protected double calculateSizeUsingPercentAttribute(NamedNodeMap attributes,
                                                        String percentSizeAttributeLabel,
                                                        double parentAvailableSize)
            throws NumberFormatException, DOMException
    {
        Node percentSizeAttribute =
                attributes.getNamedItem(percentSizeAttributeLabel);
        if (percentSizeAttribute != null)
        {
            String textContent =
                    percentSizeAttribute.getTextContent();
            Double percentSizeDoubleValue = new Double(textContent);
            double factor = percentSizeDoubleValue / 100.0;
            double calculatedSize = parentAvailableSize * factor;
            return calculatedSize;
        }
        return parentAvailableSize;
    }

    /**
     * Get the value of percentWidth
     *
     * @return the value of percentWidth
     */
    public Double getPercentWidth()
    {
        if (percentWidth == null)
        {
            percentWidth = calculatePercentSize(false);
        }
        return percentWidth;
    }

    /**
     * Get the value of percentHeight
     *
     * @return the value of percentHeight
     */
    public Double getPercentHeight()
    {
        if (percentHeight == null)
        {
            percentHeight = calculatePercentSize(true);
        }
        return percentHeight;
    }

    protected double calculatePercentSize(boolean calculateHeight)
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        if (attributes != null)
        {
            if (calculateHeight)
            {
                return calculatePercentSizeUsingAttribute(attributes,
                        PERCENT_HEIGHT_LABEL);
            } else
            {

                return calculatePercentSizeUsingAttribute(attributes,
                        PERCENT_WIDTH_LABEL);

            }
        }
        return 0.0;
    }

    protected double calculatePercentSizeUsingAttribute(NamedNodeMap attributes,
                                                        String attributeLabel)
            throws
            NumberFormatException, DOMException
    {
        Double value = new Double(0.0);
        Node namedItem = attributes.getNamedItem(attributeLabel);
        if (namedItem != null)
        {
            String textContent = namedItem.getTextContent();
            value = new Double(textContent);
            return value;
        }
        return value;
    }

    private double calculateWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        return calculateSize(false);
    }

    private double calculateHeight() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        return calculateSize(true);
    }

    @Override
    public double getAvailableHeight() throws IOException,
            URISyntaxException, IdmlLibException,
            SelectorDocumentException
    {
        Double currentStrokeWeight = getStrokeWeight();
        if (currentStrokeWeight != null)
        {
            return getHeight() - 2 * currentStrokeWeight;
        }
        return getHeight();
    }

    @Override
    public double getAvailableWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        Double currentStrokeWeight = getStrokeWeight();
        if (currentStrokeWeight != null)
        {
            return getWidth() - 2 * currentStrokeWeight;
        }
        return getWidth();
    }

    /**
     * Get the value of strokeWeight
     *
     * @return the value of strokeWeight
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public Double getStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (strokeWeight == null)
        {
            strokeWeight = calculateStrokeWeight();
        }
        return strokeWeight;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    private Double calculateStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        Boolean enableStrokeCheck = isEnableStroke();
        if (enableStrokeCheck == null || !enableStrokeCheck)
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Double candidateStrokeWeight = style.getStrokeWeight();
            if (candidateStrokeWeight != null)
            {
                return candidateStrokeWeight;
            }
        }
        return null;
    }

    /**
     * Get the value of objectStyle
     *
     * @return the value of objectStyle
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public ObjectStyle getObjectStyle() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        if (objectStyle == null)
        {
            objectStyle = calculateObjectStyle();
        }
        return objectStyle;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    protected ObjectStyle calculateObjectStyle() throws
            URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        SelectorDocument currentParentSelectorDocument =
                this.getParentSelectorDocument();
        Document supertemplateDocument =
                currentParentSelectorDocument.getSupertemplateDocument();

        String type = getType();
        if (type != null)
        {
            List<String> currentTypeWrapper = new ArrayList<>();
            currentTypeWrapper.add(type);
            String objectStyleName =
                    IdmlUtils.convertTypePathToIdmlName(currentTypeWrapper);
            ObjectStyle found = IdmlUtils.retrieveObjectStyleByName(
                    supertemplateDocument, objectStyleName);
            return found;
        }

        return null;
    }

    /**
     * trova lo stile di oggetto di questo oggetto e quelli in cui si basa, in
     * modo da poter cercare le proprietà grafiche
     *
     * @return the value of paragraphStylesChain
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    protected List<ObjectStyle> getObjectStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException, URISyntaxException
    {
        if (objectStylesChain == null)
        {
            objectStylesChain = calculateObjectStylesChain();
        }
        return Collections.unmodifiableList(objectStylesChain);
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    protected List<ObjectStyle> calculateObjectStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        return IdmlUtils.retrieveObjectStylesCurrentAndBasedOnChain(
                getParentSelectorDocument().getSupertemplateDocument(),
                getObjectStyle());
    }

    private List<ISelectorObject> processContenutoFlussoTag(
            Node contenutoFlussoTag)
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<Node> childNodes = XmlManagement.retrieveElementChildren(
                contenutoFlussoTag);
        for (int i = 0; i < childNodes.size();
                i++)
        {
            Node childNode = childNodes.get(i);
            String nodeName = childNode.getNodeName();
            switch (nodeName)
            {
                case SelectorFlowUnit.FLOW_UNIT_TAG:
                    SelectorFlowUnit creatingUnit = new SelectorFlowUnit(this,
                            childNode);
                    toReturn.add(creatingUnit);
                    break;
                case SelectorFloatingBox.FLOATING_BOX_TAG:
                    SelectorFloatingBox creatingFloatingBox =
                            new SelectorFloatingBox(this, childNode);
                    toReturn.add(creatingFloatingBox);
                    break;
            }
        }
        return toReturn;
    }

    private List<ISelectorObject> processContenutoSpazialeTag(
            Node contenutoSpazialeTag)
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<Node> childNodes = XmlManagement.retrieveElementChildren(
                contenutoSpazialeTag);
        for (int i = 0; i < childNodes.size(); i++)
        {
            Node childNode = childNodes.get(i);
            if (childNode.getNodeName().equals(SelectorSpaceUnit.SPACE_UNIT_TAG))
            {
                SelectorSpaceUnit creatingUnit = new SelectorSpaceUnit(this,
                        childNode);
                toReturn.add(creatingUnit);
            }
        }
        return toReturn;
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        //per il momento supportati solo oggetti flusso, in futuro floating box ed altre celle layout
        List<Node> retrieveElementChildren =
                XmlManagement.retrieveElementChildren(xmlDescription);
        if (!retrieveElementChildren.isEmpty())
        {
            Node contenutoTag = retrieveElementChildren.get(0);
            List<Node> contenutoTagChildren = XmlManagement.
                    retrieveElementChildren(contenutoTag);
            for (int i = 0; i < contenutoTagChildren.size();
                    i++)
            {
                Node contenutoTagChild = contenutoTagChildren.get(i);
                String nodeName = contenutoTagChild.getNodeName();
                switch (nodeName)
                {
                    case "contenuto_flusso":
                        List<ISelectorObject> processContenutoFlussoTag =
                                processContenutoFlussoTag(
                                        contenutoTagChild);
                        toReturn.
                                addAll(processContenutoFlussoTag);
                        break;
                    case "contenuto_spaziale":
                        List<ISelectorObject> processContenutoSpazialeTag =
                                processContenutoSpazialeTag(
                                        contenutoTagChild);
                        toReturn.addAll(processContenutoSpazialeTag);
                        break;
                    case LAYOUT_CELL_TAG:
                        SelectorLayoutCell containedCell =
                                new SelectorLayoutCell(
                                        contenutoTagChild, this);
                        toReturn.add(containedCell);
                        break;
                }
            }
        }

        return toReturn;
    }

    public String getFillColor() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (fillColor == null)
        {
            fillColor = calculateFillColor();
        }
        return fillColor;
    }

    private String calculateFillColor() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        Boolean enableFillCheck = isEnableFill();
        if (enableFillCheck == null || !enableFillCheck)
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            String candidateColor = style.getFillColor();
            if (candidateColor != null)
            {
                return candidateColor;
            }
        }
        return null;
    }

    public Boolean isEnableFill() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (enableFill == null)
        {
            enableFill = calculateEnableFill();
        }
        return enableFill;
    }

    private Boolean calculateEnableFill() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Boolean candidateEnable = style.isEnableFill();
            if (candidateEnable != null)
            {
                return candidateEnable;
            }
        }
        return null;
    }

    public Double getFillTint() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (fillTint == null)
        {
            fillTint = calculateFillTint();
        }
        return fillTint;
    } //*/
    //*/

    private Double calculateFillTint() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        Boolean enableFillCheck = isEnableFill();
        if (enableFillCheck == null || !enableFillCheck)
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Double candidateTint = style.getFillTint();
            if (candidateTint != null)
            {
                return candidateTint;
            }
        }
        return null;
    }

    public Boolean isEnableStrokeAndCornerOptions() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (enableStrokeAndCornerOptions == null)
        {
            enableStrokeAndCornerOptions =
                    calculateEnableStrokeAndCornerOptions();
        }
        return enableStrokeAndCornerOptions;
    }

    private Boolean calculateEnableStrokeAndCornerOptions() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Boolean candidateEnable = style.isEnableStrokeAndCornerOptions();
            if (candidateEnable != null)
            {
                return candidateEnable;
            }
        }
        return null;
    }

    public Boolean isEnableStroke() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (enableStroke == null)
        {
            enableStroke = calculateEnableStroke();
        }
        return enableStroke;
    }

    private Boolean calculateEnableStroke() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Boolean candidateEnable = style.isEnableStroke();
            if (candidateEnable != null)
            {
                return candidateEnable;
            }
        }
        return null;
    }

    public String getStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (strokeColor == null)
        {
            strokeColor = calculateStrokeColor();
        }
        return strokeColor;
    }

    private String calculateStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        Boolean enableStrokeCheck = isEnableStroke();
        if (enableStrokeCheck == null || !enableStrokeCheck)
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            String candidateColor = style.getStrokeColor();
            if (candidateColor != null)
            {
                return candidateColor;
            }
        }
        return null;
    }

    public Double getStrokeTint() throws
            SelectorDocumentException, URISyntaxException, IOException,
            IdmlLibException
    {
        if (strokeTint == null)
        {
            strokeTint = calculateStrokeTint();
        }
        return strokeTint;
    }

    private Double calculateStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException,
            SelectorDocumentException
    {
        Boolean enableStrokeCheck = isEnableStroke();
        if (enableStrokeCheck == null || !enableStrokeCheck)
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Double candidateTint = style.getStrokeTint();
            if (candidateTint != null)
            {
                return candidateTint;
            }
        }
        return null;
    }

    /**
     * Get the value of strokeTintAndColor
     *
     * @return the value of strokeTintAndColor
     * @throws java.io.IOException
     * @throws java.net.URISyntaxException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     */
    public TintAndColorWrapper getStrokeTintAndColor() throws
            IOException, URISyntaxException,
            SelectorDocumentException, IdmlLibException
    {
        if (strokeTintAndColor == null)
        {
            strokeTintAndColor = calculateStrokeTintAndColor();
        }
        return strokeTintAndColor;
    }

    private TintAndColorWrapper calculateStrokeTintAndColor() throws
            URISyntaxException,
            SelectorDocumentException, IdmlLibException, IOException
    {
        Boolean enableStrokeCheck = isEnableStroke();
        if (enableStrokeCheck == null || !enableStrokeCheck)
        {
            return null;
        }
        String fillColorOrTintSelf = getStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        fillColorOrTintSelf);
        if (createTintAndColorWrapperBySelf == null)
        {
            return null;
        }
        Double fillTintValue = getStrokeTint();
        if (fillTintValue != null && fillTintValue >= 0)//negativi sono ereditati
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(fillTintValue);
            }

        }

        return createTintAndColorWrapperBySelf;
    }

    /**
     * Get the value of alignment
     *
     * @return the value of alignment
     */
    public String getDirection()
    {
        if (direction == null)
        {
            direction = calculateDirection();
        }
        return direction;
    }

    private String calculateDirection()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        if (attributes != null)
        {
            Node namedItem = attributes.getNamedItem(DIRECTION_ATTRIBUTE_LABEL);
            if (namedItem != null)
            {
                String textContent = namedItem.getTextContent();
                return textContent;
            }
        }
        return null;
    }

    public boolean isHorizontalDirection()
    {
        String currentDirection = getDirection();
        if (currentDirection != null)
        {
            if (currentDirection.equals(HORIZONTAL_DIRECTION))
            {
                return true;
            }
        }
        return false;
    }

    public boolean isVerticalDirection()
    {
        String currentDirection = getDirection();
        if (currentDirection != null)
        {
            if (currentDirection.equals(VERTICAL_DIRECTION))
            {
                return true;
            }
        }
        return false;
    }

    public boolean isUndefinedDirection()
    {
        String currentDirection = getDirection();
        if (currentDirection != null)
        {
            return !(isHorizontalDirection() || isVerticalDirection());
        } else
        {
            return true;
        }
    }

    /**
     * Get the value of containedLayoutCells
     *
     * @return the value of containedLayoutCells
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public List<SelectorLayoutCell> getContainedLayoutCells() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (containedLayoutCells == null)
        {
            containedLayoutCells = calculateContainedLayoutCells();
        }
        return Collections.unmodifiableList(containedLayoutCells);
    }

    private List<SelectorLayoutCell> calculateContainedLayoutCells() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorLayoutCell> toReturn = new ArrayList<>();
        List<ISelectorObject> currentContainedObjects = getContainedObjects();
        for (ISelectorObject obj : currentContainedObjects)
        {
            if (obj instanceof SelectorLayoutCell)
            {
                toReturn.add((SelectorLayoutCell) obj);
            }
        }
        return toReturn;
    }

    public List<SelectorLayoutCell> getCurrentAndContainedLayoutCellsRec() throws SelectorDocumentException, SelectorFileNotExistentException, ParserConfigurationException, SAXException, IOException
    {
        if (currentAndContainedLayoutCellsRec == null)
        {
            currentAndContainedLayoutCellsRec =
                    calculateCurrentAndContainedLayoutCellsRec();
        }
        return currentAndContainedLayoutCellsRec;
    }

    /**
     * Get the value of fillTintAndColorWrapper
     *
     * @return the value of fillTintAndColorWrapper
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public TintAndColorWrapper getFillTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (fillTintAndColorWrapper == null)
        {
            fillTintAndColorWrapper = calculateFillTintAndColorWrapper();
        }
        return fillTintAndColorWrapper;
    }

    private TintAndColorWrapper calculateFillTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        Boolean enableFillCheck = isEnableFill();
        if (enableFillCheck == null || !enableFillCheck)
        {
            return null;
        }
        String fillColorOrTintSelf = getFillColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        fillColorOrTintSelf);
        Double fillTintValue = getFillTint();
        if (fillTintValue != null && fillTintValue >= 0)//se è <0 vuol dire che eredita e quindi prende dalla base
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(fillTintValue);
            }

        }

        return createTintAndColorWrapperBySelf;
    }

    public Boolean isEnableTextFrameGeneralOptions() throws URISyntaxException,
            SelectorDocumentException, IOException, IdmlLibException
    {
        if (enableTextFrameGeneralOptions == null)
        {
            enableTextFrameGeneralOptions =
                    calculateEnableTextFrameGeneralOptions();
        }
        return enableTextFrameGeneralOptions;

    }

    protected Boolean calculateEnableTextFrameGeneralOptions() throws
            URISyntaxException, SelectorDocumentException, IOException,
            IdmlLibException
    {
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Boolean candidateEnable = style.isEnableTextFrameGeneralOptions();
            if (candidateEnable != null)
            {
                return candidateEnable;
            }
        }
        return null;
    }

    public final TextFramePreference getTextFramePreference() throws
            URISyntaxException, SelectorDocumentException, IOException,
            IdmlLibException
    {
        if (textFramePreference == null)
        {
            textFramePreference = calculateTextFramePreference();
        }
        return textFramePreference;
    }

    protected TextFramePreference calculateTextFramePreference() throws
            URISyntaxException, SelectorDocumentException, IOException,
            IdmlLibException
    {
        Boolean enableFlag = isEnableTextFrameGeneralOptions();
        if (enableFlag == null || enableFlag == false)
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            TextFramePreference candidateTextFrameOptions = style.
                    getTextFramePreference();
            if (candidateTextFrameOptions != null)
            {
                return candidateTextFrameOptions;
            }
        }
        return null;
    }

    public final VerticalJustification getVerticalJustification() throws
            URISyntaxException, SelectorDocumentException, IOException,
            IdmlLibException
    {
        if (verticalJustification == null)
        {
            verticalJustification = calculateVerticalJustification();
        }
        return verticalJustification;

    }

    private VerticalJustification calculateVerticalJustification() throws
            URISyntaxException, SelectorDocumentException, IOException,
            IdmlLibException
    {
        //devo vedere se è abilitato qui
        Boolean enableFlag = isEnableTextFrameGeneralOptions();
        if (enableFlag == null || enableFlag == false)
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            TextFramePreference candidateTextFrameOptions = style.
                    getTextFramePreference();
            if (candidateTextFrameOptions != null)
            {
                //devo recuperarla dagli stili di oggetto superiori anche se ivi non è abilitata
                VerticalJustification candidateVerticalJustification =
                        candidateTextFrameOptions.getVerticalJustification();
                if (candidateVerticalJustification != null)
                {
                    return candidateVerticalJustification;
                }

            }
        }
        return null;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        direction = null;
        enableFill = null;
        enableStroke = null;
        enableStrokeAndCornerOptions = null;
        enableTextFrameGeneralOptions = null;
        fillColor = null;
        fillTint = null;
        fillTintAndColorWrapper = null;
        objectStyle = null;
        objectStylesChain = null;
        percentHeight = null;
        percentWidth = null;
        strokeColor = null;
        strokeTint = null;
        strokeTintAndColor = null;
        strokeWeight = null;
        textFramePreference = null;
        verticalJustification = null;
        height = 0;
        width = 0;
        absoluteX = null;
        absoluteY = null;
        flowLabel = null;
        flowIndex = null;
        containedFlows = null;
        leafCellsByFlowLabel = null;
        currentAndContainedLayoutCellsRec = null;
        
    }

    private Double calculateAbsoluteX() throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException, URISyntaxException, IdmlLibException
    {
        if (getParent() instanceof SelectorPage)
        {
            SelectorPage parentPage = getParentPage();
            Double leftMargin = parentPage.getLeftMargin();
            if (leftMargin != null)
            {
                return leftMargin;//per un motivo strano, la posizione iniziale x richiede di calcolare il margine mentre la posizione y è già a posto
            }
            return 0.0;
        } else if (getParent() instanceof SelectorLayoutCell)
        {
            SelectorLayoutCell parentCell = (SelectorLayoutCell) getParent();
            if (parentCell.isVerticalDirection() || parentCell.
                    isUndefinedDirection())
            {
                //se il padre è verticale, la x è la stessa del padre
                return calculateXStartingPointFromParent(parentCell);

            } else
            {
                SelectorLayoutCell previousCell = retrievePreviousLayoutCell(
                        parentCell);
                if (previousCell != null)
                {
                    Double previousCellAbsoluteX = previousCell.getAbsoluteX();
                    if (previousCellAbsoluteX != null)
                    {
                        double previousCellWidth = previousCell.getWidth();
                        Double toReturn = previousCellAbsoluteX +
                                previousCellWidth;
                        return toReturn;
                    }

                } else
                {
                    //se non ho celle precedenti, assumo che questa sia la prima cella
                    return calculateXStartingPointFromParent(parentCell);
                }
            }
        }
        return null;
    }

    protected Double calculateXStartingPointFromParent(
            SelectorLayoutCell parentCell)
            throws IdmlLibException, SelectorDocumentException, IOException,
            URISyntaxException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException
    {
        Double parentAbsoluteX = parentCell.getAbsoluteX();
        return calculateXorYStartingPosFromParent(parentAbsoluteX, parentCell);
    }

    private Double calculateAbsoluteY() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException
    {
        if (getParent() instanceof SelectorPage)
        {
            SelectorPage parentPage = getParentPage();
            Double topMargin = parentPage.getTopMargin();
            if (topMargin != null)
            {
                return topMargin;
            }
            return 0.0;
        } else if (getParent() instanceof SelectorLayoutCell)
        {
            SelectorLayoutCell parentCell = (SelectorLayoutCell) getParent();
            if (parentCell.isHorizontalDirection() || parentCell.
                    isUndefinedDirection())
            {
                //se il padre è orizzontale, la y è la stessa del padre
                return calculateYStartingPointFromParent(parentCell);
            } else
            {
                SelectorLayoutCell previousCell = retrievePreviousLayoutCell(
                        parentCell);
                if (previousCell != null)
                {
                    Double previousCellAbsoluteY = previousCell.getAbsoluteY();
                    if (previousCellAbsoluteY != null)
                    {
                        double previousCellHeight = previousCell.getHeight();
                        Double toReturn = previousCellAbsoluteY +
                                previousCellHeight;
                        return toReturn;
                    }

                } else
                {
                    //se non ho celle precedenti, assumo che questa sia la prima cella
                    return calculateYStartingPointFromParent(parentCell);
                }
            }
        }
        return null;
    }

    private SelectorLayoutCell retrievePreviousLayoutCell(
            SelectorLayoutCell parentLayoutCell) throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorLayoutCell> parentContainedLayoutCells =
                parentLayoutCell.getContainedLayoutCells();
        for (int i = 0; i < parentContainedLayoutCells.size(); i++)
        {
            SelectorLayoutCell currentCell = parentContainedLayoutCells.get(i);
            if (currentCell == this)
            {
                if (i == 0)
                {
                    return null;
                } else
                {
                    SelectorLayoutCell calculatedPreviousCell =
                            parentContainedLayoutCells.get(i - 1);
                    return calculatedPreviousCell;
                }
            }
        }
        return null;
    }

    private Double calculateYStartingPointFromParent(
            SelectorLayoutCell parentCell) throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException
    {
        Double parentAbsoluteY = parentCell.getAbsoluteY();
        return calculateXorYStartingPosFromParent(parentAbsoluteY, parentCell);
    }

    protected Double calculateXorYStartingPosFromParent(
            Double parentAbsoluteXorY,
            SelectorLayoutCell parentCell)
            throws IdmlLibException, SelectorDocumentException, IOException,
            URISyntaxException
    {
        if (parentAbsoluteXorY != null)
        {
            Double parentStrokeWeight = parentCell.getStrokeWeight();
            if (parentStrokeWeight == null)
            {
                parentStrokeWeight = 0.0;
            }
            return parentAbsoluteXorY + parentStrokeWeight;
        } else
        {
            return null;
        }
    }

    public List<SelectorLayoutCell> getContainedLeafLayoutCells() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (containedLeafLayoutCells == null)
        {
            containedLeafLayoutCells =
                    calculateContainedLeafLayoutCells();
        }
        return containedLeafLayoutCells;
    }

    private List<SelectorLayoutCell> calculateContainedLeafLayoutCells() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorLayoutCell> currentContainedLayoutCells =
                getContainedLayoutCells();
        if (currentContainedLayoutCells == null || currentContainedLayoutCells.
                isEmpty())
        {
            //vuol dire che sono già in una cella foglia e quindi restituisco me stessa con un wrapper
            List<SelectorLayoutCell> wrapper = new ArrayList<>();
            wrapper.add(this);
            return wrapper;
        } else
        {
            List<SelectorLayoutCell> toReturn = new ArrayList<>();
            for (SelectorLayoutCell contained : currentContainedLayoutCells)
            {
                List<SelectorLayoutCell> containedLeafLayoutCellsOfContainedCell =
                        contained.getContainedLeafLayoutCells();
                toReturn.addAll(containedLeafLayoutCellsOfContainedCell);
            }
            return toReturn;
        }
    }

    /**
     * @return the flowLabel
     */
    public String getFlowLabel()
    {
        if (flowLabel == null)
        {
            flowLabel = calculateFlowLabel();
        }
        return flowLabel;
    }

    /**
     * @return the flowIndex
     */
    public Integer getFlowIndex()
    {
        if (flowIndex == null)
        {
            flowIndex = calculateFlowIndex();
        }
        return flowIndex;
    }

    private String calculateFlowLabel()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node namedItem = attributes.getNamedItem(LAYOUT_FLOW_LABEL_ATTRIBUTE);
        if (namedItem != null)
        {
            String textContent = namedItem.getTextContent();
            return textContent;
        }
        return null;

    }
    public static final String LAYOUT_FLOW_LABEL_ATTRIBUTE =
            "etichetta_flusso_layout";

    private Integer calculateFlowIndex()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node namedItem = attributes.getNamedItem(LAYOUT_FLOW_INDEX_ATTRIBUTE);
        if (namedItem != null)
        {
            String textContent = namedItem.getTextContent();
            Integer integer = new Integer(textContent);
            return integer;
        }
        return null;
    }
    public static final String LAYOUT_FLOW_INDEX_ATTRIBUTE =
            "indice_flusso_layout";

    private List<String> containedFlows = null;

    /**
     * @return the containedFlows
     */
    public List<String> getContainedFlows() throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            IOException, SAXException
    {
        if (containedFlows == null)
        {
            containedFlows = calculateContainedFlows();
        }
        return containedFlows;
    }

    private List<String> calculateContainedFlows() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, IOException, SAXException
    {
        List<SelectorLayoutCell> currentContainedLeafLayoutCells =
                getContainedLeafLayoutCells();
        if ((currentContainedLeafLayoutCells != null) &&
                (!currentContainedLeafLayoutCells.isEmpty()))
        {
            List<String> toReturn = new ArrayList<>();
            for (SelectorLayoutCell leafCell : currentContainedLeafLayoutCells)
            {
                String containedFlowLabel = leafCell.getFlowLabel();
                if (containedFlowLabel != null)
                {
                    ArrayUtils.addToListIfNotAlreadyIn(toReturn,
                            containedFlowLabel);
                }

            }
            return toReturn;
        }
        return null;
    }

    private Dictionary<String, List<SelectorLayoutCell>> leafCellsByFlowLabel =
            null;

    public List<SelectorLayoutCell> getLeafCellsByFlowLabel(
            String currentFlowLabel) throws SelectorDocumentException,
            IOException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException
    {
        if (leafCellsByFlowLabel != null)
        {
            List<SelectorLayoutCell> cached = leafCellsByFlowLabel.get(
                    currentFlowLabel);
            if (cached != null)
            {
                return cached;
            }
        }
        return calculateLeafCellsByFlowLabel(currentFlowLabel);
    }

    private List<SelectorLayoutCell> calculateLeafCellsByFlowLabel(
            String currentFlowLabel) throws SelectorDocumentException,
            IOException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException
    {
        List<SelectorLayoutCell> currentContainedLeafLayoutCells =
                getContainedLeafLayoutCells();
        if ((currentContainedLeafLayoutCells != null) &&
                (!currentContainedLeafLayoutCells.isEmpty()))
        {
            List<SelectorLayoutCell> toReturn = new ArrayList<>();
            for (SelectorLayoutCell leaf : currentContainedLeafLayoutCells)
            {
                if (leaf != null)
                {
                    String examiningFlowLabel = leaf.getFlowLabel();
                    if (examiningFlowLabel != null)
                    {
                        if (examiningFlowLabel.equals(currentFlowLabel))
                        {
                            toReturn.add(leaf);
                        }
                    } else
                    {
                        if (currentFlowLabel == null)
                        {
                            toReturn.add(leaf);
                        }
                    }
                }

            }
            toReturn.sort(new CompareSelectorLayoutCellsByFlowIndex());
            if (leafCellsByFlowLabel == null)
            {
                leafCellsByFlowLabel = new Hashtable<>();
            }
            leafCellsByFlowLabel.put(currentFlowLabel, toReturn);
            return toReturn;
        }
        return null;
    }

    private List<SelectorLayoutCell> calculateCurrentAndContainedLayoutCellsRec()
            throws SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorLayoutCell> toReturn = new ArrayList<>();
        toReturn.add(this);
        List<SelectorLayoutCell> currentContainedLayoutCells =
                getContainedLayoutCells();
        if (currentContainedLayoutCells != null)
        {
            for (SelectorLayoutCell containedCell : currentContainedLayoutCells)
            {
                toReturn.addAll(containedCell.
                        getCurrentAndContainedLayoutCellsRec());
            }
        }

        return toReturn;
    }
}
