package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.datatypes.enums.VerticalJustification;
import de.fhcon.idmllib.api.elements.Document;
import de.fhcon.idmllib.api.elements.styles.CellStyle;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorTableCell extends AbstractSelectorContainer implements
        ISelectorObject, ISelectorContainer
{

    private CellStyle cellStyle = null;

    /**
     *
     */
    protected double leftEdgeStrokeWeight = 0;

    /**
     *
     */
    protected double rightEdgeStrokeWeight = 0;

    /**
     *
     */
    protected double topEdgeStrokeWeight = 0;

    /**
     *
     */
    protected double bottomEdgeStrokeWeight = 0;
    private List<CellStyle> cellStylesChain = null;
    private int columnSpan = 0;
    private Double rightEdgeStrokeTint = null;
    private Double bottomEdgeStrokeTint = null;
    private String topEdgeStrokeColor = null;
    private TintAndColorWrapper topEdgeStrokeTintAndColorWrapper = null;
    private Double fillTint = null;
    private String bottomEdgeStrokeColor = null;
    private TintAndColorWrapper bottomEdgeStrokeTintAndColorWrapper = null;

    /**
     *
     */
    protected String fillColor = null;
    private TintAndColorWrapper fillTintAndColorWrapper = null;
    private String leftEdgeStrokeColor = null;
    private Double leftEdgeStrokeTint = null;
    private TintAndColorWrapper leftEdgeStrokeTintAndColorWrapper = null;
    private String rightEdgeStrokeColor = null;
    private TintAndColorWrapper rightEdgeStrokeTintAndColorWrapper = null;
    private Double topEdgeStrokeTint = null;
    private VerticalJustification verticalJustification = null;
    private Double bottomInset = null;
    private Double topInset = null;
    private Double rightInset = null;
    private Double leftInset = null;
    private Double rotationAngle = null;
    private Double storedWidth = null;
    private Boolean storedWidthPercentageFlag = null;
    private double width;

    /**
     *
     * @param parent
     * @param xmlDescription
     */
    public SelectorTableCell(ISelectorObject parent, Node xmlDescription)
    {
        super(parent, xmlDescription);
    }

    @Override
    public List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<Node> childNodes = XmlManagement.retrieveElementChildren(
                xmlDescription);
        for (int i = 0; i < childNodes.size(); i++)
        {
            Node childNode = childNodes.get(i);
            if (childNode.getNodeName().equals("flusso"))
            {
                SelectorFlowUnit creatingUnit = new SelectorFlowUnit(this,
                        childNode);
                toReturn.add(creatingUnit);
            }
        }
        return toReturn;
    }

    @Override
    public List<String> getTypePath(ISelectorObject invoker)
    {
        if (invoker == null)
        {
            invoker = this;
        }
        //devo saltare il tipo della riga, nel codice di Selector viene fatto così
        ISelectorObject currentAncestor = getParent();
        while (currentAncestor instanceof SelectorTableRow)
        {
            currentAncestor = currentAncestor.getParent();
        }
        List<String> ancestorTypePath = currentAncestor.getTypePath(invoker);
        List<String> currentTypeWrapper = new ArrayList<>();
        String currentType = getType();
        if (currentType != null)
        {
            if (!(invoker instanceof SelectorParagraph))//nei paragrafi devo saltare lo stile di cella, su selector fa così
            {
                currentTypeWrapper.add(currentType);
            }

        }
        if (ancestorTypePath != null)
        {
            //stream di Java 8
            /*List<String> concatenated = Stream.concat(ancestorTypePath.stream(),
                    currentTypeWrapper.stream()).collect(Collectors.toList());*/
            //kors non supporta java 8, va rifatto in java 7
            List<String> concatenated = new ArrayList<>();
            for (String inAncestor:ancestorTypePath)
            {
                concatenated.add(inAncestor);
            }
            for (String inCurrent:currentTypeWrapper)
            {
                concatenated.add(inCurrent);
            }
            return concatenated;
        }

        return super.getTypePath(invoker);
    }

    @Override
    public double getAvailableHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getAvailableWidth() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException,SAXException, ParserConfigurationException
    {
        if (width == 0)
        {
            width = calculateWidth();
        }
        return width;
    }

    protected double calculateWidth() throws URISyntaxException,
            SelectorDocumentException, IdmlLibException, IOException, SAXException, ParserConfigurationException
    {
        Boolean storedWidthPercentage = isStoredWidthPercentage();
        if (storedWidthPercentage != null && storedWidthPercentage == false)
        {
            return getStoredWidth();
        }else
        {
            //devo calcolare in proporzione
            SelectorTableRow parentTableRow = getParentTableRow();
            Double fixedWidthSum = parentTableRow.getFixedWidthSum();
            double parentRowWidth = parentTableRow.getWidth();
            double remainingWidth = parentRowWidth-fixedWidthSum;
            Double percentagesWidthSum = parentTableRow.getPercentagesWidthSum();
            Double currentCellPercentage = getStoredWidth();
            double factor = currentCellPercentage/percentagesWidthSum;
            double calculatedWidth = factor*remainingWidth;
            return calculatedWidth;
            
        }
    }

    public SelectorTableRow getParentTableRow()
    {
        return (SelectorTableRow) getParent();
    }
    
    public SelectorTable getTable()
    {
        return getParentTableRow().getParentTable();
    }
    
    
    public Double getStoredWidth()
    {
        if (storedWidth == null)
        {
            storedWidth = calculateStoredWidth();
        }
        return storedWidth;
    }

    protected Double calculateStoredWidth() throws NumberFormatException,
            DOMException
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        if (attributes != null)
        {
            Node namedItem = attributes.getNamedItem(STORED_WIDTH_ATTRIBUTE);
            if (namedItem != null)
            {
                String textContent = namedItem.getTextContent();
                Double doubleValue = new Double(textContent);
                return doubleValue;
            }
            
        }
        return null;
    }
    public static final String STORED_WIDTH_ATTRIBUTE = "percentuale";
    
    public Boolean isStoredWidthPercentage()
    {
        if (storedWidthPercentageFlag == null)
        {
            storedWidthPercentageFlag = calculateIsStoredWidthPercentage();
        }
        return storedWidthPercentageFlag;
    }

    protected Boolean calculateIsStoredWidthPercentage() throws DOMException
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        if (attributes != null)
        {
            Node namedItem = attributes.getNamedItem(IS_PERCENT_WIDTH_ATTRIBUTE);
            if (namedItem != null)
            {
                String textContent = namedItem.getTextContent();
                boolean convertStringToBoolean =
                        XmlManagement.convertStringToBoolean(textContent);
                Boolean booleanValue = convertStringToBoolean;
                return booleanValue;
            }
            
        }
        return null;
        //"larghezza_percentuale";
    }
    public static final String IS_PERCENT_WIDTH_ATTRIBUTE =
            "larghezza_percentuale";
    /**
     * Get the value of cellStyle
     *
     * @return the value of cellStyle
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public CellStyle getCellStyle() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (cellStyle == null)
        {
            cellStyle = calculateAppliedCellStyle();
        }
        return cellStyle;
    }

    private CellStyle calculateAppliedCellStyle() throws URISyntaxException,
            IOException, IdmlLibException, SelectorDocumentException
    {

        SelectorDocument currentParentSelectorDocument =
                this.getParentSelectorDocument();
        Document supertemplateDocument =
                currentParentSelectorDocument.getSupertemplateDocument();
        List<String> currentTypePath = getTypePath(null);
        String cellStyleName =
                IdmlUtils.convertTypePathToIdmlName(currentTypePath);
        CellStyle found = IdmlUtils.retrieveCellStyleByName(
                supertemplateDocument, cellStyleName);
        return found;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public double getLeftEdgeStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (leftEdgeStrokeWeight == 0)
        {
            leftEdgeStrokeWeight = calculateLeftEdgeStrokeWeight();
        }
        return leftEdgeStrokeWeight;

    }

    private double calculateLeftEdgeStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateLeftEdgeStrokeWeight = style.
                    getLeftEdgeStrokeWeight();
            if (candidateLeftEdgeStrokeWeight != null)
            {
                return candidateLeftEdgeStrokeWeight;
            }
        }
        return 0;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public double getRightEdgeStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (rightEdgeStrokeWeight == 0)
        {
            rightEdgeStrokeWeight = calculateRightEdgeStrokeWeight();
        }
        return rightEdgeStrokeWeight;

    }

    private double calculateRightEdgeStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateRightEdgeStrokeWeight = style.
                    getRightEdgeStrokeWeight();
            if (candidateRightEdgeStrokeWeight != null)
            {
                return candidateRightEdgeStrokeWeight;
            }
        }
        return 0;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public double getTopEdgeStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (topEdgeStrokeWeight == 0)
        {
            topEdgeStrokeWeight = calculateTopEdgeStrokeWeight();
        }
        return topEdgeStrokeWeight;

    }

    private double calculateTopEdgeStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateTopBorderStrokeWeight = style.
                    getTopEdgeStrokeWeight();
            if (candidateTopBorderStrokeWeight != null)
            {
                return candidateTopBorderStrokeWeight;
            }
        }
        return 0;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public double getBottomEdgeStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (bottomEdgeStrokeWeight == 0)
        {
            bottomEdgeStrokeWeight = calculateBottomEdgeStrokeWeight();
        }
        return bottomEdgeStrokeWeight;

    }

    private double calculateBottomEdgeStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateBottomBorderStrokeWeight = style.
                    getBottomEdgeStrokeWeight();
            if (candidateBottomBorderStrokeWeight != null)
            {
                return candidateBottomBorderStrokeWeight;
            }
        }
        return 0;
    }

    private List<CellStyle> getCellStylesChain() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (cellStylesChain == null)
        {
            cellStylesChain = calculateCellStylesChain();
        }
        return Collections.unmodifiableList(cellStylesChain);
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    protected List<CellStyle> calculateCellStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        return IdmlUtils.retrieveCellStylesCurrentAndBasedOnChain(
                getParentSelectorDocument().getSupertemplateDocument(),
                getCellStyle());
    }

    /**
     * Get the value of columnSpan
     *
     * @return the value of columnSpan
     */
    public int getColumnSpan()
    {
        if (columnSpan == 0)
        {
            columnSpan = calculateColumnSpan();
        }
        return columnSpan;
    }

    private int calculateColumnSpan()
    {
        //col_span
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node colSpanAttribute = attributes.getNamedItem("col_span");
        if (colSpanAttribute != null)
        {
            String colSpanAttributeValue = colSpanAttribute.getNodeValue();
            Integer colSpanAttributeInteger = new Integer(colSpanAttributeValue);
            if (colSpanAttributeInteger > 0)
            {
                return colSpanAttributeInteger;
            }
        }
        return 1;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public Double getRightEdgeStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (rightEdgeStrokeTint == null)
        {
            rightEdgeStrokeTint = calculateRightEdgeStrokeTint();
        }
        return rightEdgeStrokeTint;
    }

    private Double calculateRightEdgeStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateTint = style.getRightEdgeStrokeTint();
            if (candidateTint != null && candidateTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
            {
                return candidateTint;
            }
        }
        return null;
    }

    private Double calculateTopEdgeStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateTint = style.getTopEdgeStrokeTint();
            if (candidateTint != null && candidateTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
            {
                return candidateTint;
            }
        }
        return null;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public Double getBottomEdgeStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (bottomEdgeStrokeTint == null)
        {
            bottomEdgeStrokeTint = calculateBottomEdgeStrokeTint();
        }
        return bottomEdgeStrokeTint;
    }

    private Double calculateBottomEdgeStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateTint = style.getBottomEdgeStrokeTint();
            if (candidateTint != null && candidateTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal parent
            {
                return candidateTint;
            }
        }
        return null;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public String getTopEdgeStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (topEdgeStrokeColor == null)
        {
            topEdgeStrokeColor = calculateTopEdgeStrokeColor();
        }
        return topEdgeStrokeColor;
    }

    private String calculateTopEdgeStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            String candidateColor = style.getTopEdgeStrokeColor();
            if (candidateColor != null)
            {
                return candidateColor;
            }
        }
        return null;
    }

    /**
     * Get the value of topEdgeStrokeTintAndColorWrapper
     *
     * @return the value of topEdgeStrokeTintAndColorWrapper
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public TintAndColorWrapper getTopEdgeStrokeTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (topEdgeStrokeTintAndColorWrapper == null)
        {
            topEdgeStrokeTintAndColorWrapper =
                    calculateTopEdgeStrokeTintAndColorWrapper();
        }
        return topEdgeStrokeTintAndColorWrapper;
    }

    private TintAndColorWrapper calculateTopEdgeStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String colorOrTintSelf = getTopEdgeStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        colorOrTintSelf);
        Double tintValue = getTopEdgeStrokeTint();
        if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal parent
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(tintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public Double getFillTint() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (fillTint == null)
        {
            fillTint = calculateFillTint();
        }
        return fillTint;
    }

    private Double calculateFillTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateFillTint = style.getFillTint();
            if (candidateFillTint != null && candidateFillTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal parent
            {
                return candidateFillTint;
            }
        }
        return null;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public String getBottomEdgeStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (bottomEdgeStrokeColor == null)
        {
            bottomEdgeStrokeColor = calculateBottomEdgeStrokeColor();
        }
        return bottomEdgeStrokeColor;
    }

    /**
     * Get the value of topEdgeStrokeTintAndColorWrapper
     *
     * @return the value of bottomEdgeStrokeTintAndColorWrapper
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public TintAndColorWrapper getBottomEdgeStrokeTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (bottomEdgeStrokeTintAndColorWrapper == null)
        {
            bottomEdgeStrokeTintAndColorWrapper =
                    calculateBottomEdgeStrokeTintAndColorWrapper();
        }
        return bottomEdgeStrokeTintAndColorWrapper;
    }

    private TintAndColorWrapper calculateBottomEdgeStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String colorOrTintSelf = getBottomEdgeStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        colorOrTintSelf);
        Double tintValue = getBottomEdgeStrokeTint();
       if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal parent
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(tintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;
    }

    private String calculateBottomEdgeStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            String candidateColor = style.getBottomEdgeStrokeColor();
            if (candidateColor != null)
            {
                return candidateColor;
            }
        }
        return null;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public Double getTopEdgeStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (topEdgeStrokeTint == null)
        {
            topEdgeStrokeTint = calculateTopEdgeStrokeTint();
        }
        return topEdgeStrokeTint;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public String getFillColor() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (fillColor == null)
        {
            fillColor = calculateFillColor();
        }
        return fillColor;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    protected String calculateFillColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            String candidateFillColor = style.getFillColor();
            if (candidateFillColor != null)
            {
                return candidateFillColor;
            }
        }
        return null;
    }

    /**
     * Get the value of fillTintAndColorWrapper
     *
     * @return the value of fillTintAndColorWrapper
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public TintAndColorWrapper getFillTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (fillTintAndColorWrapper == null)
        {
            fillTintAndColorWrapper = calculateFillTintAndColorWrapper();
        }
        return fillTintAndColorWrapper;
    }

    private TintAndColorWrapper calculateFillTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String fillColorOrTintSelf = getFillColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        fillColorOrTintSelf);
        Double fillTintValue = getFillTint();
        if (fillTintValue != null && fillTintValue >= 0)//se è < 0 vuol dire che eredita e quindi prende il default
        {
           
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(fillTintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public String getRightEdgeStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (rightEdgeStrokeColor == null)
        {
            rightEdgeStrokeColor = calculateRightEdgeStrokeColor();
        }
        return rightEdgeStrokeColor;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public String getLeftEdgeStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (leftEdgeStrokeColor == null)
        {
            leftEdgeStrokeColor = calculateLeftEdgeStrokeColor();
        }
        return leftEdgeStrokeColor;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    public Double getLeftEdgeStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (leftEdgeStrokeTint == null)
        {
            leftEdgeStrokeTint = calculateLeftEdgeStrokeTint();
        }
        return leftEdgeStrokeTint;
    }

    private String calculateLeftEdgeStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            String candidateColor = style.getLeftEdgeStrokeColor();
            if (candidateColor != null)
            {
                return candidateColor;
            }
        }
        return null;
    }

    private Double calculateLeftEdgeStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {

        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateTint = style.getLeftEdgeStrokeTint();
            if (candidateTint != null && candidateTint >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal parent
            {
                return candidateTint;
            }
        }
        return null;
    }

    // mettere qui left edge tint and color wrapper
    /**
     * Get the value of leftEdgeStrokeTintAndColorWrapper
     *
     * @return the value of topEdgeStrokeTintAndColorWrapper
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public TintAndColorWrapper getLeftEdgeStrokeTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (leftEdgeStrokeTintAndColorWrapper == null)
        {
            leftEdgeStrokeTintAndColorWrapper =
                    calculateLeftEdgeStrokeTintAndColorWrapper();
        }
        return leftEdgeStrokeTintAndColorWrapper;
    }

    private TintAndColorWrapper calculateLeftEdgeStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String colorOrTintSelf = getLeftEdgeStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        colorOrTintSelf);
        Double tintValue = getLeftEdgeStrokeTint();
        if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal parent
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(tintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;
    }

    private String calculateRightEdgeStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            String candidateColor = style.getRightEdgeStrokeColor();
            if (candidateColor != null)
            {
                return candidateColor;
            }
        }
        return null;
    }

    /**
     * Get the value of rightEdgeStrokeTintAndColorWrapper
     *
     * @return the value of topEdgeStrokeTintAndColorWrapper
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public TintAndColorWrapper getRightEdgeStrokeTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (rightEdgeStrokeTintAndColorWrapper == null)
        {
            rightEdgeStrokeTintAndColorWrapper =
                    calculateRightEdgeStrokeTintAndColorWrapper();
        }
        return rightEdgeStrokeTintAndColorWrapper;
    }

    private TintAndColorWrapper calculateRightEdgeStrokeTintAndColorWrapper()
            throws URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String colorOrTintSelf = getRightEdgeStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        colorOrTintSelf);
        Double tintValue = getRightEdgeStrokeTint();
        if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal parent
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(tintValue);
            }
            
        }

        return createTintAndColorWrapperBySelf;
    }

    public VerticalJustification getVerticalJustification() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (verticalJustification == null)
        {
            verticalJustification = calculateVerticalJustification();
        }
        return verticalJustification;

    }

    private VerticalJustification calculateVerticalJustification() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            VerticalJustification candidateVerticalJustification = style.
                    getVerticalJustification();
            if (candidateVerticalJustification != null)
            {
                return candidateVerticalJustification;
            }
        }
        return null;
    }

    public Double getBottomInset() throws URISyntaxException, IdmlLibException,
            SelectorDocumentException, IOException
    {
        if (bottomInset == null)
        {
            bottomInset = calculateBottomInset();
        }
        return bottomInset;
    }

    public Double getTopInset() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (topInset == null)
        {
            topInset = calculateTopInset();
        }
        return topInset;
    }

    public Double getRightInset() throws IOException, URISyntaxException,
            IdmlLibException, SelectorDocumentException
    {
        if (rightInset == null)
        {
            rightInset = calculateRightInset();
        }
        return rightInset;
    }

    public Double getLeftInset() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (leftInset == null)
        {
            leftInset = calculateLeftInset();
        }
        return leftInset;
    }

    private Double calculateBottomInset() throws URISyntaxException,
            IdmlLibException, SelectorDocumentException, IOException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateInset = style.getBottomInset();
            if (candidateInset != null)
            {
                return candidateInset;
            }
        }
        return null;
    }

    private Double calculateTopInset() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateInset = style.getTopInset();
            if (candidateInset != null)
            {
                return candidateInset;
            }
        }
        return null;

    }

    private Double calculateRightInset() throws IOException, URISyntaxException,
            IdmlLibException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateInset = style.getRightInset();
            if (candidateInset != null)
            {
                return candidateInset;
            }
        }
        return null;
    }

    private Double calculateLeftInset() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateInset = style.getLeftInset();
            if (candidateInset != null)
            {
                return candidateInset;
            }
        }
        return null;
    }

    public final Double getRotationAngle() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (rotationAngle == null)
        {
            rotationAngle = calculateRotationAngle();
        }
        return rotationAngle;
    }

    private Double calculateRotationAngle() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<CellStyle> stylesChain = getCellStylesChain();
        for (CellStyle style : stylesChain)
        {
            Double candidateAngle = style.getRotationAngle();
            if (candidateAngle != null)
            {
                return candidateAngle;
            }
        }
        return null;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        bottomEdgeStrokeColor = null;
        bottomEdgeStrokeTint = null;
        bottomEdgeStrokeTintAndColorWrapper = null;
        bottomEdgeStrokeWeight = 0;
        bottomInset = null;
        cellStyle = null;
        cellStylesChain = null;
        columnSpan = 0;
        fillColor = null;
        fillTint = null;
        fillTintAndColorWrapper = null;
        leftEdgeStrokeColor = null;
        leftEdgeStrokeTint = null;
        leftEdgeStrokeTintAndColorWrapper = null;
        leftEdgeStrokeWeight = 0;
        leftInset = null;
        rightEdgeStrokeColor = null;
        rightEdgeStrokeTint = null;
        rightEdgeStrokeWeight = 0;
        rightEdgeStrokeTintAndColorWrapper = null;
        rightInset = null;
        rotationAngle = null;
        storedWidth = null;
        storedWidthPercentageFlag = null;
        topEdgeStrokeColor = null;
        topEdgeStrokeTint = null;
        topEdgeStrokeTintAndColorWrapper = null;
        topEdgeStrokeWeight = 0;
        topInset = null;
        verticalJustification = null;
        width = 0;
    }

    
}
