/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.datatypes.enums.Justification;
import de.fhcon.idmllib.api.elements.Document;
import de.fhcon.idmllib.api.elements.complexproperties.AppliedFont;
import de.fhcon.idmllib.api.elements.complexproperties.Leading;
import de.fhcon.idmllib.api.elements.styles.ParagraphStyle;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.IdmlUtils;
import it.ekr.pdffromselector.parsingengine.utils.TintAndColorWrapper;
import it.ekr.pdffromselector.parsingengine.utils.XmlManagement;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * limitato a paragrafi con un solo elemento o una sola immagine
 *
 * @author Marco Scarpa
 */
public class SelectorParagraph extends AbstractSelectorObject implements
        ISelectorObject
{

    private ParagraphStyle appliedParagraphStyle =
            null;
    protected List<ParagraphStyle> paragraphStylesChain = null;
    private TintAndColorWrapper fillTintAndColorWrapper;
    private String fontStyle = null;
    private String fillColor = null;
    private Double fillTint = null;
    private Double spaceAfter = null;
    private Double spaceBefore = null;
    private Justification justification = null;
    private Leading leading = null;
    private Double autoLeading = null;
    private Double pointSize = null;
    private AppliedFont appliedFont = null;

    //tolti preceding types perchè metto il textblock
    /**
     *
     * @param parent
     * @param xmlDescription
     */
    public SelectorParagraph(ISelectorObject parent, Node xmlDescription)
    {
        super(parent, xmlDescription);
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        //qui cerco solo le immagini, non ho necessità di distinguere tra immagiuni inline e non inline
        List<Node> retrieveElementChildren =
                XmlManagement.retrieveElementChildren(xmlDescription);
        Node elementoTag = null; 
        if ((retrieveElementChildren != null) && (!retrieveElementChildren.isEmpty()))
        {
            elementoTag = retrieveElementChildren.get(0);
        }
        if (elementoTag != null)
        {
            NamedNodeMap elementoAttributes = elementoTag.getAttributes();
            if (elementoAttributes != null)
            {
                Node elementoTipoAttribute = elementoAttributes.getNamedItem(
                        "tipo");
                String elementoTipo = null;
                if (elementoTipoAttribute != null)
                {
                    elementoTipo = elementoTipoAttribute.getTextContent();
                    if (elementoTipo.equalsIgnoreCase("none") || elementoTipo.
                            equalsIgnoreCase("[none]"))
                    {
                        elementoTipo = null;
                    }
                }
                List<String> toPassTypes = getTypePath(null);
                if (elementoTipo != null)
                {
                    List<String> wrapper = new ArrayList<>();
                    wrapper.add(elementoTipo);
                    /*toPassTypes = Stream.concat(toPassTypes.stream(), wrapper.
                            stream()).
                            collect(Collectors.toList());*/
                    //in kors non è supportato java 8, va rifatto in java 7
                    List<String> tempToPassTypes = new ArrayList<>();
                    for (String inToPassTypes:toPassTypes)
                    {
                        tempToPassTypes.add(inToPassTypes);
                    }
                    for (String inWrapper:wrapper)
                    {
                        tempToPassTypes.add(inWrapper);
                    }
                    toPassTypes = tempToPassTypes;
                }
                Node candidateImmagineTag = elementoTag.getFirstChild();
                if (candidateImmagineTag.getNodeName().equals("immagine_inline"))
                {
                    SelectorInlineImage inlineImage = new SelectorInlineImage(
                            this,
                            candidateImmagineTag, toPassTypes);
                    toReturn.add(inlineImage);
                }
            }

        }

        return toReturn;
    }

    public AppliedFont getAppliedFont() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (appliedFont == null)
        {
            appliedFont = calculateAppliedFont();
        }
        return appliedFont;
    }

    protected AppliedFont calculateAppliedFont() throws
            SelectorDocumentException, URISyntaxException, IOException,
            IdmlLibException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            AppliedFont candidateAppliedFont = searchingStyle.getAppliedFont();
            if (candidateAppliedFont != null)
            {
                return candidateAppliedFont;
            }
        }
        return null;
    }

    public Double getPointSize() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (pointSize == null)
        {
            pointSize = calculatePointSize();
        }
        return pointSize;
    }

    protected Double calculatePointSize() throws IdmlLibException,
            SelectorDocumentException, IOException, URISyntaxException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            Double candidatePointSize = searchingStyle.getPointSize();
            if (candidatePointSize != null)
            {
                return candidatePointSize;
            }
        }
        return null;
    }

    public Double getAutoLeading() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (autoLeading == null)
        {
            autoLeading = calculateAutoLeading();
        }
        return autoLeading;
    }

    protected Double calculateAutoLeading() throws IOException, IdmlLibException,
            URISyntaxException, SelectorDocumentException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            Double candidateAutoLeading = searchingStyle.getAutoLeading();
            if (candidateAutoLeading != null)
            {
                return candidateAutoLeading;
            }
        }
        return null;
    }

    /**
     * Get the value of fontStyle
     *
     * @return the value of fontStyle
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public String getFontStyle() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (fontStyle == null)
        {
            fontStyle = calculateFontStyle();
        }
        return fontStyle;
    }

    private String calculateFontStyle() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            String candidateFontStyle = searchingStyle.getFontStyle();
            if (candidateFontStyle != null)
            {
                return candidateFontStyle;
            }
        }
        return null;
    }

    public Leading getLeading() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (leading == null)
        {
            leading = calculateIdmlLeading();
        }
        return leading;
    }

    protected Leading calculateIdmlLeading() throws IOException,
            SelectorDocumentException, URISyntaxException, IdmlLibException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            Leading candidateLeading = searchingStyle.getLeading();
            if (candidateLeading != null)
            {
                return candidateLeading;
            }
        }
        return null;
    }

    protected Double calculatedLeading = null;

    public Double getCalculatedLeading() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (calculatedLeading == null)
        {
            calculatedLeading = calculateCalculatedLeading();
        }
        return calculatedLeading;

    }

    protected Double calculateCalculatedLeading() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        Leading currentLeading = getLeading();
        if (currentLeading != null)
        {
            String stringLeading = currentLeading.getValueAsString();
            if (stringLeading.equals("Auto"))
            {
                double currentAutoLeading = getAutoLeading();
                double currentPointSize = getPointSize();
                return (currentAutoLeading * currentPointSize) / 100.0;
            } else
            {
                return currentLeading.getValueAsDouble();
            }

        }
        return null;
    }

    /**
     * estrae il contenuto testuale del paragrafo (assumendo che ci sia un solo
     * elemento)
     *
     * @return il contenuto testuale del paragrafo
     */
    public String getTextContent()
    {
        //se contiene solo un elemento con il testo, cerco il testo

        Node leaf = XmlManagement.retrieveLeafNode(xmlDescription);
        if (leaf != null)
        {
            String textContent = leaf.getTextContent();

            return textContent;
        }

        return null;
    }

    /**
     * Get the value of appliedParagraphStyle
     *
     * @return the value of appliedParagraphStyle
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public ParagraphStyle getAppliedParagraphStyle() throws
            URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        if (appliedParagraphStyle == null)
        {
            appliedParagraphStyle = calculateAppliedParagraphStyle();
        }
        return appliedParagraphStyle;
    }

    protected ParagraphStyle calculateAppliedParagraphStyle() throws
            URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        SelectorDocument currentParentSelectorDocument =
                this.getParentSelectorDocument();
        Document supertemplateDocument =
                currentParentSelectorDocument.getSupertemplateDocument();
        List<String> currentTypePath = getTypePath(null);
        String paragraphStyleName =
                IdmlUtils.convertTypePathToIdmlName(currentTypePath);
        ParagraphStyle found = IdmlUtils.retrieveParagraphStyleByName(
                supertemplateDocument, paragraphStyleName);
        return found;
    }

    @Override
    protected List<SelectorParagraph> calculateContainedSelectorParagraphs()
            throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorParagraph> wrapper = new ArrayList<>();
        wrapper.add(this);
        return wrapper;
    }

    /**
     * trova lo stile di paragrafo di questo oggetto e quelli in cui si basa, in
     * modo da poter cercare le proprietà grafiche
     *
     * @return the value of paragraphStylesChain
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    protected List<ParagraphStyle> getParagraphStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException, URISyntaxException
    {
        if (paragraphStylesChain == null)
        {
            paragraphStylesChain = calculateParagraphStylesChain();
        }
        return Collections.unmodifiableList(paragraphStylesChain);
    }

    protected List<ParagraphStyle> calculateParagraphStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        return IdmlUtils.retrieveParagraphStylesCurrentAndBasedOnChain(
                getParentSelectorDocument().getSupertemplateDocument(),
                getAppliedParagraphStyle());
    }

    public String getFillColor() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (fillColor == null)
        {
            fillColor = calculateFillColor();
        }
        return fillColor;
    }

    private String calculateFillColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            String candidateFillColor = searchingStyle.getFillColor();
            if (candidateFillColor != null)
            {
                return candidateFillColor;
            }
        }
        return null;
    }

    public Double getFillTint() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (fillTint == null)
        {
            fillTint = calculateFillTint();
        }
        return fillTint;
    }

    private Double calculateFillTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            Double tintValue = searchingStyle.getFillTint();
            if (tintValue != null && tintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
            {
                return tintValue;
            }
        }
        return null;
    }

    public TintAndColorWrapper getFillTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        if (fillTintAndColorWrapper == null)
        {
            fillTintAndColorWrapper = calculateFillTintAndColorWrapper();
        }
        return fillTintAndColorWrapper;
    }

    private TintAndColorWrapper calculateFillTintAndColorWrapper() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        String fillColorOrTintSelf = getFillColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        fillColorOrTintSelf);
        Double fillTintValue = getFillTint();
        if (fillTintValue != null && fillTintValue >= 0)//se <0 vuol dire che usa il valore ereditato, quindi lo prende dal colore
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                createTintAndColorWrapperBySelf.setTintValue(fillTintValue);
            }

        }

        return createTintAndColorWrapperBySelf;
    }

    public Double getSpaceAfter() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (spaceAfter == null)
        {
            spaceAfter = calculateSpaceAfter();
        }
        return spaceAfter;
    }

    private Double calculateSpaceAfter() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            Double space = searchingStyle.getSpaceAfter();
            if (space != null)
            {
                return space;
            }
        }
        return null;
    }

    public Double getSpaceBefore() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (spaceBefore == null)
        {
            spaceBefore = calculateSpaceBefore();
        }
        return spaceBefore;
    }

    private Double calculateSpaceBefore() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            Double space = searchingStyle.getSpaceBefore();
            if (space != null)
            {
                return space;
            }
        }
        return null;
    }

    public Justification getJustification() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (justification == null)
        {
            justification = calculateJustification();
        }
        return justification;
    }

    private Justification calculateJustification() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<ParagraphStyle> searchingChain =
                getParagraphStylesChain();
        for (ParagraphStyle searchingStyle : searchingChain)
        {
            Justification candidateJustification = searchingStyle.
                    getJustification();
            if (candidateJustification != null)
            {
                return candidateJustification;
            }
        }
        return null;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        appliedFont = null;
        appliedParagraphStyle = null;
        autoLeading = null;
        calculatedLeading = null;
        fillColor = null;
        fillTint = null;
        fillTintAndColorWrapper = null;
        fontStyle = null;
        justification = null;
        leading = null;
        paragraphStylesChain = null;
        pointSize = null;
        spaceAfter = null;
        spaceBefore = null;

    }

}
