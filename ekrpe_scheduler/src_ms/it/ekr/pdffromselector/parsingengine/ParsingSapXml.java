package it.ekr.pdffromselector.parsingengine;

import com.ekrpe.scheduler.model.*;
import com.ekrpe.scheduler.runnable.*;

import static com.ekrpe.scheduler.runnable.TaskBase.STATUS_COMPLETED;
import static com.ekrpe.scheduler.runnable.TaskBase.STATUS_RUNNING;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.itextpdf.text.Document;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import de.fhcon.idmllib.api.*;
import de.fhcon.idmllib.api.datatypes.enums.*;
import de.fhcon.idmllib.api.elements.*;
import de.fhcon.idmllib.api.elements.complexproperties.*;
import it.ekr.coordinatestransformer.*;
import it.ekr.pdffromselector.*;
import static it.ekr.pdffromselector.PdfFromSelector.PDF_POST_EXAMPLE_PROPERTY;
import static it.ekr.pdffromselector.PdfFromSelector.PDF_SEND_URL_PROPERTY;
import it.ekr.pdffromselector.parsingengine.exceptions.pdfexceptions.*;
import it.ekr.pdffromselector.parsingengine.exceptions.sapfileexceptions.*;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.*;
import it.ekr.pdffromselector.parsingengine.selectorobjects.*;
import it.ekr.pdffromselector.parsingengine.utils.*;
import it.ekr.progressbarcalculator.*;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.logging.*;

import javax.xml.parsers.*;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 *
 * @author Marco Scarpa
 */
public class ParsingSapXml extends PdfTask
{

    /**
     * tag radice
     */
    public static final String SOURCE = "source";

    /**
     * tag contenente le varie multipages
     */
    public static final String MULTIPAGES = "multipages";

    /**
     * attributo per il percorso del file generato
     */
    public static final String GENERATED_FILE = "generated_file";

    /**
     * attributo indicante il documento selector
     */
    public static final String TEMPLATE = "template";

    /**
     * tag contenente la singola multipage
     */
    public static final String MULTIPAGE = "multipage";

    /**
     * attributo indicasnte il tipo di multipage e quindi il fascicoo associato
     */
    public static final String TYPE = "type";

    /**
     * tag contenente il singolo componente
     */
    public static final String COMPONENT = "component";

    /**
     * tag con le righe corpo della tabella
     */
    public static final String BODY = "body";

    /**
     * tag con le righe intestazione della tabella
     */
    public static final String HEAD = "head";

    /**
     * tag con le righe di piede della tabella
     */
    public static final String FOOT = "foot";

    /**
     * tag con la riga di tabella
     */
    public static final String ROW = "row";

    /**
     * tag della cella di tabella
     */
    public static final String CELL = "cell";

    /**
     * attributo dello span di colonna della cella di tabella
     */
    public static final String HSPAN = "h-span";
    /**
     * cartella di default dei documenti, da usare attualmente per il doRun()
     */
    public static String DEFAULT_TEMPLATE_BASE_DIR = "W:\\smartdocs";

    /**
     * csrtella di default per le impostazioni
     */
    public static String DEFAULT_ROOT_PE = "C:\\EKR-PE";

    /**
     * sottocartella di default rispetto alla cartella delle impostazioni
     */
    public static String TE_SETTINGS_SUBDIR = "settaggi" + File.separator + "TE";

    /**
     * file di configurazione di default
     */
    public static String TE_CONFIG_FILE = "config.conf";

    /**
     * variabile d'ambiente atta a contenere il percorso della cartella radice delle impostazioni
     */
    public static String ROOT_PE_ENV_VAR = "ROOT_PE";

    public static String TEMPLATE_BASE_DIR_PROPERTY = "TEMPLATE_DIR";

    public static String IDML_LIB_LICENSE_PROPERTY = "IDML_LIB_LICENSE";

    public static String DEFAULT_EMPTY_ESCAPE_SEQUENCE = "|c|";

    public static String EMPTY_ESCAPE_SEQUENCE_PROPERTY = "EMPTY_ESCAPE";

    public static final String POST = "POST";
    /**
     * codice di licenza di idmllib di default
     */
    public static String DEFAULT_IDML_LIB_LICENSE
            = PdfFromSelector.IDML_LIB_LICENSE;

    /**
     * interpreta il file XML da SAP
     *
     * @param xmlFromSapFile il file XML proveniente da SAP
     * @param baseTemplatesDir la cartella contenente i documenti di Selector
     * @param idmlLibLicense la stringa con il codice di licenza per IDMLLIB
     * @param emptyEscape la stringa che rappresenta lo spazio vuoto
     * @param pdfSendUrl url a cui spedire il documento prodotto
     * @param postContent contenuti del post con cui inviare il documento
     * @param toDeletePaths percorsi da cancellare finita l'elaborazione
     * @return il documento creato
     * @throws it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws it.ekr.pdffromselector.parsingengine.exceptions.sapfileexceptions.SapFileException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws com.itextpdf.text.DocumentException
     * @throws it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     * @throws java.io.FileNotFoundException
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    public Document parseSapFile(File xmlFromSapFile, File baseTemplatesDir,
                                 String idmlLibLicense, String emptyEscape, URL pdfSendUrl, JsonObject postContent, List<String> toDeletePaths)
            throws SelectorDocumentException, SapFileException,
                   ParserConfigurationException, SAXException, IOException,
                   URISyntaxException, IdmlLibException, DocumentException,
                   DOMException, SelectorFileNotExistentException,
                   FileNotFoundException, NoSuchFieldException,
                   IllegalArgumentException, IllegalAccessException, Exception
    {
        Document pdf = null;
        if (xmlFromSapFile == null)
        {
            throw new SapFileNotChoosedException(
                    "file xml da SAP non selezionato");
        }
        if (xmlFromSapFile.exists())
        {
            DocumentBuilderFactory newInstance
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder newDocumentBuilder
                    = newInstance.newDocumentBuilder();
            Node xmlFromSapXml = newDocumentBuilder.parse(xmlFromSapFile);
            pdf = parseSapXml(xmlFromSapXml, baseTemplatesDir, idmlLibLicense,
                              emptyEscape, pdfSendUrl, postContent, toDeletePaths);

        } else
        {
            throw new SapFileNotExistentException("file xml " + xmlFromSapFile.getAbsolutePath() + " non esistente");
        }
        return pdf;
    }

    /**
     * metodo invocato durante la creazione dei pdf da parte del processo, usa i
     * dati dal file di configurazione per stabilire dove trovare i files e il
     * codice di licenza per la libreria IDMLLIB
     */
    @Override
    public void doRun()
    {
        setStatus(STATUS_RUNNING);
        Document pdf = null;
        boolean errorOccured = false;
        addLog(new LoggerVO("1", "Starting processing of SAP file "
                            + getDataSourceFilePath()));
        String dataSourceFilePath = getDataSourceFilePath();

        URL pdfSendUrl = null;
        JsonObject pdfPostExample = null;
        try
        {
            File sapFile = new File(dataSourceFilePath);
            sapFile = converEventuallytFromUriPathToFile(dataSourceFilePath,
                                                         sapFile);
            String env = System.getenv(ROOT_PE_ENV_VAR);
            String rootPe = DEFAULT_ROOT_PE;
            if (env != null)
            {
                rootPe = env;
            }
            File rootPeFile = new File(rootPe);
            rootPeFile = converEventuallytFromUriPathToFile(rootPe, rootPeFile);
            Path rootPePath = rootPeFile.toPath();
            Path settingsTeDir = rootPePath.resolve(TE_SETTINGS_SUBDIR);
            Path teConfigPath = settingsTeDir.resolve(TE_CONFIG_FILE);
            File teConfigFile = teConfigPath.toFile();
            Properties configProperties = new Properties();
            if (teConfigFile.exists())
            {
                FileInputStream teConfigStream = new FileInputStream(
                        teConfigFile);
                configProperties.load(teConfigStream);
            }

            String templateBaseDirPropertyValue
                    = configProperties.getProperty(TEMPLATE_BASE_DIR_PROPERTY,
                                                   DEFAULT_TEMPLATE_BASE_DIR);

            File baseDir = new File(templateBaseDirPropertyValue);

            String idmlLibLicense = configProperties.getProperty(
                    IDML_LIB_LICENSE_PROPERTY, DEFAULT_IDML_LIB_LICENSE);

            String pdfSendUrlValue = configProperties.getProperty(PDF_SEND_URL_PROPERTY);
            if (pdfSendUrlValue != null)
            {
                pdfSendUrl = new URL(pdfSendUrlValue);
            }

            String pdfPostExampleValue = configProperties.getProperty(PDF_POST_EXAMPLE_PROPERTY);
            if (pdfPostExampleValue != null)
            {
                Path pdfPostExamplePath = settingsTeDir.resolve(pdfPostExampleValue);
                File pdfPostExampleFile = pdfPostExamplePath.toFile();
                JsonParser jsonParser = new JsonParser();
                FileInputStream inputStream = new FileInputStream(pdfPostExampleFile);
                InputStreamReader reader = new InputStreamReader(inputStream);
                pdfPostExample = (JsonObject) jsonParser.parse(reader);
            }

            String emptyEscape = configProperties.getProperty(
                    EMPTY_ESCAPE_SEQUENCE_PROPERTY,
                    DEFAULT_EMPTY_ESCAPE_SEQUENCE);

            //se non esiste il file config salvo i settaggi in modo da creare un file di base da modificare, se esiste gia non vado a sovrascrivere eventuali commenti dell'utente
            if (!teConfigFile.exists())
            {
                //se non esiste il file, lo creo in modo da avere un file di default da modificare
                configProperties.setProperty(TEMPLATE_BASE_DIR_PROPERTY,
                                             templateBaseDirPropertyValue);
                configProperties.setProperty(IDML_LIB_LICENSE_PROPERTY,
                                             idmlLibLicense);
                configProperties.setProperty(EMPTY_ESCAPE_SEQUENCE_PROPERTY,
                                             emptyEscape);
                if (pdfSendUrl != null)
                {
                    configProperties.setProperty(PDF_SEND_URL_PROPERTY, pdfSendUrl.toString());
                }
                if (pdfPostExampleValue != null)
                {
                    configProperties.setProperty(PDF_POST_EXAMPLE_PROPERTY, pdfPostExampleValue);
                }
                //aggiungo qui i valori provenienti dalla properties embedded
                ResourceBundle res = ResourceBundle
                        .getBundle("com.ekrpe.scheduler.application");
                Enumeration<String> embeddedKeys = res.getKeys();
                while (embeddedKeys.hasMoreElements())
                {
                    String key = embeddedKeys.nextElement();
                    String value = res.getString(key);
                    configProperties.setProperty(key, value);
                }
                File settingsTeDirFile = settingsTeDir.toFile();
                settingsTeDirFile.mkdirs();//creo le eventusali cartelle mancanti
                FileOutputStream outputStream = new FileOutputStream(
                        teConfigFile);
                configProperties.store(outputStream, "impostazioni EKR-TE");

            }

            List<String> toDeletePaths = null;//per il momento i percorsi da cancellare non sono definiti da file di configurazione
            pdf = parseSapFile(sapFile, baseDir, idmlLibLicense, emptyEscape, pdfSendUrl, pdfPostExample, toDeletePaths);

        } catch (SelectorDocumentException |
                SapFileException |
                ParserConfigurationException | SAXException |
                IOException | URISyntaxException |
                IdmlLibException | DocumentException | DOMException |
                NoSuchFieldException | IllegalArgumentException |
                IllegalAccessException ex)
        {
            setStatus(STATUS_ERROR);
            Logger.getLogger(ParsingSapXml.class.getName()).
                    log(Level.SEVERE, null, ex);
            errorOccured = true;
        } catch (Exception ex)
        {
            setStatus(STATUS_ERROR);
            Logger.getLogger(ParsingSapXml.class.getName()).
                    log(Level.SEVERE, null, ex);
            errorOccured = true;
        }

        if (pdf == null)
        {
            if (!errorOccured)
            {
                //se non ho il pdf non posso vedere l'evento di chiusura del file
                signalProcessEnded();
            } else
            {
                addLog(new LoggerVO("2", "Finished with error"));
            }

        }

    }

    /**
     * @param possibleUriPath
     * @param originalFile
     * @return
     * @throws URISyntaxException
     */
    private File converEventuallytFromUriPathToFile(String possibleUriPath,
                                                    File originalFile) throws URISyntaxException
    {
        if (possibleUriPath != null && possibleUriPath.startsWith("file:"))
        {
            URI fileUri = new URI(possibleUriPath);
            originalFile = new File(fileUri);
        }
        return originalFile;
    }

    public void signalProcessEnded()
    {
        setStatus(STATUS_COMPLETED);
        addLog(new LoggerVO("2", "Finished"));
    }

    /**
     * interpreta i dati xml da SAP, se non si parte da file
     *
     * @param xmlFromSapXml l'xml proveniente da SAP (potrebbe arrivare da web
     * sercvices)
     * @param baseTemplatesDir cartella in cui cercare i documenti di Selector
     * @param idmlLibLicense la stringa del codice di licenza per la libreria di
     * interpretazione dell'IDML
     * @param emptyEscape la stringa per indicare uno spazio in xml
     * @param pdfSendUrl url a cui inviare il pdf
     * @param postContent contenuto del pot per inviare l'url
     * @param toDeletePaths percorsi da cancellare
     * @return il documento PDF
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     */
    public Document parseSapXml(
            Node xmlFromSapXml, File baseTemplatesDir, String idmlLibLicense,
            String emptyEscape, URL pdfSendUrl, JsonObject postContent, List<String> toDeletePaths)
            throws SAXException,
                   IOException, DOMException, IdmlLibException,
                   URISyntaxException,
                   ParserConfigurationException, DocumentException,
                   SelectorDocumentException, SelectorFileNotExistentException,
                   FileNotFoundException, NoSuchFieldException,
                   IllegalArgumentException, IllegalAccessException, Exception
    {
        //l'array può venire da file esterno e anche l'url oppure dall'xml per impostazioni specifiche
        Document pdf = null;
        List<Node> sourceNodes
                = XmlManagement.retrieveChildrenGivenName(xmlFromSapXml,
                                                          SOURCE);
        Node sourceNode = sourceNodes.get(0);
        NamedNodeMap sourceNodeAttributes = sourceNode.getAttributes();
        List<Node> multipagesNodes
                = XmlManagement.retrieveNodesRecByName(xmlFromSapXml,
                                                       MULTIPAGES);
        //assumo che ci sia solo un nodo multipagine, come da schema
        Node multipagineNode = multipagesNodes.get(0);
        /* NamedNodeMap multipagineNodeAttributes = multipagineNode.
         getAttributes();*/
        Node templateAttribute = sourceNodeAttributes.getNamedItem(
                TEMPLATE);
        String templateAttributeValue = templateAttribute.getTextContent();

        Node sendUrlAtttribute = sourceNodeAttributes.getNamedItem(PdfFromSelector.PDF_SEND_URL_OPTION);

        Node postContentAttribute = sourceNodeAttributes.getNamedItem(PdfFromSelector.PDF_POST_EXAMPLE_OPTION);

        if (sendUrlAtttribute != null)
        {
            String sendUrlValue = sendUrlAtttribute.getTextContent();
            if (!sendUrlValue.equals(""))
            {
                pdfSendUrl = new URL(sendUrlValue);
            } else
            {
                //se passo una stringa vuota vuol dire che non voglio collegarmi
                pdfSendUrl = null;
            }

        }

        if (postContentAttribute != null)
        {
            String postContentValue = postContentAttribute.getTextContent();
            JsonParser jsonParser = new JsonParser();
            postContent = (JsonObject) jsonParser.parse(postContentValue);
        }

        try
        {
            return parseSapXmlLastPart(sourceNodeAttributes, postContent, templateAttributeValue, baseTemplatesDir, idmlLibLicense, pdf, multipagineNode, emptyEscape, pdfSendUrl, toDeletePaths);
        } catch (Exception e)
        {
            //se c'è stata un'eccezione reinvio il messaggio di errore come feedback
            sendPdfToUrl(pdfSendUrl, postContent, null, e.getLocalizedMessage(), false, null);
            throw e;
        }

    }

    public Document parseSapXmlLastPart(NamedNodeMap sourceNodeAttributes, JsonObject postContent, String templateAttributeValue, File baseTemplatesDir, String idmlLibLicense, Document pdf, Node multipagineNode, String emptyEscape, URL pdfSendUrl, List<String> toDeletePaths) throws DOMException, IOException, Exception, JsonSyntaxException
    {
        Node postContentAttribute = sourceNodeAttributes.getNamedItem(PdfFromSelector.PDF_POST_EXAMPLE_OPTION);
        //a differenza dell'impostazione con il file di config o da linea di comendo, non definisce un file ma mostra direttamente l'esempio
        if (postContentAttribute != null)
        {
            String postContentString = postContentAttribute.getTextContent();
            if (!postContentString.equals(""))
            {
                JsonParser jsonParser = new JsonParser();
                postContent = (JsonObject) jsonParser.parse(postContentString);
            } else
            {
                //se passo una stringa vuota, vuol dire che voglio un vettore vuoto
                //da correggere, va passato un oggetto json
                postContent = new JsonObject();
            }

        }

        String pdfFileName = "documento.pdf";
        
       

        Node generatedFileAttribute = sourceNodeAttributes.getNamedItem(
                GENERATED_FILE);
        if (generatedFileAttribute != null)
        {
            String generatedFileAttributeValue = generatedFileAttribute.
                    getTextContent();
            if (generatedFileAttributeValue != null
                    && !generatedFileAttributeValue.isEmpty())
            {
                pdfFileName = generatedFileAttributeValue;
                if (!pdfFileName.endsWith(".pdf"))
                {
                    pdfFileName += ".pdf";
                }
            }
        }
        
        

        File pdfFile = new File(pdfFileName);
        pdfFile = converEventuallytFromUriPathToFile(pdfFileName, pdfFile);
        
        String finalPdfFilename = pdfFile.getName();
        
        int lastDotPos = finalPdfFilename.lastIndexOf(".");//creo il pdf come file temporaneo in modo da poterlo cancellare
        String firstFileNamePart = finalPdfFilename.substring(0, lastDotPos);
        String extension = finalPdfFilename.substring(lastDotPos);
        
        File tempFullPdf = File.createTempFile(firstFileNamePart, extension);

        Node toDeleteFiles = sourceNodeAttributes.getNamedItem(PdfFromSelector.TO_DELETE_FILES_OPTION);
        if (toDeleteFiles != null)
        {
            if (toDeletePaths == null)//se non è nullo comanda la riga di comando
            {
                toDeletePaths = new ArrayList<>();
                String toDeleteFilesString = toDeleteFiles.getTextContent();
                if (toDeleteFilesString.startsWith("["))
                {
                    //se è un vettore, assumo che sia un vettore json di percorsi o nomi file
                    JsonParser jsonParser = new JsonParser();
                    JsonArray toDeleteFilesJA = (JsonArray) jsonParser.parse(toDeleteFilesString);
                    Iterator<JsonElement> iterator = toDeleteFilesJA.iterator();
                    while (iterator.hasNext())
                    {
                        JsonElement next = iterator.next();
                        String toDeletePathToStore = next.getAsString();
                        toDeletePaths.add(toDeletePathToStore);
                    }
                } else
                {
                    toDeletePaths.add(toDeleteFilesString);
                }
            }

        }

        File selectorDocumentFolder;
        String templateTerminalFolder = templateAttributeValue;
        if (!templateTerminalFolder.endsWith(".ekrs"))
        {
            templateTerminalFolder += ".ekrs";
        }
        Path baseTemplateDirPath = baseTemplatesDir.toPath();
        Path completeSelectorDocumentPath = baseTemplateDirPath.resolve(
                templateTerminalFolder);
        selectorDocumentFolder = completeSelectorDocumentPath.toFile();
        if (selectorDocumentFolder.exists())
        {
            Idml.setLicense(idmlLibLicense);
            pdf = createPdfDocument(tempFullPdf, selectorDocumentFolder,
                                    multipagineNode, emptyEscape);
            //salvo ora la copia compressa
            Document.compress = true;
            PdfReader reader = new PdfReader(new FileInputStream(
                    tempFullPdf));
            reader.eliminateSharedStreams();
            Document copyDoc = new Document();
            int pagesNum = reader.getNumberOfPages();
            PdfCopy copier = new PdfSmartCopy(copyDoc, new FileOutputStream(pdfFile));
            copier.setFullCompression();
            copyDoc.open();
            for (int i = 0; i < pagesNum;)
            {
                copier.addPage(copier.getImportedPage(reader, ++i));
            }
            copyDoc.close();
            tempFullPdf.delete();
        } else
        {
            String errorMessage = "Cartella documento Selector " + selectorDocumentFolder.
                    getAbsolutePath() + " non esistente";

            throw new SelectorFileNotExistentException(
                    errorMessage);
        }
        if (pdf != null)
        {
            // se ho creato il pdf lo leggo e lo invio
            if (!pdf.isOpen())
            {
                List<File> deletedPdfs = deleteOtherPdfs(pdfFile, toDeletePaths);
                if (pdfSendUrl != null)
                {
                    sendPdfToUrl(pdfSendUrl, postContent, pdfFile, "", true, deletedPdfs);
                }

            } else
            {
                if (pdfSendUrl != null)
                {
                    //il file esiste ma non è stato creato
                    sendPdfToUrl(pdfSendUrl, postContent, pdfFile, "", false, null);
                }
            }
        } else
        {
            //il pdf creato non c'è, quindi non posso mettere il contenuto
            sendPdfToUrl(pdfSendUrl, postContent, null, "", false, null);
        }
        return pdf;
    }

    private synchronized List<File> deleteOtherPdfs(File pdfFile, List<String> otherPaths) throws URISyntaxException, IOException
    {
        if (otherPaths != null)
        {
            List<File> toReturn = new ArrayList<>();
            for (String otherPath : otherPaths)
            {
                File otherFile = new File(otherPath);
                otherFile = converEventuallytFromUriPathToFile(otherPath, otherFile);
                if (!otherFile.exists())
                {
                    File pdfFileDir = pdfFile.getParentFile();
                    if (pdfFileDir != null)
                    {
                        //se non ho un percorso o un url completo di un file esistente provo a risolbverlo relativo al file pdf creato
                        Path pdfFileDirPath = pdfFileDir.toPath();
                        Path resolvedOther = pdfFileDirPath.resolve(otherPath);
                        File resolvedOtherFile = resolvedOther.toFile();
                        if (resolvedOtherFile.exists())
                        {
                            otherFile = resolvedOtherFile;
                        }

                    }
                }

                if (otherFile.exists())
                {
                    boolean canDelete = otherFile.renameTo(otherFile); //provo a spostare il file su sè stesso, se non si può non può essere cancellato
                    if (canDelete)
                    {
                        otherFile.delete();
                        toReturn.add(otherFile);
                    }
                }

            }
            return toReturn;
        }

        return null;
    }

    private synchronized void sendPdfToUrl(URL pdfSendUrl, JsonObject postContent, File pdfFile, String errorMessage, boolean created, List<File> deletedPdfs) throws IOException
    {

        //devo eseguire una richiesta alla volta, soprattutto per il debug
        if ((postContent != null) && (pdfSendUrl != null))//se è null non invio nulla
        {
            //per la questione dei cookies bisogna usare HttpClient
            Base64.Encoder encoder = Base64.getEncoder();
            /*
             String userInfo = pdfSendUrl.getUserInfo();
           
             String userInfoEncoded = null;
             if (userInfo != null)
             {
             userInfoEncoded = encoder.encodeToString(userInfo.getBytes());
             }

             HttpURLConnection connection = (HttpURLConnection) pdfSendUrl.openConnection();
             if (userInfo != null)
             {
             connection.setRequestProperty("Authorization", "Basic " + userInfoEncoded);
             }

             connection.setRequestMethod(POST);
            
             */

            HttpClient httpClient = new HttpClient();
            HttpState httpState = httpClient.getState();
            HttpClientParams httpClientParams = httpClient.getParams();

            PostMethod post = new PostMethod(pdfSendUrl.toString());
            //post.setFollowRedirects(true); //non si può redirezionare senza intervento utente, causa eccezione
            HttpMethodParams httpMethodParams = post.getParams();
            httpMethodParams.setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, false));
            String userInfo = pdfSendUrl.getUserInfo();
            if (userInfo != null)
            {
                List<String> authPrefs = new ArrayList<String>(2);
                //authPrefs.add(AuthPolicy.DIGEST);  non so se è supportata l'autenticazione digest
                authPrefs.add(AuthPolicy.BASIC);
                httpClientParams.setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
                Credentials credentials = new UsernamePasswordCredentials(userInfo);
                httpState.setCredentials(AuthScope.ANY, credentials);
                httpClientParams.setAuthenticationPreemptive(true);
                post.setDoAuthentication(true);

            }

            String stringToSend;
            if (pdfFile != null)
            {
                FileInputStream fileInStream = new FileInputStream(pdfFile);
                int available = fileInStream.available();
                byte[] data = new byte[available];
                int read = fileInStream.read(data);
                String encoded = encoder.encodeToString(data);
                fileInStream.close();
                long lastModified = pdfFile.lastModified();
                ZonedDateTime modificationInstant;
                LocalDateTime localDateTime;
                Instant instant = Instant.ofEpochMilli(lastModified);
                localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                modificationInstant = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
                stringToSend = createStringToSend(postContent, encoded, getJobId(), pdfFile, errorMessage, created, modificationInstant, deletedPdfs);
            } else
            {
                stringToSend = createStringToSend(postContent, "", getJobId(), null, errorMessage, created, null, deletedPdfs);
            }
            /*
             sendStringUsingConnection(connection, stringToSend);//devo aprire la connessione qui dentro perchè solo quando ho creato la stringa posso saperne le dimensioni che è un parametro di connessione
             */
            sendStringUsingHttpClientAndMethod(httpClient, post, stringToSend);
        }

    }

    protected void sendStringUsingHttpClientAndMethod(HttpClient client, PostMethod post, String stringToSend) throws HttpException, IOException
    {
        StringRequestEntity entityToSend = new StringRequestEntity(stringToSend, "text/plain", "UTF-8");//non avendo un tipo prestabilito, indico che la risposta è testo plain
        post.setRequestEntity(entityToSend);
        client.executeMethod(post);
        HttpState httpState = client.getState();
        Cookie[] cookies = httpState.getCookies();
        post.releaseConnection();
        if (isIntoSapSession(cookies))
        {
            org.apache.commons.httpclient.URI postUri = post.getURI();
            String host = postUri.getHost();
            int port = postUri.getPort();
            String protocol = postUri.getScheme();
            String userInfo = postUri.getUserinfo();
            org.apache.commons.httpclient.URI tokenRequestURI = new org.apache.commons.httpclient.URI(protocol, userInfo, host, port, "/sap/hana/xs/formLogin/token.xsjs");
            GetMethod tokenRequestGet = new GetMethod(tokenRequestURI.toString());
            HttpMethodParams tokenRequestHttpMethodParams = tokenRequestGet.getParams();
            //tokenRequestGet.setFollowRedirects(true); //non si può redirezionare senza intervento utente, causa eccezione
            tokenRequestHttpMethodParams.setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, false));
            tokenRequestGet.setDoAuthentication(true);
            tokenRequestGet.setRequestHeader("X-CSRF-Token", "Fetch");
            client.executeMethod(tokenRequestGet);
            Header[] tokenList = tokenRequestGet.getResponseHeaders("x-csrf-token");
            if (tokenList.length > 0)
            {
                Header tokenHeader = tokenList[0];
                String token = tokenHeader.getValue();
                if ("unsafe".equals(token))
                {
                    //sessione già scaduta, non devo chiuderla
                    tokenRequestGet.abort();
                    return;
                } else
                {
                    tokenRequestGet.releaseConnection();
                    org.apache.commons.httpclient.URI sessionClosingUri = new org.apache.commons.httpclient.URI(protocol, userInfo, host, port, "/sap/hana/xs/formLogin/logout.xscfunc");
                    PostMethod sessionClosingPost = new PostMethod(sessionClosingUri.toString());
                    sessionClosingPost.setDoAuthentication(true);
                    //sessionClosingPost.setFollowRedirects(true); //non si può redirezionare senza intervento utente, causa eccezione
                    HttpMethodParams sessionClosingPostParams = sessionClosingPost.getParams();
                    sessionClosingPostParams.setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, false));
                    sessionClosingPost.setRequestHeader("X-CSRF-Token", token);
                    client.executeMethod(sessionClosingPost);
                    sessionClosingPost.releaseConnection();
                }
            } else
            {
                tokenRequestGet.abort();
            }

        }

    }

    protected boolean isIntoSapSession(Cookie[] cookies)
    {
        if (cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                if ("sapxslb".equals(cookie.getName()))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static final int HTTP_REDIRECT = 302;

    public void sendStringUsingConnection(HttpURLConnection connection, String stringToSend) throws IOException
    {
        connection.setRequestProperty("Content-Length",
                                      "" + Integer.toString(stringToSend.getBytes().length));

        connection.setDoOutput(true);
        OutputStream connectionOutputStream = connection.getOutputStream();
        OutputStreamWriter connectionWriter = new OutputStreamWriter(connectionOutputStream);
        connectionWriter.write(stringToSend);
        connectionWriter.flush();
        connectionWriter.close();
        connectionOutputStream.close();
        connection.connect();
        if (connection.getResponseCode() == HTTP_REDIRECT)
        {
            //redirect, devo chiamare un altro url
            Map<String, List<String>> headers = connection
                    .getHeaderFields();
            if (headers.containsKey(LOCATION_FIELD))
            {
                List<String> locationList = headers.get(LOCATION_FIELD);
                String location = locationList.get(0);
                connection.disconnect();
                URL pdfSendUrl = new URL(location);
                connection = (HttpURLConnection) pdfSendUrl.openConnection();
                connection.setRequestMethod(POST);
                connection.connect();
            }
        }

        connection.disconnect();
    }
    public static final String LOCATION_FIELD = "Location";

    private String createStringToSend(JsonObject postContent, String pdfEncoded, Long jobId, File file, String errorMessage, boolean generated, ZonedDateTime creationInstant, List<File> deletedPdfs) throws IOException
    {
        //dall'oggetto json mi viene detto il tipo di output (per fare gli eventuali escape) e come deve essere costruita la stringa
        JsonElement outputTypeElement = postContent.get(OUTPUT_TYPE);
        String outputType = null;
        if (outputTypeElement != null)
        {
            outputType = outputTypeElement.getAsString();
        }

        JsonElement exampleElement = postContent.get(EXAMPLE);
        JsonArray example = (JsonArray) exampleElement;
        String toReturn = "";
        for (int i = 0; i < example.size(); i++)
        {
            JsonElement examiningElement = example.get(i);
            if (examiningElement.isJsonPrimitive())
            {
                JsonPrimitive primitive = (JsonPrimitive) examiningElement;
                String jsonStringField = primitive.getAsString();
                toReturn = toReturn + jsonStringField;
            } else if (examiningElement.isJsonObject())
            {
                JsonObject jsonObj = (JsonObject) examiningElement;
                String parsedObj = parseJsonObjectForPdfSendExample(jsonObj, pdfEncoded, jobId, file, errorMessage, generated, creationInstant, deletedPdfs, outputType);
                toReturn = toReturn + parsedObj;
            }
        }
        return toReturn;
    }
    public static final String EXAMPLE = "example";
    public static final String OUTPUT_TYPE = "output_type";

    private String parseJsonObjectForPdfSendExample(JsonObject obj, String pdfEncoded, Long jobId, File file, String errorMessage, boolean generated, ZonedDateTime creationInstant, List<File> deletedPdfs, String outputType) throws IOException
    {
        JsonElement fieldElement = obj.get(FIELD);
        String fieldContent = fieldElement.getAsString();
        JsonElement dontEscapeElement = obj.get(DONT_ESCAPE);
        boolean dontEscape = false;//assumo di norma che siano campi interni da aggiungere (quindi vadano sostituite le entities in xml e fatto escape delle virgolette,degli apostrofi e delle barre in json)
        if (dontEscapeElement != null)
        {
            dontEscape = dontEscapeElement.getAsBoolean();
        }
        //riprendere qui aggiungendo escaping
        switch (fieldContent)
        {
            case JOB_ID:
                return calculateEscaping(jobId.toString(), outputType, dontEscape);
            case PDF:
                return calculateEscaping(pdfEncoded, outputType, dontEscape);
            case PATH:
            {
                String canonicalPath = "";
                if (file != null)
                {
                    canonicalPath = file.getCanonicalPath();
                }
                return calculateEscaping(canonicalPath, outputType, dontEscape);
            }

            case FILENAME:
            {
                String fileName = "";
                if (file != null)
                {
                    fileName = file.getName();
                }
                return calculateEscaping(fileName, outputType, dontEscape);
            }
            case DELETED_PATHS:
            {

                return pipeConcatenatedFilenamesOrPaths(deletedPdfs, false);
            }
            case DELETED_FILENAMES:
            {
                return pipeConcatenatedFilenamesOrPaths(deletedPdfs, true);
            }
            case ERROR_MESSAGE:
            {
                return calculateEscaping(errorMessage, outputType, dontEscape);
            }
            case GENERATED_BOOL:
            {
                return Boolean.toString(generated);
            }
            case GENERATED_NUM:
            {
                if (generated)
                {
                    return "1";
                }
                return "0";
            }
            case CREATION_MOMENT:
            {
                if (creationInstant != null)
                {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss.SSS");
                    String formattedTime = formatter.format(creationInstant);
                    return formattedTime;
                } else
                {
                    LocalDateTime now = LocalDateTime.now();
                    //per kors il creation moment è obbligatorio, quindi va messo un valore
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss.SSS");
                    String formattedTime = formatter.format(now);
                    return formattedTime;
                }

            }
            case CREATION_MOMENT_NULLABLE:
            {
                if (creationInstant != null)
                {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss.SSS");
                    String formattedTime = formatter.format(creationInstant);
                    return formattedTime;
                } else
                {
                    //se il creation moment può essere vuoto lascio vuoto
                    return "";
                }

            }
            case CREATION_MOMENT_ISO:
            {
                if (creationInstant != null)
                {
                    DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
                    String formattedTime = creationInstant.format(formatter);
                    return formattedTime;
                } else
                {
                    ZonedDateTime now = ZonedDateTime.now();
                    //per kors il creation moment è obbligatorio, quindi va messo un valore
                    DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
                    String formattedTime = now.format(formatter);
                    return formattedTime;
                }
            }
            case CREATION_MOMENT_NULLABLE_ISO:
            {
                if (creationInstant != null)
                {
                    DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
                    String formattedTime = creationInstant.format(formatter);
                    return formattedTime;
                } else
                {
                    //se il creation moment può essere vuoto lascio vuoto
                    return "";
                }

            }
        }

        return "";
    }

    /**
     * crea l'elenco dei percorsi o dei nomi file cancellati, separati da pipe
     *
     * @param deletedPdfs l'elenco degli oggetti file per indicare i pdf
     * @param filenames se true concatena i filename, altrimenti concatena i
     * percorsi
     * @return i nomi file o i percorsi dei pdf cancellati, separati da pipe
     * @throws IOException
     */
    protected String pipeConcatenatedFilenamesOrPaths(List<File> deletedPdfs, boolean filenames) throws IOException
    {
        String toReturn = "";
        if (deletedPdfs != null)
        {
            for (File deletedPdf : deletedPdfs)
            {

                String stringToConcat = deletedPdf.getName();
                if (filenames == false)
                {
                    stringToConcat = deletedPdf.getCanonicalPath();
                }
                toReturn += stringToConcat + "|";
            }
            int length = toReturn.length();
            if (length > 0)
            {
                toReturn = toReturn.substring(0, length - 1);
            }
        }

        return toReturn;
    }
    public static final String DONT_ESCAPE = "dont_escape";
    public static final String CREATION_MOMENT = "CREATION_MOMENT";
    public static final String CREATION_MOMENT_NULLABLE = "CREATION_MOMENT_NULLABLE";
    static final String CREATION_MOMENT_ISO = "CREATION_MOMENT_ISO";
    public static final String CREATION_MOMENT_NULLABLE_ISO = "CREATION_MOMENT_NULLABLE_ISO";
    public static final String GENERATED_NUM = "GENERATED_NUM";
    public static final String GENERATED_BOOL = "GENERATED_BOOL";
    public static final String ERROR_MESSAGE = "ERROR_MESSAGE";
    public static final String PATH = "PATH";
    public static final String FILENAME = "FILENAME";
    public static final String DELETED_PATHS = "DELETED_PATHS";
    public static final String DELETED_FILENAMES = "DELETED_FILENAMES";
    public static final String PDF = "PDF";
    public static final String JOB_ID = "JOB_ID";
    public static final String FIELD = "field";

    private String calculateEscaping(String original, String outputType, boolean dontEscape)
    {
        if (dontEscape)
        {
            return original;
        }
        switch (outputType)
        {
            case "xml":
            {
                return calculateEscapingForXML(original);
            }
            case "json":
            {
                return calculateEscapingForJson(original);
            }
            default:
            {
                return original;
            }
        }

    }

    private String calculateEscapingForXML(String original)
    {
        //sostituisco le entities
        //&amp; per prima in modo da non confondere con le entities già esistenti
        String toReturn = original;
        toReturn = toReturn.replace("&", "&amp;");
        //&apos;
        toReturn = toReturn.replace("\'", "&apos;");
        //&gt;
        toReturn = toReturn.replace(">", "&gt;");
        //&lt;
        toReturn = toReturn.replace("<", "&lt;");
        //&quot;
        toReturn = toReturn.replace("\"", "&quot;");
        return toReturn;
    }

    private String calculateEscapingForJson(String original)
    {
        //sostituisco le entities
        //&amp; per prima le barre in modo da non confondere le barre aggiunte con il resto
        String toReturn = original;
        toReturn = toReturn.replace("\\", "\\\\");
        toReturn = toReturn.replace("\'", "\\'");
        //&quot;
        toReturn = toReturn.replace("\"", "\\\"");
        return toReturn;
    }

    //private Document pdf = null; //devo crearlo come parametro perche la prima pagina deve essere fatta alla creazione del documento
    private ProgressBarCalculator pbCalculator = null;

    /**
     * crea il documento PDF
     *
     * @param pdfFile il file da creare
     * @param selectorDocumentFolder la cartella contenente i documenti Selector
     * @param multipagesNode il nodo multipages nell'XML da SAP
     */
    private Document createPdfDocument(
            File pdfFile, File selectorDocumentFolder, Node multipagesNode,
            String emptyEscape)
            throws URISyntaxException, SAXException, SelectorDocumentException,
                   IOException, ParserConfigurationException,
                   SelectorFileNotExistentException, IdmlLibException,
                   FileNotFoundException, DOMException, DocumentException,
                   NoSuchFieldException, IllegalArgumentException,
                   IllegalAccessException, Exception
    {
        Document pdf = null;
        PdfWriter pdfWriter = null;
        ImageBackgrounder imageBackgrounder = null;
        HeaderFooterMaker headerFooterMaker = null;
        PdfDocumentAndWriterWrapper docAndWriter;
        PageLayoutCellsMaker layoutCellsMaker = null;
        PageNumberCreator pageNumberCreator = null;
        IdmlUtils.cleanupAllCache();
        SelectorDocument selectorDocument = new SelectorDocument(
                selectorDocumentFolder);
        //essendo appena creato il documento è pulito
        //selectorDocument.cleanupCachedData();
        List<Node> multipageList
                = XmlManagement.
                retrieveNodesRecByNameStoppingAtFirstLevelFound(
                        multipagesNode, MULTIPAGE);
        int done = 0;
        int total = multipageList.size();
        if (total == 0)
        {
            selectorDocument.cleanupCachedData();
            throw new NoTagException(MULTIPAGE);
        }
        for (Node multipage : multipageList)
        {
            if (pbCalculator == null)
            {
                pbCalculator = new ProgressBarCalculator();
            }
            pbCalculator.bindLabelWithPosition("processMultipage");
            Double progress
                    = pbCalculator.calculateProgressByLabelNormalized(
                            (double) done, (double) total, "processMultipage",
                            100.0);
            setProgress(progress.intValue());
            try
            {
                docAndWriter
                        = processMultipage(multipage, selectorDocument, pdf,
                                           pdfFile,
                                           emptyEscape, pdfWriter, imageBackgrounder,
                                           headerFooterMaker, layoutCellsMaker, pageNumberCreator);
                if (docAndWriter != null)
                {
                    //anche se è null, alla prima occasione queste variabili vengono inizializzate e nelle successive chiamate prenderà queste inizializzazioni
                    pdfWriter = docAndWriter.getWriter();
                    pdf = docAndWriter.getPdf();
                    imageBackgrounder = docAndWriter.getImageBackgrounder();
                    headerFooterMaker = docAndWriter.getHeaderFooterMaker();
                    layoutCellsMaker = docAndWriter.getPageLayoutCellsMaker();
                    pageNumberCreator = docAndWriter.getPageNumberCreator();

                }

            } catch (PdfWrapperException ex)
            {
                pdf = ex.getPdf();
                SelectorDocument selDoc = ex.getSelectorDoc();
                if (selDoc != null)
                {
                    selDoc.cleanupCachedData();
                }
                Idml idmlDoc = ex.getIdmlDoc();
                if (idmlDoc != null)
                {
                    idmlDoc.close();
                }
                if (pdf != null)
                {
                    try
                    {
                        pdf.close();
                    } catch (Exception closingException)
                    {
                        //non mi interessa l'eccezione in chiusura del pdf, mi interessa l'eccezione che lo ha causato
                        throw ex.getException();

                    }

                }
                throw ex.getException();
            }
            done++;
        }
        if (pdf != null)
        {
            pdf.close();
            if (!getStatus().equals(STATUS_ERROR))
            {
                signalProcessEnded();
            }

        }
        //visto che ho finito le operazioni, non mi serve più il documento selector
        selectorDocument.cleanupCachedData();
        return pdf;
    }

    /**
     * elabora il nodo multipage
     *
     * @param multipage il nodo multipage dall'XML SAP
     * @param selectorDocument il documento Selector da usare come template
     * @param pdf il documento PDF in creazione
     * @param pdfFile il file del documento PDF in creazione
     */
    private PdfDocumentAndWriterWrapper processMultipage(Node multipage,
                                                         SelectorDocument selectorDocument,
                                                         Document pdf,
                                                         File pdfFile,
                                                         String emptyEscape,
                                                         PdfWriter pdfWriter,
                                                         ImageBackgrounder imageBackgrounder,
                                                         HeaderFooterMaker headerFooterMaker,
                                                         PageLayoutCellsMaker layoutCellsMaker, PageNumberCreator pageNumberCreator)
            throws IOException, IdmlLibException, SelectorDocumentException,
                   URISyntaxException, SAXException, DOMException,
                   DocumentException,
                   ParserConfigurationException,
                   SelectorFileNotExistentException,
                   NoSuchFieldException, IllegalArgumentException,
                   IllegalAccessException, PdfWrapperException, NoPageNumberOffsetsException
    {
        PdfDocumentAndWriterWrapper docAndWriter = null;
        NamedNodeMap multipageAttributes = multipage.getAttributes();
        Node multipageTypeAttribute = multipageAttributes.getNamedItem(TYPE);
        String multipageTypeAttributeValue
                = multipageTypeAttribute.getTextContent();
        Node activatePageNumberAttribute = multipageAttributes.getNamedItem("aggiungi_numero_pagina");
        Double pageNumberOffsetX = null;
        Double pageNumberOffsetY = null;
        Double pageNumberFontSize = null;
        if (activatePageNumberAttribute != null)
        {
            String activatePageNumberAttributeValue = activatePageNumberAttribute.getTextContent();
            boolean activatePageNumberAttributeBooleanValue = XmlManagement.convertStringToBoolean(
                    activatePageNumberAttributeValue);
            if (activatePageNumberAttributeBooleanValue)
            {
                boolean isNumericX = true;
                boolean isNumericY = true;
                String errorMessageForPageNumber = null;
                Node pageNumberOffsetXAttribute = multipageAttributes.getNamedItem(OFFSET_PAGE_NUMBER_X_ATTRIBUTE_LABEL);
                Node pageNumberOffsetYAttribute = multipageAttributes.getNamedItem(OFFSET_PAGE_NUMBER_Y_ATTRIBUTE_LABEL);
                if (pageNumberOffsetXAttribute != null)
                {
                    String pageNumberOffsetXAttributeValue = pageNumberOffsetXAttribute.getTextContent();
                    if (pageNumberOffsetXAttributeValue != null && !pageNumberOffsetXAttributeValue.equals(""))
                    {
                        isNumericX = org.apache.commons.lang.StringUtils.isNumeric(
                                pageNumberOffsetXAttributeValue);
                        if (isNumericX)
                        {
                            pageNumberOffsetX = new Double(pageNumberOffsetXAttributeValue);
                        }

                    }
                }
                if (pageNumberOffsetYAttribute != null)
                {
                    String pageNumberOffsetYAttributeValue = pageNumberOffsetYAttribute.getTextContent();
                    if (pageNumberOffsetYAttributeValue != null && !pageNumberOffsetYAttributeValue.equals(""))
                    {
                        isNumericY = org.apache.commons.lang.StringUtils.isNumeric(
                                pageNumberOffsetYAttributeValue);
                    }

                    if (isNumericY)
                    {
                        pageNumberOffsetY = new Double(pageNumberOffsetYAttributeValue);
                    }
                }

                errorMessageForPageNumber = createErrorMessageForPageNumbers(pageNumberOffsetXAttribute, isNumericX, pageNumberOffsetYAttribute, isNumericY, errorMessageForPageNumber, multipageTypeAttributeValue);

                if (errorMessageForPageNumber != null)
                {
                    throw new NoPageNumberOffsetsException(errorMessageForPageNumber);
                }

                Node pageNumberFontSizeAttribute = multipageAttributes.getNamedItem("dim_font_numero_pag");
                if (pageNumberFontSizeAttribute != null)
                {
                    String pageNumberFontSizeAttributeValue = pageNumberFontSizeAttribute.getTextContent();
                    if (pageNumberFontSizeAttributeValue != null)
                    {
                        if (org.apache.commons.lang.StringUtils.isNumeric(pageNumberFontSizeAttributeValue))
                        {
                            pageNumberFontSize = new Double(pageNumberFontSizeAttributeValue);
                        } else
                        {
                            addLog(new LoggerVO("6", "dimensione font numero pagina non valida, verrà usata "
                                                + PageNumberCreator.DEFAULT_FONT_SIZE_FOR_PAGE_NUMBER));
                        }
                    }
                }
            }
        }

        if (multipageTypeAttributeValue != null
                && (!multipageTypeAttributeValue.isEmpty()))
        {
            SelectorFascicle multipageAssignedFascicle
                    = selectorDocument.searchFascicleByName(
                            multipageTypeAttributeValue);
            if (multipageAssignedFascicle != null)
            {
                docAndWriter
                        = processMultipageAssignedFascicle(
                                multipageAssignedFascicle,
                                multipage,
                                pdf, pdfFile, emptyEscape, selectorDocument,
                                null, 0, pdfWriter, headerFooterMaker,
                                imageBackgrounder, layoutCellsMaker, pageNumberCreator, pageNumberOffsetX, pageNumberOffsetY,
                                pageNumberFontSize);
                pdf = docAndWriter.getPdf();
                pdfWriter = docAndWriter.getWriter();
                imageBackgrounder = docAndWriter.getImageBackgrounder();
                headerFooterMaker = docAndWriter.getHeaderFooterMaker();
                layoutCellsMaker = docAndWriter.getPageLayoutCellsMaker();
                pageNumberCreator = docAndWriter.getPageNumberCreator();
            } else
            {
                //errore o salta gli oggetti di quella multipagina?
                addLog(new LoggerVO("3", "manca il fascicolo "
                                    + multipageTypeAttributeValue));
            }

        }
        return docAndWriter;
    }

    protected String createErrorMessageForPageNumbers(Node pageNumberOffsetXAttribute, boolean isNumericX,
                                                      Node pageNumberOffsetYAttribute, boolean isNumericY,
                                                      String errorMessageForPageNumberOffset,
                                                      String multipageTypeAttributeValue)
    {
        String xErrorPart = null;
        String xAttributeLabel = OFFSET_PAGE_NUMBER_X_ATTRIBUTE_LABEL;
        xErrorPart = calculateErrorMessageOffsetPart(pageNumberOffsetXAttribute, xAttributeLabel, isNumericX, xErrorPart);
        String yErrorPart = null;
        String yAttributeLabel = OFFSET_PAGE_NUMBER_Y_ATTRIBUTE_LABEL;
        yErrorPart = calculateErrorMessageOffsetPart(pageNumberOffsetYAttribute, yAttributeLabel, isNumericY, yErrorPart);
        if (xErrorPart != null)
        {
            errorMessageForPageNumberOffset = "Si è chiesto di creare il numero di pagina per la multipage " + multipageTypeAttributeValue + " ma non è possibile perché " + xErrorPart;
        }
        if (yErrorPart != null)
        {
            errorMessageForPageNumberOffset = "Si è chiesto di creare il numero di pagina per la multipage " + multipageTypeAttributeValue + " ma non è possibile perché " + yErrorPart;
        }
        if (xErrorPart != null && yErrorPart != null)
        {
            errorMessageForPageNumberOffset = "Si è chiesto di creare il numero di pagina per la multipage " + multipageTypeAttributeValue + " ma non è possibile perché " + xErrorPart + " e " + yErrorPart;
        }
        return errorMessageForPageNumberOffset;
    }

    protected String calculateErrorMessageOffsetPart(Node pageNumberOffsetAttribute, String attributeLabel,
                                                     boolean isNumeric, String errorPart)
    {
        if (pageNumberOffsetAttribute == null)
        {
            errorPart = "manca l'attributo " + attributeLabel;
        } else
        {
            if (!isNumeric)
            {
                errorPart = "l'attributo " + attributeLabel + " non è valido";
            }
        }
        return errorPart;
    }

    public static final String OFFSET_PAGE_NUMBER_Y_ATTRIBUTE_LABEL = "offset_numero_pagina_y";
    public static final String OFFSET_PAGE_NUMBER_X_ATTRIBUTE_LABEL = "offset_numero_pagina_x";

    //private 
    /**
     * Elabora una multipage dopo che si e trovato il fascicolo corrisapondente
     *
     * @param multipageAssignedFascicle il fascicolo corrispondente alla
     * multipage, deve essere un fascicolo con una sola pagina e il cui nome
     * corrisponde all'attributo type del tag multipage
     * @param multipage il tag multipage associato al fascicolo
     * @param pdf il documento PDF
     * @param pdfFile il file del documento PDF eventualmente da creare
     * @param emptyEscape the value of emptyEscape
     * @param selectorDocument the value of selectorDocument
     * @param templateSelectorPage the value of templateSelectorPage
     * @param level the value of level
     * @param pdfWriter the value of pdfWriter
     * @param headerFooterMaker the value of headerFooterMaker
     * @param imageBackgrounder the value of imageBackgrounder
     * @return the
     * it.ekr.pdffromselector.parsingengine.utils.PdfDocumentAndWriterWrapper
     */
    private PdfDocumentAndWriterWrapper processMultipageAssignedFascicle(
            SelectorFascicle multipageAssignedFascicle, Node multipage,
            Document pdf, File pdfFile, String emptyEscape,
            SelectorDocument selectorDocument, SelectorPage templateSelectorPage,
            int level, PdfWriter pdfWriter, HeaderFooterMaker headerFooterMaker,
            ImageBackgrounder imageBackgrounder, PageLayoutCellsMaker layoutCellsMaker,
            PageNumberCreator pageNumberCreator, Double pageNumberOffsetX, Double pageNumberOffsetY,
            Double pageNumberFontSize)
            throws
            ParserConfigurationException,
            SelectorDocumentException, DocumentException,
            IdmlLibException, IOException, SAXException,
            URISyntaxException, DOMException,
            SelectorFileNotExistentException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException,
            PdfWrapperException
    {

        try
        {

            if (templateSelectorPage == null)
            {
                List<SelectorPage> pages
                        = multipageAssignedFascicle.getPages();
                //prendo la prima pagina, perche e quella da cui si prende l'esempio
                templateSelectorPage = pages.get(0);
            }

            double height = templateSelectorPage.getHeight();
            double width = templateSelectorPage.getWidth();
            boolean pageNumberCreatorBuiltNow = false;
            if (pageNumberOffsetX != null)
            {
                if (pageNumberOffsetY != null)
                {
                    if (pageNumberCreator == null)
                    {
                        pageNumberCreator = new PageNumberCreator();
                        pageNumberCreatorBuiltNow = true;
                    }
                }
            }
            if (pageNumberCreator != null)
            {
                //resetto eventualmente le dimensioni e gli offset
                pageNumberCreator.xOffset = pageNumberOffsetX;
                pageNumberCreator.yOffset = pageNumberOffsetY;
                pageNumberCreator.fontSize = pageNumberFontSize;
            }

            SelectorLayoutCell mainCell = extractMainCellFromPage(
                    templateSelectorPage);
            List<SelectorLayoutCell> mainAndContainedLayoutCellsRec
                    = mainCell.
                    getCurrentAndContainedLayoutCellsRec();
            List<SelectorLayoutCell> mainAndContainedRecWithoutHeaderAndFooter
                    = removeHeaderAndFooterFromRecFoundLayoutCells(
                            mainAndContainedLayoutCellsRec, HEADER, FOOTER);
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles
                    = populateCellsAndRectanglesFromCells(
                            mainAndContainedRecWithoutHeaderAndFooter, height);
            if (layoutCellsMaker == null)
            {
                layoutCellsMaker = new PageLayoutCellsMaker();
            }
            layoutCellsMaker.setFile(pdfFile);
            layoutCellsMaker.setLayoutCellsAndrectangles(cellsRectangles);
            Rectangle pageRect = new Rectangle((float) width, (float) height);
            Double leftMargin = templateSelectorPage.getLeftMargin();
            if (leftMargin == null)
            {
                leftMargin = 0.0;
            }
            Double rightMargin = templateSelectorPage.getRightMargin();
            if (rightMargin == null)
            {
                rightMargin = 0.0;
            }
            Double topMargin = templateSelectorPage.getTopMargin();
            if (topMargin == null)
            {
                topMargin = 0.0;
            }
            Double bottomMargin = templateSelectorPage.getBottomMargin();
            if (bottomMargin == null)
            {
                bottomMargin = 0.0;
            }
            SelectorLayoutCell headerCell = extractHeader(templateSelectorPage);
            SelectorLayoutCell footerCell = extractFooter(templateSelectorPage);
            float headerHeight = 0;//qui andra messa l'altezza della cella header se presente
            float footerHeight = 0;//qui andra messa l'altezza della cella footer se presente
            if (headerCell != null)
            {
                headerHeight = (float) headerCell.getHeight();
            }
            if (footerCell != null)
            {
                footerHeight = (float) footerCell.getHeight();
            }

            ISelectorObject bodyCell = extractBodyCellOfPage(
                    templateSelectorPage);
            SelectorLayoutCell bodyCellAsSelectorLayoutCell
                    = (SelectorLayoutCell) bodyCell;
            if (pdf == null)
            {
                //poiche non si riesce ad eliminare la pagina iniziale, il documento viene creato nel momento in cui compare la prima pagina
                pdf = new Document(pageRect, leftMargin.floatValue()/* */,
                                   rightMargin.floatValue()/*0*/,
                                   topMargin.
                                   floatValue() /*0*/,
                                   bottomMargin.floatValue()/*0*/);
                //this.pdf = pdf;
                File pdfDir = pdfFile.getParentFile();
                if (!pdfDir.exists())
                {
                    pdfDir.mkdirs();
                }
                Document.compress = true;//non conviene disabilitare la compressione nei parziali e comprimere tutto alla fine, il file viene più grande
                pdfWriter
                        = PdfWriter.
                        getInstance(pdf, new FileOutputStream(pdfFile));
                pdfWriter.setFullCompression(); 
                headerFooterMaker = new HeaderFooterMaker();
                headerFooterMaker.file = pdfFile;
                headerFooterMaker.setTask(this);
                float llx = leftMargin.floatValue();
                double graphicLly
                        = pageRect.getHeight() - bottomMargin - footerHeight;
                float pdfLly = (float) CoordinatesTransformer.
                        fromUpperLeftYToLowerLeftY(graphicLly, height);
                float urx
                        = pageRect.getWidth() - rightMargin.
                        floatValue();
                float graphicUry
                        = topMargin.
                        floatValue() + headerHeight;
                float pdfUry = (float) CoordinatesTransformer.
                        fromUpperLeftYToLowerLeftY(graphicUry, height);

                //System.out.println("art ury "+ury);
                // Rectangle art = new Rectangle(llx, pdfLly, urx, pdfUry);//*/
                //new Rectangle(llx, lly, urx,ury);
                //pdfWriter.setBoxSize("art", art);
                pdfWriter.setPageEvent(layoutCellsMaker);

                pdf.open();
                pdf.setAccessibleAttribute(PdfName.TITLE, new PdfString(pdfFile.
                                           getCanonicalPath()));
                //deve essere messa dopo la chiusura della pagina
                headerFooterMaker.setHeader(headerCell);
                headerFooterMaker.setFooter(footerCell);
                headerFooterMaker.setBodyCell(bodyCellAsSelectorLayoutCell);
                headerFooterMaker.setPageHeight(height);
                imageBackgrounder = new ImageBackgrounder();
                imageBackgrounder.setFile(pdfFile);
                imageBackgrounder.setTask(this);
                imageBackgrounder.resetStoredImage();
                File backgroundImage = null;
                List<File> imagesInMastroFirstLevel
                        = templateSelectorPage.getImagesInMastroFirstLevel();
                if (imagesInMastroFirstLevel != null
                        && !imagesInMastroFirstLevel.
                        isEmpty())//si assume che la mastro abbia una sola immagine che e lo sfondo
                {
                    backgroundImage = imagesInMastroFirstLevel.get(0);
                }
                imageBackgrounder.setBackgroundImage(backgroundImage, pdfWriter);
                pdfWriter.setPageEvent(imageBackgrounder);
                pdfWriter.setPageEvent(headerFooterMaker);
                if (pageNumberCreatorBuiltNow)
                {
                    pdfWriter.setPageEvent(pageNumberCreator);
                }
            } else
            {
                //preparo la pagina per accogliere i componenti
                pdf.setPageSize(pageRect);
                pdf.
                        setMargins(leftMargin.floatValue(), rightMargin.
                                   floatValue(),
                                   topMargin.floatValue(),
                                   bottomMargin.
                                   floatValue());
                float llx = leftMargin.floatValue();
                float graphicLly
                        = pageRect.getHeight() - bottomMargin.
                        floatValue() - footerHeight;
                float pdfLly = (float) CoordinatesTransformer.
                        fromUpperLeftYToLowerLeftY(graphicLly, height);
                float urx
                        = pageRect.getWidth() - rightMargin.
                        floatValue();
                float graphicUry
                        = topMargin.
                        floatValue() + headerHeight;
                float pdfUry = (float) CoordinatesTransformer.
                        fromUpperLeftYToLowerLeftY(graphicUry, height);
                //System.out.println("art ury "+ury);
                /*Rectangle art = new Rectangle(llx,
                 pdfLly, urx,pdfUry);//*/

                //pdfWriter.setBoxSize("art", art);
                pdf.newPage();//sembra che elinmini automaticamente le pagine vuote
                //deve essere messa dopo la chiusura della pagina
                headerFooterMaker.setHeader(headerCell);
                headerFooterMaker.setFooter(footerCell);
                headerFooterMaker.setBodyCell(bodyCellAsSelectorLayoutCell);
                headerFooterMaker.setPageHeight(height);
                imageBackgrounder.resetStoredImage();
                File backgroundImage = null;
                List<File> imagesInMastroFirstLevel
                        = templateSelectorPage.getImagesInMastroFirstLevel();
                if (imagesInMastroFirstLevel != null
                        && !imagesInMastroFirstLevel.
                        isEmpty())//si assume che la mastro abbia una sola immagine che e lo sfondo
                {
                    backgroundImage = imagesInMastroFirstLevel.get(0);
                }
                imageBackgrounder.setBackgroundImage(backgroundImage, pdfWriter);//temporaneo, dovra recuperare l'immagine dal supertemplate
                /* non dovrebbe servire se ci sono più pagine
                 pdfWriter.setPageEvent(imageBackgrounder);
                 pdfWriter.setPageEvent(headerFooterMaker);*/
                if (pageNumberCreatorBuiltNow)
                {
                    pdfWriter.setPageEvent(pageNumberCreator);
                }
            }
            /*List<ISelectorObject> mainCellWrapper =
             templateSelectorPage.getContainedObjects();//*/

            if (headerCell != null)
            {
                PdfPTable headerTable = new PdfPTable(1);
                PdfPCell headerPdfCell
                        = processHeaderFooterLayoutCell(headerCell,
                                                        multipage, emptyEscape,
                                                        selectorDocument,
                                                        height,pdfWriter);//header e footer non possono contenere subpage, per questo non viene passato il livello
                headerTable.addCell(headerPdfCell);
                headerFooterMaker.setHeaderTable(headerTable);
            }
            if (footerCell != null)
            {
                PdfPTable footerTable = new PdfPTable(1);
                PdfPCell footerPdfCell
                        = processHeaderFooterLayoutCell(footerCell,
                                                        multipage, emptyEscape,
                                                        selectorDocument,
                                                        height,pdfWriter);//header e footer non possono contenere subpage, per questo non viene passato il livello
                footerTable.addCell(footerPdfCell);
                headerFooterMaker.setFooterTable(footerTable);
            }

            List<Node> componentList
                    = XmlManagement.
                    retrieveNodesRecByNameStoppingAtFirstLevelFound(multipage,
                                                                    COMPONENT);
            int done = 0;
            int total = componentList.size();
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle
                    = null;
            ColumnText ct = new ColumnText(pdfWriter.getDirectContent());
            for (Node componentTag : componentList)
            {
                if (level == 0)//aggiorno lo stato solo per il primo livello,, se no è un problema per i vari livelli di annidamento
                {
                    pbCalculator.
                            addLabelAfterLabelIfNotAlreadyIn("processComponent",
                                                             "processMultipage");
                    Double calculateProgressByLabelNormalized
                            = pbCalculator.calculateProgressByLabelNormalized(
                                    (double) done, (double) total,
                                    "processComponent", 100.0);
                    setProgress(calculateProgressByLabelNormalized.intValue());
                }

                reachedLayoutCellAndRectangle
                        = processComponent(componentTag, bodyCell, pdf,
                                           emptyEscape, reachedLayoutCellAndRectangle,
                                           ct,
                                           selectorDocument, level, pdfWriter, height,
                                           cellsRectangles);
//                if (reachedLayoutCellAndRectangle != null)
//                {
//                    //System.out.println("reached layoutCell Y "+reachedLayoutCell.getAbsoluteY());
//                }

                done++;
            }
        } catch (Exception ex)
        {
            PdfWrapperException exceptionWithPdf = new PdfWrapperException();
            exceptionWithPdf.setPdf(pdf);
            exceptionWithPdf.setSelectorDoc(selectorDocument);
            exceptionWithPdf.setException(ex);
            throw exceptionWithPdf;
        }

        PdfDocumentAndWriterWrapper docAndWriter
                = new PdfDocumentAndWriterWrapper(pdf,
                                                  pdfWriter,
                                                  headerFooterMaker,
                                                  imageBackgrounder, layoutCellsMaker,
                                                  pageNumberCreator);
        return docAndWriter;
    }

    protected List<SelectorLayoutCellAndRectangleWrapper> populateCellsAndRectanglesFromCells(
            List<SelectorLayoutCell> layoutCellsList, double pageHeight) throws
            URISyntaxException, ParserConfigurationException, SAXException,
            IdmlLibException, IOException, SelectorDocumentException,
            DocumentException
    {
        List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles
                = new ArrayList<>();
        for (SelectorLayoutCell layoutCell : layoutCellsList)
        {
            SelectorLayoutCellAndRectangleWrapper wrapper
                    = new SelectorLayoutCellAndRectangleWrapper();
            wrapper.setLayoutCell(layoutCell);
            wrapper.setAlreadyInsertedRectangle(false);
            // new Rectangle(llx, lly, urx, ury)
            wrapper = makeReachedCellRectangle(wrapper, pageHeight, null, false);//il document non serve perchè non faccio aggiunte, quindi posso passare un null

            cellsRectangles.add(wrapper);
        }
        return cellsRectangles;
    }

    protected void resetInsertedCellsRectanglesStatus(
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
    {
        for (SelectorLayoutCellAndRectangleWrapper cellAndRect : cellsRectangles)
        {
            cellAndRect.setAlreadyInsertedRectangle(false);
        }
    }

    protected SelectorLayoutCellAndRectangleWrapper retrieveSelectorLayoutCellAndRectFromLayoutCell(
            SelectorLayoutCell layoutCell,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
    {
        for (SelectorLayoutCellAndRectangleWrapper wrapper : cellsRectangles)
        {
            if (wrapper.getLayoutCell() == layoutCell)
            {
                //cerco proprio lo stesso oggetto, per questo non usop equals
                return wrapper;
            }
        }

        return null;
    }

    protected List<SelectorLayoutCell> removeHeaderAndFooterFromRecFoundLayoutCells(
            List<SelectorLayoutCell> mainAndContainedLayoutCellsRec,
            String headerType, String footerType)
    {
        List<SelectorLayoutCell> mainAndContainedRecWithoutHeaderAndFooter
                = new ArrayList<>();
        for (SelectorLayoutCell layoutCell : mainAndContainedLayoutCellsRec)
        {
            String type = layoutCell.getType();
            if (type != null)
            {
                if (!type.equals(headerType))
                {
                    if (!type.equals(footerType))
                    {
                        mainAndContainedRecWithoutHeaderAndFooter.
                                add(layoutCell);
                    }
                }
            } else
            {
                //celle senza tipo vanno bene
                mainAndContainedRecWithoutHeaderAndFooter.add(layoutCell);
            }
        }
        return mainAndContainedRecWithoutHeaderAndFooter;
    }

    /**
     * trova la cella header nella pagina
     *
     * @param page la pagina in cui si cerca la cella header
     * @return la cella di Selector header o null se non esiste cella header
     */
    private SelectorLayoutCell extractHeader(SelectorPage page) throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        String searchingCellType = HEADER;
        boolean searchingBodyCell = false;
        return searchHeaderFooterBodyInPage(page, searchingBodyCell,
                                            searchingCellType);
    }

    /**
     * tipo della cella layout di intestazione
     */
    public static final String HEADER = "header";

    /**
     * cerca la cella header, footer o body
     *
     * @param page la pagina Selector in cui si cerca la cella
     * @param searchingBodyCell se true si sta cercando la cella body
     * @param searchingCellType il tipo di cella cercato secondo le costanti
     * indicate
     * @see #BODY_CELL
     * @see #HEADER
     * @see #FOOTER
     * @return
     */
    private SelectorLayoutCell searchHeaderFooterBodyInPage(SelectorPage page,
                                                            boolean searchingBodyCell,
                                                            String searchingCellType)
            throws SAXException, IOException, SelectorDocumentException,
                   ParserConfigurationException
    {
        SelectorLayoutCell mainCell = extractMainCellFromPage(page);
        if (mainCell.isHorizontalDirection() || mainCell.isUndefinedDirection())
        {
            //se la main cell e divisa in colonne o non ha divisioni in celle interne
            return fallbackEventuallyInMainCell(searchingBodyCell, mainCell);
        } else
        {
            //devo farmi dare gli oggetti della main cell e verificare se sono celle layout e in tal caso vedere se c'e l'header
            List<ISelectorObject> mainCellContents
                    = mainCell.getContainedObjects();
            if (mainCellContents.size() > 3)
            {
                //vuol dire che ci sono piu oggetti o comuunque non essendo divisa in tre o meno non posso gestirla
                return fallbackEventuallyInMainCell(searchingBodyCell,
                                                    mainCell);

            } else
            {
                for (ISelectorObject candidateAbstractCell : mainCellContents)
                {
                    if (candidateAbstractCell instanceof SelectorLayoutCell)
                    {
                        SelectorLayoutCell foundCell
                                = (SelectorLayoutCell) candidateAbstractCell;
                        if (searchingBodyCell)
                        {
                            //la cella contenuto non e marcata come header o footer di norma
                            if (candidateAbstractCell.getType() == null
                                    || candidateAbstractCell.getType().isEmpty())
                            {
                                return foundCell;
                            }
                        }
                        if (candidateAbstractCell.getType() != null
                                && candidateAbstractCell.getType().equals(
                                        searchingCellType))
                        {
                            return foundCell;
                        }
                    } else
                    {
                        //risparmio i cicli e restituisco eventualmente la main cell
                        return fallbackEventuallyInMainCell(searchingBodyCell,
                                                            mainCell);
                    }

                }
            }
        }

        return null;
    }

    protected SelectorLayoutCell extractMainCellFromPage(SelectorPage page)
            throws SelectorDocumentException, IOException, SAXException,
                   ParserConfigurationException
    {
        List<ISelectorObject> containedObjects = page.getContainedObjects();
        ISelectorObject mainCellAbstract = containedObjects.get(0);
        SelectorLayoutCell mainCell = (SelectorLayoutCell) mainCellAbstract;
        return mainCell;
    }

    /**
     * fornisce eventualmente la main cell della pagina se si sta cercando la
     * cella body
     *
     * @param searchingBodyCell se true sto cercando la cella body
     * @param mainCell la cella principale della pagina
     * @return la cella principale della paginsa se si sta cercando la cella
     * body, null altrimenti
     */
    private SelectorLayoutCell fallbackEventuallyInMainCell(
            boolean searchingBodyCell,
            SelectorLayoutCell mainCell)
    {
        if (searchingBodyCell)
        {
            return mainCell;
        } else
        {
            return null;
        }
    }

    /**
     * estrae la cella body in cui vanno inseriti i contenuti che non sono in
     * header o footer
     *
     * @param page la pagina di Selector da cui recuperare la cella body
     * @return la cella layout in cui inserire i contenuti
     * @see
     * #extractFooter(it.ekr.pdffromselector.parsingengine.selectorobjects.SelectorPage)
     * @see
     * #extractHeader(it.ekr.pdffromselector.parsingengine.selectorobjects.SelectorPage)
     */
    private SelectorLayoutCell extractBodyCellOfPage(SelectorPage page)
            throws SAXException, IOException, SelectorDocumentException,
                   ParserConfigurationException
    {
        String searchingCellType = BODY_CELL;
        boolean searchingBodyCell = true;
        return searchHeaderFooterBodyInPage(page, searchingBodyCell,
                                            searchingCellType);
    }

    /**
     * tipo della cella layout corpo, opzionale perche di deafault come corpo
     * cerca la cella senza tipo
     */
    public static final String BODY_CELL = "body";

    /**
     * trova la cella footer nella pagina di Selector
     *
     * @param page la pagina di Selector
     * @return la layout cell footer se esistente, oppure null
     * @see
     * #extractBodyCellOfPage(it.ekr.pdffromselector.parsingengine.selectorobjects.SelectorPage)
     * @see
     * #extractHeader(it.ekr.pdffromselector.parsingengine.selectorobjects.SelectorPage)
     */
    private SelectorLayoutCell extractFooter(SelectorPage page) throws
            ParserConfigurationException, SAXException, IOException,
            SelectorDocumentException
    {
        String searchingCellType = FOOTER;
        boolean searchingBodyCell = false;
        return searchHeaderFooterBodyInPage(page, searchingBodyCell,
                                            searchingCellType);
    }

    /**
     * tipo della cella layout footer
     */
    public static final String FOOTER = "footer";

    /**
     * elabora un component nella multipage
     *
     * @param componentTag il tag component
     * @param mainCell la cella body del documento Selector
     * @param pdf il documento PDF in cui inserire i componenti
     */
    private SelectorLayoutCellAndRectangleWrapper processComponent(
            Node componentTag,
            ISelectorObject mainCell,
            Document pdf, String emptyEscape,
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            ColumnText ct,
            SelectorDocument selectorDocument,
            int level, PdfWriter pdfWriter,
            double pageHeight,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws IdmlLibException, IOException,
                   DocumentException, ParserConfigurationException,
                   URISyntaxException, DOMException, SAXException,
                   SelectorDocumentException, NoSuchFieldException,
                   IllegalArgumentException, IllegalAccessException,
                   BadElementException, Exception
    {
        //recupero l'oggetto in base al tag figlio del componente
        Node firstChild = componentTag.getFirstChild();
        while (firstChild != null && firstChild.getNodeType()
                != Node.ELEMENT_NODE)
        {
            firstChild = firstChild.getNextSibling();
        }
        String nodeName = firstChild.getNodeName();
        SelectorLayoutCell mainCellAsLayoutCell = (SelectorLayoutCell) mainCell;
        List<SelectorLayoutCell> containedLeafLayoutCells
                = mainCellAsLayoutCell.
                getContainedLeafLayoutCells();
        if (containedLeafLayoutCells != null)
        {
            if (containedLeafLayoutCells.size() == 1)
            {
                //verifico se è la main cell
                SelectorLayoutCell comparingLayoutCell
                        = containedLeafLayoutCells.get(0);
                if (comparingLayoutCell != mainCellAsLayoutCell)
                {
                    containedLeafLayoutCells
                            = retrieveFirstFlowLayoutCells(mainCellAsLayoutCell,
                                                           containedLeafLayoutCells);
                }
            } else
            {
                //non essendo la main cell, recupero le celle del primo flusso
                containedLeafLayoutCells
                        = retrieveFirstFlowLayoutCells(mainCellAsLayoutCell,
                                                       containedLeafLayoutCells);
            }
        }
        if (nodeName.equals(SUBPAGE_TAG))
        {
            reachedLayoutCellAndRectangle = processSubpage(firstChild,
                                                           containedLeafLayoutCells,
                                                           pdf, emptyEscape,
                                                           reachedLayoutCellAndRectangle,
                                                           ct, selectorDocument,
                                                           level, pdfWriter,
                                                           pageHeight,
                                                           cellsRectangles);
            //System.out.println(reachedLayoutCell.getFlowIndex());
//            Double absoluteY = reachedLayoutCellAndRectangle.layoutCell.getAbsoluteY();
//            double fromUpperLeftYToLowerLeftY =
//                    CoordinatesTransformer.fromUpperLeftYToLowerLeftY(absoluteY,
//                            pageHeight);
//            System.out.println(
//                    "reached Cell Y " + absoluteY.toString() + " -> " +
//                    fromUpperLeftYToLowerLeftY);
            return reachedLayoutCellAndRectangle;//se sto elaborando una subpage, non devo proseguire oltre in questo metodo
        }
        //se non è una subpage proseguo cercando l'oggetto nelle celle foglia
        String[] split = nodeName.split("\\-");
        List<String> splittedName = Arrays.asList(split);
        List<ISelectorObject> containedObjectsByTypePath = null;
        for (SelectorLayoutCell containedLeaf : containedLeafLayoutCells)
        {
            List<ISelectorObject> candidateContainedObjectsByTypePath
                    = containedLeaf.getContainedObjectsByTypePath(
                            splittedName);
            if ((candidateContainedObjectsByTypePath != null)
                    && (!candidateContainedObjectsByTypePath.isEmpty()))
            {
                containedObjectsByTypePath = candidateContainedObjectsByTypePath;
                break;
            }
        }

        if ((containedObjectsByTypePath != null)
                && (!containedObjectsByTypePath.
                isEmpty()))
        {
            ISelectorObject selectorObject
                    = containedObjectsByTypePath.get(0);
            if (selectorObject instanceof SelectorFlowUnit)
            {
                List<ISelectorObject> flowObjectChildren
                        = selectorObject.getContainedObjects();
                if (flowObjectChildren.size() == 1)
                {
                    //ho un solo figlio dell'oggetto flusso, verifico se ha un solo o piu paragrafi
                    ISelectorObject flowObjectChild = flowObjectChildren.get(0);
                    List<SelectorParagraph> containedSelectorParagraphs
                            = flowObjectChild.
                            getContainedSelectorParagraphs();
                    if (containedSelectorParagraphs.size() == 1)
                    {
                        SelectorParagraph singleParagraph
                                = containedSelectorParagraphs.get(0);
                        //ho un solo paragrafo, devo verificare se il tag xml mi da una sola foglia
                        if (XmlManagement.retrieveLeafNode(firstChild) != null)
                        {
                            //addParagraphToPdf(firstChild, singleParagraph, pdf,emptyEscape);
                            reachedLayoutCellAndRectangle
                                    = addParagraphToMultipleLayoutCells(firstChild,
                                                                        singleParagraph,
                                                                        pdf, emptyEscape,
                                                                        containedLeafLayoutCells,
                                                                        reachedLayoutCellAndRectangle,
                                                                        ct,
                                                                        true,
                                                                        pageHeight,
                                                                        pdfWriter,
                                                                        cellsRectangles);
                        } else
                        {
                            //va gestito come multiparagrafo
                            //processMultiparagraphInFlowUnit(firstChild,selectorObject, pdf,emptyEscape);
                            reachedLayoutCellAndRectangle
                                    = processMultiparagraphInFlowUnitToMultipleLayoutCells(
                                            firstChild,
                                            selectorObject, pdf, emptyEscape,
                                            containedLeafLayoutCells,
                                            reachedLayoutCellAndRectangle, ct,
                                            true,
                                            pageHeight, pdfWriter,
                                            cellsRectangles);
                        }

                    } else
                    {
                        if (flowObjectChild instanceof SelectorTable)
                        {
                            reachedLayoutCellAndRectangle
                                    = addTableToMultipleLayoutCells(
                                            flowObjectChild, firstChild,
                                            emptyEscape,
                                            reachedLayoutCellAndRectangle,
                                            containedLeafLayoutCells, ct, pdf,
                                            pageHeight, pdfWriter,
                                            cellsRectangles);
                        } else
                        {
                            //si tratta di un multiparagrafo dentro un singolo blocco testo

                            //processMultiparagraphInFlowUnit(firstChild,
                            //        selectorObject, pdf,emptyEscape);
                            reachedLayoutCellAndRectangle
                                    = processMultiparagraphInFlowUnitToMultipleLayoutCells(
                                            firstChild,
                                            selectorObject, pdf, emptyEscape,
                                            containedLeafLayoutCells,
                                            reachedLayoutCellAndRectangle, ct,
                                            true,
                                            pageHeight, pdfWriter,
                                            cellsRectangles);

                        }
                    }
                } else
                {
                    //processMultiparagraphInFlowUnit(firstChild, selectorObject,
                    //        pdf,emptyEscape);
                    reachedLayoutCellAndRectangle
                            = processMultiparagraphInFlowUnitToMultipleLayoutCells(
                                    firstChild,
                                    selectorObject, pdf, emptyEscape,
                                    containedLeafLayoutCells,
                                    reachedLayoutCellAndRectangle,
                                    ct, true, pageHeight, pdfWriter,
                                    cellsRectangles);

                }
            } else
            {
                  //se non ho oggetti per quel tipo, vuol dire che devo cercare i floating box che come tipo abbiano tutto il nome del tag
                reachedLayoutCellAndRectangle = processFloatingBoxInComponentWithMultipleCellsCyclingPdfImages(mainCell, nodeName, firstChild, pdf, emptyEscape, containedLeafLayoutCells, reachedLayoutCellAndRectangle, ct, pageHeight, pdfWriter, cellsRectangles);
               
                
            }
        } else
        {
            //se non ho oggetti per quel tipo, vuol dire che devo cercare i floating box che come tipo abbiano tutto il nome del tag
            //processFloatingBoxInComponent(mainCell, nodeName, firstChild, pdf,
            //        emptyEscape);
            reachedLayoutCellAndRectangle
                    = processFloatingBoxInComponentWithMultipleCellsCyclingPdfImages(
                            mainCell, nodeName, firstChild,
                            pdf, emptyEscape, containedLeafLayoutCells,
                            reachedLayoutCellAndRectangle, ct, pageHeight,
                            pdfWriter, cellsRectangles);
        }
        /*Double absoluteY = reachedLayoutCell.getAbsoluteY();
         double fromUpperLeftYToLowerLeftY =
         CoordinatesTransformer.fromUpperLeftYToLowerLeftY(absoluteY, pageHeight);
         System.out.println("reached Cell Y "+absoluteY.toString()+" -> "+fromUpperLeftYToLowerLeftY);*/
        return reachedLayoutCellAndRectangle;
    }

    private SelectorLayoutCellAndRectangleWrapper processFloatingBoxInComponentWithMultipleCellsCyclingPdfImages(ISelectorObject mainCell, String nodeName, Node firstChild, Document pdf, String emptyEscape, List<SelectorLayoutCell> containedLeafLayoutCells, SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle, ColumnText ct, double pageHeight, PdfWriter pdfWriter, List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles) throws NoSuchFieldException, SelectorDocumentException, SAXException, ParserConfigurationException, IdmlLibException, DOMException, IOException, URISyntaxException, TrialsLimitException, IllegalAccessException, IllegalArgumentException, DocumentException
    {
        //se non ho oggetti per quel tipo, vuol dire che devo cercare i floating box che come tipo abbiano tutto il nome del tag
        //processFloatingBoxInComponent(mainCell, nodeName, firstChild,
        //        pdf, emptyEscape);
        //se dentro i floating box ho immagini PDF, devo replicare il floating box per tante pagine quante ne ha il PDF di immagine
        int imageIndex = 1;
        boolean lastPage;
        do
        {
            ReachedCellRectangleAndImageIndexWrapper processFloatingBoxInComponentWithMultipleCells = processFloatingBoxInComponentWithMultipleCells(mainCell,
                                                                                                                                                              nodeName,
                                                                                                                                                              firstChild,
                                                                                                                                                              pdf,
                                                                                                                                                              emptyEscape,
                                                                                                                                                              containedLeafLayoutCells,
                                                                                                                                                              reachedLayoutCellAndRectangle,
                                                                                                                                                              ct, pageHeight,
                                                                                                                                                              pdfWriter,
                                                                                                                                                              cellsRectangles,imageIndex);
            lastPage = processFloatingBoxInComponentWithMultipleCells.stopIterating;
            imageIndex = processFloatingBoxInComponentWithMultipleCells.imageIndex + 1;//preparo per la prossima immagine
            reachedLayoutCellAndRectangle = processFloatingBoxInComponentWithMultipleCells.cellAndRectangleWrapper;
        }while (!lastPage);
        return reachedLayoutCellAndRectangle;
    }
    public static final String SUBPAGE_TAG = "subpage";

    protected List<SelectorLayoutCell> retrieveFirstFlowLayoutCells(
            SelectorLayoutCell mainCellAsLayoutCell,
            List<SelectorLayoutCell> containedLeafLayoutCells)
            throws IOException, SAXException, SelectorDocumentException,
                   ParserConfigurationException
    {
        List<String> containedFlows
                = mainCellAsLayoutCell.getContainedFlows();
        if ((containedFlows != null) && (!containedFlows.isEmpty()))
        {
            String firstFlow = containedFlows.get(0);
            containedLeafLayoutCells = mainCellAsLayoutCell.
                    getLeafCellsByFlowLabel(firstFlow);
        }
        return containedLeafLayoutCells;
    }

    protected SelectorLayoutCellAndRectangleWrapper addTableToMultipleLayoutCells(
            ISelectorObject flowObjectChild,
            Node firstChild,
            String emptyEscape,
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            List<SelectorLayoutCell> containedLeafLayoutCells,
            ColumnText ct,
            Document pdf1, double pageHeight, PdfWriter writer,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws SecurityException, ParserConfigurationException,
                   NoSuchFieldException, URISyntaxException,
                   SelectorDocumentException,
                   SAXException, DocumentException, IOException,
                   IllegalAccessException,
                   IllegalArgumentException, IdmlLibException,
                   TrialsLimitException
    {
        PdfPTable pdfTable
                = createPdfTable(flowObjectChild, firstChild,
                                 emptyEscape);
        reachedLayoutCellAndRectangle
                = addElementToMultipleLayoutCellsAndProsecutePages(
                        reachedLayoutCellAndRectangle,
                        containedLeafLayoutCells, ct, pdfTable, pdf1, pageHeight,
                        false, writer, cellsRectangles);
        return reachedLayoutCellAndRectangle;
    }

    /* non più utilizzato
    protected void processFloatingBoxInComponent(ISelectorObject mainCell,
                                                 String nodeName,
                                                 Node firstChild, Document pdf1,
                                                 String emptyEscape, PdfWriter writer, int imageIndex)
            throws SelectorDocumentException, SAXException,
                   IllegalAccessException, IllegalArgumentException, IOException,
                   DocumentException, IdmlLibException,
                   ParserConfigurationException,
                   NoSuchFieldException, DOMException, URISyntaxException
    {
        List<SelectorFloatingBox> containedSelectorFloatingBoxesByType
                = mainCell.
                getContainedSelectorFloatingBoxesByType(
                        nodeName);
        if (containedSelectorFloatingBoxesByType != null
                && !containedSelectorFloatingBoxesByType.isEmpty())
        {
            SelectorFloatingBox floatingBox
                    = containedSelectorFloatingBoxesByType.get(0);
            PdfPTable floatingBoxTable = new PdfPTable(1);
            Double spaceBeforeFromFloatingBox = floatingBox.
                    getSpaceBefore();
            if (spaceBeforeFromFloatingBox != null)
            {
                floatingBoxTable.setSpacingBefore(
                        spaceBeforeFromFloatingBox.floatValue());
            }
            Double spaceAfterFromFloatingBox = floatingBox.
                    getSpaceAfter();
            if (spaceAfterFromFloatingBox != null)
            {
                floatingBoxTable.setSpacingAfter(
                        spaceAfterFromFloatingBox.
                        floatValue());
            }
            SelectorLayoutCell mainLayoutCell
                    = (SelectorLayoutCell) mainCell;
            SelectorLayoutCell mainCellFloatingBox
                    = floatingBox.getMainCellFloatingBox();
            double factor = mainCellFloatingBox.getWidth() / mainLayoutCell.
                    getWidth();
            factor *= 100.0;
            floatingBoxTable.setWidthPercentage((float) factor);
            List<Node> retrieveElementChildren
                    = XmlManagement.retrieveElementChildren(firstChild);
            if (retrieveElementChildren.isEmpty())
            {
                //se non ha elementi figli, vuol dire che voglio usare il nodo stesso (per eviitare l'obbligo di avere un figlio unico)
                retrieveElementChildren = new ArrayList<>();
                retrieveElementChildren.add(firstChild);
            }
            Iterator<Node> iterator = retrieveElementChildren.iterator();
            ReachedCellRectangleAndImageIndexWrapper mainFloatingBoxTableCellWrapped
                    = processInternalLayoutCell(
                            mainCellFloatingBox, floatingBoxTable,
                            retrieveElementChildren, iterator, emptyEscape,writer,imageIndex);
            floatingBoxTable.addCell(mainFloatingBoxTableCellWrapped.reachedCell);
            String horizontalAlign = floatingBox.getHorizontalAlign();
            if (horizontalAlign != null)
            {
                int horizontalAlignForTable = PdfSupportUtils.
                        convertFloatingBoxAlignToTableAlign(horizontalAlign);
                floatingBoxTable.setHorizontalAlignment(horizontalAlignForTable);
            }
            pdf1.add(floatingBoxTable);
        }
    }//*/

    /**
     * inserisce il multiparagrafo dentro una layout cell
     *
     * @param firstChild il tag contenente il multiparagrafo nell'XML SAP
     * @param selectorObject l'oggetto di Selector contenente il multiparagrafo
     * @param headerOrFooter la cella di tabella che rappresenta header o footer
     * @param emptyEscape
     * @see #processMultiparagraphInFlowUnit(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     */
    public void processMultiparagraphInFlowUnitInLayoutCell(Node firstChild,
                                                            ISelectorObject selectorObject,
                                                            PdfPCell headerOrFooter,
                                                            String emptyEscape)
            throws
            IllegalArgumentException, NoSuchFieldException, DocumentException,
            IllegalAccessException, IdmlLibException, URISyntaxException,
            IOException, ParserConfigurationException, DOMException,
            SelectorDocumentException, SAXException
    {
        //devo gestire in questo caso un multiparagrafo su blocchi testo diversi;
        //lo gestisco come sopra visto che la unit selector viene usata come wrapper per i paragrafi e quindi salta i blocchi testo
        List<Node> retrieveElementChildren
                = XmlManagement.retrieveElementChildren(
                        firstChild);
        for (int i = 0; i < retrieveElementChildren.size();
             i++)
        {
            Node currentContainedChild
                    = retrieveElementChildren.get(i);
            addParagraphToLayoutCell(currentContainedChild, i, selectorObject,
                                     headerOrFooter, emptyEscape, true);
        }
    }

    /**
     * elabora ricorsivamente le celle layout interne, dei floating box
     *
     * @param internalCell la cella layout in elaborazione
     * @param container la tabella PDF che contiene la cella che verra creata
     * @param contents i contenuti foglia del nodo dell'XML da SAP da inserire
     * nelle celle layout foglia
     * @param contentsIterator un iterator sulla lista dei contenuti in modo da
     * sapere in che posizione ci si trova quando si deve inserire il contenuto
     * in una cella layout foglia
     * @param writer pdfWriter per lettura files PDF
     * @return la cella di ttabella che rappresenta la cella layout corrente
     */
    public ReachedCellRectangleAndImageIndexWrapper processInternalLayoutCell(SelectorLayoutCell internalCell,
                                              PdfPTable container,
                                              List<Node> contents,
                                              Iterator<Node> contentsIterator,
                                              String emptyEscape,PdfWriter writer, int imageIndex)
            throws SelectorDocumentException, SelectorFileNotExistentException,
                   ParserConfigurationException, SAXException, IOException,
                   URISyntaxException, IdmlLibException, DocumentException,
                   DOMException, NoSuchFieldException, IllegalArgumentException,
                   IllegalAccessException//i floating box e le celle interne sono modellate come tabelle, per poterle aggiungere nel flusso
    {
        ReachedCellRectangleAndImageIndexWrapper toReturn = new ReachedCellRectangleAndImageIndexWrapper();
        toReturn.imageIndex = imageIndex;
        PdfPCell toReturnPdfCell = new PdfPCell();
        TintAndColorWrapper fillTintAndColorWrapper
                = internalCell.getFillTintAndColorWrapper();
        if (fillTintAndColorWrapper != null)
        {
            CellBackgroundColoring colorer = new CellBackgroundColoring();
            colorer.setColor(fillTintAndColorWrapper);
            toReturnPdfCell.setCellEvent(colorer);
        }
        VerticalJustification verticalJustification
                = internalCell.getVerticalJustification();

        if (verticalJustification != null)
        {
            toReturnPdfCell.setVerticalAlignment(PdfSupportUtils.
                    convertTableCellVerticalJustification(verticalJustification));
        }

        toReturnPdfCell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        double height = internalCell.getHeight();
        toReturnPdfCell.setMinimumHeight((float) height);
        Double strokeWeight = internalCell.getStrokeWeight();
        if (strokeWeight != null)
        {
            toReturnPdfCell.setBorderWidth(strokeWeight.floatValue());
            //per allineare piu verso l'interno aggiungo un padding left pari allo stroke, verificato nel pdf di ouput
            //toReturn.setPaddingLeft(strokeWeight.floatValue());
            toReturnPdfCell.setTop(strokeWeight.floatValue());
        } else
        {
            toReturnPdfCell.setBorder(0);
        }

        toReturnPdfCell.setPadding(0);//tentativo per problema di spaziatura testi

        TintAndColorWrapper strokeTintAndColor
                = internalCell.getStrokeTintAndColor();
        if (strokeTintAndColor != null)
        {
            BaseColor strokeBaseColor
                    = PdfSupportUtils.
                    calculateBaseColorFromTintAndColorWrapper(
                            strokeTintAndColor);
            toReturnPdfCell.setBorderColor(strokeBaseColor);
        }

        double width = internalCell.getWidth();
        List<SelectorLayoutCell> containedLayoutCells
                = internalCell.getContainedLayoutCells();
        if (containedLayoutCells == null || containedLayoutCells.isEmpty())
        {
            Node currentProcessingNode = null;
            if (contentsIterator.hasNext())
            {
                currentProcessingNode = contentsIterator.next();
            }
            //sono su una cella foglia, devo vedere se ho paragrafi e tabelle o immagine spaziale
            List<ISelectorObject> containedObjects
                    = internalCell.getContainedObjects();

            if (currentProcessingNode != null)
            {
                if (containedObjects.size() == 1)
                {
                    ISelectorObject containedObject = containedObjects.get(0);
                    if (containedObject instanceof SelectorFlowUnit)
                    {

                        List<Node> leaves
                                = XmlManagement.retrieveLeavesNodes(
                                        currentProcessingNode);//con questa richiesta arrivo ai nodi para eventuali
                        List<Node> tablesAndLeaves
                                = raiseLeafAndTablesNodes(leaves);
                        int parCounter = 0;
                        int tablesCounter = 0;
                        for (int i = 0; i < tablesAndLeaves.size(); i++)
                        {
                            Node leafOrTable = tablesAndLeaves.get(i);
                            /*System.out.println(XmlManagement.nodeToString(
                             leafOrTable));*/
                            if (!checkIfComponentIsTable(leafOrTable))
                            {
                                addParagraphToLayoutCell(leafOrTable, parCounter,
                                                         containedObject,
                                                         toReturnPdfCell, emptyEscape, true);
                                parCounter++;
                            } else
                            {
                                addTableToLayoutCell(containedObject, tablesCounter,
                                                     leafOrTable, emptyEscape,
                                                     toReturnPdfCell);
                                tablesCounter++;
                            }

                        }

                    } else if (containedObject instanceof SelectorSpaceUnit)
                    {
                        SelectorSpaceUnit spaceUnit
                                = (SelectorSpaceUnit) containedObject;
                        SelectorSpaceImage containedImage
                                = spaceUnit.getContainedImage();
                        PdfPTable spaceUnitTable = new PdfPTable(1);
                        double spaceUnitWidth = spaceUnit.getWidth();
                        double internalCellWidth
                                = internalCell.
                                getWidth();
                        double factor = spaceUnitWidth / internalCellWidth;
                        factor *= 100.0;
                        spaceUnitTable.setWidthPercentage((float) factor);
                        URI href = containedImage.getHref();
                        if (currentProcessingNode != null)
                        {
                            //se c'e il nodo devo inserire i contenuti
                            currentProcessingNode = XmlManagement.retrieveLeafNode(
                                    currentProcessingNode);
                            String textContent = currentProcessingNode.
                                    getTextContent();
                            textContent = textContent.replaceAll("\\t|\\n", "");
                            if (textContent != null && textContent.length() > 0)//se il nodo e vuoto devo inserire i contenuti gia presenti
                            {
                                if (textContent.startsWith("file:"))
                                {
                                    href = new URI(textContent);
                                } else
                                {
                                    File file = new File(textContent);
                                    href = file.toURI();
                                }

                            }
                            if (FileUtilitiesForPdfFromSelector.
                                    hrefPointedFileExists(href))
                            {
                                Image image;
                                if (href.toString().toLowerCase().endsWith("pdf"))
                                {
                                    //importo la prima pagina del pdf fornito
                                    PdfReader reader = new PdfReader(new FileInputStream(new File(href))); 
                                    int numberOfPages = reader.getNumberOfPages();
                                    if (imageIndex != numberOfPages)
                                    {
                                        toReturn.stopIterating = false;
                                    }
                                    PdfImportedPage importedPage = writer.getImportedPage(reader, imageIndex);
                                    image = Image.getInstance(importedPage);
                
                                }else
                                {
                                    image = Image.getInstance(href.toURL());
                                }
                                
                                image.setPaddingTop(0);
                                image.setSpacingAfter(0);
                                image.setSpacingBefore(0);
                                image.setBorderWidth(0);
                                PdfPCell spaceObjectCell = new PdfPCell();
                                spaceObjectCell.
                                        setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                                spaceObjectCell.setVerticalAlignment(
                                        PdfPCell.ALIGN_MIDDLE);
                                double heightSpaceUnit
                                        = spaceUnit.
                                        getHeight();
                                spaceObjectCell.setMinimumHeight(
                                        (float) heightSpaceUnit);
                                spaceObjectCell.setFixedHeight(
                                        (float) heightSpaceUnit);
                                spaceObjectCell.setBorderWidth((float) 0.0);
                                Double spaceUnitStrokeWeight = spaceUnit.
                                        getStrokeWeight();
                                if (spaceUnitStrokeWeight != null)
                                {
                                    spaceObjectCell.setBorderWidth(
                                            spaceUnitStrokeWeight.
                                            floatValue());
                                }
                                PdfPTable imageTable = new PdfPTable(1);
                                PdfPCell imageCell = new PdfPCell();
                                double imageWidth = containedImage.getWidth();
                                double spaceWidth = spaceUnit.getWidth();
                                factor = imageWidth / spaceWidth * 100.0;
                                imageTable.setWidthPercentage((float) factor);
                                imageCell.setBorderWidth(0.0f);
                                double containedImageHeight
                                        = containedImage.
                                        getHeight();
                                imageCell.setMinimumHeight(
                                        (float) containedImageHeight);
                                imageCell.setFixedHeight(
                                        (float) containedImageHeight);
                                Double imageStrokeWeight = containedImage.
                                        getStrokeWeight();
                                float imageStrokeWeightValue = 0;
                                if (imageStrokeWeight != null)
                                {
                                    imageStrokeWeightValue = imageStrokeWeight.
                                            floatValue();
                                    imageCell.setBorderWidth(imageStrokeWeightValue);
                                }
                                imageCell.
                                        setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
                                imageCell.setHorizontalAlignment(
                                        PdfPCell.ALIGN_CENTER);
                                imageCell.addElement(image);
                                //image.scaleAbsolute(imageCell.getWidth()-2.0f*imageCell.getBorderWidth(), imageCell.getHeight()-2.0f*imageCell.getBorderWidth());
                                image.setAlignment(Image.ALIGN_CENTER);
                                imageTable.addCell(imageCell);

                                spaceObjectCell.addElement(imageTable);

                                spaceUnitTable.addCell(spaceObjectCell);
                                float fixedHeight = imageCell.getFixedHeight();
                                //allineamento in centro, da capire in futuro se andrà sostituito
                                image.scaleToFit((float) spaceUnitWidth, (float) fixedHeight);
                                //image.scaleToFit(imageCell);
                                float scaledWidth = image.getScaledWidth();
                                float paddingLeft = (float) Math.abs((width - scaledWidth) / 2.0F);
                                toReturnPdfCell.setPaddingLeft(paddingLeft);
                                toReturnPdfCell.setPaddingRight(paddingLeft);
                                float scaledHeight = image.getScaledHeight();
                                float paddingTop = (float) Math.abs((height - scaledHeight) / 2.0F);
                                //toReturn.setPaddingTop(0);
                                toReturnPdfCell.setPaddingTop(paddingTop);
                                //toReturn.setPaddingBottom(paddingTop);
                                //toReturn.setPaddingRight((float) (width-image.getScaledWidth())/2.0F);
                                //toReturn.setPaddingBottom((float) (height-image.getScaledHeight())/2.0F);
                                toReturnPdfCell.addElement(spaceUnitTable);
                            }

                        }

                    }
                } else if (!containedObjects.isEmpty())
                {
                    //vuol dire che ho piu oggetti in questa cella, poiche gli oggetti space non possono stare piu di uno per cella, sono oggetti flusso e quindi multiparagrafi e tabelle
                    List<Node> leaves
                            = XmlManagement.retrieveLeavesNodes(
                                    currentProcessingNode);//con questa richiesta arrivo ai nodi para eventuali
                    List<Node> leavesAndTables
                            = raiseLeafAndTablesNodes(leaves);
                    int parCounter = 0;
                    int tablesCounter = 0;
                    for (int i = 0; i < leavesAndTables.size(); i++)
                    {

                        Node leafOrTable = leavesAndTables.get(i);
                        if (!checkIfComponentIsTable(leafOrTable))
                        {
                            addParagraphToLayoutCell(leafOrTable, parCounter,
                                                     internalCell,
                                                     toReturnPdfCell, emptyEscape, true);
                            parCounter++;
                        } else
                        {
                            addTableToLayoutCell(internalCell, tablesCounter,
                                                 leafOrTable, emptyEscape, toReturnPdfCell);
                            tablesCounter++;
                        }
                    }
                } else
                {
                    //la cella e vuota quindi va messa vuota

                }
            } else
            {
                addLog(new LoggerVO("3", "ci sono meno tag rispetto alle celle layout disponibili, una cella layout di tipo  " + internalCell.getType() + " risulterà vuota, si consiglia di verificare l'xml"));
            }

        } else
        {
            if (internalCell.isHorizontalDirection())
            {
                float[] relativeWidths = null;
                int size = containedLayoutCells.size();
                relativeWidths = new float[size];
                for (int i = 0; i < size; i++)
                {
                    SelectorLayoutCell examiningLayoutCell
                            = containedLayoutCells.get(i);
                    Double percentWidth = examiningLayoutCell.getPercentWidth();
                    relativeWidths[i] = percentWidth.floatValue();
                }
                //devo creare una tabella interna con tante colonne quante le celle e una sola riga, la dimensione relative delle colonne deve essere pari alle dimensioni percentuali

                PdfPTable internalTable = new PdfPTable(relativeWidths);
                internalTable.setTotalWidth((float) width);
                double widthPercentReduction = 0.0;
                /*if (strokeWeight != null && strokeWeight != 0.0)
                 {
                 widthPercentReduction = (strokeWeight/width)*100.0;
                 }//*/
                internalTable.setWidthPercentage((float) (100.0
                        - widthPercentReduction));
                //internalTable.setWidthPercentage(relativeWidths, bounds);

                for (SelectorLayoutCell contained : containedLayoutCells)
                {

                    ReachedCellRectangleAndImageIndexWrapper processInternalLayoutCell
                            = processInternalLayoutCell(contained,
                                                        internalTable,
                                                        contents,
                                                        contentsIterator,
                                                        emptyEscape,writer,imageIndex);
                    //imposto l'image index che ho raggiunto nelle celle interne
                    toReturn.imageIndex = processInternalLayoutCell.imageIndex;
                    //se in almeno una cella inerna ho raggiunto il massimo delle immagini, interrompo
                     if (processInternalLayoutCell.stopIterating)
                    {
                        toReturn.stopIterating = true;
                    }
                    internalTable.addCell(processInternalLayoutCell.reachedCell);
                }
                //vari aggiustamenti per allineare la tabella interna
                /*toReturn.setPaddingRight((float) 0.0);
                 toReturn.setPaddingBottom((float) 0.0);
                 toReturn.setPaddingTop((float) 0.0);
                 internalTable.setSpacingAfter((float) 0.0);*/
                internalTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
//                if (strokeWeight != null)
//                {
//                    //internalTable.setSpacingBefore(strokeWeight.floatValue());
//                }
                toReturnPdfCell.addElement(internalTable);
            } else if (internalCell.isVerticalDirection())
            {
                PdfPTable internalTable;
                internalTable = new PdfPTable(1);
                float[] columnWidth = new float[1];
                columnWidth[0] = (float) 100.0;
                internalTable.setTotalWidth((float) width);
                double widthPercentReduction = 0.0;
                /*if (strokeWeight != null && strokeWeight != 0.0)
                 {
                 widthPercentReduction = (strokeWeight/width)*100.0;
                 }//*/
                internalTable.setWidthPercentage((float) (100.0
                        - widthPercentReduction));
                //internalTable.setWidthPercentage(columnWidth, bounds);

                for (SelectorLayoutCell contained : containedLayoutCells)
                {

                    ReachedCellRectangleAndImageIndexWrapper processInternalLayoutCell
                            = processInternalLayoutCell(contained,
                                                        internalTable,
                                                        contents,
                                                        contentsIterator,
                                                        emptyEscape,writer,imageIndex);
                    //imposto l'image index che ho raggiunto nelle celle interne
                    toReturn.imageIndex = processInternalLayoutCell.imageIndex;
                    //se una delle celle interne ha raggiunto l'ultima pagina, non posso aggiungere ulteriori immagini
                    if (processInternalLayoutCell.stopIterating)
                    {
                        toReturn.stopIterating = true;
                    }
                    internalTable.addCell(processInternalLayoutCell.reachedCell);
                }
                //vari aggiustamenti per allineare la tabella interna
                /*toReturn.setPaddingRight((float) 0.0);
                 internalTable.setSpacingAfter((float) 0.0);
                 toReturn.setPaddingBottom((float) 0.0);
                 toReturn.setPaddingTop((float) 0.0);*/
                if (strokeWeight != null)
                {
                    //internalTable.setSpacingBefore(strokeWeight.floatValue());
                }

                internalTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
                toReturnPdfCell.addElement(internalTable);
            }
        }

       
        toReturn.reachedCell = toReturnPdfCell;
        return toReturn;
    }

    protected void addTableToLayoutCell(ISelectorObject containedObject,
                                        int tablesCounter, Node leafOrTable,
                                        String emptyEscape, PdfPCell toReturn)
            throws SAXException, SecurityException, IllegalArgumentException,
                   NoSuchFieldException, IdmlLibException,
                   SelectorDocumentException,
                   IllegalAccessException, URISyntaxException, DocumentException,
                   IOException, ParserConfigurationException
    {
        SelectorTable templateTable
                = calculateTemplateTable(containedObject,
                                         tablesCounter);
        PdfPTable pdfTable
                = createPdfTable(templateTable, leafOrTable,
                                 emptyEscape);
        toReturn.addElement(pdfTable);
    }

    protected SelectorTable calculateTemplateTable(
            ISelectorObject containedObject,
            int tablesCounter) throws
            ParserConfigurationException, IOException, SAXException,
            SelectorDocumentException
    {
        List<SelectorTable> containedSelectorTables
                = containedObject.getContainedSelectorTables();
        SelectorTable templateTable;
        int size = containedSelectorTables.size();
        if (tablesCounter < size)
        {
            templateTable = containedSelectorTables.get(tablesCounter);
        } else
        {
            templateTable = containedSelectorTables.get(size - 1);
        }

        return templateTable;
    }

    protected List<Node> raiseLeafAndTablesNodes(List<Node> leaves)
    {
        List<Node> toReturn = new ArrayList<>();
        for (Node leaf : leaves)
        {
            Node foundTable = retrieveTableFromInnerNode(leaf);
            if (foundTable != null)
            {
                ArrayUtils.addToListIfNotAlreadyIn(toReturn, foundTable);
            } else
            {
                toReturn.add(leaf);//se la foglia non ha una tabella antenata, va messa come sta
            }
        }
        return toReturn;
    }

    /**
     * recupera i nodi "tabella" che contengono le foglie a cui si è arrivati
     *
     * @param leaves
     * @return
     */
    protected List<Node> retrieveTablesFromLeaves(List<Node> leaves)
    {
        List<Node> toReturn = new ArrayList<>();
        for (Node leaf : leaves)
        {
            Node tableNode = retrieveTableFromInnerNode(leaf);
            if (tableNode != null)
            {
                ArrayUtils.addToListIfNotAlreadyIn(toReturn, tableNode);
            }
        }
        return toReturn;
    }

    protected List<Node> pruneLeavesFromTableOnes(List<Node> leaves,
                                                  List<Node> tables)
    {
        return retrieveTableOrNonTableLeaves(leaves, false, tables);
    }

    protected List<Node> retrieveTableLeaves(List<Node> leaves,
                                             List<Node> tables)
    {
        return retrieveTableOrNonTableLeaves(leaves, true, tables);
    }

    protected List<Node> retrieveTableOrNonTableLeaves(List<Node> leaves,
                                                       boolean retrieveTableLeaves,
                                                       List<Node> tables)
    {
        if (tables == null)
        {
            tables = retrieveTablesFromLeaves(leaves);
        }
        if ((tables == null) || (tables.isEmpty()))
        {
            if (retrieveTableLeaves)
            {
                return null;
            } else
            {
                return leaves;
            }

        }
        List<Node> toReturn = new ArrayList<>();
        for (Node leaf : leaves)
        {
            boolean leafIsInATable = checkIfLeafIsInATable(leaf, tables);
            if (retrieveTableLeaves)
            {
                if (leafIsInATable)
                {
                    toReturn.add(leaf);
                }
            } else
            {
                if (!leafIsInATable)
                {
                    toReturn.add(leaf);
                }
            }

        }
        return toReturn;
    }

    protected boolean checkIfLeafIsInATable(Node leaf, List<Node> tables)
    {
        for (Node table : tables)
        {
            boolean checkIfLeafIsInTable = XmlManagement.checkIfNodeIsAncestor(
                    table, leaf);
            if (checkIfLeafIsInTable)
            {
                return true;
            }
        }
        //se sono arrivato qui vuol dire che la foglia non è in nessuna tabella
        return false;
    }

    protected Node retrieveTableFromInnerNode(Node innerNode)
    {
        if (checkIfComponentIsTable(innerNode))
        {
            return innerNode;
        }
        Node parentNode = innerNode.getParentNode();
        if (parentNode != null)
        {
            return retrieveTableFromInnerNode(parentNode);
        } else
        {
            return null;
        }
    }

    protected boolean checkIfComponentIsTable(Node componentNode)
    {
        //devo vedere se ha un solo body, che contiene poi row e celle
        List<Node> bodyList
                = XmlManagement.
                retrieveChildrenGivenName(componentNode, BODY);
        if ((bodyList != null) && (bodyList.size() == 1))
        {
            Node bodyNode = bodyList.get(0);
            return checkIfBodyContainsRowsWithCells(bodyNode);

        } else
        {
            //non ho body oppure ho più di un body
            return false;
        }

    }

    protected boolean checkIfBodyContainsRowsWithCells(Node bodyNode)
    {
        List<Node> bodyChildren = XmlManagement.retrieveElementChildren(
                bodyNode);
        if (bodyChildren != null)
        {
            if (bodyChildren.isEmpty())
            {
                return false;//la tabella deve contenere almeno una riga, se non c'è nessun nodo figlio non contiene righe
            }
            for (Node bodyChild : bodyChildren)//verifico se i figli sono tutti riga
            {
                if (bodyChild.getNodeName().equals(ROW))
                {
                    return checkIfAllRowChildrenAreCell(bodyChild);
                } else
                {
                    //non è tabella perchè un nodo non è row
                    return false;
                }
            }
        } else
        {
            // se il body non ha elementi figli, non è una tabella
            return false;
        }
        return false;
    }

    protected boolean checkIfAllRowChildrenAreCell(Node bodyChild)
    {
        List<Node> rowChildren
                = XmlManagement.retrieveElementChildren(bodyChild);
        if (rowChildren != null)
        {
            if (rowChildren.isEmpty())
            {
                //se la row non ha figli allora non è tabella
                return false;
            }
            for (Node rowChild : rowChildren)
            {
                if (!(rowChild.getNodeName().equals(CELL)))
                {
                    //un figlio della riga non è cella e quindi non è una tabella
                    return false;
                }
            }
            //se dopo il ciclo ho trovato tutti figli cell, allora ho una tabella
            return true;
        } else
        {
            //se row non ha figli non è una tabella
            return false;
        }
    }

    /**
     * crea la cella di tabella che rappresenta l'header o il footer delle
     * pagine nel multipagina
     *
     * @param headerFooterCell La cella di Selector da usare come header o
     * footer
     * @param multipage il tag multipage da SAP con gli oggetti da inserire in
     * header o footer
     * @param emptyEscape the value of emptyEscape
     * @param selectorDocument the value of selectorDocument
     * @param pageHeight the value of pageHeight
     * @see
     * #processInternalLayoutCell(it.ekr.pdffromselector.parsingengine.selectorobjects.SelectorLayoutCell,
     * com.itextpdf.text.pdf.PdfPTable, java.util.List, java.util.Iterator)
     * @return the com.itextpdf.text.pdf.PdfPCell
     */
    protected PdfPCell processHeaderFooterLayoutCell(
            SelectorLayoutCell headerFooterCell, Node multipage,
            String emptyEscape, SelectorDocument selectorDocument,
            double pageHeight, PdfWriter writer) throws
            SAXException,
            URISyntaxException, SelectorDocumentException, IdmlLibException,
            DocumentException, IOException, DOMException,
            ParserConfigurationException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException
    {
        /*Double absoluteY = headerFooterCell.getAbsoluteY();
         double fromUpperLeftYToLowerLeftY =
         CoordinatesTransformer.fromUpperLeftYToLowerLeftY(absoluteY, pageHeight);
         System.out.println("reached Cell Y "+absoluteY.toString()+" -> "+fromUpperLeftYToLowerLeftY);*/
        PdfPCell toReturn = new PdfPCell();
        TintAndColorWrapper fillTintAndColorWrapper
                = headerFooterCell.getFillTintAndColorWrapper();
        if (fillTintAndColorWrapper != null)
        {
            CellBackgroundColoring colorer = new CellBackgroundColoring();
            colorer.setColor(fillTintAndColorWrapper);
            toReturn.setCellEvent(colorer);
        }
        VerticalJustification verticalJustification
                = headerFooterCell.getVerticalJustification();

        if (verticalJustification != null)
        {
            toReturn.setVerticalAlignment(PdfSupportUtils.
                    convertTableCellVerticalJustification(verticalJustification));
        }

        toReturn.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        double height = headerFooterCell.getHeight();
        toReturn.setMinimumHeight((float) height);
        Double strokeWeight = headerFooterCell.getStrokeWeight();
        if (strokeWeight != null)
        {
            toReturn.setBorderWidth(strokeWeight.floatValue());
            //per allineare piu verso l'interno aggiungo un padding left pari allo stroke, verificato nel pdf di ouput
            //toReturn.setPaddingLeft(strokeWeight.floatValue());
            toReturn.setTop(strokeWeight.floatValue());
        } else
        {
            toReturn.setBorder(0);
        }

        TintAndColorWrapper strokeTintAndColor
                = headerFooterCell.getStrokeTintAndColor();
        if (strokeTintAndColor != null)
        {
            BaseColor strokeBaseColor
                    = PdfSupportUtils.
                    calculateBaseColorFromTintAndColorWrapper(
                            strokeTintAndColor);
            toReturn.setBorderColor(strokeBaseColor);
        }

        //double width = headerFooterCell.getWidth();
        //da qui non devo cercare le celle layout contenute ma devo aggiungere come faccio quando aggiungo in pagina
        List<Node> componentList
                = XmlManagement.
                retrieveNodesRecByNameStoppingAtFirstLevelFound(multipage,
                                                                COMPONENT);
        int done = 0;
        int total = componentList.size();
        for (Node componentTag : componentList)
        {
            pbCalculator.addLabelAfterLabelIfNotAlreadyIn(
                    "processComponentInHeaderFooter", "processMultipage");
            Double calculateProgressByLabelNormalized
                    = pbCalculator.calculateProgressByLabelNormalized(
                            (double) done, (double) total,
                            "processComponentInHeaderFooter", 100.0);
            setProgress(calculateProgressByLabelNormalized.intValue());
            processComponentInHeaderFooter(componentTag, headerFooterCell,
                                           toReturn, emptyEscape,writer);//header e footer non possono contenere subpages, per questo non viene passato il livello
            done++;
        }
        pbCalculator.removeLabel("processComponentInHeaderFooter");

        return toReturn;
    }

    /**
     * inserisce un nodo component dentro l'header o il footer
     *
     * @param componentTag il tag del componente da creare
     * @param headerFooterCell la layout cell di Selector da usare come header o
     * footer
     * @param headerFooterTableCell la cella di tabella per creare nel pdf la
     * cella layout contenente header e footer (similmente ai floating box)
     * @see
     * #processInternalLayoutCell(it.ekr.pdffromselector.parsingengine.selectorobjects.SelectorLayoutCell,
     * com.itextpdf.text.pdf.PdfPTable, java.util.List, java.util.Iterator)
     * @see #processComponent(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     */
    private void processComponentInHeaderFooter(Node componentTag,
                                                ISelectorObject headerFooterCell,
                                                PdfPCell headerFooterTableCell,
                                                String emptyEscape,PdfWriter writer)
            throws IdmlLibException, IOException,
                   DocumentException, ParserConfigurationException,
                   URISyntaxException, DOMException, SAXException,
                   SelectorDocumentException, NoSuchFieldException,
                   IllegalArgumentException, IllegalAccessException
    {
        //recupero l'oggetto in base al tag figlio del componente
        Node firstChild = componentTag.getFirstChild();
        while (firstChild != null && firstChild.getNodeType()
                != Node.ELEMENT_NODE)
        {
            firstChild = firstChild.getNextSibling();
        }
        String nodeName = firstChild.getNodeName();
        String[] split = nodeName.split("\\-");
        List<String> splittedName = Arrays.asList(split);
        List<ISelectorObject> containedObjectsByTypePath
                = headerFooterCell.getContainedObjectsByTypePath(
                        splittedName);
        if ((containedObjectsByTypePath != null)
                && (!containedObjectsByTypePath.
                isEmpty()))
        {
            ISelectorObject selectorObject
                    = containedObjectsByTypePath.get(0);
            if (selectorObject instanceof SelectorFlowUnit)
            {
                List<ISelectorObject> flowObjectChildren
                        = selectorObject.getContainedObjects();
                if (flowObjectChildren.size() == 1)
                {
                    //ho un solo figlio dell'oggetto flusso, verifico se ha un solo o piu paragrafi
                    ISelectorObject flowObjectChild = flowObjectChildren.get(0);
                    List<SelectorParagraph> containedSelectorParagraphs
                            = flowObjectChild.
                            getContainedSelectorParagraphs();
                    if (containedSelectorParagraphs.size() == 1)
                    {
                        SelectorParagraph singleParagraph
                                = containedSelectorParagraphs.get(0);
                        //ho un solo paragrafo, quindi sono su un oggetto singolo di Selector, ma se ho piu foglie devo gestirlo come multiparagrafo
                        if (XmlManagement.retrieveLeafNode(firstChild) != null)
                        {
                            addParagraphToLayoutCell(firstChild, singleParagraph,
                                                     headerFooterTableCell,
                                                     emptyEscape, true);

                        } else
                        {
                            processMultiparagraphInFlowUnitInLayoutCell(
                                    firstChild, selectorObject,
                                    headerFooterTableCell, emptyEscape);
                        }

                    } else
                    {
                        if (flowObjectChild instanceof SelectorTable)
                        {
                            PdfPTable pdfTable
                                    = createPdfTable(flowObjectChild,
                                                     firstChild,
                                                     emptyEscape);
                            headerFooterTableCell.addElement(pdfTable);
                        } else
                        {
                            //si tratta di un multiparagrafo dentro un singolo blocco testo

                            processMultiparagraphInFlowUnitInLayoutCell(
                                    firstChild,
                                    selectorObject, headerFooterTableCell,
                                    emptyEscape);

                        }
                    }
                } else
                {
                    processMultiparagraphInFlowUnitInLayoutCell(firstChild,
                                                                selectorObject,
                                                                headerFooterTableCell,
                                                                emptyEscape);

                }
            } else
            {
                //se non ho oggetti per quel tipo, vuol dire che devo cercare i floating box che come tipo abbiano tutto il nome del tag
                processFloatingBoxInHeaderFooter(headerFooterCell, nodeName,
                                                 firstChild,
                                                 headerFooterTableCell,
                                                 emptyEscape,writer);
            }
        } else
        {
            //se non ho oggetti per quel tipo, vuol dire che devo cercare i floating box che come tipo abbiano tutto il nome del tag
            processFloatingBoxInHeaderFooter(headerFooterCell, nodeName,
                                             firstChild,
                                             headerFooterTableCell, emptyEscape,writer);
        }

    }

    protected void processFloatingBoxInHeaderFooter(
            ISelectorObject headerFooterCell,
            String nodeName,
            Node firstChild,
            PdfPCell headerFooterTableCell, String emptyEscape,PdfWriter writer)
            throws DocumentException, IllegalArgumentException,
                   IllegalAccessException, SelectorDocumentException,
                   DOMException,
                   ParserConfigurationException, NoSuchFieldException,
                   IdmlLibException,
                   SAXException, IOException, URISyntaxException
    {

        List<SelectorFloatingBox> containedSelectorFloatingBoxesByType
                = headerFooterCell.
                getContainedSelectorFloatingBoxesByType(
                        nodeName);
        if (containedSelectorFloatingBoxesByType != null
                && !containedSelectorFloatingBoxesByType.isEmpty())
        {
            SelectorFloatingBox floatingBox
                    = containedSelectorFloatingBoxesByType.get(0);
            PdfPTable floatingBoxTable = new PdfPTable(1);
            Double spaceBeforeFromFloatingBox = floatingBox.getSpaceBefore();
            if (spaceBeforeFromFloatingBox != null)
            {
                floatingBoxTable.setSpacingBefore(
                        spaceBeforeFromFloatingBox.floatValue());
            }

            Double spaceAfterFromFloatingBox = floatingBox.getSpaceAfter();
            if (spaceAfterFromFloatingBox != null)
            {
                floatingBoxTable.setSpacingAfter(spaceAfterFromFloatingBox.
                        floatValue());
            }
            SelectorLayoutCell mainLayoutCell
                    = (SelectorLayoutCell) headerFooterCell;

            SelectorLayoutCell mainCellFloatingBox
                    = floatingBox.getMainCellFloatingBox();
            double factor = mainCellFloatingBox.getWidth() / mainLayoutCell.
                    getWidth();
            factor *= 100.0;
            floatingBoxTable.setWidthPercentage((float) factor);
            List<Node> retrieveElementChildren
                    = XmlManagement.retrieveElementChildren(firstChild);
            if (retrieveElementChildren.isEmpty())
            {
                //se non ha elementi figli, vuol dire che voglio usare il nodo stesso (per eviitare l'obbligo di avere un figlio unico)
                retrieveElementChildren = new ArrayList<>();
                retrieveElementChildren.add(firstChild);
            }
            Iterator<Node> iterator = retrieveElementChildren.iterator();
            ReachedCellRectangleAndImageIndexWrapper mainFloatingBoxTableCellWrapped = processInternalLayoutCell(
                    mainCellFloatingBox, floatingBoxTable,
                    retrieveElementChildren, iterator, emptyEscape,writer,1);//finora in header e footer metto sempre solo la prima immagine
            floatingBoxTable.addCell(mainFloatingBoxTableCellWrapped.reachedCell);
            String horizontalAlign = floatingBox.getHorizontalAlign();
            if (horizontalAlign != null)
            {
                int horizontalAlignForTable = PdfSupportUtils.
                        convertFloatingBoxAlignToTableAlign(horizontalAlign);
                floatingBoxTable.setHorizontalAlignment(horizontalAlignForTable);
            }

            headerFooterTableCell.addElement(floatingBoxTable);

        }
    }

    /**
     * creazione tabella pdf
     *
     * @param flowObjectChild il figlio dell'oggetto flusso contenente la
     * tabella
     * @param firstChild il nodo dell'XML SAP contenente la tabella
     * @return la tabella PDF creata
     */
    public PdfPTable createPdfTable(ISelectorObject flowObjectChild,
                                    Node firstChild, String emptyEscape) throws
            IdmlLibException,
            IOException, NoSuchFieldException, SAXException,
            ParserConfigurationException, IllegalAccessException,
            SecurityException, DocumentException, IllegalArgumentException,
            URISyntaxException, SelectorDocumentException
    {
        SelectorTable table
                = (SelectorTable) flowObjectChild;
        //int maxColumns = table.getMaxColumns();
        List<Double> columnWidths = table.getColumnWidths();
        float[] columnWidthsFloat
                = ArrayUtils.convertDoubleListToFloatArray(columnWidths);
        PdfPTable pdfTable = new PdfPTable(columnWidthsFloat);
        double width = table.getWidth();
        //pdfTable.setTotalWidth((float) width); non prende la dimensione, va messa al 100%
        pdfTable.setWidthPercentage(100);
        List<Node> headTagWrapper
                = XmlManagement.retrieveChildrenGivenName(
                        firstChild, HEAD);
        List<TintAndColorWrapper> colors = new ArrayList<>();
        List<PdfPCell> createdTableHeaderRows = null;
        int numHeaderRows = 0;
        if (!headTagWrapper.isEmpty())
        {
            Node headTag = headTagWrapper.get(0);

            List<SelectorTableRow> headerRows
                    = table.getHeaderRows();

            createdTableHeaderRows
                    = createTableRows(headTag, headerRows, colors, emptyEscape);

            //devo calcolare il numero di righe
            List<Node> rowsTags
                    = XmlManagement.retrieveChildrenGivenName(
                            headTag, ROW);
            numHeaderRows = rowsTags.size();

        }

        int numFooterRows = 0;
        List<PdfPCell> createdTableFooterRows = null;
        List<Node> footTagWrapper = XmlManagement.retrieveChildrenGivenName(firstChild, FOOT);
        //le righe di footer vanno messe subito dopo l'intestazione
        if (!footTagWrapper.isEmpty())
        {
            Node footTag = footTagWrapper.get(0);

            List<SelectorTableRow> footerRows
                    = table.getBodyRows();//le righe di footer sono un sottoinsieme delle righe body, per la precisione le ultime

            //devo calcolare il numero di righe
            List<Node> rowsTags
                    = XmlManagement.retrieveChildrenGivenName(
                            footTag, ROW);
            numFooterRows = rowsTags.size();

            List<SelectorTableRow> prunedFooterRows = footerRows;
            prunedFooterRows = ArrayUtils.reverseList(prunedFooterRows);
            if (prunedFooterRows.size() >= numFooterRows)
            {
                //prendo solo le righe che sono di footer
                prunedFooterRows = prunedFooterRows.subList(0, numFooterRows);
            }
            //dopo aver preso solo i pezzi che mi interessano reinverto la lista
            prunedFooterRows = ArrayUtils.reverseList(prunedFooterRows);
            createdTableFooterRows
                    = createTableRows(footTag, prunedFooterRows, colors, emptyEscape);

        }

        /*
        
         */
        List<Node> bodyTagWrapper
                = XmlManagement.retrieveChildrenGivenName(
                        firstChild, BODY);
        Node bodyTag = bodyTagWrapper.get(0);
        List<SelectorTableRow> bodyRows
                = table.getBodyRows();
        List<SelectorTableRow> prunedBodyRows = bodyRows;
        if (numFooterRows > 0)
        {
            //se ho il footer devo rimuovere le ultime righe dalle righe usate come template per il corpo, sempre che ce ne siano abbastanza
            if (bodyRows.size() - numFooterRows > 0)
            {
                prunedBodyRows = new ArrayList<>();
                for (int i = 0; i < bodyRows.size() - numFooterRows; i++)
                {
                    SelectorTableRow addingBodyRow = bodyRows.get(i);
                    prunedBodyRows.add(addingBodyRow);
                }

            }
        }
        List<PdfPCell> createdBodyTableRows
                = createTableRows(bodyTag, prunedBodyRows, colors, emptyEscape);

        //aggiungo ora le righe di intestazione perche devo avere la lista dei colori completa
        Iterator<TintAndColorWrapper> colorsIterator = colors.iterator();
        if (createdTableHeaderRows != null)
        {
            for (PdfPCell headerCell : createdTableHeaderRows)
            {
                CellBackgroundColoring colorer = new CellBackgroundColoring();
                TintAndColorWrapper currentCellColor = colorsIterator.next();
                colorer.setColor(currentCellColor);
                headerCell.setCellEvent(colorer);
                pdfTable.addCell(headerCell);
            }
            pdfTable.setHeaderRows(numHeaderRows + numFooterRows);//nell'header vanno comprese anche le righe di footer, poi si estrarrà il footer dalle righe di header
        }
        //le righe di footer vanno messe dopo l'header, itext le vule così
        if (createdTableFooterRows != null)
        {
            for (PdfPCell footerCell : createdTableFooterRows)
            {
                CellBackgroundColoring colorer = new CellBackgroundColoring();
                TintAndColorWrapper currentCellColor = colorsIterator.next();
                colorer.setColor(currentCellColor);
                footerCell.setCellEvent(colorer);
                pdfTable.addCell(footerCell);
            }
            if (numHeaderRows == 0)
            {
                //visto che le righe di footer vanno comprese nell'header, nel caso non abbia header devo dargli le righe di footer
                pdfTable.setHeaderRows(numFooterRows);
            }
            pdfTable.setFooterRows(numFooterRows);
        }

        //il corpo va messo per ultimo
        for (PdfPCell bodyCell : createdBodyTableRows)
        {
            CellBackgroundColoring colorer = new CellBackgroundColoring();
            TintAndColorWrapper currentCellColor = colorsIterator.next();
            colorer.setColor(currentCellColor);
            bodyCell.setCellEvent(colorer);
            pdfTable.addCell(bodyCell);
        }

        //pdfTable.setTotalWidth((float) width);
        Double spaceBefore = table.getSpaceBefore();
        if (spaceBefore != null)
        {
            pdfTable.setSpacingBefore(spaceBefore.floatValue());
        }
        Double spaceAfter = table.getSpaceAfter();
        if (spaceAfter != null)
        {
            pdfTable.setSpacingAfter(spaceAfter.floatValue());
        }
        //pdfTable.setTotalWidth((float) width);
        //pdfTable.setWidthPercentage(100);
        return pdfTable;
    }

    /**
     * crea le righe di tabella appartenenti a intestazione o corpo
     *
     * @param headOrBodyTag tag nell'XML da SAP contenente le righe
     * dell'intestazione o del corpo
     * @param templateHeaderOrBody insieme di righe dalla tabella di Selector
     * che possono essere le righe di intestazione o del corpo
     * @param colors colori da assegnare alle celle
     */
    private List<PdfPCell> createTableRows(Node headOrBodyTag,
                                           List<SelectorTableRow> templateHeaderOrBody,
                                           List<TintAndColorWrapper> colors,
                                           String emptyEscape)
            throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException,
            URISyntaxException, IdmlLibException, SecurityException,
            IllegalAccessException, IllegalArgumentException,
            NoSuchFieldException, DocumentException
    {
        List<PdfPCell> toReturn = new ArrayList<>();
        List<Node> rowsTags
                = XmlManagement.retrieveChildrenGivenName(headOrBodyTag, ROW);
        for (int i = 0; i < rowsTags.size(); i++)
        {
            Node rowTag = rowsTags.get(i);
            SelectorTableRow templateRow = calculateTemplateRow(
                    templateHeaderOrBody, i);
            List<PdfPCell> createdSingleTableRow
                    = createSingleTableRow(rowTag, templateRow, colors,
                                           emptyEscape);
            toReturn.addAll(createdSingleTableRow);
        }
        return toReturn;
    }

    /**
     * crea una riga della tabella
     *
     * @param row il nodo nel documento SAP che descrive la riga
     * @param templateRow la riga di tabella nel documento di Selector
     * @param colors la lista dei colori che verra usata per assegnare i colori
     * di sfondo alla tabella
     * @return la lista delle celle di tabella che costituiscono la riga
     */
    private List<PdfPCell> createSingleTableRow(Node row,
                                                SelectorTableRow templateRow,
                                                List<TintAndColorWrapper> colors,
                                                String emptyEscape)
            throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException,
            URISyntaxException, IdmlLibException, SecurityException,
            IllegalAccessException, IllegalArgumentException,
            NoSuchFieldException, DocumentException
    {
        List<PdfPCell> toReturn = new ArrayList<>();
        List<Node> cellTags
                = XmlManagement.retrieveChildrenGivenName(row, CELL);
        int reachedIndexUsingSpan = 0;
        List<SelectorTableCell> cells = templateRow.getCells();
        for (Node cellTag : cellTags)
        {
            SelectorTableCell templateCell = calculateTemplateCell(cells,
                                                                   reachedIndexUsingSpan);
            PdfPCell createdCell = createSingleTableCell(cellTag, templateCell,
                                                         colors, emptyEscape);
            toReturn.add(createdCell);
            int currentCellNodeSpan = calculateHSpanFromCellNode(cellTag);
            reachedIndexUsingSpan += currentCellNodeSpan;
        }
        return toReturn;
    }

    /**
     * estrae il valore dell'attributo span orizzontale dal nodo cella nel
     * documento SAP
     *
     * @param cellTag il tag contenente la cella nel documento SAP
     * @return il valore dell'attributo h-span o 1 se non ha l'attributo
     */
    private int calculateHSpanFromCellNode(Node cellTag) throws
            DOMException, NumberFormatException
    {
        int toReturn = 1;
        NamedNodeMap attributes = cellTag.getAttributes();
        Node hSpanAttribute = attributes.getNamedItem(HSPAN);
        if (hSpanAttribute != null)
        {
            String textContent = hSpanAttribute.getTextContent();
            Integer integerValue = new Integer(textContent);
            if (integerValue > 0)
            {
                toReturn = integerValue;
            }
        }
        return toReturn;
    }

    /**
     * costruisce la cella di tabella da inserire
     *
     * @param cell il nodo che descrive la cella nell'XML da SAP
     * @param templateCell la cella della tabella del documento di Selector da
     * usare come template
     * @param colors lista dei colori di sfondo da assegnare alle celle finita
     * la creazione della tabella, in PDF lib infatti i colori, affinche
     * supportino anche il canale alpha vanno impostati a fine tabella
     * @return la cella pdf creata a partire dal template e i dati dell'XML SAP
     */
    private PdfPCell createSingleTableCell(Node cell,
                                           SelectorTableCell templateCell,
                                           List<TintAndColorWrapper> colors,
                                           String emptyEscape)
            throws URISyntaxException, IdmlLibException, IOException,
                   SelectorDocumentException, SelectorFileNotExistentException,
                   ParserConfigurationException, SAXException, SecurityException,
                   IllegalAccessException, IllegalArgumentException,
                   NoSuchFieldException, DocumentException
    {
        PdfPCell toReturn = new PdfPCell();
        NamedNodeMap attributes = cell.getAttributes();
        Node hSpanAttribute = attributes.getNamedItem(HSPAN);
        if (hSpanAttribute != null)
        {
            String textContent = hSpanAttribute.getTextContent();
            Integer hSpanValue = new Integer(textContent);
            toReturn.setColspan(hSpanValue);
        }

        double topEdgeStrokeWeight = templateCell.getTopEdgeStrokeWeight();
        toReturn.setBorderWidthTop((float) topEdgeStrokeWeight);
        TintAndColorWrapper topEdgeStrokeTintAndColorWrapper
                = templateCell.getTopEdgeStrokeTintAndColorWrapper();
        BaseColor topEdgeStrokeBaseColor
                = PdfSupportUtils.calculateBaseColorFromTintAndColorWrapper(
                        topEdgeStrokeTintAndColorWrapper);
        toReturn.setBorderColorTop(topEdgeStrokeBaseColor);
        Double topInset = templateCell.getTopInset();
        if (topInset != null)
        {
            toReturn.setPaddingTop(topInset.floatValue());
        }

        double bottomEdgeStrokeWeight = templateCell.getBottomEdgeStrokeWeight();
        toReturn.setBorderWidthBottom((float) bottomEdgeStrokeWeight);
        TintAndColorWrapper bottomEdgeStrokeTintAndColorWrapper
                = templateCell.
                getBottomEdgeStrokeTintAndColorWrapper();
        BaseColor bottomEdgeStrokeBaseColor
                = PdfSupportUtils.calculateBaseColorFromTintAndColorWrapper(
                        bottomEdgeStrokeTintAndColorWrapper);
        toReturn.setBorderColorBottom(bottomEdgeStrokeBaseColor);
        Double bottomInset = templateCell.getBottomInset();
        if (bottomInset != null)
        {
            toReturn.setPaddingBottom(bottomInset.floatValue());
        }

        double leftEdgeStrokeWeight = templateCell.getLeftEdgeStrokeWeight();
        toReturn.setBorderWidthLeft((float) leftEdgeStrokeWeight);
        TintAndColorWrapper leftEdgeStrokeTintAndColorWrapper
                = templateCell.
                getLeftEdgeStrokeTintAndColorWrapper();
        BaseColor leftEdgeStrokeBaseColor
                = PdfSupportUtils.calculateBaseColorFromTintAndColorWrapper(
                        leftEdgeStrokeTintAndColorWrapper);
        toReturn.setBorderColorLeft(leftEdgeStrokeBaseColor);
        Double leftInset = templateCell.getLeftInset();
        if (leftInset != null)
        {
            toReturn.setPaddingLeft(leftInset.floatValue());
        }

        double rightEdgeStrokeWeight = templateCell.getRightEdgeStrokeWeight();
        toReturn.setBorderWidthRight((float) rightEdgeStrokeWeight);
        TintAndColorWrapper rightEdgeStrokeTintAndColorWrapper
                = templateCell.
                getRightEdgeStrokeTintAndColorWrapper();
        BaseColor rightEdgeStrokeBaseColor
                = PdfSupportUtils.calculateBaseColorFromTintAndColorWrapper(
                        rightEdgeStrokeTintAndColorWrapper);
        toReturn.setBorderColorRight(rightEdgeStrokeBaseColor);
        Double rightInset = templateCell.getRightInset();
        if (rightInset != null)
        {
            toReturn.setPaddingRight(rightInset.floatValue());
        }

        TintAndColorWrapper fillTintAndColorWrapper
                = templateCell.getFillTintAndColorWrapper();
        colors.add(fillTintAndColorWrapper);

        List<SelectorParagraph> containedSelectorParagraphs
                = templateCell.getContainedSelectorParagraphs();
        List<Node> cellTagElementChildren
                = XmlManagement.retrieveElementChildren(cell);
        if (cellTagElementChildren.isEmpty())
        {
            int paragraphIndex = 0;
            createMultiparagraphOrImageInTableCell(containedSelectorParagraphs,
                                                   paragraphIndex, cell,
                                                   toReturn, emptyEscape);
        } else
        {
            for (int paragraphIndex = 0; paragraphIndex
                 < cellTagElementChildren.size();
                 paragraphIndex++)
            {
                createMultiparagraphOrImageInTableCell(
                        containedSelectorParagraphs,
                        paragraphIndex, cell, toReturn, emptyEscape);
            }
        }
        VerticalJustification verticalJustification
                = templateCell.getVerticalJustification();
        if (verticalJustification != null)
        {
            int convertedCellJustification
                    = PdfSupportUtils.convertTableCellVerticalJustification(
                            verticalJustification);
            toReturn.setVerticalAlignment(convertedCellJustification);
        }
        Double rotationAngle = templateCell.getRotationAngle();
        if (rotationAngle != null)
        {
            toReturn.setRotation(rotationAngle.intValue());
        }
        return toReturn;
    }

    /**
     * inserisce il testo del paragrafo o l'immagine nella cella di tabella
     *
     * @param containedSelectorParagraphs i paragrafi di selector da usare come
     * template
     * @param paragraphIndex l'indice del paragrafo all'interno della cella
     * nella tabrella del file da SAP
     * @param cell il nodo cella nella tabella dell' XML fornito da SAP
     * @param pdfTableCell la cella di tabella PDF destinata ad accogliere
     * l'immagine o i paragrafi
     */
    private void createMultiparagraphOrImageInTableCell(
            List<SelectorParagraph> containedSelectorParagraphs,
            int paragraphIndex,
            Node cell,
            PdfPCell pdfTableCell, String emptyEscape) throws
            DOMException, SecurityException, IOException, URISyntaxException,
            IllegalAccessException, ParserConfigurationException,
            NoSuchFieldException, SelectorDocumentException, IdmlLibException,
            DocumentException, SAXException, IllegalArgumentException
    {
        SelectorParagraph choosenParagraph = calculateTemplateParagraph(
                containedSelectorParagraphs, paragraphIndex);

        Node currentCellLeaf = XmlManagement.retrieveLeafNode(cell);
        if (currentCellLeaf == null)
        {
            //vuol dire che ho piu foglie nel nodo cella
            List<Node> retrieveLeavesNodes
                    = XmlManagement.retrieveLeavesNodes(cell);
            currentCellLeaf = retrieveLeavesNodes.get(paragraphIndex);
        }
        if (currentCellLeaf != null)
        {
            //puo essere null se non ho trovato un nodo foglia dell'indice corrispondente
            List<ISelectorObject> choosenParagraphChildren
                    = choosenParagraph.getContainedObjects();
            String textContent = currentCellLeaf.getTextContent();
            if ((choosenParagraphChildren != null) && (choosenParagraphChildren.
                    size() > 0))
            {
                //vuol dire che contiene almeno un figlio che e una immagine e quindi va aggiunta una immagine
                createImageInTableCell(textContent, choosenParagraphChildren,
                                       pdfTableCell);

            } else
            {

                Paragraph content = new Paragraph();
                stilyzeParagraph(choosenParagraph, content);
                if (textContent == null || textContent.length() == 0)
                {
                    textContent = choosenParagraph.getTextContent();
                }
                textContent = textContent.replace(emptyEscape, "");//sostituisco i |c| anche se sono nell'xml
                content.add(textContent);
                pdfTableCell.addElement(content);

            }
        }
    }

    /**
     * inserisce una immagine in tabella, la cella template del documento
     * Selector deve essere una cella con un solo paragrafo con dentro una sola
     * immagine inline
     *
     * @param textContent il contenuto testuale del nodo del documento SAP che
     * dovrebbe essere un percorso di file completo
     * @param choosenParagraphChildren i figkli del paragrafo, dovrebbe essere
     * una sola immagine inline
     * @param pdfTableCell la cella di tabella PDF destinata a contenere
     * l'immagine
     */
    protected void createImageInTableCell(String textContent,
                                          List<ISelectorObject> choosenParagraphChildren,
                                          PdfPCell pdfTableCell) throws
            URISyntaxException, IOException, BadElementException
    {

        File imageFile = new File(textContent);
        imageFile = converEventuallytFromUriPathToFile(textContent, imageFile);
        if (textContent == null || textContent.length() == 0)
        {
            ISelectorObject paragraphChild = choosenParagraphChildren.
                    get(
                            0);
            SelectorInlineImage inlineImage
                    = (SelectorInlineImage) paragraphChild;
            URI href = inlineImage.getHref();
            imageFile = new File(href);
        }
        if (imageFile.exists() && (!imageFile.isDirectory()))
        {
            Image inCellImage = Image.getInstance(imageFile.
                    getCanonicalPath());
            //da specifiche le immagini devono essere delle dimensioni corrette gia
            float height = inCellImage.getHeight();
            float width = inCellImage.getWidth();
            inCellImage.scaleAbsolute(width, height);//*/
            pdfTableCell.addElement(inCellImage);
        }
    }

    /**
     * calcola che cella di tabella si Selector usare come template,
     * considerando gli span di colonna
     *
     * @param cells l'elenco delle celle da usare come template, queste celle
     * non devono avere span
     * @param index l'indice della cella sul documento SAP
     * @return la cella di tabella da usare come template
     */
    private SelectorTableCell calculateTemplateCell(
            List<SelectorTableCell> cells,
            int index)
    {
        int reachedIndexWithSpan = 0;
        SelectorTableCell toReturn = null;
        for (SelectorTableCell tableCell : cells)
        {
            if (reachedIndexWithSpan > index)
            {
                return toReturn;
            } else
            {
                toReturn = tableCell;
                reachedIndexWithSpan += tableCell.
                        getColumnSpan();
            }
        }

        return toReturn;
    }

    /**
     * calcola la riga di tabella di Selector da usare per stilizzare la riga
     * della tabella indicata dall'indice
     *
     * @param templateRows la lista delle righe di tabella di Selector da usare
     * @param index l'indice della riga di tabella della tabella sul documento
     * SAP
     * @return la riga corrispondente all'indice o l'ultima riga se l'indice
     * eccede il numero delle righe di template
     */
    private SelectorTableRow calculateTemplateRow(
            List<SelectorTableRow> templateRows,
            int index)
    {
        int templateRowsQuantity = templateRows.size();
        if (index >= templateRowsQuantity)
        {
            //prendo l'ultima riga
            return templateRows.get(templateRowsQuantity - 1);
        } else
        {
            return templateRows.get(index);
        }
    }

    /**
     * aggiunge il paragrafo alla pagina (cella preincipale) assumendo che ci
     * sia solo un paragrafo nel nodo del documento SAP
     *
     * @param leafNodeForParagraph il nodo del documento SAP contenente il
     * paragrafo
     * @param selectorParagraphWrapper l'oggetto Selector contenente il
     * paragrafo da usare come template
     * @param pdf il documento su cui inserire il paragrafo
     * @see #addParagraphToPdf(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     */
    private void addParagraphToPdf(Node leafNodeForParagraph,
                                   ISelectorObject selectorParagraphWrapper,
                                   Document pdf, String emptyEscape, boolean excludeTableParagraphs) throws
            SAXException,
            URISyntaxException,
            SelectorDocumentException, IdmlLibException,
            DocumentException, DOMException, IOException,
            ParserConfigurationException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException
    {
        addParagraphToPdf(leafNodeForParagraph, 0, selectorParagraphWrapper, pdf,
                          emptyEscape, excludeTableParagraphs);
    }

    /**
     * aggiunge il paragrafo alla pagina (cella preincipale) secondo il suo
     * indice sul documento SAP
     *
     * @param leafNodeForParagraph il nodo del documento SAP contenente il
     * paragrafo
     * @param paragraphIndex l'indice di paragrafo nel nodo contenitore del
     * documento SAP
     * @param selectorParagraphWrapper l'oggetto Selector contenente i paragrafi
     * da usare come template
     * @param pdf il documento su cui inserire il paragrafo
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToPdf(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     */
    private void addParagraphToPdf(Node leafNodeForParagraph, int paragraphIndex,
                                   ISelectorObject selectorParagraphWrapper,
                                   Document pdf, String emptyEscape, boolean excludeTableParagraphs) throws
            SAXException,
            URISyntaxException,
            SelectorDocumentException, IdmlLibException,
            DocumentException, DOMException, IOException,
            ParserConfigurationException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException
    {
        //il nodo e solo un testo e quindi lo aggiungo come testo singolo
        leafNodeForParagraph
                = XmlManagement.retrieveLeafNode(leafNodeForParagraph);
        String textContent = leafNodeForParagraph.getTextContent();
        Paragraph pdfParagraph = new Paragraph();
        List<SelectorParagraph> containedSelectorParagraphs
                = selectorParagraphWrapper.
                getContainedSelectorParagraphs(excludeTableParagraphs);
        SelectorParagraph exampleParagraph = calculateTemplateParagraph(
                containedSelectorParagraphs, paragraphIndex);
        stilyzeParagraph(exampleParagraph, pdfParagraph);
        if (textContent == null || textContent.length() == 0)
        {
            textContent = exampleParagraph.getTextContent();
        }
        textContent = textContent.replace(emptyEscape, "");//elimino i |c| anche se sono nell'xml
        boolean add = pdfParagraph.add(textContent);
        pdf.add(pdfParagraph);
    }

    /**
     * calcola quale paragrafo usare come template, infatti se il nodo fornito
     * da SAP contiene piu paragrafi dell'oggetto Selector, si deve usare come
     * template sempre l'ultimo
     *
     * @param paragraphs lista dei paragrafi da cui si estraggono i template
     * @param index indice del paragrafo nel nodo fornito da SAP
     * @return il paragrafo dalla lista corrispondente all'indice o l'ultimo
     * della lista se l'indice eccede il numero di paragrafi nell'oggetto
     * Selector
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToPdf(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     * @see #addParagraphToPdf(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     */
    private SelectorParagraph calculateTemplateParagraph(
            List<SelectorParagraph> paragraphs, int index)
    {
        if (index < paragraphs.size())
        {
            SelectorParagraph correspondingPar = paragraphs.get(index);
            return correspondingPar;
        } else
        {
            int lastIndex = paragraphs.size() - 1;
            if (lastIndex >= 0)
            {
                SelectorParagraph lastPar = paragraphs.get(lastIndex);
                return lastPar;
            }
        }
        //vuol dire che la lista e vuota
        return null;
    }

    /**
     * inserisce un paragrafo all'interno di una cella layout, dovrebbe essre
     * l'unico paragrafo del nodo e quindi dell'oggetto in Selector
     *
     * @param leafNodeForParagraph nodo contenente il testo del paragrafo
     * @param selectorParagraphWrapper oggetto contenente il paragrafo da usare
     * come template per il paragrafo corrente
     * @param pdfLayoutCell la cella di tabella PDF che rappresenta la cella
     * layout destinata a contenere il paragrafo
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToPdf(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     * @see #addParagraphToPdf(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     */
    private void addParagraphToLayoutCell(Node leafNodeForParagraph,
                                          ISelectorObject selectorParagraphWrapper,
                                          PdfPCell pdfLayoutCell,
                                          String emptyEscape,
                                          boolean excludeTableParagraphs) throws
            SAXException,
            URISyntaxException,
            SelectorDocumentException, IdmlLibException,
            DocumentException, DOMException, IOException,
            ParserConfigurationException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException
    {
        addParagraphToLayoutCell(leafNodeForParagraph, 0,
                                 selectorParagraphWrapper, pdfLayoutCell,
                                 emptyEscape,
                                 excludeTableParagraphs);
    }

    /**
     * inserisce un paragrafo all'interno di una cella layout prendendone la
     * posizione nel nodo che lo contiene
     *
     * @param leafNodeForParagraph nodo contenente il testo del paragrafo
     * @param paragraphIndex indice del paragrafo, se il nodo paragrafo fa parte
     * di un nodo superiore che contiene altri paragrafi
     * @param selectorParagraphWrapper oggetto contenente i paragrafi da usare
     * come template per il paragrafo corrente (blocco testo o unit contenente i
     * paragrafi, selezionati poi tramite paragraphIndex)
     * @param pdfLayoutCell la cella di tabella PDF che rappresenta la cella
     * layout destinata a contenere il paragrafo
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToPdf(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     * @see #addParagraphToPdf(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     */
    private void addParagraphToLayoutCell(Node leafNodeForParagraph,
                                          int paragraphIndex,
                                          ISelectorObject selectorParagraphWrapper,
                                          PdfPCell pdfLayoutCell,
                                          String emptyEscape,
                                          boolean excludeTableParagraphs) throws
            SAXException, URISyntaxException,
            SelectorDocumentException, IdmlLibException,
            DocumentException, DOMException, IOException,
            ParserConfigurationException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException
    {
        //il nodo e solo un testo e quindi lo aggiungo come testo singolo
        leafNodeForParagraph
                = XmlManagement.retrieveLeafNode(leafNodeForParagraph);
        String textContent = leafNodeForParagraph.getTextContent();
        Paragraph pdfParagraph = new Paragraph();
        List<SelectorParagraph> containedSelectorParagraphs
                = selectorParagraphWrapper.
                getContainedSelectorParagraphs();
        if (excludeTableParagraphs)
        {
            List<SelectorParagraph> alreadyFound = containedSelectorParagraphs;
            containedSelectorParagraphs = selectorParagraphWrapper.
                    getContainedSelectorParagraphsExcludingTableOnes();
            if ((containedSelectorParagraphs == null)
                    || containedSelectorParagraphs.isEmpty())
            {
                //in extremis recupero gli stili da quelli presenti in tabella
                containedSelectorParagraphs = alreadyFound;
            }
        }
        SelectorParagraph exampleParagraph = calculateTemplateParagraph(
                containedSelectorParagraphs, paragraphIndex);
        stilyzeParagraph(exampleParagraph, pdfParagraph);
        if (textContent == null || textContent.length() == 0)
        {
            textContent = exampleParagraph.getTextContent();
        }
        textContent = textContent.replace(emptyEscape, "");//devo sostituire |c| anche se è nell'xml
        pdfParagraph.add(textContent);
        pdfLayoutCell.addElement(pdfParagraph);
    }

    /**
     * imposta lo stile del paragrafo in pdf prendendoli dallo stilre di
     * paragrafo in Selector
     *
     * @param exampleParagraph il paragrafo in Selector da cui prendere lo stile
     * @param pdfParagraph il paragrafo in pdf da stilizzare
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToLayoutCell(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.pdf.PdfPCell)
     * @see #addParagraphToPdf(org.w3c.dom.Node,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     * @see #addParagraphToPdf(org.w3c.dom.Node, int,
     * it.ekr.pdffromselector.parsingengine.selectorobjects.ISelectorObject,
     * com.itextpdf.text.Document)
     */
    public void stilyzeParagraph(SelectorParagraph exampleParagraph,
                                 Paragraph pdfParagraph) throws
            URISyntaxException, NoSuchFieldException, IdmlLibException,
            SecurityException, SelectorDocumentException, DocumentException,
            IllegalArgumentException, IllegalAccessException, IOException
    {
        AppliedFont appliedFont
                = exampleParagraph.getAppliedFont();
        Double pointSize = exampleParagraph.getPointSize();
        //imposrarte prima font e poi testo
        String fontFamilyString = null;
        if (appliedFont != null)
        {
            fontFamilyString = appliedFont.getValueAsString();
        }

        String fontStyle = exampleParagraph.getFontStyle();

        Font pdfFont = new Font();
        if (fontFamilyString != null)
        {
            BaseFont createFont = PdfSupportUtils.getBaseFont(fontFamilyString,
                                                              fontStyle);
            pdfFont = new Font(createFont);
        }

        if (pointSize != null)
        {
            float floatFontSize = pointSize.floatValue();
            pdfFont.setSize(floatFontSize);
        }

        TintAndColorWrapper fillTintAndColorWrapper
                = exampleParagraph.getFillTintAndColorWrapper();
        BaseColor baseColorForFill
                = PdfSupportUtils.calculateBaseColorFromTintAndColorWrapper(
                        fillTintAndColorWrapper);
        if (baseColorForFill != null)
        {
            pdfFont.setColor(baseColorForFill);
        }

        Double spaceBefore = exampleParagraph.getSpaceBefore();
        Double spaceAfter = exampleParagraph.getSpaceAfter();
        pdfParagraph.setFont(pdfFont);
        Double calculatedLeading = exampleParagraph.getCalculatedLeading();
        if (calculatedLeading != null)
        {
            pdfParagraph.setLeading(calculatedLeading.floatValue());
        }
        if (spaceBefore != null)
        {
            pdfParagraph.setSpacingBefore(spaceBefore.floatValue());
        }
        if (spaceAfter != null)
        {
            pdfParagraph.setSpacingAfter(spaceAfter.floatValue());
        }

        Justification justification = exampleParagraph.getJustification();
        if (justification != null)
        {
            int pdfJustification
                    = PdfSupportUtils.convertParagraphJustification(justification);
            pdfParagraph.setAlignment(pdfJustification);
        }
    }

    private SelectorLayoutCellAndRectangleWrapper addParagraphToMultipleLayoutCells(
            Node leafNodeForParagraph,
            ISelectorObject selectorParagraphWrapper,
            Document pdf, String emptyEscape,
            List<SelectorLayoutCell> containedLeafLayoutCells,
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            ColumnText ct,
            boolean excludeTableParagraphs, double pageHeight, PdfWriter writer,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws
            SelectorDocumentException, IllegalAccessException,
            IllegalArgumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, URISyntaxException,
            SecurityException, IdmlLibException, NoSuchFieldException,
            DocumentException, IOException, TrialsLimitException
    {
        return addParagraphToMultipleLayoutCells(leafNodeForParagraph, 0,
                                                 selectorParagraphWrapper, pdf,
                                                 emptyEscape,
                                                 containedLeafLayoutCells,
                                                 reachedLayoutCellAndRectangle,
                                                 ct,
                                                 excludeTableParagraphs,
                                                 pageHeight, writer,
                                                 cellsRectangles);
    }

    private SelectorLayoutCellAndRectangleWrapper addParagraphToMultipleLayoutCells(
            Node leafNodeForParagraph,
            int paragraphIndex,
            ISelectorObject selectorParagraphWrapper,
            Document pdf,
            String emptyEscape,
            List<SelectorLayoutCell> containedLeafLayoutCells,
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            ColumnText ct,
            boolean excludeTableParagraphs, double pageHeight, PdfWriter writer,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, ParserConfigurationException,
            SAXException, URISyntaxException, NoSuchFieldException,
            DocumentException, DocumentException, IOException, IdmlLibException,
            SecurityException, SecurityException, IllegalArgumentException,
            IllegalAccessException, TrialsLimitException
    {

        leafNodeForParagraph
                = XmlManagement.retrieveLeafNode(leafNodeForParagraph);
        String textContent = leafNodeForParagraph.getTextContent();
        Paragraph pdfParagraph = new Paragraph();
        List<SelectorParagraph> containedSelectorParagraphs
                = selectorParagraphWrapper.
                getContainedSelectorParagraphs();
        if (excludeTableParagraphs)
        {
            List<SelectorParagraph> alreadyFound = containedSelectorParagraphs;
            containedSelectorParagraphs = selectorParagraphWrapper.
                    getContainedSelectorParagraphsExcludingTableOnes();
            if ((containedSelectorParagraphs == null)
                    || containedSelectorParagraphs.isEmpty())
            {
                //in extremis recupero gli stili da quelli presenti in tabella
                containedSelectorParagraphs = alreadyFound;
            }
        }
        SelectorParagraph exampleParagraph = calculateTemplateParagraph(
                containedSelectorParagraphs, paragraphIndex);
        stilyzeParagraph(exampleParagraph, pdfParagraph);
        if (textContent == null || textContent.length() == 0)
        {
            textContent = exampleParagraph.getTextContent();
        }
        textContent = textContent.replace(emptyEscape, "");//elimino i |c| anche se sono nell'xml

        boolean add = pdfParagraph.add(textContent);
        reachedLayoutCellAndRectangle
                = addElementToMultipleLayoutCellsAndProsecutePages(
                        reachedLayoutCellAndRectangle,
                        containedLeafLayoutCells, ct, pdfParagraph, pdf,
                        pageHeight, false, writer, cellsRectangles);
        /*System.out.println("X after add "+reachedLayoutCell.getAbsoluteX());
         System.out.println("Y after add "+reachedLayoutCell.getAbsoluteY());
         System.out.println("width after add "+reachedLayoutCell.getWidth());
         System.out.println("height after add "+reachedLayoutCell.getHeight());*/
        return reachedLayoutCellAndRectangle;
    }

    /**
     *
     * @param reachedLayoutCellAndRectangle
     * @param containedLeafLayoutCells
     * @param ct
     * @param pageHeight
     * @param pdf
     * @param cellsRectangles
     * @return
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws SelectorDocumentException
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws DocumentException
     */
    protected SelectorLayoutCellAndRectangleWrapper initializeReachedCellColumn(
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            List<SelectorLayoutCell> containedLeafLayoutCells,
            ColumnText ct, double pageHeight, Document pdf,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws SAXException, ParserConfigurationException,
                   SelectorDocumentException, URISyntaxException, IOException,
                   IdmlLibException, DocumentException
    {

        SelectorLayoutCell reachedLayoutCell
                = containedLeafLayoutCells.get(
                        0);
        reachedLayoutCellAndRectangle
                = retrieveSelectorLayoutCellAndRectFromLayoutCell(
                        reachedLayoutCell, cellsRectangles);
        if (reachedLayoutCellAndRectangle == null)
        {
            reachedLayoutCellAndRectangle
                    = new SelectorLayoutCellAndRectangleWrapper();
            reachedLayoutCellAndRectangle.setLayoutCell(reachedLayoutCell);

            reachedLayoutCellAndRectangle = makeReachedCellRectangle(
                    reachedLayoutCellAndRectangle, pageHeight, pdf, true);
        }

        //*/
        //new Rectangle(llx, lly, urx, ury);
        //non salvo il rettangolo perchè non è stato aggiunto nel documento
        ct.setSimpleColumn(reachedLayoutCellAndRectangle.getRectangle());
        return reachedLayoutCellAndRectangle;
    }

    private SelectorLayoutCellAndRectangleWrapper processMultiparagraphInFlowUnitToMultipleLayoutCells(
            Node firstChild,
            ISelectorObject selectorObject,
            Document pdfDoc,
            String emptyEscape,
            List<SelectorLayoutCell> containedLeafLayoutCells,
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            ColumnText ct, boolean excludeTableParagraphs, double pageHeight,
            PdfWriter writer,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws
            SelectorDocumentException,
            SelectorDocumentException, IllegalAccessException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, URISyntaxException, NoSuchFieldException,
            DocumentException, IOException, IdmlLibException, SecurityException,
            TrialsLimitException
    {
        List<Node> retrieveElementChildren
                = XmlManagement.retrieveElementChildren(
                        firstChild);
        for (int i = 0; i < retrieveElementChildren.size();
             i++)
        {
            Node currentContainedChild
                    = retrieveElementChildren.get(i);
            reachedLayoutCellAndRectangle = addParagraphToMultipleLayoutCells(
                    currentContainedChild, i, selectorObject, pdfDoc,
                    emptyEscape, containedLeafLayoutCells,
                    reachedLayoutCellAndRectangle, ct,
                    excludeTableParagraphs, pageHeight, writer, cellsRectangles);
        }
        return reachedLayoutCellAndRectangle;
    }

    private ReachedCellRectangleAndImageIndexWrapper processFloatingBoxInComponentWithMultipleCells(
            ISelectorObject mainCell,
            String nodeName,
            Node firstChild,
            Document pdf1,
            String emptyEscape,
            List<SelectorLayoutCell> containedLeafLayoutCells,
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            ColumnText ct, double pageHeight, PdfWriter writer,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles, int imageIndex) throws
            SelectorDocumentException,
            IOException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, URISyntaxException, IdmlLibException,
            DocumentException, DOMException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException,
            TrialsLimitException
    {
        ReachedCellRectangleAndImageIndexWrapper toReturn = new ReachedCellRectangleAndImageIndexWrapper();
        List<SelectorFloatingBox> containedSelectorFloatingBoxesByType
                = mainCell.
                getContainedSelectorFloatingBoxesByType(nodeName);
        if (containedSelectorFloatingBoxesByType != null
                && !containedSelectorFloatingBoxesByType.isEmpty())
        {
            SelectorFloatingBox floatingBox
                    = containedSelectorFloatingBoxesByType.get(0);
            PdfPTable floatingBoxTable = new PdfPTable(1);
            Double spaceBeforeFromFloatingBox = floatingBox.
                    getSpaceBefore();
            if (spaceBeforeFromFloatingBox != null)
            {
                floatingBoxTable.setSpacingBefore(
                        spaceBeforeFromFloatingBox.floatValue());
            }
            Double spaceAfterFromFloatingBox = floatingBox.
                    getSpaceAfter();
            if (spaceAfterFromFloatingBox != null)
            {
                floatingBoxTable.setSpacingAfter(
                        spaceAfterFromFloatingBox.
                        floatValue());
            }
            SelectorLayoutCell mainLayoutCell
                    = (SelectorLayoutCell) mainCell;
            SelectorLayoutCell mainCellFloatingBox
                    = floatingBox.getMainCellFloatingBox();
            double factor = mainCellFloatingBox.getWidth() / mainLayoutCell.
                    getWidth();
            factor *= 100.0;
            floatingBoxTable.setWidthPercentage((float) factor);
            List<Node> retrieveElementChildren
                    = XmlManagement.retrieveElementChildren(firstChild);
            if (retrieveElementChildren.isEmpty())
            {
                //se non ha elementi figli, vuol dire che voglio usare il nodo stesso (per eviitare l'obbligo di avere un figlio unico)
                retrieveElementChildren = new ArrayList<>();
                retrieveElementChildren.add(firstChild);
            }
            Iterator<Node> iterator = retrieveElementChildren.iterator();
            ReachedCellRectangleAndImageIndexWrapper mainFloatingBoxTableCellWrapped
                    = processInternalLayoutCell(
                            mainCellFloatingBox, floatingBoxTable,
                            retrieveElementChildren, iterator, emptyEscape,writer,imageIndex);
            toReturn.imageIndex = mainFloatingBoxTableCellWrapped.imageIndex;
            toReturn.stopIterating = mainFloatingBoxTableCellWrapped.stopIterating;
            floatingBoxTable.addCell(mainFloatingBoxTableCellWrapped.reachedCell);
            String horizontalAlign = floatingBox.getHorizontalAlign();
            if (horizontalAlign != null)
            {
                int horizontalAlignForTable = PdfSupportUtils.
                        convertFloatingBoxAlignToTableAlign(horizontalAlign);
                floatingBoxTable.setHorizontalAlignment(horizontalAlignForTable);
            }
            reachedLayoutCellAndRectangle
                    = addElementToMultipleLayoutCellsAndProsecutePages(
                            reachedLayoutCellAndRectangle,
                            containedLeafLayoutCells, ct, floatingBoxTable, pdf1,
                            pageHeight, false, writer, cellsRectangles);

        }

        
        toReturn.cellAndRectangleWrapper = reachedLayoutCellAndRectangle;
        return toReturn;
    }

    protected SelectorLayoutCellAndRectangleWrapper addElementToMultipleLayoutCellsAndProsecutePages(
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            List<SelectorLayoutCell> containedLeafLayoutCells,
            ColumnText ct,
            com.itextpdf.text.Element pdfElement,
            Document pdf1, double pageHeight, boolean isSubpage,
            PdfWriter writer,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws URISyntaxException, SAXException, IdmlLibException,
                   ParserConfigurationException, DocumentException,
                   SelectorDocumentException, IOException, TrialsLimitException
    {
        //pdf1.add(floatingBoxTable);
        //boolean firstText = false;
        int cellIndex = 0;
        if (reachedLayoutCellAndRectangle == null)
        {
            reachedLayoutCellAndRectangle = initializeReachedCellColumn(
                    reachedLayoutCellAndRectangle,
                    containedLeafLayoutCells, ct, pageHeight, pdf1,
                    cellsRectangles);
            //System.out.println("initial y "+yLine);
            //ct.setYLine(reachedLayoutCell.getAbsoluteY().floatValue());
            //firstText = true;
        } else
        {
            cellIndex
                    = findReachedCellIndex(containedLeafLayoutCells,
                                           reachedLayoutCellAndRectangle.getLayoutCell(),
                                           cellIndex);
        }
//        if (firstText)
//        {
//            ct.setYLine(reachedLayoutCell.getAbsoluteY().floatValue());
//        }
//        if (pdfElement instanceof Paragraph)
//        {
//            ct.addText((Paragraph)pdfElement);
//        }else
        float height = 0;
        float width = 0;
        Image subpageImage = null;

        if (isSubpage)
        {
            subpageImage = (Image) pdfElement;
            height = subpageImage.getHeight();
            width = subpageImage.getWidth();
            subpageImage.scaleAbsolute(width, height);
        }
        ct.addElement(pdfElement);
        SelectorLayoutCellAndRectangleWrapper retrievedCellAndRect
                = retrieveSelectorLayoutCellAndRectFromLayoutCell(
                        reachedLayoutCellAndRectangle.getLayoutCell(),
                        cellsRectangles);
        if (retrievedCellAndRect == null)
        {
            reachedLayoutCellAndRectangle = makeReachedCellRectangle(
                    reachedLayoutCellAndRectangle,
                    pageHeight, pdf1, true);
        } else
        {
            reachedLayoutCellAndRectangle = retrievedCellAndRect;
        }

        Rectangle rect = reachedLayoutCellAndRectangle.getRectangle();
        float top = rect.getTop(rect.getBorderWidthTop());
        float yLine = ct.getYLine();
        SelectorLayoutCellAndTrialsWrapper layoutCellsAndTrials
                = new SelectorLayoutCellAndTrialsWrapper();
        layoutCellsAndTrials.setCellAndRect(reachedLayoutCellAndRectangle);
        Double filledPart = (Double) (double) (top - yLine);
        layoutCellsAndTrials.setFilledPart(filledPart);
        if (isSubpage)
        {
            layoutCellsAndTrials.setContentHeight((Double) (double) height);
            layoutCellsAndTrials.setContentWidth((Double) (double) width);
            layoutCellsAndTrials.setTrialLimit((Integer) containedLeafLayoutCells.size());
            layoutCellsAndTrials.setCurrentTrial((Integer) 0);
            while (!layoutCellsAndTrials.fitsCell())
            {
                SelectorLayoutCell reachedLayoutCellProsecuting
                        = prosecuteCellAndPage(
                                cellIndex,
                                containedLeafLayoutCells, pdf1, cellsRectangles);
                retrievedCellAndRect
                        = retrieveSelectorLayoutCellAndRectFromLayoutCell(
                                reachedLayoutCellProsecuting,
                                cellsRectangles);
                if (retrievedCellAndRect == null)
                {
                    reachedLayoutCellAndRectangle
                            = new SelectorLayoutCellAndRectangleWrapper();
                    reachedLayoutCellAndRectangle.setLayoutCell(
                            reachedLayoutCellProsecuting);
                    reachedLayoutCellAndRectangle.setRectangle(null);
                    reachedLayoutCellAndRectangle.setAlreadyInsertedRectangle(
                            false);
                    reachedLayoutCellAndRectangle = makeReachedCellRectangle(
                            reachedLayoutCellAndRectangle,
                            pageHeight, pdf1, true);
                } else
                {
                    reachedLayoutCellAndRectangle = retrievedCellAndRect;
                }

                layoutCellsAndTrials.setCellAndRect(reachedLayoutCellAndRectangle);
                layoutCellsAndTrials.setFilledPart((Double) 0.0);//visto che cambio cella, non è riempita precedentemente

                cellIndex++;
                rect = reachedLayoutCellAndRectangle.getRectangle();
                ct.setSimpleColumn(rect);
                if (!layoutCellsAndTrials.fitsCell())//visto che cambio cella, devo verificare se ci sta prima di incrementare i tentativi
                {
                    layoutCellsAndTrials.incTrial();
                } else
                {
                    layoutCellsAndTrials.setTrialLimit(null);
                    layoutCellsAndTrials.setCurrentTrial(null);
                    break;
                }

                if (layoutCellsAndTrials.reachedTrialLimit())
                {
                    //sollevare eccezione qui
                    throw new TrialsLimitException(layoutCellsAndTrials);
                }
            }
        }
        layoutCellsAndTrials.initializeFromReachedCellAndRectangle(
                reachedLayoutCellAndRectangle);
        layoutCellsAndTrials
                = prosecuteToOtherLayoutCellsAndPagesIfCellNotEnough(ct,
                                                                     cellIndex,
                                                                     containedLeafLayoutCells,
                                                                     pdf1,
                                                                     layoutCellsAndTrials,
                                                                     pageHeight,
                                                                     isSubpage, writer,
                                                                     cellsRectangles);
        if (!layoutCellsAndTrials.fitsCell())//visto che cambio layout cell, devo verificare se ci sta già prima di incrementare i tentativi
        {
            layoutCellsAndTrials.incTrial();
        } else
        {
            layoutCellsAndTrials.setTrialLimit(null);
            layoutCellsAndTrials.setCurrentTrial(null);

        }
        if (layoutCellsAndTrials.reachedTrialLimit())
        {
            throw new TrialsLimitException(layoutCellsAndTrials);
        }
        return reachedLayoutCellAndRectangle;
    }

//    protected SelectorLayoutCell goToNextCell (ColumnText ct,
//            int cellIndex,
//            List<SelectorLayoutCell> containedLeafLayoutCells,
//            Document pdf1,
//            SelectorLayoutCell reachedLayoutCell)
//    {
//        cellIndex++;
//        
//    }
    protected SelectorLayoutCellAndTrialsWrapper prosecuteToOtherLayoutCellsAndPagesIfCellNotEnough(
            ColumnText ct,
            int cellIndex,
            List<SelectorLayoutCell> containedLeafLayoutCells,
            Document pdf1,
            SelectorLayoutCellAndTrialsWrapper reachedLayoutCellAndTrial,
            double pageHeight, boolean mustFit, PdfWriter writer,
            List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws URISyntaxException, IdmlLibException, IOException,
                   SAXException, DocumentException, SelectorDocumentException,
                   ParserConfigurationException, TrialsLimitException
    {

        SelectorLayoutCell reachedLayoutCell;
        /*reachedLayoutCell = reachedLayoutCellAndTrial.getCellAndRect().
                getLayoutCell();//*/
        int goStatus = ct.go();
        //qui va messo in simulation mode e poi vanno scaricati i contenuti già esistenti
//        reachedLayoutCellAndTrial.calculateCenteredRectangle(
//                reachedLayoutCell.getVerticalJustification());
        //System.out.println("last y "+ct.getYLine());
        while (ColumnText.hasMoreText(goStatus))
        {
            reachedLayoutCell
                    = prosecuteCellAndPage(cellIndex, containedLeafLayoutCells,
                                           pdf1, cellsRectangles);
            SelectorLayoutCellAndRectangleWrapper foundCellAndRect
                    = retrieveSelectorLayoutCellAndRectFromLayoutCell(
                            reachedLayoutCell, cellsRectangles);
            if (foundCellAndRect == null)
            {
                reachedLayoutCellAndTrial.setCellAndRect(makeReachedCellRectangle(reachedLayoutCellAndTrial.
                        getCellAndRect(),
                                                                                  pageHeight, pdf1, true));
            } else
            {
                reachedLayoutCellAndTrial.setCellAndRect(foundCellAndRect);
            }

            reachedLayoutCellAndTrial.setFilledPart((Double) 0.0);//visto che cambio cella, non è parzialmente riempita
            if (mustFit)
            {
                //per le subpage devo verificare se la cella layout ha le dimensioni adeguate
                while (!reachedLayoutCellAndTrial.fitsCell())
                {
                    reachedLayoutCell = prosecuteCellAndPage(cellIndex,
                                                             containedLeafLayoutCells,
                                                             pdf1,
                                                             cellsRectangles);
                    foundCellAndRect
                            = retrieveSelectorLayoutCellAndRectFromLayoutCell(
                                    reachedLayoutCell, cellsRectangles);
                    if (foundCellAndRect == null)
                    {
                        reachedLayoutCellAndTrial.setCellAndRect(new SelectorLayoutCellAndRectangleWrapper());
                        reachedLayoutCellAndTrial.getCellAndRect().setLayoutCell(
                                reachedLayoutCell);
                        reachedLayoutCellAndTrial.getCellAndRect().setRectangle(null);
                        reachedLayoutCellAndTrial.getCellAndRect().
                                setAlreadyInsertedRectangle(false);
                        reachedLayoutCellAndTrial.setCellAndRect(makeReachedCellRectangle(reachedLayoutCellAndTrial.
                                getCellAndRect(),
                                                                                          pageHeight, pdf1, true));
                    } else
                    {
                        reachedLayoutCellAndTrial.setCellAndRect(foundCellAndRect);
                    }

                    cellIndex++;

                    reachedLayoutCellAndTrial.setFilledPart((Double) 0.0);
                    if (!reachedLayoutCellAndTrial.fitsCell())//visto che cambio layout cell devo verificare se ci stya prima di incrementare i tentativi
                    {
                        reachedLayoutCellAndTrial.incTrial();
                    } else
                    {
                        reachedLayoutCellAndTrial.setTrialLimit(null);
                        reachedLayoutCellAndTrial.setCurrentTrial(null);
                        break;
                    }
                    if (reachedLayoutCellAndTrial.reachedTrialLimit())
                    {
                        throw new TrialsLimitException(reachedLayoutCellAndTrial);
                    }
                }

            }
            ct.setSimpleColumn(reachedLayoutCellAndTrial.getCellAndRect().
                    getRectangle());
            //ct.setYLine(reachedLayoutCell.getAbsoluteY().floatValue());
            goStatus = ct.go();

        }
        return reachedLayoutCellAndTrial;
    }

    protected SelectorLayoutCell prosecuteCellAndPage(int cellIndex,
                                                      List<SelectorLayoutCell> containedLeafLayoutCells,
                                                      Document pdf1,
                                                      List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
    {
        SelectorLayoutCell reachedLayoutCell;
        //devo passare alla cella successiva
        cellIndex++;
        cellIndex = cellIndex % containedLeafLayoutCells.size();
        if (cellIndex == 0)
        {
            resetInsertedCellsRectanglesStatus(cellsRectangles);
            //se ho raggiunto di nuovo lo 0 (e visto che sto verificando has more text vuol dire che ho un overflow e quindi ho superato la cella 0 della pagina)
            pdf1.newPage();
        }
        reachedLayoutCell = containedLeafLayoutCells.get(cellIndex);
        return reachedLayoutCell;
    }

    protected SelectorLayoutCellAndRectangleWrapper makeReachedCellRectangle(
            SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
            double pageHeight, Document pdf,
            boolean insertRectangleIfNotAlreadyInserted) throws
            URISyntaxException, SelectorDocumentException, SAXException,
            ParserConfigurationException, IOException, IdmlLibException,
            DocumentException
    {

        SelectorLayoutCell reachedLayoutCell
                = reachedLayoutCellAndRectangle.getLayoutCell();

        //se la cella raggiunta è a null devo inizializzare le colonne
        //se la cella raggiunta è a null devo inizializzare le colonne
        Rectangle rect = reachedLayoutCellAndRectangle.getRectangle();
        if (rect == null)
        {
            Double llx = reachedLayoutCell.getAbsoluteX();
            double graphicLly
                    = reachedLayoutCell.getAbsoluteY() + reachedLayoutCell.
                    getHeight();
            double pdfLly = CoordinatesTransformer.fromUpperLeftYToLowerLeftY(
                    graphicLly, pageHeight);
            double urx
                    = reachedLayoutCell.getAbsoluteX() + reachedLayoutCell.
                    getWidth();
            Double graphicUry = reachedLayoutCell.getAbsoluteY();
            double pdfUry = CoordinatesTransformer.fromUpperLeftYToLowerLeftY(
                    graphicUry, pageHeight);
            rect = new Rectangle(llx.
                    floatValue(), (float) pdfLly, (float) urx, (float) pdfUry);

            rect.setBorder(Rectangle.BOX);
            TintAndColorWrapper strokeTintAndColor
                    = reachedLayoutCell.getStrokeTintAndColor();
            if (strokeTintAndColor != null)
            {
                BaseColor baseColorFromStrokeTintAndColor
                        = PdfSupportUtils.
                        calculateBaseColorFromTintAndColorWrapper(
                                strokeTintAndColor);
                rect.setBorderColor(baseColorFromStrokeTintAndColor);
            }
            Double strokeWeight = reachedLayoutCell.getStrokeWeight();
            if (strokeWeight != null)
            {
                rect.setBorderWidth(strokeWeight.floatValue());
            }
            TintAndColorWrapper fillTintAndColorWrapper
                    = reachedLayoutCell.getFillTintAndColorWrapper();
            if (fillTintAndColorWrapper != null)
            {
                BaseColor baseColorFromFillTintAndColorWrapper
                        = PdfSupportUtils.
                        calculateBaseColorFromTintAndColorWrapper(
                                fillTintAndColorWrapper);
                rect.setBackgroundColor(baseColorFromFillTintAndColorWrapper);
            }

            reachedLayoutCellAndRectangle.setRectangle(rect);
        }
        if (reachedLayoutCellAndRectangle.isAlreadyInsertedRectangle() == false)
        {
            if (insertRectangleIfNotAlreadyInserted)
            {
                reachedLayoutCellAndRectangle.setRectangle(rect);
                pdf.add(rect);
                reachedLayoutCellAndRectangle.setAlreadyInsertedRectangle(true);
            }

        }
        return reachedLayoutCellAndRectangle;
    }

    protected int findReachedCellIndex(
            List<SelectorLayoutCell> containedLeafLayoutCells,
            SelectorLayoutCell reachedLayoutCell,
            int cellIndex)
    {
        //cerco l'indice della cella che ho raggiunto
        for (int i = 0; i < containedLeafLayoutCells.size(); i++)
        {
            SelectorLayoutCell examiningCell = containedLeafLayoutCells.
                    get(
                            i);
            if (examiningCell == reachedLayoutCell)
            {
                cellIndex = i;
                break;
            }
        }
        return cellIndex;
    }

    private SelectorLayoutCellAndRectangleWrapper processSubpage(Node subpageTag,
                                                                 List<SelectorLayoutCell> leafCells,
                                                                 Document pdf,
                                                                 String emptyEscape,
                                                                 SelectorLayoutCellAndRectangleWrapper reachedLayoutCellAndRectangle,
                                                                 ColumnText ct,
                                                                 SelectorDocument selectorDocument,
                                                                 int parentLevel,
                                                                 PdfWriter writer,
                                                                 double pageHeight,
                                                                 List<SelectorLayoutCellAndRectangleWrapper> cellsRectangles)
            throws
            SelectorFileNotExistentException, SelectorDocumentException,
            ParserConfigurationException, SAXException, IOException,
            BadElementException, URISyntaxException, IdmlLibException,
            DocumentException, NoSuchFieldException, IllegalArgumentException,
            IllegalAccessException, Exception
    {
        NamedNodeMap attributes = subpageTag.getAttributes();
        if (attributes != null)
        {
            Node namedItem = attributes.getNamedItem(TYPE);
            if (namedItem != null)
            {
                String fascicleName = namedItem.getTextContent();
                if (fascicleName != null)
                {
                    SelectorFascicle subpageFascicle = selectorDocument.
                            searchFascicleByName(fascicleName);
                    if (subpageFascicle != null)
                    {
                        List<SelectorPage> pages = subpageFascicle.getPages();
                        SelectorPage lastFasciclePage = pages.get(pages.size()
                                - 1);
                        PdfImportedPage createdSubpage = null;
                        int currentLevel = parentLevel + 1;
                        for (SelectorPage selectorPage : pages)
                        {
                            createdSubpage = createSubpage(selectorPage,
                                                           lastFasciclePage,
                                                           currentLevel,
                                                           subpageFascicle,
                                                           subpageTag,
                                                           emptyEscape,
                                                           selectorDocument,
                                                           writer, pageHeight);
                            if (createdSubpage != null)
                            {
                                break;
                            }
                        }
                        Image pdfPageToImagepdfPageToImage = Image.getInstance(
                                createdSubpage);
                        float subpageHeight = createdSubpage.getHeight();
                        float subpageWidth = createdSubpage.getWidth();
                        /*System.out.println("height "+subpageHeight);
                         System.out.println("width "+subpageWidth);*/
                        pdfPageToImagepdfPageToImage.scaleAbsoluteHeight(
                                subpageHeight);
                        pdfPageToImagepdfPageToImage.scaleAbsoluteWidth(
                                subpageWidth);//*/
                        SelectorLayoutCellAndRectangleWrapper addElementToMultipleLayoutCellsAndProsecutePages
                                = addElementToMultipleLayoutCellsAndProsecutePages(
                                        reachedLayoutCellAndRectangle, leafCells,
                                        ct,
                                        pdfPageToImagepdfPageToImage, pdf,
                                        pageHeight, true, writer,
                                        cellsRectangles);
                        return addElementToMultipleLayoutCellsAndProsecutePages;

                    }

                }

            }

        }

        return reachedLayoutCellAndRectangle;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private PdfImportedPage createSubpage(SelectorPage selectorPage,
                                          SelectorPage lastFasciclePage,
                                          int level,
                                          SelectorFascicle subpageFascicle,
                                          Node subpageTag, String emptyEscape,
                                          SelectorDocument selectorDocument,
                                          PdfWriter writer, double pageHeight)
            throws IOException,
                   ParserConfigurationException, SelectorDocumentException,
                   DocumentException, IdmlLibException, SAXException,
                   URISyntaxException, DOMException, NoSuchFieldException,
                   IllegalArgumentException, IllegalAccessException, Exception
    {
        PdfWriter subpagePdfWriter = null;
        File subpagesPdfFile = null;
        Document subpagePdf = null;
        HeaderFooterMaker subpageHeaderFooterMaker = null;
        ImageBackgrounder subpageImageBackgrounder = null;
        PdfDocumentAndWriterWrapper docAndWriter;
        PageLayoutCellsMaker subpageLayoutCellsMaker = null;
        PageNumberCreator subpagePageNumberCreator = null;
        try
        {

            subpagesPdfFile = File.createTempFile(level
                    + SUBPAGE_TEMP_FILE_HEADER_LAST_PART,
                                                  SUBPAGE_TEMP_FILE_NAME_TRAILER);
            docAndWriter = processMultipageAssignedFascicle(subpageFascicle,
                                                            subpageTag,
                                                            subpagePdf,
                                                            subpagesPdfFile,
                                                            emptyEscape,
                                                            selectorDocument,
                                                            selectorPage, level,
                                                            subpagePdfWriter,
                                                            subpageHeaderFooterMaker,
                                                            subpageImageBackgrounder, subpageLayoutCellsMaker, subpagePageNumberCreator, null,
                                                            null, null);
            if (docAndWriter != null)//previsto per usi futuri, se ci sarà ottimizzazione
            {
                subpagePdf = docAndWriter.getPdf();
                subpagePdfWriter = docAndWriter.getWriter();
                subpageHeaderFooterMaker = docAndWriter.getHeaderFooterMaker();
                subpageImageBackgrounder = docAndWriter.getImageBackgrounder();
                subpageLayoutCellsMaker = docAndWriter.getPageLayoutCellsMaker();
                subpagePageNumberCreator = docAndWriter.getPageNumberCreator();

            }

            if (subpagePdf != null)
            {
                //vuol dire che la subpage è stata creata
                subpagePdf.close();
                PdfReader reader = new PdfReader(new FileInputStream(
                        subpagesPdfFile));
                if (selectorPage != lastFasciclePage)
                {
                    //per l'ultima pagina disponibile faccio crop, mentre per le altre se creo più di una pagina devo buttare il documento
                    int numberOfPages = reader.getNumberOfPages();
                    if (numberOfPages > 1)
                    {
                        //butto via il documento creato e restituisco null
                        deleteTempPdf(subpagesPdfFile);
                        return null;
                    }
                }
                //recupero pagina 1, verificato anche comprimendo il file della subpage ma non ha portato vantaggi rispetto a comprimere tutto alla fine una volta composto
                
                PdfImportedPage importedPage = writer.getImportedPage(reader, 1);
                //una volta importata la pagina non mi serve più il file temporaneo
                deleteTempPdf(subpagesPdfFile);
                return importedPage;
            } else
            {
                //se non ho prodotto pdf, cancello il file temporaneo
                deleteTempPdf(subpagesPdfFile);
            }
        } catch (IOException | ParserConfigurationException |
                SelectorDocumentException | DocumentException |
                IdmlLibException | SAXException | URISyntaxException |
                DOMException | NoSuchFieldException |
                IllegalArgumentException | IllegalAccessException ex)
        {
            if (subpagePdf != null)
            {
                try
                {
                    subpagePdf.close();
                } catch (Exception closingEx)
                {
                    //non mi interessa l'eccezione in chiusura pdf, mi interessa la causa
                    deleteTempPdf(subpagesPdfFile);
                    throw ex;

                }

            }
            deleteTempPdf(subpagesPdfFile);
            throw ex;
        } catch (PdfWrapperException ex)
        {
            Document pdf = ex.getPdf();
            SelectorDocument selDoc = ex.getSelectorDoc();
            if (selDoc != null)
            {
                selDoc.cleanupCachedData();
            }
            Idml idmlDoc = ex.getIdmlDoc();
            if (idmlDoc != null)
            {
                idmlDoc.close();
            }
            if (pdf != null)
            {
                try
                {
                    pdf.close();
                } catch (Exception closingEx)
                {
                    //non mi interessa l'eccezione in chiusura pdf, mi interessa la causa
                    deleteTempPdf(subpagesPdfFile);
                    throw ex.getException();
                }

            }
            deleteTempPdf(subpagesPdfFile);
            throw ex.getException();
        }

        return null;
    }
    public static final String SUBPAGE_TEMP_FILE_NAME_TRAILER = "subpage.pdf";
    public static final String SUBPAGE_TEMP_FILE_HEADER_LAST_PART = "subpage";

    protected void deleteTempPdf(File subpagesPdfFile)
    {
        if (subpagesPdfFile != null)
        {
            if (subpagesPdfFile.exists())
            {
                subpagesPdfFile.delete();
            }
        }
    }
}
