/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.progressbarcalculator;

import java.util.*;

/**
 *
 * @author Marco Scarpa
 */
public class ProgressBarCalculator
{

    private Dictionary<String, Integer> positionsConverter = null;
    private Vector<Double> dones = null;
    private Vector<Double> totals = null;

    // = 0 = null
    public ProgressBarCalculator(int position, String label)
    {
        if (label != null)
        {
            bindLabelWithPosition(label, position);
        }
    }

    public ProgressBarCalculator(String label)
    {
        this(0, label);
    }

    public ProgressBarCalculator(int position)
    {
        this(position, null);
    }

    public ProgressBarCalculator()
    {
        this(0, null);
    }

    //= 0
    public void bindLabelWithPosition(String label, int position)
    {
        if (positionsConverter == null)
        {
            positionsConverter = new Hashtable<>();
        }
        positionsConverter.put(label, position);
    }

    public void bindLabelWithPosition(String label)
    {
        bindLabelWithPosition(label, 0);
    }

    public static Double calculateProgress(Double currentDone,
                                           Double currentTotal,
                                           Vector<Double> previousDones,
                                           Vector<Double> previousTotals,
                                           boolean innerToExternal)
    {
        if (previousDones == null || previousDones.isEmpty())
        {
            return currentDone;
        }
        if (innerToExternal == false)
        {
            previousDones = (Vector<Double>) previousDones.clone();
            Collections.reverse(previousDones);
            previousTotals = (Vector<Double>) previousTotals.clone();
            Collections.reverse(previousTotals);
        }
        if (previousDones.size() <= previousTotals.size())
        {
            previousDones = ArrayUtils.fillDoubleArrayFromHead(previousDones,
                    currentDone, previousTotals.size());//se è già della lunghezza giusta non verrà messo filler
        } else
        {
            previousTotals = ArrayUtils.fillDoubleArrayFromHead(previousTotals,
                    currentTotal, previousTotals.size());
        }
        Vector<Double> remainingDones = null;
        Vector<Double> remainingTotals = null;
        if (previousDones.size() > 1)
        {
            remainingDones = new Vector<Double>(previousDones.subList(1,
                    previousDones.size()));
            remainingTotals = new Vector<Double>(previousTotals.subList(1,
                    previousTotals.size()));
        }
        //assumo che tutti i precedenti abbiano lo stesso numero di componrenti dell'attuale
        return currentDone + calculateProgress(previousDones.get(0),
                previousTotals.get(0), remainingDones, remainingTotals) *
                currentTotal;

    }

    public static Double calculateProgress(Double currentDone,
                                           Double currentTotal,
                                           Vector<Double> previousDones,
                                           Vector<Double> previousTotals)
    {
        return calculateProgress(currentDone, currentTotal, previousDones,
                previousTotals, true);
    }

    public static Double calculateTotal(Double currentDone, Double currentTotal,
                                        Vector<Double> previousDones,
                                        Vector<Double> previousTotals,
                                        boolean innerToExternal)
    {
        if (previousTotals == null || previousTotals.isEmpty())
        {
            return currentTotal;
        }
        if (innerToExternal == false)
        {
            previousDones = (Vector<Double>) previousDones.clone();
            Collections.reverse(previousDones);
            previousTotals = (Vector<Double>) previousTotals.clone();
            Collections.reverse(previousTotals);
        }
        if (previousDones.size() <= previousTotals.size())
        {
            previousDones = ArrayUtils.fillDoubleArrayFromHead(previousDones,
                    currentDone, previousTotals.size());//se è già della lunghezza giusta non verrà messo filler
        } else
        {
            previousTotals = ArrayUtils.fillDoubleArrayFromHead(previousTotals,
                    currentTotal, previousTotals.size());
        }
        Vector<Double> remainingDones = null;
        Vector<Double> remainingTotals = null;
        if (previousTotals.size() > 1)
        {
            remainingDones = new Vector<>(previousDones.subList(1,
                    previousDones.size()));
            remainingTotals = new Vector<>(previousTotals.subList(1,
                    previousTotals.size()));
        }

        return currentTotal * calculateTotal(previousDones.get(
                0), previousTotals.get(0), remainingDones, remainingTotals);
    }

    public static Double calculateTotal(Double currentDone, Double currentTotal,
                                        Vector<Double> previousDones,
                                        Vector<Double> previousTotals)
    {
        return calculateTotal(currentDone, currentTotal, previousDones,
                previousTotals, true);
    }

    public Double calculateProgressByLabel(Double currentDone,
                                           Double currentTotal, String label)
    {
        return calculateProgressByPosition(currentDone, currentTotal,
                retrivePositionFromLabel(label));
    }

    public Double calculateProgressByPosition(Double done, Double total,
                                              int position)
    {
        //creo i vettori e li allungo
        if (dones == null || dones.size() == 0)
        {
            dones = new Vector<Double>();
        }
        while (dones.size() <= position)
        {
            dones.add(done);
        }
        if (totals == null || totals.size() == 0)
        {
            totals = new Vector<Double>();
        }
        while (totals.size() <= position)
        {
            totals.add(total);
        }
        Vector<Double> donesSlice = new Vector<>(dones.subList(0, position));
        Vector<Double> totalsSlice = new Vector<>(totals.subList(0, position));
        dones.set(position, done);
        totals.set(position, total);
        return calculateProgress(done, total, donesSlice, totalsSlice, false);
    }

    public Double calculateTotalByLabel(Double currentDone, Double currentTotal,
                                        String label)
    {
        return calculateTotalByPosition(currentDone, currentTotal,
                retrivePositionFromLabel(label));
    }

    public Double retriveLocalDoneByLabel(String label
    )
    {
        return retriveLocalDoneByPosition(retrivePositionFromLabel(label));
    }

    public Double retriveLocalTotalByLabel(String label)
    {
        return retriveLocalTotalByPosition(retrivePositionFromLabel(label));
    }

    public Double retriveLocalDoneByPosition(int position)
    {
        return dones.get(position);
    }

    public Double retriveLocalTotalByPosition(int position)
    {
        return totals.get(position);
    }

    public Double retriveProgressByLabel(String label
    )
    {
        return retriveProgressByPosition(retrivePositionFromLabel(label));
    }

    public Double retriveTotalByLabel(String label
    )
    {
        return retriveTotalByPosition(retrivePositionFromLabel(label));
    }

    public Double retriveProgressByPosition(int position)
    {
        return calculateProgressByPosition(retriveLocalDoneByPosition(position),
                retriveLocalTotalByPosition(position), position);
    }

    public Double retriveTotalByPosition(int position)
    {
        return calculateTotalByPosition(retriveLocalDoneByPosition(position),
                retriveLocalTotalByPosition(position), position);
    }

    public int retrivePositionFromLabel(String label)
    {
        if (positionsConverter.get(label) != null)
        {
            return positionsConverter.get(label);
        }

        throw new IllegalArgumentException("Etichetta " + label +
                " non presente nelle etichette delle progress bar");
    }

    public Double calculateTotalByPosition(Double done, Double total,
                                           int position)
    {
        //creo i vettori e li allungo
        if (dones == null)
        {
            dones = new Vector<Double>();

        }
        while (dones.size() <= position)
        {
            dones.add(done);
        }

        if (totals == null || totals.isEmpty())
        {
            totals = new Vector<Double>();
        }
        while (totals.size() <= position)
        {
            totals.add(total);
        }
        Vector<Double> donesSlice = new Vector<>(dones.subList(0, position));
        Vector<Double> totalsSlice = new Vector<>(totals.subList(0, position));
        dones.set(position, done);
        totals.set(position, total);
        return calculateTotal(done, total, donesSlice, totalsSlice, false);
    }

    public void addLabelAfterPosition(String currentLabel, int previousPosition)
    {
        addLabelAfterLabel(currentLabel, retriveLabelFromPosition(
                previousPosition));
    }

    public void addLabelAfterLabel(String currentLabel, String previousLabel)
    {
        //sposto in avanti di uno tutte le label successive per fare posto alla nuova
        Vector<String> followings = retriveFollowingLabels(previousLabel);
        shiftLabels(followings, +1);
        //inserisco la posizione corrente subito dopo la label creata, che quindi rimane in  iata
        bindLabelWithPosition(currentLabel, retrivePositionFromLabel(
                previousLabel) + 1);
    }

    public void addLabelAfterLabelIfNotAlreadyIn(String currentLabel,
                                                 String previousLabel)
    {
        if (hasLabel(currentLabel) == false)
        {
            addLabelAfterLabel(currentLabel, previousLabel);
        }
    }

    public String retriveLabelFromPosition(int position)
    {
        Enumeration<String> keys = positionsConverter.keys();
        while (keys.hasMoreElements())
        {
            String candidateLabel = keys.nextElement();
            if (position == positionsConverter.get(candidateLabel))
            {
                return candidateLabel;
            }
        }

        throw new IllegalArgumentException("Posizione " + position +
                " non presente nelle etichette delle progress bar");
    }

    public boolean hasLabel(String label)
    {
        if (positionsConverter != null)
        {

            Integer get = positionsConverter.get(label);
            return get != null;

        }

        return false;
    }

    public boolean hasPosition(int position)
    {
        Enumeration<Integer> elements = positionsConverter.elements();
        while (elements.hasMoreElements())
        {
            Integer nextElement = elements.nextElement();
            if (nextElement != null)
            {
                if (nextElement == position)
                {
                    return true;
                }
            }

        }
        return false;
    }

    public Vector<String> retriveFollowingLabels(String label)
    {
        return retriveFollowingLabelsByPosition(retrivePositionFromLabel(label));
    }

    public Vector<String> retriveFollowingLabelsByPosition(int pos)
    {
        Vector<String> toReturn = new Vector<String>();
        Enumeration<String> keys = positionsConverter.keys();
        while (keys.hasMoreElements())
        {
            String dictionaryLabel = keys.nextElement();
            Integer correspondingPos =
                    positionsConverter.get(dictionaryLabel);
            if (correspondingPos != null && correspondingPos > pos)
            {
                toReturn.add(dictionaryLabel);
            }

        }
        return toReturn;
    }

    public void removeLabel(String label)
    {
        if (hasLabel(label))
        {
            Vector<String> followings = retriveFollowingLabels(label);
            shiftLabels(followings, -1);
            positionsConverter.remove(label);
        }

    }
    
    public void removePosition(int position)
    {
        if (hasPosition(position))
        {
            removeLabel(retriveLabelFromPosition(position));
        }

    }
    
    public void shiftLabels(Vector<String> labels, int shift)
    {
        for (String label : labels)
        {
            bindLabelWithPosition(label, retrivePositionFromLabel(label) + shift);
        }
    }
    
    public void removeAllLabels()
    {
        Vector<String> labels = getAllLabels();
        for (String label : labels)
        {
            removeLabel(label);
        }

    }
    
    public Vector<String> getAllLabels()
    {
        Vector<String> toReturn = new Vector<String>();
        Enumeration<String> keys = positionsConverter.keys();
        while (keys.hasMoreElements())
        {
            String nextElement = keys.nextElement();
            toReturn.add(nextElement);

        }
        return toReturn;
    }
    
    public Vector<Integer> getAllPositions()
    {
        Vector<String> labels = getAllLabels();
        Vector<Integer> toReturn = new Vector<>();
        for (String label : labels)
        {
            toReturn.add(retrivePositionFromLabel(label));
        }
        return toReturn;
    }

    /**
     * serve per calcolare il progress per oggetto che ad esempio vogliono valori da 0 a 100
     * @param currentDone
     * @param currentTotal
     * @param label
     * @param normalizing
     * @return 
     */
    public Double calculateProgressByLabelNormalized(Double currentDone,
                                           Double currentTotal, String label,Double normalizing)
    {
        Double calculateProgressByLabel =
                calculateProgressByLabel(currentDone, currentTotal, label);
        Double calculateTotalByLabel =
                calculateTotalByLabel(currentDone, currentTotal, label);
        Double ratio =  calculateProgressByLabel/calculateTotalByLabel;
        Double normalized = ratio*normalizing;
        return normalized;
    }
    
    public Double calculateProgressByPositionNormalized(Double done, Double total,
                                              int position,Double normalizing)
    {
        return calculateProgressByLabelNormalized(done, total, retriveLabelFromPosition(position), normalizing);
    }
    
}
