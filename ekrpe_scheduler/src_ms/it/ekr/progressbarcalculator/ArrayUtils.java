/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.progressbarcalculator;

import java.util.*;

/**
 *
 * @author Marco Scarpa
 */
class ArrayUtils
{

    public static Vector<Double> fillDoubleArrayFromHead(
            Vector<Double> incomplete, Double filler, int desiredLength)
    {
        Vector<Double> toReturn = (Vector<Double>) incomplete.clone();
        Collections.reverse(toReturn);
        toReturn = fillDoubleArrayAppending(toReturn, filler, desiredLength);
        Collections.reverse(toReturn);
        return toReturn;
    }

    public static Vector<Double> fillDoubleArrayAppending(
            Vector<Double> incomplete, Double filler, int desiredLength)
    {
        Vector<Double> toReturn = (Vector<Double>) incomplete.clone();
        while (toReturn.size() < desiredLength)
        {
            toReturn.add(filler);
        }

        return toReturn;
    }
}
